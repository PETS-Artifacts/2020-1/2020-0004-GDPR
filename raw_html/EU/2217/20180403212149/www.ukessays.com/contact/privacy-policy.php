<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta content="True" name="HandheldFriendly">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="canonical" href="https://www.ukessays.com/contact/privacy-policy.php">
<title>Privacy Policy | UK Essays</title>
<meta name="description" content="UK Essays Privacy Policy - Our privacy policy explains how we will collect and use the information you give us.">
<meta name="msvalidate.01" content="128C0C89DAE09DC01AB01781988DA032">
<meta property="og:locale" content="en_GB">
<meta property="og:url" content="http://www.ukessays.com/contact/privacy-policy.php">
<meta property="og:site_name" content="UKEssays">
<meta property="article:publisher" content="https://www.facebook.com/UKEssays">
<meta property="og:type" content="article">
<meta itemprop="og:headline" content="Privacy Policy | UK Essays">
<meta itemprop="og:description" content="UK Essays Privacy Policy - Our privacy policy explains how we will collect and use the information you give us.">
<meta property="og:image" content="https://www.ukessays.com/images/assets/ukessays-share-logo.jpg">
<link rel="preload" href="/css/main.css?exp=1522768824" as="style">
<link rel="preload" href="/images/uk-essays.png" as="image">
<link rel="shortcut icon" href="/favicon.ico">
<link href="/css/main.css?exp=1522768824" rel="stylesheet" type="text/css">
<meta name="theme-color" content="#222">
<script>
        window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
        d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
        _.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
        $.src="https://v2.zopim.com/?4VMOYiSgxVvvYHqDuojZl9cAehGUmDcB";z.t=+new Date;$.
        type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");

        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','gau');
        gau('create', 'UA-226227-1', 'auto');

        
        gau('send', 'pageview');

        (function(h,o,t,j,a,r){
            h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
            h._hjSettings={hjid:208129,hjsv:6};
            a=o.getElementsByTagName('head')[0];
            r=o.createElement('script');r.async=1;
            r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
            a.appendChild(r);
        })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
    </script>
</head>
<body>
<div class="bg-dark d-none d-lg-block pb-2">
<ul class="container g-account-bar d-flex list-unstyled align-items-center mb-0">
<li class="mr-3"><a class="text-white" href="/contact/fair-use-policy.php">
<svg xmlns="http://www.w3.org/2000/svg" viewbox="0 0 384 512" fill="currentColor" width="18" height="18"><path d="M369.9 97.9L286 14C277 5 264.8-.1 252.1-.1H48C21.5 0 0 21.5 0 48v416c0 26.5 21.5 48 48 48h288c26.5 0 48-21.5 48-48V131.9c0-12.7-5.1-25-14.1-34zM332.1 128H256V51.9l76.1 76.1zM48 464V48h160v104c0 13.3 10.7 24 24 24h104v288H48z" /></svg>
Fair Use Policy</a></li>
<li><a class="text-white" href="https://support.ukessays.com/hc/en-us">
<svg xmlns="http://www.w3.org/2000/svg" viewbox="0 0 512 512" fill="currentColor" width="18" height="18"><path d="M464 32H48C21.5 32 0 53.5 0 80v352c0 26.5 21.5 48 48 48h416c26.5 0 48-21.5 48-48V80c0-26.5-21.5-48-48-48zm-208 80c0-17.7 14.3-32 32-32s32 14.3 32 32-14.3 32-32 32-32-14.3-32-32zm-96 0c0-17.7 14.3-32 32-32s32 14.3 32 32-14.3 32-32 32-32-14.3-32-32zm-96 0c0-17.7 14.3-32 32-32s32 14.3 32 32-14.3 32-32 32-32-14.3-32-32zm400 314c0 3.3-2.7 6-6 6H54c-3.3 0-6-2.7-6-6V192h416v234z" /></svg>
Help Centre</a></li>
<li class="ml-auto mr-2 notification position-relative">
<button class="btn btn-link py-1 text-white" type="button">
<svg xmlns="http://www.w3.org/2000/svg" viewbox="0 0 448 512" fill="currentColor" width="18" height="18"><path d="M425.403 330.939c-16.989-16.785-34.546-34.143-34.546-116.083 0-83.026-60.958-152.074-140.467-164.762A31.843 31.843 0 0 0 256 32c0-17.673-14.327-32-32-32s-32 14.327-32 32a31.848 31.848 0 0 0 5.609 18.095C118.101 62.783 57.143 131.831 57.143 214.857c0 81.933-17.551 99.292-34.543 116.078C-25.496 378.441 9.726 448 66.919 448H160c0 35.346 28.654 64 64 64 35.346 0 64-28.654 64-64h93.08c57.19 0 92.415-69.583 44.323-117.061zM224 472c-13.234 0-24-10.766-24-24h48c0 13.234-10.766 24-24 24zm157.092-72H66.9c-16.762 0-25.135-20.39-13.334-32.191 28.585-28.585 51.577-55.724 51.577-152.952C105.143 149.319 158.462 96 224 96s118.857 53.319 118.857 118.857c0 97.65 23.221 124.574 51.568 152.952C406.278 379.661 397.783 400 381.092 400z" /></svg>
Notifications</button>
<div class="feed d-none position-absolute bg-light text-left border">Loading...</div>
</li>
<li><a class="text-white" href="/myaccount/">
<svg xmlns="http://www.w3.org/2000/svg" viewbox="0 0 448 512" fill="currentColor" width="18" height="18"><path d="M355.165 226.983l-11.257-3.216C353.3 209.665 368 182.136 368 144 368 64.519 303.584 0 224 0 144.519 0 80 64.416 80 144c0 38.131 14.696 65.659 24.093 79.767l-11.257 3.216C56.355 237.407 0 276.069 0 350.058v88.799C0 479.188 32.812 512 73.143 512h301.714C415.188 512 448 479.188 448 438.857v-88.799c0-73.384-55.581-112.43-92.835-123.075zM224 48c53.019 0 96 42.981 96 96s-42.981 96-96 96-96-42.981-96-96 42.981-96 96-96zm176 390.857C400 452.743 388.743 464 374.857 464H73.143C59.257 464 48 452.743 48 438.857v-88.799c0-35.719 23.678-67.109 58.022-76.922l36.46-10.417C165.929 278.843 194.106 288 224 288c29.852 0 58.036-9.133 81.518-25.281l36.46 10.417C376.322 282.949 400 314.34 400 350.058v88.799z" /></svg>
Sign In</a></li>
</ul>
</div>
<header class="g-header bg-dark sticky-top">
<div class="container py-2">
<div class="g-brand d-flex align-items-center justify-content-between text-white">
<a href="/" title="Go to the UKEssays homepage" class="d-lg-flex text-white text-nowrap text-center text-lg-left mr-auto mr-lg-0 logo">
<img src="/images/uk-essays.png" width="80" height="52" alt="UKEssays logo" class="mr-2 align-self-center">
<div>
<span class="h4 text-uppercase font-brand">UK<span class="small">Essays</span></span>
<span class="small text-uppercase d-none d-lg-block">Trusted by students since 2003</span>
</div>
</a>
<form method="get" action="/search.php" class="g-search w-100 mx-5 d-none d-lg-block">
<div class="input-group">
<input type="search" name="q" class="form-control" placeholder="Search" aria-label="Search" required>
<div class="input-group-append">
<button class="btn btn-primary" type="submit" aria-label="Search">
<svg xmlns="http://www.w3.org/2000/svg" viewbox="0 0 512 512" fill="currentColor" width="18" height="18"><path d="M508.5 468.9L387.1 347.5c-2.3-2.3-5.3-3.5-8.5-3.5h-13.2c31.5-36.5 50.6-84 50.6-136C416 93.1 322.9 0 208 0S0 93.1 0 208s93.1 208 208 208c52 0 99.5-19.1 136-50.6v13.2c0 3.2 1.3 6.2 3.5 8.5l121.4 121.4c4.7 4.7 12.3 4.7 17 0l22.6-22.6c4.7-4.7 4.7-12.3 0-17zM208 368c-88.4 0-160-71.6-160-160S119.6 48 208 48s160 71.6 160 160-71.6 160-160 160z" /></svg>
</button>
</div>
</div>
</form>
<div class="g-contact text-right text-nowrap d-none d-lg-block">
<span class="h4">
<svg xmlns="http://www.w3.org/2000/svg" viewbox="0 0 512 512" fill="currentColor" width="22" height="22" class="align-middle"><path d="M493.397 24.615l-104-23.997c-11.314-2.611-22.879 3.252-27.456 13.931l-48 111.997a24 24 0 0 0 6.862 28.029l60.617 49.596c-35.973 76.675-98.938 140.508-177.249 177.248l-49.596-60.616a24 24 0 0 0-28.029-6.862l-111.997 48C3.873 366.516-1.994 378.08.618 389.397l23.997 104C27.109 504.204 36.748 512 48 512c256.087 0 464-207.532 464-464 0-11.176-7.714-20.873-18.603-23.385z" /></svg>
0115 966 7955</span>
<span class="d-block small">Today's Opening Times 10:00 - 20:00 (BST)</span>
</div>
<a class="btn btn-outline-light d-block d-lg-none mr-2" href="/search.php" aria-label="Search">
<svg xmlns="http://www.w3.org/2000/svg" viewbox="0 0 512 512" fill="currentColor" width="18" height="18"><path d="M508.5 468.9L387.1 347.5c-2.3-2.3-5.3-3.5-8.5-3.5h-13.2c31.5-36.5 50.6-84 50.6-136C416 93.1 322.9 0 208 0S0 93.1 0 208s93.1 208 208 208c52 0 99.5-19.1 136-50.6v13.2c0 3.2 1.3 6.2 3.5 8.5l121.4 121.4c4.7 4.7 12.3 4.7 17 0l22.6-22.6c4.7-4.7 4.7-12.3 0-17zM208 368c-88.4 0-160-71.6-160-160S119.6 48 208 48s160 71.6 160 160-71.6 160-160 160z" /></svg>
</a>
<button class="btn btn-outline-light d-block d-lg-none g-nav-toggle" aria-label="Menu">
<svg xmlns="http://www.w3.org/2000/svg" viewbox="0 0 448 512" fill="currentColor" width="18" height="18"><path d="M436 124H12c-6.627 0-12-5.373-12-12V80c0-6.627 5.373-12 12-12h424c6.627 0 12 5.373 12 12v32c0 6.627-5.373 12-12 12zm0 160H12c-6.627 0-12-5.373-12-12v-32c0-6.627 5.373-12 12-12h424c6.627 0 12 5.373 12 12v32c0 6.627-5.373 12-12 12zm0 160H12c-6.627 0-12-5.373-12-12v-32c0-6.627 5.373-12 12-12h424c6.627 0 12 5.373 12 12v32c0 6.627-5.373 12-12 12z" /></svg>
</button>
</div>
<nav class="g-nav pt-2 d-none d-lg-block">
<ul class="d-lg-flex list-unstyled align-items-center mb-0">
<li class="d-none d-lg-block"><a class="btn btn-link text-white pl-0" href="/">
<svg xmlns="http://www.w3.org/2000/svg" viewbox="0 0 576 512" fill="currentColor" width="18" height="18" class="mr-1"><path d="M488 312.7V456c0 13.3-10.7 24-24 24H348c-6.6 0-12-5.4-12-12V356c0-6.6-5.4-12-12-12h-72c-6.6 0-12 5.4-12 12v112c0 6.6-5.4 12-12 12H112c-13.3 0-24-10.7-24-24V312.7c0-3.6 1.6-7 4.4-9.3l188-154.8c4.4-3.6 10.8-3.6 15.3 0l188 154.8c2.7 2.3 4.3 5.7 4.3 9.3zm83.6-60.9L488 182.9V44.4c0-6.6-5.4-12-12-12h-56c-6.6 0-12 5.4-12 12V117l-89.5-73.7c-17.7-14.6-43.3-14.6-61 0L4.4 251.8c-5.1 4.2-5.8 11.8-1.6 16.9l25.5 31c4.2 5.1 11.8 5.8 16.9 1.6l235.2-193.7c4.4-3.6 10.8-3.6 15.3 0l235.2 193.7c5.1 4.2 12.7 3.5 16.9-1.6l25.5-31c4.2-5.2 3.4-12.7-1.7-16.9z" /></svg>
UK Essays</a></li>
<li>
<a class="btn btn-link text-white" href="/services/">Writing Services</a>
<div class="g-dropdown position-absolute bg-light">
<ul class="list-unstyled mb-0 container categories position-relative">
<li>
<a href="/services/essay-writing-service.php" class="d-flex justify-content-between align-items-center bg-white border-right p-3 pt-4">Essay Writing
<svg xmlns="http://www.w3.org/2000/svg" viewbox="0 0 256 512" fill="currentColor" width="18" height="18"><path d="M24.707 38.101L4.908 57.899c-4.686 4.686-4.686 12.284 0 16.971L185.607 256 4.908 437.13c-4.686 4.686-4.686 12.284 0 16.971L24.707 473.9c4.686 4.686 12.284 4.686 16.971 0l209.414-209.414c4.686-4.686 4.686-12.284 0-16.971L41.678 38.101c-4.687-4.687-12.285-4.687-16.971 0z" /></svg>
</a>
<ul class="list-unstyled mb-0 category-links position-absolute">
<li><h3 class="h4 text-dark">Essay Services</h3></li>
<li><a href="/services/essay-writing-service.php">Essay Writing Service</a></li>
<li><a href="/services/essay-writing-service.php?service=assignment">Assignment Writing Service</a></li>
 <li><a href="/services/essay-writing-service.php?service=coursework">Coursework Writing Service</a></li>
<li><a href="/services/essay-writing-service.php?service=skeleton-answer">Essay Outline/Plan Service</a></li>
</ul>
</li>
<li>
<a href="/services/dissertation-writing-services.php" class="d-flex justify-content-between align-items-center bg-white border-right p-3 pt-4">Dissertation Writing
<svg xmlns="http://www.w3.org/2000/svg" viewbox="0 0 256 512" fill="currentColor" width="18" height="18"><path d="M24.707 38.101L4.908 57.899c-4.686 4.686-4.686 12.284 0 16.971L185.607 256 4.908 437.13c-4.686 4.686-4.686 12.284 0 16.971L24.707 473.9c4.686 4.686 12.284 4.686 16.971 0l209.414-209.414c4.686-4.686 4.686-12.284 0-16.971L41.678 38.101c-4.687-4.687-12.285-4.687-16.971 0z" /></svg>
</a>
<ul class="list-unstyled mb-0 category-links position-absolute d-none">
<li><h3 class="h4 text-dark">Dissertation Services</h3></li>
<li><a href="/services/dissertation-writing-services.php">Dissertation Writing Service</a></li>
<li><a href="/services/proposal.php">Dissertation Proposal Service</a></li>
<li><a href="/services/topic-with-titles.php">Topics with Titles Service</a></li>
<li><a href="/services/literature-review.php">Literature Review Service</a></li>
</ul>
</li>
<li>
<a href="/services/" class="d-flex justify-content-between align-items-center bg-white border-right p-3 pt-4">Other Services
<svg xmlns="http://www.w3.org/2000/svg" viewbox="0 0 256 512" fill="currentColor" width="18" height="18"><path d="M24.707 38.101L4.908 57.899c-4.686 4.686-4.686 12.284 0 16.971L185.607 256 4.908 437.13c-4.686 4.686-4.686 12.284 0 16.971L24.707 473.9c4.686 4.686 12.284 4.686 16.971 0l209.414-209.414c4.686-4.686 4.686-12.284 0-16.971L41.678 38.101c-4.687-4.687-12.285-4.687-16.971 0z" /></svg>
</a>
<ul class="list-unstyled mb-0 category-links position-absolute d-none">
<li><h3 class="h4 text-dark">Other Services</h3></li>
<li><a href="/marking/premium-service.php">Marking Services</a></li>
<li><a href="/services/essay-writing-service.php?service=report">Report Writing Service</a></li>
<li><a href="/services/reflective-practice.php">Reflective Practice Service</a></li>
<li><a href="/services/powerpoint-presentation-service.php">PowerPoint Presentation Service</a></li>
<li><a href="/services/exam-revision.php">Exam Revision Service</a></li>
</ul>
</li>
<li><a href="/services/samples/" class="d-block bg-white border-right p-3">Samples of Our Work</a></li>
<li><a href="/services/" class="d-block bg-white border-right p-3">View All Services</a></li>
</ul>
</div>
</li>
<li><a class="btn btn-link text-white" href="/guarantees.php">Guarantees</a></li>
<li><a class="btn btn-link text-white" href="/quote.php?country=2&amp;product=1">Prices</a></li>
<li><a class="btn btn-link text-white" href="/essays/">Essays</a></li>
<li>
<a class="btn btn-link text-white" href="/resources/">Free Resources</a>
<div class="g-dropdown position-absolute bg-light">
<ul class="list-unstyled mb-0 container categories position-relative">
<li>
<a class="d-flex justify-content-between align-items-center bg-white border-right p-3 pt-4" href="/essays/">Essays
<svg xmlns="http://www.w3.org/2000/svg" viewbox="0 0 256 512" fill="currentColor" width="18" height="18"><path d="M24.707 38.101L4.908 57.899c-4.686 4.686-4.686 12.284 0 16.971L185.607 256 4.908 437.13c-4.686 4.686-4.686 12.284 0 16.971L24.707 473.9c4.686 4.686 12.284 4.686 16.971 0l209.414-209.414c4.686-4.686 4.686-12.284 0-16.971L41.678 38.101c-4.687-4.687-12.285-4.687-16.971 0z" /></svg>
</a>
<div class="category-links position-absolute">
<ul class="list-unstyled mb-3 mr-0 d-inline-block mr-lg-4">
<li><h3 class="h4 text-dark">Essays</h3></li>
<li><a href="/essays/">Essays (Written by Students)</a></li>
<li><a href="/services/example-essays/">Example Essays (Written by Professionals)</a></li>
<li><a href="/coursework/">Example Coursework (Written by Students)</a></li>
<li><a href="/assignments/">Example Assignments (Written by Students)</a></li>
</ul>
</div>
</li>
<li>
<a class="d-flex justify-content-between align-items-center bg-white border-right p-3 pt-4" href="/dissertation/">Dissertations
<svg xmlns="http://www.w3.org/2000/svg" viewbox="0 0 256 512" fill="currentColor" width="18" height="18"><path d="M24.707 38.101L4.908 57.899c-4.686 4.686-4.686 12.284 0 16.971L185.607 256 4.908 437.13c-4.686 4.686-4.686 12.284 0 16.971L24.707 473.9c4.686 4.686 12.284 4.686 16.971 0l209.414-209.414c4.686-4.686 4.686-12.284 0-16.971L41.678 38.101c-4.687-4.687-12.285-4.687-16.971 0z" /></svg>
</a>
<ul class="list-unstyled mb-0 category-links d-none position-absolute">
<li><h3 class="h4 text-dark">Dissertations</h3></li>
<li><a href="/dissertation/examples/">Dissertation Examples (Written by Students)</a></li>
<li><a href="/services/samples/undergraduate-2-1-business-dissertation.php">Sample Dissertation (Professionally Written)</a></li>
<li><a href="/dissertation/titles/">Dissertation Titles</a></li>
<li><a href="/dissertation/topics/">Dissertation Topics</a></li>
<li><a href="/dissertation/proposal/">Dissertation Proposals</a></li>
<li><a href="/dissertation/introduction/">Introductions</a></li>
<li><a href="/dissertation/methodology/">Methodologies</a></li>
<li><a href="/dissertation/literature-review/">Literature Reviews</a></li>
</ul>
</li>
<li>
<a class="d-flex justify-content-between align-items-center bg-white border-right p-3 pt-4" href="/referencing/">Referencing
<svg xmlns="http://www.w3.org/2000/svg" viewbox="0 0 256 512" fill="currentColor" width="18" height="18"><path d="M24.707 38.101L4.908 57.899c-4.686 4.686-4.686 12.284 0 16.971L185.607 256 4.908 437.13c-4.686 4.686-4.686 12.284 0 16.971L24.707 473.9c4.686 4.686 12.284 4.686 16.971 0l209.414-209.414c4.686-4.686 4.686-12.284 0-16.971L41.678 38.101c-4.687-4.687-12.285-4.687-16.971 0z" /></svg>
</a>
<div class="category-links d-none position-absolute">
<ul class="list-unstyled mb-3 mr-0 d-inline-block mr-lg-4 verticle-top">
<li><h3 class="h4 text-dark">Referencing Tools</h3></li>
<li><a href="/referencing/apa/generator/">APA Reference Generator</a></li>
<li><a href="/referencing/harvard/generator/">Harvard Reference Generator</a></li>
<li><a href="https://www.lawteacher.net/oscola-referencing/">OSCOLA Reference Generator
<svg xmlns="http://www.w3.org/2000/svg" viewbox="0 0 576 512" fill="currentColor" width="18" height="18"><path d="M576 14.4l-.174 163.2c0 7.953-6.447 14.4-14.4 14.4H528.12c-8.067 0-14.56-6.626-14.397-14.691l2.717-73.627-2.062-2.062-278.863 278.865c-4.686 4.686-12.284 4.686-16.971 0l-23.029-23.029c-4.686-4.686-4.686-12.284 0-16.971L474.379 61.621l-2.062-2.062-73.626 2.717C390.626 62.44 384 55.946 384 47.879V14.574c0-7.953 6.447-14.4 14.4-14.4L561.6 0c7.953 0 14.4 6.447 14.4 14.4zM427.515 233.74l-24 24a12.002 12.002 0 0 0-3.515 8.485V458a6 6 0 0 1-6 6H54a6 6 0 0 1-6-6V118a6 6 0 0 1 6-6h301.976c10.691 0 16.045-12.926 8.485-20.485l-24-24A12.002 12.002 0 0 0 331.976 64H48C21.49 64 0 85.49 0 112v352c0 26.51 21.49 48 48 48h352c26.51 0 48-21.49 48-48V242.225c0-10.691-12.926-16.045-20.485-8.485z" /></svg>
</a></li>
<li><a href="/referencing/vancouver/generator/">Vancouver Reference Generator</a></li>
<li class="d-lg-none"><a href="/referencing/vancouver/generator/">All Referencing Tools &amp; Guides</a></li>
</ul>
<ul class="list-unstyled mb-3 mr-0 d-none d-lg-inline-block mr-lg-4">
<li><h3 class="h4 text-dark">Referencing Guides</h3></li>
<li><a href="/referencing/apa/">APA Referencing Guide</a></li>
<li><a href="/referencing/harvard/">Harvard Referencing Guide</a></li>
<li><a href="/referencing/oscola/">OSCOLA Referencing Guide</a></li>
<li><a href="/referencing/vancouver/">Vancouver Referencing Guide</a></li>
<li><a href="/referencing/">More Referencing Guides</a></li>
</ul>
</div>
</li>
<li>
<a class="d-flex justify-content-between align-items-center bg-white border-right p-3 pt-4" href="/resources/">Help Guides
<svg xmlns="http://www.w3.org/2000/svg" viewbox="0 0 256 512" fill="currentColor" width="18" height="18"><path d="M24.707 38.101L4.908 57.899c-4.686 4.686-4.686 12.284 0 16.971L185.607 256 4.908 437.13c-4.686 4.686-4.686 12.284 0 16.971L24.707 473.9c4.686 4.686 12.284 4.686 16.971 0l209.414-209.414c4.686-4.686 4.686-12.284 0-16.971L41.678 38.101c-4.687-4.687-12.285-4.687-16.971 0z" /></svg>
</a>
<ul class="list-unstyled mb-0 category-links d-none position-absolute">
<li><h3 class="h4 text-dark">Help Guides</h3></li>
<li><a href="/resources/undergraduate/">Undergraduate Help Guides</a></li>
<li><a href="/resources/masters/">Masters Help Guides</a></li>
<li><a href="/resources/undergraduate/how-to-write/">Guide to Writing an Essay</a></li>
</ul>
</li>
<li><a class="d-block bg-white border-right p-3" href="/resources/exam-revision/">Exam Revision Guides</a></li>
</ul>
</div>
</li>
<li>
<a class="btn btn-link text-white" href="/contact/">About Us</a>
<div class="g-dropdown position-absolute bg-light">
<ul class="list-unstyled mb-0 container categories position-relative">
<li>
<a href="/contact/about.php" class="d-flex justify-content-between align-items-center bg-white border-right p-3 pt-4">About
<svg xmlns="http://www.w3.org/2000/svg" viewbox="0 0 256 512" fill="currentColor" width="18" height="18"><path d="M24.707 38.101L4.908 57.899c-4.686 4.686-4.686 12.284 0 16.971L185.607 256 4.908 437.13c-4.686 4.686-4.686 12.284 0 16.971L24.707 473.9c4.686 4.686 12.284 4.686 16.971 0l209.414-209.414c4.686-4.686 4.686-12.284 0-16.971L41.678 38.101c-4.687-4.687-12.285-4.687-16.971 0z" /></svg>
</a>
<div class="category-links position-absolute">
<h3 class="h4 text-dark">About UKEssays</h3>
<ul class="list-unstyled mb-0 mr-0 mr-lg-4 d-inline-block">
<li><a href="/contact/about.php">About UK Essays</a></li>
<li><a href="https://support.ukessays.com/hc/en-us">Help Centre - FAQs</a></li>
<li><a href="/contact/about.php">Meet The Team</a></li>
<li><a href="/quality/">Our Quality Procedures</a></li>
<li><a href="/contact/review-testimonials.php">Customer Reviews</a></li>
<li><a href="/contact/freelance-writing-jobs.php">Writing Jobs</a></li>
</ul>
</div>
</li>
<li>
<a href="/contact/" class="d-flex justify-content-between align-items-center bg-white border-right p-3 pt-4">Contact
<svg xmlns="http://www.w3.org/2000/svg" viewbox="0 0 256 512" fill="currentColor" width="18" height="18"><path d="M24.707 38.101L4.908 57.899c-4.686 4.686-4.686 12.284 0 16.971L185.607 256 4.908 437.13c-4.686 4.686-4.686 12.284 0 16.971L24.707 473.9c4.686 4.686 12.284 4.686 16.971 0l209.414-209.414c4.686-4.686 4.686-12.284 0-16.971L41.678 38.101c-4.687-4.687-12.285-4.687-16.971 0z" /></svg>
</a>
<ul class="list-unstyled mb-0 category-links position-absolute d-none">
<li><h3 class="h4 text-dark">Contact</h3></li>
<li><a href="/contact/">Contact UK Essays</a></li>
<li><a href="/contact/press-center.php">Press Centre &amp; Enquiries</a></li>
</ul>
</li>
<li><a href="/contact/fair-use-policy.php" class="d-block bg-white border-right p-3">Fair Use Policy</a></li>
<li><a href="/myaccount/" class="d-block bg-white border-right p-3">Account Login</a></li>
</ul>
</div>
</li>
<li class="d-block d-lg-none"><a class="btn btn-link text-white" href="/myaccount/">Sign in</a></li>
<li class="d-block d-lg-none"><a class="btn btn-link text-white" href="tel:+440115 966 7955">Call us on 0115 966 7955</a></li>
<li class="ml-auto"><a href="/order/?country=2&amp;product=1" class="btn btn-success btn-block px-5 py-1">Order Now</a></li>
</ul>
</nav>
 </div>
</header>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog modal-lg">
<div class="modal-content"></div>
</div>
</div>
<main><div class="container">
<div class="row">
<article class="col-sm-9">
<div id="static-include">
<h1>UK Essays Privacy Policy</h1>
<p>UK Essays is a website belonging to All Answers Limited, a UK Registered Company (No. 4964706). We take the privacy of your information very seriously.</p>
<p>Our privacy policy below explains how we will collect and use the information you give us. We may vary this policy from time to time and the current version will be that published on this site. The word 'we' refers to All Answers Limited, unless otherwise indicated.</p>
<h2>Collecting your information</h2>
<p>From time to time, you will be asked to submit personal information about yourself (e.g. name and email address etc) in order to receive or use services on our website(s). This may happen, for example, when you make a purchase from us, when you apply to be a writer, when you subscribe to a newsletter, when you enter a competition or when you register for personalised services such as your customer control panel.</p>
<p>We will also need personal information in order to supply our products and services to you, and sometimes to help us identify you when you contact us. When we collect this information we will take reasonable measures to ensure that it is transmitted to us securely. We will only collect payment information over a secure connection.</p>
<p>If you communicate with us by email over the Internet you should be aware that the nature of the Internet is such that unencrypted communication may not be secure and may pass through several different countries on route to us. Please do not email us with confidential or sensitive information such as your credit card details. We cannot accept responsibility for unauthorised access to your information that is outside our control.</p>
<p>We may combine this information with other information that we may hold about you if you are an existing customer, or have made enquiries of us before.</p>
<p>We will act in accordance with current legislation and we will maintain a link to our privacy policy, which includes our Data Protection Notice, in a prominent place on our website.</p>
<h3>UK Essays Data Protection Notice</h3>
<p>Unlike many reputable (and many more not so reputable) companies who trade on the Internet, we will <strong><u>NEVER</u></strong> share the information you provide, together with other information, with any other company.</p>
<p>We may use the information you provide for administration, marketing, customer services and profiling your purchasing preferences. However, we promise not to send you more than four marketing communications per month, and we promise to give you the ability to stop receiving marketing communications at any time.</p>
<p>For information about the way we use cookies and other web tracking devices please refer to the sections below in this privacy policy.</p>
<p>If you give us information about another person, in doing so you confirm that they have given you permission to provide it to us and for us to be able to process their personal data (including sensitive personal data) and also that you have told them who we are and what we will use their data for, as set out in this notice.</p>
<p>You have the right to ask for a copy of your information (for which we reserve the right to charge a small fee) and to correct any inaccuracies.</p>
<p>For our joint protection and training purposes, telephone calls may be recorded and/or monitored.</p>
<h3>Informing you about products and services</h3>
<p>We offer student education related products such as plagiarism software, past papers, marking and proofreading services. By providing us with your contact details, you will be indicating to us your consent to us contacting you by mail, telephone, fax, email, and SMS/MMS to let you know about any goods, services or promotions of our own which may be of interest to you unless you indicate an objection to receiving such messages.</p>
<p>As stated in our Data Protection Notice, we will never send you more than four marketing communications per month (in practice, we rarely send out more than one marketing communication per month) and we will always give you the opportunity of opting out of such marketing communications.</p>
<h3>Sensitive data</h3>
<p>You may be asked on the telephone to give your credit or debit card details for the purpose of paying for a service you have requested from us. You will always have the option to enter these through our website instead, which uses Barclay's Secure Merchant Services. We also accept Paypal payments which do not require you to hand over your credit or debit card details.</p>
<p>We never store customers' sensitive data - for example, credit or debit card details.</p>
<p>If you are a writer for our company, you acknowledge that to speed up the processing of your invoices, we need to store your bank details as supplied by you. We promise to take all reasonable precautions to ensure these are kept safe.</p>
<h3>How we collect information about your visits to our websites</h3>
<p>When you visit our web pages, we may check your IP address to determine your location. This helps us give you content relevant to where you are located.</p>
<p>We may put something called a `cookie' on your computer. Thousands of reputable websites use cookies and you should not be alarmed by this. They help us track how often you visit the website. They also help us confirm your identity when you log into our website, and they help us determine whether you are logged in or logged out.</p>
<p>Some of our websites use analytics software which records non-personal statistical information about what you do on the website. For example: the duration of time a page was viewed, common paths taken through the site, data on screen settings and other general information. None of this information is person-specific.</p>
<h2>What will we do with your personal information?</h2>
<p>We will only use your information for the purposes that you would reasonably anticipate or that we state when we collect it and, where necessary, for which you have given us your consent. We will not contact you, or pass your information to any other person or company, ever.</p>
<p>We are committed to treating your personal data with due care and in accordance with the data protection principles. In addition, you will see that our privacy policy is far more protective than many major companies, who often reserve the right to pass your details to third parties for marketing.</p>
<h3>How to access your personal information</h3>
<p>If you would like to see a copy of the information that we hold for you, please email our enquiries team, and mark your email for the attention of the legal department. We reserve the right to ask you to provide certain information to help us confirm your identity.</p>
<p>We reserve the right to charge a small fee for the provision of this information, although we will probably not do so.</p>
<p>If you believe that any of the data we hold about you is incorrect or being misused or want further information you may contact us at our enquiries email address.</p>
<h3>Users aged 16 years and under</h3>
<p>If you are 16 or under you must have permission from a parent or guardian before you give us personal information. If we find that we have received information from you without the appropriate consent, we reserve the right to cancel all transactions and services and remove all personal data that you have supplied. You will be able to re-submit the information when you have the required permission.</p>
<h3>What is a cookie?</h3>
<p>Cookies are text files that identify your computer (through what is known as an IP address) to our server. </p>
<p>You generally have the opportunity to set your computer to accept all cookies, to notify you when a cookie is issued, or not to receive cookies at any time. You can do this through your Internet browser. With most browsers, this facility can be reached via the `tools' (e.g. Internet Explorer), or `edit', or 'task' (e.g. Netscape) menu. If you have any problems finding this area, the `help' function within your browser will be able to provide assistance.</p>
<p>If you refuse a cookie it may prevent the proper operation of the site or even prevent your access to certain areas. The customer login area, for example, relies on cookies to help identify you, and to identify if you are logged in or logged out at any time.</p>
<p><a href="/cookie-info.php" target="_blank" rel="nofollow">Click here for more information about the cookies we use on this website.</a></p>
<h2>Links to other websites</h2>
<p>Our websites may contain links to other sites outside the group. All Answers Limited is not responsible for the privacy and security of these sites unless they belong to All Answers. This privacy policy applies only to All Answers Limited Websites.</p>
<p>If you are unsure whether a website belongs to All Answers Limited or not, just email our enquiries email address, or give us a call.</p>
<p>If you have any more questions about the way we handle data, please call or email us. If you have a more in-depth question, please email our enquiries address, marked for the attention of the legal department.</p>
</div>
</article>
</div>
</div>
<div class="bg-primary py-4 text-center text-sm-left lazy" data-src="/images/services/essay/samples-background.jpg">
<div class="container d-sm-flex align-items-center text-uppercase">
<h5 class="h3 mb-0 text-white">Invest in your future <b>today</b></h5>
<a href="/order/?country=2&amp;product=1" class="btn btn-light btn-lg ml-auto mt-3 mt-sm-0">Place an Order</a>
</div>
</div>
<nav class="g-breadcrumb g-footer-breadcrumb text-light"><ul class="container d-flex list-inline mb-0 py-2"><li class="d-none d-lg-inline"><a href="/">UKEssays</a></li><li class="d-lg-none"><a href="/">
<svg xmlns="http://www.w3.org/2000/svg" viewbox="0 0 576 512" fill="currentColor" width="18" height="18" class="mb-1"><path d="M488 312.7V456c0 13.3-10.7 24-24 24H348c-6.6 0-12-5.4-12-12V356c0-6.6-5.4-12-12-12h-72c-6.6 0-12 5.4-12 12v112c0 6.6-5.4 12-12 12H112c-13.3 0-24-10.7-24-24V312.7c0-3.6 1.6-7 4.4-9.3l188-154.8c4.4-3.6 10.8-3.6 15.3 0l188 154.8c2.7 2.3 4.3 5.7 4.3 9.3zm83.6-60.9L488 182.9V44.4c0-6.6-5.4-12-12-12h-56c-6.6 0-12 5.4-12 12V117l-89.5-73.7c-17.7-14.6-43.3-14.6-61 0L4.4 251.8c-5.1 4.2-5.8 11.8-1.6 16.9l25.5 31c4.2 5.1 11.8 5.8 16.9 1.6l235.2-193.7c4.4-3.6 10.8-3.6 15.3 0l235.2 193.7c5.1 4.2 12.7 3.5 16.9-1.6l25.5-31c4.2-5.2 3.4-12.7-1.7-16.9z" /></svg>
</a></li><li><span class="mx-2">&gt;</span><a href="/contact/">Contact</a></li></ul></nav>
</main>
<footer class="g-footer bg-dark pt-5 text-white">
<div class="container">
<div class="row align-items-center mb-5">
<div class="col-lg-6 order-2 order-lg-1">
<ul class="list-inline d-flex flex-wrap mb-0">
<li class="mr-3 py-1">
<svg xmlns="http://www.w3.org/2000/svg" viewbox="0 0 512 512" fill="currentColor" width="16" height="16" class="mr-1"><path d="M476.536 22.921L382.288 1.18c-21.6-4.984-43.609 6.185-52.339 26.556l-43.5 101.492c-7.982 18.626-2.604 40.598 13.081 53.43l40.016 32.741c-28.537 52.375-71.771 95.609-124.147 124.147l-32.741-40.015c-12.832-15.685-34.804-21.063-53.43-13.081L27.736 329.949C7.365 338.68-3.804 360.691 1.18 382.287l21.742 94.25C27.74 497.417 46.072 512 67.5 512 312.347 512 512 313.731 512 67.5c0-21.429-14.583-39.761-35.464-44.579zM69.289 463.996l-20.921-90.669 98.212-42.091 55.716 68.094c98.805-46.353 150.588-98.036 197.036-197.035l-68.097-55.715 42.092-98.212 90.669 20.921c-.947 217.588-177.09 393.755-394.707 394.707z" /></svg>
0115 966 7955</li>
<li class="mr-3"><a class="text-white py-1 d-inline-block" href="/cdn-cgi/l/email-protection#6f0a011e1a061d060a1c2f1a040a1c1c0e161c410c0002">
<svg xmlns="http://www.w3.org/2000/svg" viewbox="0 0 512 512" fill="currentColor" width="16" height="16" class="mr-1"><path d="M464 64H48C21.49 64 0 85.49 0 112v288c0 26.51 21.49 48 48 48h416c26.51 0 48-21.49 48-48V112c0-26.51-21.49-48-48-48zm0 48v40.805c-22.422 18.259-58.168 46.651-134.587 106.49-16.841 13.247-50.201 45.072-73.413 44.701-23.208.375-56.579-31.459-73.413-44.701C106.18 199.465 70.425 171.067 48 152.805V112h416zM48 400V214.398c22.914 18.251 55.409 43.862 104.938 82.646 21.857 17.205 60.134 55.186 103.062 54.955 42.717.231 80.509-37.199 103.053-54.947 49.528-38.783 82.032-64.401 104.947-82.653V400H48z" /></svg>
<span class="__cf_email__" data-cfemail="f99c97888c908b909c8ab98c929c8a8a98808ad79a9694">[email&#160;protected]</span></a></li>
<li class="mr-3"><a class="text-white py-1 d-inline-block" href="/contact/">
<svg xmlns="http://www.w3.org/2000/svg" viewbox="0 0 320 512" fill="currentColor" width="16" height="16" class="mr-1"><path d="M320 160C320 71.965 248.9.536 160.99.003 72.711-.532-.001 71.721 0 160.002c.001 80.207 59.021 146.626 136 158.206v160.625l13.267 26.534c4.422 8.845 17.044 8.845 21.466 0L184 478.833V318.208c76.98-11.581 136-78 136-158.208zM160 272c-61.898 0-112-50.092-112-112C48 98.102 98.092 48 160 48c61.898 0 112 50.092 112 112 0 61.898-50.092 112-112 112zm8-176c0 8.836-7.163 16-16 16-22.056 0-40 17.944-40 40 0 8.836-7.163 16-16 16s-16-7.164-16-16c0-39.701 32.299-72 72-72 8.837 0 16 7.164 16 16z" /></svg>
Contact</a></li>
<li class="mr-3"><a class="text-white py-1 d-inline-block" href="/contact/about.php">
<svg xmlns="http://www.w3.org/2000/svg" viewbox="0 0 512 512" fill="currentColor" width="16" height="16" class="mr-1"><path d="M256 8C119.043 8 8 119.083 8 256c0 136.997 111.043 248 248 248s248-111.003 248-248C504 119.083 392.957 8 256 8zm0 448c-110.532 0-200-89.431-200-200 0-110.495 89.472-200 200-200 110.491 0 200 89.471 200 200 0 110.53-89.431 200-200 200zm0-338c23.196 0 42 18.804 42 42s-18.804 42-42 42-42-18.804-42-42 18.804-42 42-42zm56 254c0 6.627-5.373 12-12 12h-88c-6.627 0-12-5.373-12-12v-24c0-6.627 5.373-12 12-12h12v-64h-12c-6.627 0-12-5.373-12-12v-24c0-6.627 5.373-12 12-12h64c6.627 0 12 5.373 12 12v100h12c6.627 0 12 5.373 12 12v24z" /></svg>
About</a></li>
<li><a class="text-white py-1 d-inline-block" href="/contact/freelance-writing-jobs.php">
<svg xmlns="http://www.w3.org/2000/svg" viewbox="0 0 448 512" fill="currentColor" width="16" height="16" class="mr-1"><path d="M246.6 177.9l55.5 55.5c2.3 2.3 2.3 6.1 0 8.5L166.4 377.6l-57.1 6.3c-7.6.8-14.1-5.6-13.3-13.3l6.3-57.1L238 177.8c2.4-2.2 6.2-2.2 8.6.1zm98.4-12.8L314.9 135c-9.4-9.4-24.6-9.4-33.9 0l-23.1 23.1c-2.3 2.3-2.3 6.1 0 8.5l55.5 55.5c2.3 2.3 6.1 2.3 8.5 0L345 199c9.3-9.3 9.3-24.5 0-33.9zM448 80v352c0 26.5-21.5 48-48 48H48c-26.5 0-48-21.5-48-48V80c0-26.5 21.5-48 48-48h352c26.5 0 48 21.5 48 48zm-48 346V86c0-3.3-2.7-6-6-6H54c-3.3 0-6 2.7-6 6v340c0 3.3 2.7 6 6 6h340c3.3 0 6-2.7 6-6z" /></svg>
Become a Researcher</a></li>
</ul>
</div>
<div class="col-lg-6 order-1 mb-5 mb-lg-0">
<form method="get" action="/search.php">
<div class="input-group">
<input type="search" name="q" class="form-control" placeholder="Search" aria-label="Search" required>
<div class="input-group-append">
<button class="btn btn-primary" type="submit" aria-label="Search">
<svg fill="#fff" height="18" viewbox="0 0 24 24" width="18" xmlns="http://www.w3.org/2000/svg">
<path d="M15.5 14h-.79l-.28-.27C15.41 12.59 16 11.11 16 9.5 16 5.91 13.09 3 9.5 3S3 5.91 3 9.5 5.91 16 9.5 16c1.61 0 3.09-.59 4.23-1.57l.27.28v.79l5 4.99L20.49 19l-4.99-5zm-6 0C7.01 14 5 11.99 5 9.5S7.01 5 9.5 5 14 7.01 14 9.5 11.99 14 9.5 14z" />
<path d="M0 0h24v24H0z" fill="none" />
</svg>
</button>
</div>
</div>
</form>
</div>
</div>
<div class="row text-left mb-5">
<div class="col-6 col-lg-3 order-1 order-lg-1"> 
<h5>Services</h5>
<ul class="list-unstyled">
<li><a class="text-white py-1 d-inline-block" href="/services/essay-writing-service.php">Essay Writing Service</a></li>
<li><a class="text-white py-1 d-inline-block" href="/services/dissertation-writing-services.php">Dissertation Writing Service</a></li>
<li><a class="text-white py-1 d-inline-block" href="/marking/premium-service.php">Marking Service</a></li>
<li><a class="text-white py-1 d-inline-block" href="/services/">All Services</a></li>
</ul>
</div>
<div class="col-6 col-lg-3 order-2 order-lg-2">
<h5>Useful Resources</h5>
<ul class="list-unstyled">
<li><a class="text-white py-1 d-inline-block" href="/essays/">Essays</a></li>
<li><a class="text-white py-1 d-inline-block" href="/dissertation/examples/">Dissertation Examples</a></li>
<li><a class="text-white py-1 d-inline-block" href="/referencing/">Referencing Tools</a></li>
<li><a class="text-white py-1 d-inline-block" href="/resources/">Free Resources</a></li>
</ul>
</div>
<div class="col-6 col-lg-2 order-4 order-lg-3">
<h5>Company</h5>
<ul class="list-unstyled">
<li><a class="text-white py-1 d-inline-block" href="/contact/fair-use-policy.php">Fair Use Policy</a></li>
<li><a class="text-white py-1 d-inline-block" href="/contact/complaints.php">Complaints</a></li>
<li><a class="text-white py-1 d-inline-block" href="https://support.ukessays.com/hc/en-us">Help Centre</a></li>
</ul>
</div>
<div class="col-6 col-lg-4 order-3 order-lg-4">
<div class="row">
<div class="col-lg-5">
<img src="/images/uk-essays.png" data-src="/images/icons/all-answers.png" alt="All Answers Ltd" width="123" class="img-fluid lazy">
</div>
<div class="col-lg-7">
<div class="font-weight-bold">Part of</div>
<h4>All Answers Ltd</h4>
<p class="mb-0">A Market Leading Independent Academic Support Company</p>
</div>
</div>
</div>
</div>
</div>
<div class="bg-white text-dark pt-3">
<div class="container">
<p class="small mb-0">Copyright &copy; 2003 - 2018 - UKEssays is a trading name of All Answers Ltd, a company registered in England and Wales. Company Registration No: 4964706. VAT Registration No: 842417633. Registered Data Controller No: Z1821391. Registered office: Venture House, Cross Street, Arnold, Nottingham, Nottinghamshire, NG5 7PJ.</p>
<div class="d-md-flex align-items-center py-3">
<ul class="list-unstyled d-flex mb-md-0 mr-auto">
<li class="mr-3"><a href="/contact/privacy-policy.php">Privacy Policy</a></li>
<li class="mr-3"><a href="/terms.php">Terms &amp; Conditions</a></li>
<li class="mr-3"><a href="/copyright-notice.php">Copyright Notice</a></li>
<li><a href="/contact/cookie-info.php">Cookies</a></li>
</ul>
<ul class="list-unstyled d-flex align-items-center mb-0">
<li class="ml-auto ml-md-3">
<a href="https://www.facebook.com/UKEssays" target="_blank" rel="nofollow noopener" aria-label="Facebook">
<svg xmlns="http://www.w3.org/2000/svg" viewbox="0 0 264 512" fill="currentColor" width="18" height="18"><path d="M76.7 512V283H0v-91h76.7v-71.7C76.7 42.4 124.3 0 193.8 0c33.3 0 61.9 2.5 70.2 3.6V85h-48.2c-37.8 0-45.1 18-45.1 44.3V192H256l-11.7 91h-73.6v229" /></svg>
</a>
</li>
<li class="ml-3">
<a href="https://twitter.com/UKEssays" target="_blank" rel="nofollow noopener" aria-label="LinkedIn">
<svg xmlns="http://www.w3.org/2000/svg" viewbox="0 0 512 512" fill="currentColor" width="18" height="18"><path d="M459.37 151.716c.325 4.548.325 9.097.325 13.645 0 138.72-105.583 298.558-298.558 298.558-59.452 0-114.68-17.219-161.137-47.106 8.447.974 16.568 1.299 25.34 1.299 49.055 0 94.213-16.568 130.274-44.832-46.132-.975-84.792-31.188-98.112-72.772 6.498.974 12.995 1.624 19.818 1.624 9.421 0 18.843-1.3 27.614-3.573-48.081-9.747-84.143-51.98-84.143-102.985v-1.299c13.969 7.797 30.214 12.67 47.431 13.319-28.264-18.843-46.781-51.005-46.781-87.391 0-19.492 5.197-37.36 14.294-52.954 51.655 63.675 129.3 105.258 216.365 109.807-1.624-7.797-2.599-15.918-2.599-24.04 0-57.828 46.782-104.934 104.934-104.934 30.213 0 57.502 12.67 76.67 33.137 23.715-4.548 46.456-13.32 66.599-25.34-7.798 24.366-24.366 44.833-46.132 57.827 21.117-2.273 41.584-8.122 60.426-16.243-14.292 20.791-32.161 39.308-52.628 54.253z" /></svg>
</a>
</li>
<li class="ml-3">
<a href="https://plus.google.com/+UkessaysOfficial" target="_blank" rel="nofollow noopener" aria-label="Google Plus">
<svg xmlns="http://www.w3.org/2000/svg" viewbox="0 0 640 512" fill="currentColor" width="18" height="18"><path d="M386.061 228.496c1.834 9.692 3.143 19.384 3.143 31.956C389.204 370.205 315.599 448 204.8 448c-106.084 0-192-85.915-192-192s85.916-192 192-192c51.864 0 95.083 18.859 128.611 50.292l-52.126 50.03c-14.145-13.621-39.028-29.599-76.485-29.599-65.484 0-118.92 54.221-118.92 121.277 0 67.056 53.436 121.277 118.92 121.277 75.961 0 104.513-54.745 108.965-82.773H204.8v-66.009h181.261zm185.406 6.437V179.2h-56.001v55.733h-55.733v56.001h55.733v55.733h56.001v-55.733H627.2v-56.001h-55.733z" /></svg>
</a>
</li>
</ul>
</div>
</div>
</div>
</footer>
<script data-cfasync="false" src="/cdn-cgi/scripts/d07b1474/cloudflare-static/email-decode.min.js"></script><script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script defer src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-590acf86551be62a"></script>
<script defer src="//cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script defer src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script defer src="/js/main.js"></script>
<div class="d-none">Build Time: 0.2293 Seconds
</div>
</body>
</html>