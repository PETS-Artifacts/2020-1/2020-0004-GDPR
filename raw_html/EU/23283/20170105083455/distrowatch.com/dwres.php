<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"><html><head>
<title>DistroWatch.com: Put the fun back into computing. Use Linux, BSD.</title>
<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
<meta name="description" content="News and feature lists of Linux and BSD distributions." />
<meta name="keywords" content="distrowatch, linux, android, bsd, unix, distro, distros, distributions, ubuntu, debian, suse, opensuse, fedora, red hat, centos, mageia, knoppix, gentoo, slackware, freebsd, openbsd" />
<meta name="author" content="DistroWatch" />
<!-- <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" />
<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Ubuntu:regular,italic,bold,bolditalic&amp;subset=Latin" />-->
<link rel="alternate" type="application/rss+xml" title="DistroWatch: Latest News" href="news/dw.xml" />
<link rel="alternate" type="application/rss+xml" title="DistroWatch: Latest Distributions" href="news/dwd.xml" />
<link rel="alternate" type="application/rss+xml" title="DistroWatch: Latest Packages" href="news/dwp.xml" />
<link rel="alternate" type="application/rss+xml" title="DistroWatch: DistroWatch Weekly" href="news/dww.xml" />
<link rel="alternate" type="application/rss+xml" title="DistroWatch: Latest DWW Podcast (MP3)" href="news/podcast.xml" />
<link rel="alternate" type="application/rss+xml" title="DistroWatch: Latest DWW Podcast (OGG)" href="news/oggcast.xml" />
<link rel="icon" href="favicon.ico" type="image/ico" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />

<!-- comply with European Union cookie law -->

<!-- 
<script type="text/javascript">window.cookieconsent_options = {"message":"This website uses cookies to customize your experience. Third-party cookies may also be used by our advertisers.","dismiss":"Accept","learnMore":"Learn more","link":"http://distrowatch.com/dwres.php?resource=privacy","theme":"dark-bottom"};</script><script type="text/javascript" src="js/cookieconsent.latest.min.js"></script>--> 

<link rel="stylesheet" href="css/distro-mobile.css" type="text/css"><style type="text/css"></style>
<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-95786-1']);
  _gaq.push(['_trackPageview']);
  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>
</head>
<body bcolor="#ffffff">
<table style="width: 100%; border: 2px silver groove; background-color: #f6edc8;"><tr>
    <td width=50% border: 0px">
        <a href="index-mobile.php"><img src="images/cpxtu/dwbanner.png" width=100% border="0" title="DistroWatch.com. Put the fun back into computing, use Linux, BSD." /></a>
    </td>
</tr>
</table>
<table cellpadding=5 style="width: 100%; border: 2px silver groove; background-color: #f6edc8;">
  <tr>
   <td style="border: 0px" width=50%>
<form name=navigation method=post action="page-navigation.php"><select style="font-size: x-large;" name=page><option value="Home">Navigation</option>
<option value="Home">Home</option>
<option value="FUll">Full Website</option>
<option value="About">About</option>
<option value="Advertise">Advertise</option>
<option value="FAQ">FAQ</option>
<option value="Glossary">Glossary</option>
<option value="Hardware">Hardware</option>
<option value="Headlines">Headlines</option>
<option value="LatestReleases">Latest Releases</option>
<option value="Major">Major Distributions</option>
<option value="Packages">Packages</option>
<option value="PackageManagement">Package Management</option>
<option value="PHR">Page Hit Ranking</option>
<option value="RandomDistro">Random Distribution</option>
<option value="ReviewPodcastNews">Reviews, Podcasts, Newsletters</option>
<option value="SearchArticle">Search - Articles</option>
<option value="SearchDistro">Search - Distro</option>
<option value="SiteMap">Site Map</option>
<option value="Submit">Submit Distribution</option>
<option value="Upcoming">Upcoming Releases</option>
<option value="WaitingList">Waiting List</option>
<option value="Weekly">Weekly</option>
<option value="Comments">Weekly Comments</option>
<option value="WeeklyPast">Weekly - Past Issues</option>
<option value="CryptoCoin">CryptoCoin.cc</option>
</select>&nbsp;&nbsp;<input style="font-size: x-large;" type="submit" value="Go" /></form>   </td>
    <td width=50% style="border: 0px;">
            <form action="table-mobile.php" method="get">
            <select style="font-size: x-large;" name="distribution" onchange="location = t
his.options[this.selectedIndex].value;">
<option selected value="">เลือกดิสทริบิวชัน</option>
<option value="3cx">3CX</option><option value="4mlinux">4MLinux</option><option value="absolute">Absolute</option><option value="abuledu">AbulÉdu</option><option value="alpine">Alpine</option><option value="alt">ALT</option><option value="androidx86">Android-x86</option><option value="antergos">Antergos</option><option value="antix">antiX</option><option value="apodio">APODIO</option><option value="apricity">Apricity</option><option value="arch">Arch</option><option value="archbang">ArchBang</option><option value="arkos">arkOS</option><option value="arya">Arya</option><option value="asianux">Asianux</option><option value="asterisknow">AsteriskNOW</option><option value="austrumi">AUSTRUMI</option><option value="avlinux">AV Linux</option><option value="backbox">BackBox</option><option value="baruwa">Baruwa</option><option value="berry">Berry</option><option value="bicom">Bicom</option><option value="biolinux">Bio-Linux</option><option value="blacklab">Black Lab</option><option value="blackarch">BlackArch</option><option value="blackpanther">blackPanther</option><option value="blankon">BlankOn</option><option value="blueonyx">BlueOnyx</option><option value="bluestar">Bluestar</option><option value="bodhi">Bodhi</option><option value="boss">BOSS</option><option value="bridge">Bridge</option><option value="bsdrp">BSDRP</option><option value="bunsenlabs">BunsenLabs</option><option value="caine">CAINE</option><option value="caixamagica">Caixa Mágica</option><option value="calculate">Calculate</option><option value="canaima">Canaima</option><option value="centos">CentOS</option><option value="chakra">Chakra</option><option value="chaletos">ChaletOS</option><option value="chapeau">Chapeau</option><option value="clear">Clear</option><option value="clearos">ClearOS</option><option value="clonezilla">Clonezilla</option><option value="connochaet">Connochaet</option><option value="container">Container</option><option value="crux">CRUX</option><option value="debian">Debian</option><option value="skolelinux">Debian Edu</option><option value="deepin">deepin</option><option value="devil">Devil</option><option value="devuan">Devuan</option><option value="dragonflybsd">DragonFly</option><option value="duzeru">DuZeru</option><option value="easynas">EasyNAS</option><option value="edubuntu">Edubuntu</option><option value="elastix">Elastix</option><option value="elementary">elementary</option><option value="elive">Elive</option><option value="emmabuntus">Emmabuntüs</option><option value="endian">Endian</option><option value="endlessos">Endless</option><option value="exe">Exe</option><option value="exherbo">Exherbo</option><option value="extix">ExTiX</option><option value="ezey">eZeY</option><option value="fedora">Fedora</option><option value="fermi">Fermi</option><option value="finnix">Finnix</option><option value="freebsd">FreeBSD</option><option value="freenas">FreeNAS</option><option value="frugalware">Frugalware</option><option value="fuguita">FuguIta</option><option value="funtoo">Funtoo</option><option value="gecko">Gecko</option><option value="gentoo">Gentoo</option><option value="ghostbsd">GhostBSD</option><option value="gnewsense">gNewSense</option><option value="gobo">GoboLinux</option><option value="gparted">GParted</option><option value="greenie">Greenie</option><option value="grml">Grml</option><option value="guadalinex">Guadalinex</option><option value="guixsd">GuixSD</option><option value="haiku">Haiku</option><option value="handy">HandyLinux</option><option value="hanthana">Hanthana</option><option value="ipcop">IPCop</option><option value="ipfire">IPFire</option><option value="kaiana">Kaiana</option><option value="kali">Kali</option><option value="kanotix">KANOTIX</option><option value="kaos">KaOS</option><option value="karoshi">Karoshi</option><option value="kdeneon">KDE neon</option><option value="keysoft">Keysoft</option><option value="knoppix">KNOPPIX</option><option value="kodachi">Kodachi</option><option value="kolibri">KolibriOS</option><option value="korora">Korora</option><option value="kubuntu">Kubuntu</option><option value="kwort">Kwort</option><option value="kxstudio">KXStudio</option><option value="lakka">Lakka</option><option value="leeenux">Leeenux</option><option value="legacy">Legacy</option><option value="lfs">LFS</option><option value="linhes">LinHES</option><option value="linpus">Linpus</option><option value="linuxbbq">LinuxBBQ</option><option value="linuxconsole">LinuxConsole</option><option value="linuxfx">Linuxfx</option><option value="lite">Lite</option><option value="lliurex">LliureX</option><option value="lubuntu">Lubuntu</option><option value="lunar">Lunar</option><option value="luninux">LuninuX</option><option value="lxle">LXLE</option><option value="mageia">Mageia</option><option value="makulu">MakuluLinux</option><option value="mangaka">Mangaka</option><option value="manjaro">Manjaro</option><option value="maui">Maui</option><option value="max">MAX</option><option value="metamorphose">Metamorphose</option><option value="midnightbsd">MidnightBSD</option><option value="minino">MiniNo</option><option value="minix">MINIX</option><option value="mint">Mint</option><option value="miros">MirOS</option><option value="mx">MX Linux</option><option value="nanolinux">Nanolinux</option><option value="nas4free">NAS4Free</option><option value="neptune">Neptune</option><option value="netbsd">NetBSD</option><option value="nethserver">NethServer</option><option value="netrunner">Netrunner</option><option value="nexentastor">NexentaStor</option><option value="nixos">NixOS</option><option value="nst">NST</option><option value="nutyx">NuTyX</option><option value="ob2d">OB2D</option><option value="olpc">OLPC</option><option value="omoikane">Omoikane</option><option value="openbsd">OpenBSD</option><option value="openelec">OpenELEC</option><option value="openindiana">OpenIndiana</option><option value="openlx">OpenLX</option><option value="openmandriva">OpenMandriva</option><option value="openmediavault">OpenMediaVault</option><option value="opensuse">openSUSE</option><option value="openwall">Openwall</option><option value="opnsense">OPNsense</option><option value="oracle">Oracle</option><option value="osmc">OSMC</option><option value="overclockix">Overclockix</option><option value="paldo">paldo</option><option value="parabola">Parabola</option><option value="pardus">Pardus</option><option value="parrotsecurity">Parrot</option><option value="parsix">Parsix</option><option value="partedmagic">Parted Magic</option><option value="pclinuxos">PCLinuxOS</option><option value="peachosi">Peach OSI</option><option value="pearl">Pearl</option><option value="pelicanhpc">PelicanHPC</option><option value="pentoo">Pentoo</option><option value="peppermint">Peppermint</option><option value="pfsense">pfSense</option><option value="pinguy">Pinguy</option><option value="pisi">Pisi</option><option value="plamo">Plamo</option><option value="pld">PLD</option><option value="plop">Plop</option><option value="point">Point</option><option value="poliarch">PoliArch</option><option value="porteus">Porteus</option><option value="porteuskiosk">Porteus Kiosk</option><option value="primtux">PrimTux</option><option value="proxmox">Proxmox</option><option value="puppy">Puppy</option><option value="q4os">Q4OS</option><option value="qubes">Qubes</option><option value="quirky">Quirky</option><option value="rancheros">RancherOS</option><option value="raspbian">Raspbian</option><option value="raspbsd">RaspBSD</option><option value="reactos">ReactOS</option><option value="rebeccablackos">RebeccaBlackOS</option><option value="rebellin">Rebellin</option><option value="redhat">Red Hat</option><option value="refracta">Refracta</option><option value="remixos">RemixOS</option><option value="remnux">REMnux</option><option value="rescatux">Rescatux</option><option value="resulinux">Resulinux</option><option value="risc">RISC</option><option value="robolinux">Robolinux</option><option value="rockscluster">Rocks Cluster</option><option value="rockstor">Rockstor</option><option value="rosa">ROSA</option><option value="runtu">Runtu</option><option value="sabayon">Sabayon</option><option value="salentos">SalentOS</option><option value="salix">Salix</option><option value="scientific">Scientific</option><option value="securepoint">Securepoint</option><option value="selks">SELKS</option><option value="semplice">Semplice</option><option value="siduction">siduction</option><option value="simplicity">Simplicity</option><option value="slackel">Slackel</option><option value="slackware">Slackware</option><option value="slitaz">SliTaz</option><option value="smartos">SmartOS</option><option value="smeserver">SME Server</option><option value="smoothwall">Smoothwall</option><option value="sms">SMS</option><option value="solaris">Solaris</option><option value="solus">Solus</option><option value="solydxk">SolydXK</option><option value="sonar">Sonar</option><option value="sophos">Sophos</option><option value="sourcemage">Source Mage</option><option value="sparky">SparkyLinux</option><option value="springdale">Springdale</option><option value="steamos">SteamOS</option><option value="stella">Stella</option><option value="stresslinux">StressLinux</option><option value="sulix">SuliX</option><option value="superx">SuperX</option><option value="sle">SUSE</option><option value="symphony">SymphonyOS</option><option value="systemrescue">SystemRescue</option><option value="t2">T2</option><option value="tails">Tails</option><option value="tanglu">Tanglu</option><option value="tens">TENS</option><option value="thinstation">Thinstation</option><option value="tinycore">Tiny Core</option><option value="torios">ToriOS</option><option value="toutou">Toutou</option><option value="trisquel">Trisquel</option><option value="trueos">TrueOS</option><option value="turnkey">TurnKey</option><option value="uberstudent">UberStudent</option><option value="ubuntu">Ubuntu</option><option value="ubuntubudgie">Ubuntu Budgie</option><option value="ubuntudp">Ubuntu DP</option><option value="ubuntugnome">Ubuntu GNOME</option><option value="ubuntukylin">Ubuntu Kylin</option><option value="ubuntumate">Ubuntu MATE</option><option value="ubuntustudio">Ubuntu Studio</option><option value="uhu">UHU-Linux</option><option value="ulteo">Ulteo</option><option value="ultimate">Ultimate</option><option value="univention">Univention</option><option value="untangle">Untangle</option><option value="urix">URIX</option><option value="uruk">Uruk</option><option value="vector">Vector</option><option value="vine">Vine</option><option value="vinux">Vinux</option><option value="void">Void</option><option value="volumio">Volumio</option><option value="vortexbox">VortexBox</option><option value="voyager">Voyager</option><option value="vyos">VyOS</option><option value="wattos">wattOS</option><option value="webconverger">Webconverger</option><option value="whonix">Whonix</option><option value="wifislax">Wifislax</option><option value="wmlive">WM Live</option><option value="xstreamos">XStreamOS</option><option value="xubuntu">Xubuntu</option><option value="zentyal">Zentyal</option><option value="zenwalk">Zenwalk</option><option value="zeroshell">Zeroshell</option><option value="zevenos">ZevenOS</option><option value="zorin">Zorin</option>            </select>
&nbsp;            <input style="font-size: x-large;" type="submit" value="ไป" />
            </form>
    </td>
  </tr></table>
<table style="width: 100%; border: 1px silver groove; background-color: #f6edc8;"><tr><td style="border: 0px;"><table style="width: 100%; border: none; background-color: #f6efc8">
  <tr>
    <td class="NewsLogo" style="border: none; text-align: center; background-color: #f6efc8; width: 100%">
<span style="vertical-align: bottom; color: silver; text-align:center;">DistroWatch.com is sponsored by </span><a href="https://www.3cx.com/"><img style="vertical-align: middle" src="images/k/3cx-19.png" border="0" alt="3CX - Software Based VoIP IP PBX / PABX" title="3CX - Software Based VoIP IP PBX / PABX"></a>&nbsp;<a href="https://www.acunetix.com/"><img style="vertical-align: bottom" src="images/k/acunetix-19.png" border="0" alt="Web application security with Acunetix" title="Web application security with Acunetix"></a>    </td>
  </tr>
</table>
</td></tr></table>
<h2><div>
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- large_leaderboard -->
<ins class="adsbygoogle"
     style="display:inline-block;width:970px;height:90px"
     data-ad-client="ca-pub-1979814992302941"
     data-ad-slot="4787466537"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
</div></h2><table style="width: 100%; border: 2px groove silver; background-color: #f6edc8">
  <td class="Introduction1" style="text-align: center; border: 0px; width: 100%"><h1>DistroWatch Privacy Policy</h1></td>
</table><br>
<table class="NavLogo" width="100%">
    <td style="width: 78%; vertical-align: top; border: none">
      <table class="News">
        <tr>
          <th class="Invert">DistroWatch Privacy Policy</th>
        </tr>
        <tr>
          <td class="News1">
            <table>
              <tr class="News">
                <td class="NewsDate">&nbsp;</td>
                <td class="NewsHeadline">DistroWatch Privacy Policy</td>
              </tr>
              <tr>
                <td class="NewsLogo"><img src="images/icon-large/other.png" border="0">
                <td class="NewsText"><br>

                <b>DistroWatch Privacy Statement</b><br><br>

                Personal information, such as your name, address or email address that you entrust to DistroWatch will not be shared with any third-party without your explicit permission to do so. We do not sell or rent our mailing lists to third parties.<br><br>
                We also use cookies on our website in order to save your language preferences and to highlight new content on our front page that has been posted since your last visit.<br><br>

                <b>Third-Party Advertising</b><br><br>

                We use various third-party advertising companies to serve advertisements when you visit our website. These companies may use information (not including your name, address, email address or telephone number) about your visits to this and other websites in order to provide advertisements on this site and other sites about goods and services that may be of interest to you. If you would like more information about this practice and to know your choices about not having this information used by these companies, please click <a href="http://www.networkadvertising.org/optout_nonppii.asp">here</a>.<br><br>

                <b>Third-Party Cookies</b><br><br>

                In the course of serving advertisements to this site, our third-party advertiser may place or recognize a unique "cookie" on your browser.<br><br>

                <b>Cookie notification bar</b><br><br>

Readers visiting this website from the European Union will be shown a banner at the bottom of the page, informing them that this website makes use of cookies. (See above for examples of local and third-party cookie use.) We display this banner in order to comply with European Union law. Please see <a href="http://ec.europa.eu/justice/data-protection/article-29/documentation/opinion-recommendation/files/2013/wp208_en.pdf">this document</a> (in PDF format) for further information on European Union regulations.
<br><br>
Readers who consent to cookies being delivered to their computer can continue to browse DistroWatch without taking any further action. If, however, you wish to opt-out of using cookies, most modern web browsers will allow you to disable cookies used by this website. The following paragraphs explain how to turn off web browser cookies when visiting DistroWatch.
<br><br>
<b>Chrome/Chromium</b><br><br>
Click the <b>Menu</b> button and select <b>Settings</b>. In the window that appears, click the <b>Advanced settings</b> link at the bottom of the page. Scroll down to the <b>Privacy</b> section and click the button labelled <b>Content settings...</b>. Under the <b>Cookies</b> header, click the <b>Manage exceptions</b> button. In the box marked <b>Hostname pattern</b> type <b>distrowatch.com</b> and change the <b>Behaviour</b> drop-down box to <b>Block</b>, then click <b>Finished</b>.
<br><br>

<b>Firefox/Iceweasel</b><br><br>
Click on the <b>Menu</b> button, click on <b>Preferences</b> and then select <b>Privacy</b> when the settings window appears. Find the "Accept cookies from sites" option and click the <b>Exceptions...</b> button that appears next to this text. In the "Address of website" box, type <b>distrowatch.com</b> and then click the <b>Block</b> button. Then click <b>Save changes</b>.
<br><br>

<b>Opera</b><br><br>
Click the <b>Opera</b> button, select <b>Settings</b> from the Opera menu. In the Settings window that appears, click <b>Privacy &amp; security</b> from the side menu. At the bottom of the page, under the <b>Cookies</b> section, click the <b>Manage exceptions...</b> button. In the <b>Hostname pattern</b> box, type <b>distrowatch.com</b> and change the <b>Behaviour</b> drop-down box to <b>Block</b>. Finally, click the <b>Done</b> button.
<br><br>
<b>Internet Explorer</b><br><br>
Click the <b>Tools</b> menu and then select <b>Internet Options</b>. In the window that appears, click the <b>Privacy</b> tab. Click the <b>Sites</b> button. In the <b>Address of website</b> box, type <b>distrowatch.com</b> and then click <b>Block</b>. Finally, click the <b>OK</b> button.
<br><br>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
    <td style="width: 22%; vertical-align: top; border: none">
      <table class="News" width="100%">
        <tr>
          <th class="Invert">การลงโฆษณา</th>
        </tr>
        <tr>
          <td class="News" style="text-align: center; direction: ltr">
          <div>
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- wide_skyscraper -->
<ins class="adsbygoogle"
     style="display:inline-block;width:160px;height:600px"
     data-ad-client="ca-pub-1979814992302941"
     data-ad-slot="5306729335"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
</div>
          </td>
        </tr>
      </table>
      <br>
    </td>
</table><br>
<hr><table width="100%" border="0">  <tr><td style="width: 100%; text-align: center; border: 0px; font-size: small;">Copyright (C) 2001 - 2017 Unsigned Integer Limited. All rights reserved. All trademarks are the property of their respective owners. <a href="dwres.php?resource=privacy">Privacy policy</a>.<br />DistroWatch.com is hosted at <a href="http://distrowatch.com/">Copenhagen</a>. <!-- and mirrored at <a href="http://distrowatch.gdsw.at/">Wien</a>.--><br /><br />ติดต่อ แจ้งแก้ไข และข้อเสนอแนะ: <a href="mailto:&#100;&#105;&#115;&#116;&#114;&#111;&#64;&#100;&#105;&#115;&#116;&#114;&#111;&#119;&#97;&#116;&#99;&#104;&#46;&#99;&#111;&#109;">Jesse Smith</a>    </td>
  </tr>
</table>
<br />
</body>
</html>
