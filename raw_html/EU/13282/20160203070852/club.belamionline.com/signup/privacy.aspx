

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head><meta charset="utf-8" /><meta name="viewport" content="width=device-width" /><title>
	Join BelAmi!
</title><link rel="icon" type="image/x-icon" href="https://www.belamionline.com/favicon.ico" /><link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700,400italic,700italic" rel="stylesheet" type="text/css" /><link href="Styles/Site.css?RST=1" rel="stylesheet" type="text/css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  	<script src="Scripts/slick.min.js" type="text/javascript"></script>
	<script src="Scripts/jquery.easing.1.3.js" type="text/javascript"></script>
    <script src="Scripts/jquery.watermarkinput.js"></script>
    <script src="jquery-checkradios/js/jquery.checkradios.js"></script>
    <script src="Scripts/jquery.validate.min.js"></script>
    <script src="Scripts/modernizr-custom.js"></script>
    <link rel="stylesheet" href="jquery-checkradios/css/jquery.checkradios.css" /><link rel="stylesheet" type="text/css" href="Styles/slick.css" />
    <script src="Scripts/join.js?RSTa=16wwsa5"></script>
</head>
<body>
    <form method="post" action="./default.aspx?aspxerrorpath=%2fsignup%2fprivacy.aspx" id="mainform">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwUJODY0OTUyMzk4D2QWAmYPZBYCAgMPZBYCZg9kFgYCBQ8PFgIeB1Zpc2libGVoZGQCBg9kFgICAQ9kFgICAQ9kFgQCAQ8PFgIfAGhkZAIDDw8WAh8AZ2RkAgsPFgIeC18hSXRlbUNvdW50AgUWCgIBD2QWAmYPFQGQAjxhIGhyZWY9Imh0dHBzOi8vbmV3dG91ci5iZWxhbWlvbmxpbmUuY29tL2NvbnRlbnRfdmlkZW8uYXNweD9WaWRlb0lEPTEyNzk2IiB0YXJnZXQ9Il9ibGFuayIgY2xhc3M9IkJhbm5lckxpbmsiPjxpbWcgY2xhc3M9IlNsaWRlckltYWdlIiBzcmM9Imh0dHBzOi8vZnJlZWNkbi5iZWxhbWlvbmxpbmUuY29tL0RhdGEvQmFubmVycy9lNDE2Mzg1YjE4MTQ2M2YxNmI2NTA4OTZhMTBkMmNhMy5qcGciIGFsdD0iQkVUVEVSIFRIQU4gRVZFUi4uLiB3aXRoIEpJTSAmIERBQU4iLz48L2E+ZAICD2QWAmYPFQGJAjxhIGhyZWY9Imh0dHBzOi8vbmV3dG91ci5iZWxhbWlvbmxpbmUuY29tL2NvbnRlbnRfdmlkZW8uYXNweD9WaWRlb0lEPTEyNzkxIiB0YXJnZXQ9Il9ibGFuayIgY2xhc3M9IkJhbm5lckxpbmsiPjxpbWcgY2xhc3M9IlNsaWRlckltYWdlIiBzcmM9Imh0dHBzOi8vZnJlZWNkbi5iZWxhbWlvbmxpbmUuY29tL0RhdGEvQmFubmVycy8xZGIzNjMwZjc2OGQ1ODZmODMxNzcyMGI4OGUzYmM3Zi5qcGciIGFsdD0iWU9VICYgTUUgQkFCWS4uLiBBcmllbCAmIFRvbSIvPjwvYT5kAgMPZBYCZg8VAaICPGEgaHJlZj0iaHR0cHM6Ly9uZXd0b3VyLmJlbGFtaW9ubGluZS5jb20vY29udGVudF92aWRlby5hc3B4P1ZpZGVvSUQ9MTI3OTQiIHRhcmdldD0iX2JsYW5rIiBjbGFzcz0iQmFubmVyTGluayI+PGltZyBjbGFzcz0iU2xpZGVySW1hZ2UiIHNyYz0iaHR0cHM6Ly9mcmVlY2RuLmJlbGFtaW9ubGluZS5jb20vRGF0YS9CYW5uZXJzLzhmODQwZjc4ZjgwM2MyMjVjMjU5YzVmNTNiNjUyYzE1LmpwZyIgYWx0PSIyIElTIEdPT0QgQlVUIDMgSVMgQkVUVEVSLi4uIENMQVVERSBXSVRIIFNDT1RUICYgTUFSQyIvPjwvYT5kAgQPZBYCZg8VAYwCPGEgaHJlZj0iaHR0cHM6Ly9uZXd0b3VyLmJlbGFtaW9ubGluZS5jb20vY29udGVudF92aWRlby5hc3B4P1ZpZGVvSUQ9MTI3NzkiIHRhcmdldD0iX2JsYW5rIiBjbGFzcz0iQmFubmVyTGluayI+PGltZyBjbGFzcz0iU2xpZGVySW1hZ2UiIHNyYz0iaHR0cHM6Ly9mcmVlY2RuLmJlbGFtaW9ubGluZS5jb20vRGF0YS9CYW5uZXJzLzExZTUxNjA0NTU5NDNiOTQxOTlkNmMxNjA3MWVjZmM5LmpwZyIgYWx0PSJCQUNLIEZPUiBNT1JFIHdpdGggR1JFRyBhbmQgSk9OIi8+PC9hPmQCBQ9kFgJmDxUBmwI8YSBocmVmPSJodHRwczovL25ld3RvdXIuYmVsYW1pb25saW5lLmNvbS9jb250ZW50X3ZpZGVvLmFzcHg/VmlkZW9JRD0xMjc5NSIgdGFyZ2V0PSJfYmxhbmsiIGNsYXNzPSJCYW5uZXJMaW5rIj48aW1nIGNsYXNzPSJTbGlkZXJJbWFnZSIgc3JjPSJodHRwczovL2ZyZWVjZG4uYmVsYW1pb25saW5lLmNvbS9EYXRhL0Jhbm5lcnMvNmYzZmRhMTI2ODMyNTBmNWM1NTVjNjBhZTZjMjllNjAuanBnIiBhbHQ9IlNJWkUgTUFUVEVSUyB3aXRoIEpvZWwgQmlya2luICYgS2llcmFuIEJlbm5pbmciLz48L2E+ZGTbfH57EBjRGWDh7W7V3ISa+Ic1G6KAryBJnDnGDluYqA==" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="ACC6C507" />
<input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="/wEdAAKKUM6NDO50S7+wIY+HqCS3XEp+0NTKKE1Av4BO5d+qHjGLCBCj6hjegWIr9iji60ndy37iyu5lyV1DiqcqKbV8" />

  <div id="TermsWrapper">
  <div id="TermsPopUp">
  <div id="TermsPopUpContent">
    <img src="https://freecdn.belamionline.com/Signup/Images/warninglogo.png" class="TermsLogo"/>
    <p class="DisclaimerMessage">Your agreement to all the terms and conditions is required for you to become a member of, or otherwise access or use the restricted members areas of belamionline.com. If you do not agree you will not be accepted as a member and you will not be authorized to use any of the content or other materials in the members portion of the web site.</p>
    						<div class="TermsConfirm">
							<ul><li><a href="#" id="IAgree">I AGREE</a></li></ul>
						</div>
                        <div class="clr"></div>
    <p class="Links">
    <a href="#" id="TermsBad">I do not agree and want to leave</a>
    <br /><br />
    <a href="terms.aspx" class="bigger" target="_blank">VIEW TERMS AND CONDITIONS</a>    
    </p>
    
  </div>
  </div>
</div>

    <div class="general">
    
        <div class="top_wrap">
            <div class="top">
                <a href="http://tour.belamionline.com" class="logo"><img src="https://freecdn.belamionline.com/Signup/Images/logo.png"/></a>
                <a href="http://tour.belamionline.com" class="logoMobile"><img src="https://freecdn.belamionline.com/Signup/Images/belami-logo.png"/></a>
                <a href="http://tour.belamionline.com" class="benefits"><img src="https://freecdn.belamionline.com/Signup/Images/awards-join-2019.png"/></a>
            </div>
        </div>
        
        <div class="content_wrap">
            

   <div class="content">

   

                     
        <div class="column_model">
        <div id="modelSlideshow">
                        
                        <div class="slide"><img src="https://freecdn.belamionline.com/Signup/Images/join-models1.jpg" alt="BelAmi Online Exclusive Models" class="SliderImage"/></div>  
                        <div class="slide"><img src="https://freecdn.belamionline.com/Signup/Images/join-models2.jpg" alt="BelAmi Online Exclusive Models" class="SliderImage"/></div>  
                        <div class="slide"><img src="https://freecdn.belamionline.com/Signup/Images/join-models3.jpg" alt="BelAmi Online Exclusive Models" class="SliderImage"/></div>  
                        <div class="slide"><img src="https://freecdn.belamionline.com/Signup/Images/join-models4.jpg" alt="BelAmi Online Exclusive Models" class="SliderImage"/></div>  
                        <div class="slide"><img src="https://freecdn.belamionline.com/Signup/Images/join-models5.jpg" alt="BelAmi Online Exclusive Models" class="SliderImage"/></div>  
                        <div class="slide"><img src="https://freecdn.belamionline.com/Signup/Images/join-models6.jpg" alt="BelAmi Online Exclusive Models" class="SliderImage"/></div>  
                        <div class="slide"><img src="https://freecdn.belamionline.com/Signup/Images/join-models7.jpg" alt="BelAmi Online Exclusive Models" class="SliderImage"/></div>  
                        <div class="slide"><img src="https://freecdn.belamionline.com/Signup/Images/join-models8.jpg" alt="BelAmi Online Exclusive Models" class="SliderImage"/></div>  
                        <div class="slide"><img src="https://freecdn.belamionline.com/Signup/Images/join-models9.jpg" alt="BelAmi Online Exclusive Models" class="SliderImage"/></div>  
                            
                  </div></div>

       

   <div class="column_payment">  

               
       
    
       <div class="column_resizer">           

    <div class="headerLine">
        <div class="numberCircle">1</div>
        <div class="description">SELECT YOUR PLAN</div>
    </div>
      <div class="clr" style="height:10px;"></div>
           <div id="DirectDebitBlock" style="display:none;">
                   <div id="PanelBankTransferNormal">
	
                   <label for="1MonthDD"><div ID="PPDD1" class="SelectorWide"><div class="SelectorButtonPrice"><input type="radio" class="checkradios" ID="1MonthDD" checked value="1MNR" name="PlanLengthDD"> <div class="PriceLabel"><div>1 MONTH</div><div class="PriceDetail"></div></div></div><div class="SelectorPrice">$49.95</div></div></label>
                   <label for="3MonthDD"><div ID="PPDD2" class="SelectorWide"><div class="SelectorButtonPrice"><input type="radio" class="checkradios" ID="3MonthDD" value="12MNR" name="PlanLengthDD"> <div class="PriceLabel"><div>12 MONTHS</div><div class="PriceDetail">Most popular plan</div></div></div><div class="SelectorPrice">$279.95</div></div></label>
                     
</div>
                   
                      <div class="clr"></div>
                      <div class="RebillLabel">No recurring (no following transactions)</div>    
                      <div class="clr" style="height:30px;"></div>
                      <div class="DirectDebitInfo">
                In selected countries we offer payment via pre-paid cards using Paysafecard, Postepay and Mister Cash or real-time bank transfer method using Sofort Banking (Sofort Überweisung), iDeal, Giropay, SafetyPay and other services. Available options will be displayed for Austria, Belgium, Bulgaria, Croatia, Cyprus, Denmark, Estonia, Finland, France, Germany, Greece, Hungary, Iceland, Ireland, Italy, Latvia, Liechtenstein, Lithuania, Luxembourg, Malta, Monaco, Netherlands, Norway, Poland, Portugal, Romania, San Marino, Slovakia, Slovenia, Spain, Sweden, Switzerland, United Kingdom and also for other countries.</div>
               </div>
      <div id="CreditCardBlock">
      
     

          
         

     <div id="PanelNonTrials">
	          
     <div id="MainContent_NonDiscountedPricePoints">
		          
         <div id="MainContent_Panel1MonthNonDiscount">
			
             
             <div id="MainContent_Panel1MTest29">
				
                              <label for="1Month"><div ID="PP1" class="SelectorWide"><div class="SelectorButtonPrice"><div style="float:left;"><input type="radio" class="checkradios" ID="1Month" checked value="1M29T" name="PlanLength"></div><div class="PriceLabel"><div>1 MONTH</div><div class="PriceDetail">Most popular plan</div></div></div><div class="SelectorPrice">$29<sup>.95</sup></div></div></label>
             
			</div>
         
		</div>
         
         
         
         <div id="MainContent_PanelNonDiscounted3and6">
			
    <label for="3Month"><div ID="PP2" class="SelectorWide"><div class="SelectorButtonPrice"><div style="float:left;"><input type="radio" class="checkradios" ID="3Month" value="3M" name="PlanLength"></div><div class="PriceLabel"><div>3 MONTHS</div><div class="PriceDetail"><b>Save $10</b> the first 3 months</div></div></div><div class="SelectorPrice">$79<sup>.95</sup></div></div></label>
    <label for="6Month"><div ID="PP3" class="SelectorWide"><div class="SelectorButtonPrice"><div style="float:left;"><input type="radio" class="checkradios" ID="6Month" value="6M" name="PlanLength"></div><div class="PriceLabel"><div>6 MONTHS</div><div class="PriceDetail">Best value offer - <b>save 40%!</b></div></div></div><div class="SelectorPrice">$149<sup>.95</sup></div></div></label>
    
		</div>
         
	</div>
         
         
         
              

          

         
             
          
     
       <div class="clr"></div>
     
       <div class="RebillLabel">
       <div id="BasicRebill"><b>THEN $29<sup>.95</sup></b> BILLED <b>EVERY 1 MONTH</b>, CANCEL ANYTIME</div>
       <div id="Rebill2015" class="HiddenOptions"><b>THEN ONLY $20<sup>.15</sup></b> BILLED <b>EVERY 1 MONTH</b>, CANCEL ANYTIME</div>
       <div id="ValentineRebill" class="HiddenOptions">BELAMIONLINE REBILLS AT $29<sup>.95</sup> AND KINKY ANGELS AT $14<sup>.95</sup><br />
           UNTIL CANCELED / CANCEL ANYTIME</div>
        <div class="clr" style="height:10px;"></div>
        
         <div id="MainContent_NonDiscountedPricePointsNRN">
		 
        <label for="1MonthNR"><div ID="PP5" class="SelectorWide"><div class="SelectorButtonPrice"><div style="float:left;"><input type="radio" class="checkradios" ID="1MonthNR" value="1MNR" name="PlanLength"></div><div class="PriceLabel"><div>1 MONTH</div><div class="PriceDetail">Non-recurring option</div></div></div><div class="SelectorPrice">$49<sup>.95</sup></div></div></label>
        <div id="HiddenOptionMonth" class="HiddenOptions">
        <label for="12Month"><div ID="PP4" class="SelectorWide"><div class="SelectorButtonPrice"><div style="float:left;"><input type="radio" class="checkradios" ID="12Month" value="12MNR" name="PlanLength"></div><div class="PriceLabel"><div>12 MONTHS</div><div class="PriceDetail"><b>Save 40% monthly</b> non-recurring option</div></div></div><div class="SelectorPrice">$279<sup>.95</sup></div></div></label>
        </div>
        
	</div>

         



              
            <div id="MainContent_PanelOtherOptions">
		   
             <div id="OtherOptions" class="OtherOptions">
               Other membership options <a href="#" onclick="$('#HiddenOptionMonth').show(500);$('#OtherOptions').hide(500);">CLICK HERE</a>
               </div> 
                
	</div>              
               <div class="clr" style="height:5px;"></div>              
    			
</div>
          
          <div id="PanelKinky">
	
                <div class="clr" style="height:5px;"></div>
                <div class="KinkyWrapper">
                    <div class="Header">
                        Add this site for the special price 
                    </div>
                </div>
                <div class="clr"></div>
                    <label for="RadioKA"><div ID="KA1" class="SelectorWide KinkyBackground"><div class="SelectorButtonPrice"><input type="checkbox" class="checkradios" ID="RadioKA" value="KA1" name="ADDKa"> <div class="PriceLabel"><div>Freshmen.net Unlimited Access</div><div class="PriceDetail">1 month access, rebills at $19.95/month</div></div></div><div class="SelectorPrice">$19<sup>.95</sup></div></div></label>
                <div class="clr" style="height:3px;"></div>
                <div class="KinkyWrapper">
                    <div class="Link"><a href="http://www.freshmen.net/" target="_blank">About Freshmen.net</a></div>
                </div>                
               
</div>

                        <div class="clr" style="height:5px;"></div> 
           <label for="RadioChat"><div ID="CHAT1" class="SelectorWideA KinkyBackground"><div class="SelectorButtonPrice"><input type="checkbox" class="checkradios" ID="RadioChat" value="CHAT1" name="ADDChat" checked/> <div class="PriceLabel"><div>Sign me up also for BelAmiChat.com account</div></div></div><div class="SelectorPrice">FREE</div></div></label>

          </div>
          
           

               </div>
               
         <div class="clr" style="height:15px;"></div>   
       
        <div class="column_resizer">
         <div class="headerLine">
            <div class="numberCircle">2</div>
            <div class="description">SELECT PAYMENT METHOD</div>
        </div>
      <div class="clr" style="height:10px;"></div>
    <label for="CreditCard"><div ID="PT1" class="SelectorLine"><div class="SelectorButton"><input type="radio" class="checkradios" ID="CreditCard" checked value="CreditCard" name="PaymentMethod"> Credit Card</div><div class="SelectorPaymentType"><img src="Images/credit-cards.png" /></div></div></label>
    <label for="PayPal"><div ID="PT2" class="SelectorLine"><div class="SelectorButton"><input type="radio" class="checkradios" ID="PayPal" value="PayPal" name="PaymentMethod"> PayPal</div><div class="SelectorPaymentType"><img src="Images/epoch-paypal.png" /></div></div></label>
    <div id="PanelDirectDebitSelector">
	       
    <label for="DirectDebit"><div ID="PT3" class="SelectorLine"><div class="SelectorButton"><input type="radio" class="checkradios" ID="DirectDebit" value="DirectDebit" name="PaymentMethod"> Other Methods</div><div class="SelectorPaymentType"><img src="Images/direct1.png" /></div></div></label>
    
</div>

   </div>

    <div class="clr" style="height:25px;"></div>
             </div>
     
           <div class="headerLine">
        <div class="numberCircle">3</div>
        <div class="description">CREATE YOUR ACCOUNT</div>                
    </div>
    <div class="clr"></div>
    <div class="NewRenewGroup">
        <input type="radio" class="checkradios" id="NewSelector" checked value="New" name="NewRenew"> <label for="NewSelector">I AM NEW MEMBER</label>&nbsp;&nbsp;&nbsp;
        <input type="radio" class="checkradios" id="RenewSelector"   value="Renew" name="NewRenew"> <label for="RenewSelector">I ALREADY HAVE ACCOUNT</label>
    </div>    
       <div class="clr" style="height:10px;"></div>   
    
    
      <input type="text" ID="Username" name="Username" Class="InputField" tabindex="1"  value="" required />
    <input type="text" ID="Password" name="Password" Class="InputField" tabindex="2"  value="" required  title="Minimum six characters, both numbers and letters are required"/>
    <div id="EmailBlock">
    <input type="text" ID="Email" name="Email" Class="InputField" tabindex="3"  value="" />
    </div>  
            
       
       <div class="column_resizer">
    <div class="clr" style="height:10px;"></div>

                             <div id="Agree">
                    <div class="CheckboxField">
                        <img src="Images/agree.png" id="AgreeCheckbox" onclick="$('#TermsWrapper').addClass('TermsUnknown');"/></div>
                    <div class="Message">I have read and agree to the <a href="terms.aspx" target="_blank">Terms & Conditions</a></div>
                </div>
                
                         <div class="confirm">
                         <input type="submit" name="ctl00$MainContent$JoinButton" value="GET ACCESS NOW!" id="MainContent_JoinButton" />
                        </div>         
           
     
             </div>   </div>   
                       
   
   <div class="clr" style="height:30px;"></div>      
   
   <div id="Secure">
       <img src="https://freecdn.belamionline.com/Signup/Images/SECURE.jpg" />
   </div>

    

      
      <div class="slideshow">
						<div id="mainSlideshow">
                         
      
							<div class="slide">
								<a href="https://newtour.belamionline.com/content_video.aspx?VideoID=12796" target="_blank" class="BannerLink"><img class="SliderImage" src="https://freecdn.belamionline.com/Data/Banners/e416385b181463f16b650896a10d2ca3.jpg" alt="BETTER THAN EVER... with JIM & DAAN"/></a>
							</div>        
      
							<div class="slide">
								<a href="https://newtour.belamionline.com/content_video.aspx?VideoID=12791" target="_blank" class="BannerLink"><img class="SliderImage" src="https://freecdn.belamionline.com/Data/Banners/1db3630f768d586f8317720b88e3bc7f.jpg" alt="YOU & ME BABY... Ariel & Tom"/></a>
							</div>        
      
							<div class="slide">
								<a href="https://newtour.belamionline.com/content_video.aspx?VideoID=12794" target="_blank" class="BannerLink"><img class="SliderImage" src="https://freecdn.belamionline.com/Data/Banners/8f840f78f803c225c259c5f53b652c15.jpg" alt="2 IS GOOD BUT 3 IS BETTER... CLAUDE WITH SCOTT & MARC"/></a>
							</div>        
      
							<div class="slide">
								<a href="https://newtour.belamionline.com/content_video.aspx?VideoID=12779" target="_blank" class="BannerLink"><img class="SliderImage" src="https://freecdn.belamionline.com/Data/Banners/11e5160455943b94199d6c16071ecfc9.jpg" alt="BACK FOR MORE with GREG and JON"/></a>
							</div>        
      
							<div class="slide">
								<a href="https://newtour.belamionline.com/content_video.aspx?VideoID=12795" target="_blank" class="BannerLink"><img class="SliderImage" src="https://freecdn.belamionline.com/Data/Banners/6f3fda12683250f5c555c60ae6c29e60.jpg" alt="SIZE MATTERS with Joel Birkin & Kieran Benning"/></a>
							</div>        
      
						</div>
					</div>
					<script type="text/javascript">
					    $(document).ready(function () {
					        $('#mainSlideshow').slick({
					            autoplay: true,
					            dots: true,
					            arrows: false,
					            //fade: true,
					            autoplaySpeed: 3000
					        });
					    });
					</script>
                    		<script type="text/javascript">
                    		    $(document).ready(function () {
                    		        $('#modelSlideshow').slick({
                    		            autoplay: true,
                    		            dots: false,
                    		            arrows: false,
                    		            fade: true,
                    		            speed: 1000,
                    		            autoplaySpeed: 5000
                    		        });
                    		    });
					</script>
       

       <div class="clr" style="height:15px;"></div>
 
 <div class="content">
      <div id="BenefitsWrapper">
        <div class="column borderright">
            <ul>
                <li><span class="checkmark">✔</span> CONDOM FREE SCENES</li>
				<li><span class="checkmark">✔</span> 7 NEW UPDATES A WEEK</li>
			</ul>
       </div>
        <div class="column borderright">
            <ul>
                <li><span class="checkmark">✔</span> UNLIMITED DOWNLOADS</li>
				<li><span class="checkmark">✔</span> BACKSTAGE <span class="checkmark">&</span> INTERVIEWS</li>
			</ul>
       </div>
       <div class="column">
            <ul>
                <li><span class="checkmark">✔</span> 1.000+ HD VIDEOS <span class="checkmark">&</span> PHOTOS</li>
				<li><span class="checkmark">✔</span> 100’S OF EXCLUSIVE MODELS</li>
			</ul>
            
       <div class="muchmore">
       ...AND MUCH MORE
       </div>
       </div>
      </div>
      </div>

       <div id="AwardsBottom">
                <a href="http://tour.belamionline.com" class="benefits"><img src="Images/25-logo-tour.jpg"/></a>
            </div>

 </div>
           
        </div>
        <div class="bottom_separator"></div>
        <div class="bottom_main">
         <div class="bottom_wrap">        
          <div class="bottom_content BottomMenu">      
                <div class="bottom_column1">
                 <a href="http://club.belamionline.com/entrance/" style="color:#bf151e;">LOG IN!</a><br />
                 <a href="http://helpdesk.belamionline.com/" target="_blank">SUPPORT</a><br />
                 <a href="http://club.belamionline.com/clublogin/recurcancel.asp">CANCEL YOUR ACCOUNT</a><br />
                 <a href="https://www.belamionline.com/signup/terms.aspx" target="_blank">TERMS AND CONDITIONS</a>
                </div>
                <div class="bottom_column2">
                 <a href="http://www.freshmen.net">FRESHMEN.NET</a><br />
                 <a href="http://www.belamicash.com/" target="_blank">WEBMASTERS</a><br />         
                 <a href="http://www.belamimodelsearch.com/" target="_blank">BECOME A MODEL</a><br />
                  <a href="http://www.buybelami.com" target="_blank" style="color:#bf151e;">DVD STORE</a>
                </div>                                
                <div class="bottom_column3">                
                 <img src="https://freecdn.belamionline.com/Signup/Images/MainPage/ikony-paticka.jpg" class="bottom_icons" />
                 <div class="BottomSmallText">Web site design and content © George Duroy. All photographs © 1993-2019 George Duroy.
All rights reserved. Bel Ami® is a registered trademark of George Duroy.<br /><a href="http://www.belamionline.com/2257info.html" target="_blank" style="color:#bf151e;">18 U.S.C 2257 Record-Keeping Requirements Compliance Statement</a> | <a href="https://club.belamionline.com/signup/privacy.aspx" target="_blank" style="color:#bf151e;">Privacy Policy</a>
</div>
<div style="float:right; margin-top:5px;"><span id="LabelGenerated"><font color="White" size="1">- -</font></span> <a href="http://www.rtalabel.org/" target="_blank"><img src="https://freecdn.belamionline.com/Signup/Images/MainPage/RTA.png" /></a>&nbsp;<a href="http://www.asacp.com/" target="_blank"><img src="https://freecdn.belamionline.com/Signup/Images/MainPage/ASACP.png" /></a></div>
</div>
            </div>            
  </div>
    </div>
    
    </div>
    </form>
<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date(); a = s.createElement(o),
  m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-2902124-18', 'auto');
    ga('send', 'pageview');

</script>
</body>
</html>
