
Our Privacy Policy
Acceptance of Terms
  Certain products and services may from time to time be made available to you  ("you" or "User") by CL Educate Ltd. (hereinafter referred  to as “CL”). By registering/ purchasing any products or services through CL  website (www.careerlauncher.com), you signify that you have read, understood  and agreed to be bound by the Terms of Sale in effect at the time of purchase  ("Terms of Sale"). 
  These Terms of Sale are  subject to change without prior written notice at any time, in Company's sole discretion,  and such changes shall apply to any purchases made after such changes are  posted to the Site. Therefore, you should review these Terms of Sale prior to  each registration/ purchase so you will understand the terms applicable to such  transaction. If you do not agree to these Terms of Sale, do not make any  purchases/ register yourself on www.careerlauncher.com.
  PLEASE READ THESE TERMS  OF SALE CAREFULLY AS THEY CONTAIN IMPORTANT INFORMATION REGARDING YOUR LEGAL  RIGHTS, REMEDIES AND OBLIGATIONS. THESE INCLUDE VARIOUS LIMITATIONS AND  EXCLUSIONS.
 Purchase Qualifications; Account Security
  To make a purchase on CL website, you must comply with these Terms of Sale. You  acknowledge that you are responsible for maintaining the security of, and  restricting access to, your account and password, and you agree to accept  responsibility for all purchases/ registration and other activities that occur  under your account. CL reserves the right to refuse or cancel orders at any  time in its sole discretion.
Termination
  Termination by you: You may, at any point in time, choose to terminate your  subscription to a service by sending us an e-mail. Such termination will not  change any amounts due to us.
  Termination by CL: Breach of any of the terms of this Agreement constitutes a  termination of the Agreement. Any and all amounts due under this Agreement  shall survive the Term of this Agreement.
  If you fail, or CL suspects that you have failed, to comply with any of the  provisions of this Agreement, including but not limited to failure to make  payment of fees due, failure to provide CL with a valid credit card or with  accurate and complete Registration Data, failure to safeguard your Account  information, violation of the Usage Rules or any license to the software, or  infringement or other violation of third parties' rights, CL, after notice of  07 days to rectify such failure to you, may terminate your subscription and/or  your Account if such failure on your part still continues. No refunds will be  issued for early Termination.
Refund incase of incorrect purchase 
  Product purchased will not be refunded or changed to another program on grounds  of incorrect selection at the time of purchase.
Refund incase of server error
  In case the same product is purchased twice due to some server error, the  following refund rules will apply – If the learner informs us within 7 days,  full refund would be done thereafter he/she would be provided with a credit  note, which is 10% more than the purchase value.
Payment Method and Terms
  We currently accept the following forms of payment:
  For offline payment:
  Demand draft in favour of "CL Educate Ltd." payable at New Delhi
 

