
Boomkat are committed to protecting and respecting your privacy. We do not send unsolicited emails to customers, and do not pass on your private details to any third parties.
This policy (together with our terms of use: www.boomkat.com/terms) and any other documents referred to on it sets out the basis on which any personal data we collect from you, or that you provide to us, will be processed by us.  Please read the following carefully to understand our views and practices regarding your personal data and how we will treat it. By visiting boomkat.com you are accepting and consenting to the practices described in this policy.
For the purpose of the Data Protection Act 1998 (the Act), the data controller is Boomkat Limited of 2nd Floor Swan Building, 20 Swan Street, Manchester, M4 5JM
INFORMATION WE MAY COLLECT FROM YOU
We may collect and process the following data about you:

Information you give us. You may give us information about you by filling in forms on our site http://boomkat.com or by corresponding with us by phone, e-mail or otherwise. This includes information you provide when you register to use our site, subscribe to our service, search for a product, place an order on our site, enter a competition, promotion or survey, or when you report a problem with our site. The information you give us may include your name, address, e-mail address and phone number, financial and credit card information, or any other personal description.
Information we collect about you. With regard to each of your visits to our site we may automatically collect the following information:

technical information, including the Internet protocol (IP) address used to connect your computer to the Internet, your login information, browser type and version, time zone setting, browser plug-in types and versions, operating system and platform;
information about your visit, including the URL clickstream to, through and from our site (including date and time); products you viewed or searched for; page response times, download errors, length of visits to certain pages, page interaction information (such as scrolling, clicks, and mouse-overs), and methods used to browse away from the page 

COOKIES
Our website uses cookies to distinguish you from other users of our website. This helps us to provide you with a good experience when you browse our website and also allows us to improve our site. For more information on the cookies we use and the purposes for which we use them see our Cookie Policy further down the page.
USES MADE OF THE INFORMATION
We use information held about you in the following ways:
Information you give to us. We will use this information:

To process your orders
to carry out our obligations in providing you with the information, products and services that you request from us;
to provide you with information about other goods and services we offer that are similar to those that you have already purchased or enquired about;
to provide you with information about goods or services we feel may interest you. 
to notify you about changes to our service;
to ensure that content from our site is presented in the most effective manner for you and for your computer.

Information we collect about you. We will use this information:

to administer our site and for internal operations, including troubleshooting, data analysis, testing, research and statistical purposes;
to improve our site to ensure that content is presented in the most effective manner for you and for your computer;
to allow you to participate in interactive features of our service, when you choose to do so;
as part of our efforts to keep our site safe and secure;
to make suggestions and recommendations to you and other users of our site about goods or services that may interest you or them.

DISCLOSURE OF YOUR INFORMATION
We may share your personal information with any member of our group, which means our subsidiaries, our ultimate holding company and its subsidiaries, as defined in section 1159 of the UK Companies Act 2006.
We may disclose your personal information to third parties If we are under a duty to disclose or share your personal data in order to comply with any legal obligation, or in order to enforce or apply our Terms of Website Use (www.boomkat.com/terms) and other agreements; or to protect the rights, property, or safety of Boomkat Limited, our customers, or others.
WHERE WE STORE YOUR PERSONAL DATA
The data that we collect from you may be transferred to, and stored at, a destination outside the European Economic Area ("EEA"). It may also be processed by staff operating outside the EEA who work for us or for one of our suppliers. Such staff maybe engaged in, among other things, the fulfilment of your order, the processing of your payment details and the provision of support services. By submitting your personal data, you agree to this transfer, storing or processing. We will take all steps reasonably necessary to ensure that your data is treated securely and in accordance with this privacy policy, all information you provide to us is stored on our secure servers. 
We take the security of our website and of your transactions extremely seriously.  We encrypt all traffic involving personal data with industry-standard SSL certificates and we are also PCI compliant - meaning that we follow all current data security standards and undergo weekly scans monitoring our security status.
Additionally, we do not store any card details at all, all payments are handled using a system of Tokenisation which is an industry-standard method of secure payment handling. When you place an order with us payment is either handled via your Paypal account or if you choose to pay by credit/debit card we create a "Token" with your payment details which is stored by the Bank payment gateway. When you return to make a purchase it basically reactivates the "Token" so no details need to be entered again and those details are not stored by us.
Where we have given you (or where you have chosen) a password which enables you to access certain parts of our site, you are responsible for keeping this password confidential. We ask you not to share a password with anyone.
Although we will always do our best to protect your personal data, we cannot 100% guarantee the security of your data transmitted to our site due to the nature of the internet. Any transmission is at your own risk, but once we have received your information, we will always use strict procedures and security protocols to try to ensure the safety of that information.
CHANGES TO OUR PRIVACY POLICY
Any changes we may make to our privacy policy in the future will be posted on this page. Please check back to see any updates or changes to our privacy policy.
CONTACT
Questions, comments and requests regarding this privacy policy are welcome and should be addressed to contact@boomkat.com.
COOKIE POLICY
Our website uses cookies to distinguish you from other users of our website. This helps us to provide you with a good experience when you browse our website and also allows us to improve our site. By continuing to browse the site, you are agreeing to our use of cookies.

