

    Privacy Policy


ADA Contact Information
ADA.org is the website of the American Dental Association (ADA). The ADA respects the privacy of our members and other users of our website. This Privacy Policy addresses our use of information about you collected by virtue of your use of ADA.org.
You may contact us about this Privacy Policy:
Via postal address 211 E. Chicago Ave.
Chicago, IL 60611
By email online@ada.org
Information We May Collect About You
 The information we collect via our Website is used to improve the content of ADA.org and advance the ADA's mission.
For each visitor to our Website, our Web server automatically recognizes only the user's domain name, but not the email address (where possible).
We collect the domain name and internet protocol (IP) address of visitors to our Website, aggregate information on what pages users access or visit, user-specific information on what pages users access or visit, and information volunteered by users, including from surveys, purchases and/or registrations. We also may collect the email addresses of those who communicate with us via email.
With respect to cookies: We use cookies for a number of purposes, including to administer our website and to record session information, such as items that users add to their shopping cart and user-specific information on what pages users access or visit, and to ensure that visitors are not repeatedly sent the same banner ads.
How We May Use Information We Collect 
The ADA, its subsidiaries and affiliates may use your contact information to alert you to new information, products and services, events and other opportunities. From time to time, we may also make your contact information (but not your email address) available to other reputable organizations whose products or services we think you might find interesting.
The ADA maintains a directory of dentists that may include information you submit via our website. This directory is shared with state and local dental societies. A subset of this directory, containing information about ADA members, is publicly accessible on ADA.org.
With respect to your email address: We have instituted stringent reviews and opt-out capabilities to ensure that you do not receive unwanted email from the ADA. We will not give or sell your email address to any entity outside the ADA, its subsidiaries and affiliated entities (such as state and local dental societies).
A particular ADA subsidiary or affiliate may have certain privacy policies even more stringent than noted here, which will be indicated on its specific portion of ADA.org or its own site.
Your Rights
 To help us keep our promise to respect your privacy, it is important when you wish to exercise your rights that you contact us via any of the means listed in the first section of this Privacy Policy.

If you do not want to receive email, postal mail, telephone calls or faxes from us in the future, or if you do not want us to share your contact information in the manner described above, please provide us with your exact name, email and postal addresses, phone and fax numbers and, if appropriate, ADA membership number. We will no longer use information you so identify and secured via ADA.org , if that is your wish. While we will continue to share information we maintain about you with state and local dental societies, we will do our best to be sure any other specific contact information you so identify is removed from any list we share with other organizations.
On request we provide site visitors with access to all information that we may maintain about them, including financial information (e.g., credit card account information), unique identifier information (e.g., user/member number or password), transaction information (e.g., dates on which users made purchases, amounts and types of purchases), communications that the visitor has directed to our site (e.g., emails, user inquiries), and contact information (e.g., name, address, phone number).
On request we also offer visitors the ability to have inaccuracies corrected in contact information, financial information, unique identifiers, transaction information and communications that the visitor has directed to the site.
You may also contact us if you believe your information is being used for purposes other than those for which it was originally collected.
Children's Privacy
We are concerned about protecting children's privacy. We do not offer or sell products or services for purchase by children. If you are under 18 years of age, you may not purchase products or services from, and you should not provide information to, ADA.org. without the involvement of a parent or guardian. We do not collect online contact information without prior parental consent or parental notification, including an opportunity for the parent to prevent use of the information and participation in the activity. Without prior parental consent, online information will be used only to respond directly to the child's request and will not be used for other purposes. Nor without prior parental consent do we collect personally identifiable offline contact information, distribute to third parties any personally identifiable information or give the ability to publicly post or otherwise distribute personally identifiable contact information. We do not entice by the prospect of a special game, prize or other activity, to divulge more information than is needed to participate in the activity.
More About Our Privacy Practices 
With respect to other websites and servers: ADA.org contains links to other websites. We have no control over and take no responsibility for the privacy practices or content of those sites.

The ADA does not partner with or have special relationships with ad server companies. However, we are running an advertising campaign pertaining to Direct Reimbursement on outside Websites that link to ADA.org. The ADA utilizes transparent 1x1 pixel GIF files, provided by DoubleClick to help manage this online advertising. The information collected is anonymous and not personally identifiable. For more information about DoubleClick, including information about how to opt out of these technologies, go to http://www.doubleclick.net/us/corporate/privacy/.
With respect to security: We use industry-standard encryption technologies when transferring and receiving consumer data exchanged with our site. When we transfer and receive certain types of sensitive information such as financial or health information, we redirect visitors to a secure server and will notify visitors through a pop-up screen on our site. We have appropriate security measures in place in our physical facilities to protect against the loss, misuse or alteration of information that we have collected from you at our site. When users choose to make payments via credit cards, we submit the information needed to obtain payment to the appropriate clearinghouses. We do not store or reuse this credit card information, unless you have given your authorization for us to do so.
Note: Health information you may share via ADA.org is neither private nor confidential and should be shared, if at all, only in accordance with applicable laws.
Changes, Comments and Questions 
If our information practices change at some time in the future we will post the policy changes to our Website to notify you and allow you to opt out of new uses. The latest version of our Privacy Policy will always be available at ADA.org. If you are concerned about how your information may be used, please check back at our Website periodically.

We welcome your comments or questions about our Privacy Policy and privacy practices. If you feel that this site is not following its stated policy, please let us know.



