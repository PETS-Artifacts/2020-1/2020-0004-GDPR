IELTS Australia Privacy Policy
Privacy Statement

Introduction

We are committed to meeting your expectations and will comply with all applicable privacy and data protection legislation in relation to your Personal Information and Sensitive Information. 
We know you love the small print, but here’s a summary of how we protect your privacy: 

We only collect what we need to help you with your request or provide you with information that you ask for, personalising it to your interests.
 We won’t hang on to your information. If we don’t need it, and you don’t want us to keep it we’ll delete it.
We want you to understand everything we do with your information, so we’ll try our best to explain with no nonsense.
No surprises - you decide what and how you hear from us.
That goes for everyone. We do not pass your information to third parties for marketing purposes without your knowledge.

The rest of this policy provides you with more detail of our Privacy and Personal Information handling practices.
Definitions of terms referred to in this Policy can be found in the ‘Glossary’ at the end of this document.
Who we are
IELTS Australia Pty Ltd is a subsidiary of IDP Education Limited. You can find the IDP Group privacy policy here:
www.idp.com/global/privacy-policy/ 
Data Controller Contact Details
IELTS Australia
Level 8
535 Bourke Street
MELBOURNE 3000
Victoria
Australia
Contact our DPO or Privacy Officer via email to: privacyofficer@idp.com 
We will endeavour to respond to you within a reasonable time.
Collection of Personal Information by IELTS Australia allows us to:

conduct the IELTS test and offer IELTS Test Practice and Preparation.
forward any enquiries you have to another of the IELTS Owners (British Council and Cambridge Assessment English) in accordance with your expressed preference (e.g. when you make a general enquiry about sitting an IELTS test);
inform you of and provide you with information on the services we provide (including events and opportunities to participate in projects/programs);
manage our internal business operations; and
respond to your questions.


IELTS Testing

IELTS test centres operate in more than 140 countries, across Europe, Asia, Australia and New Zealand, North America, South America, Africa and the Middle East.  IELTS Australia operates and manages some of these IELTS test centres and the British Council operates and manages others, to which you may be referred. 
IELTS Australia is a joint Data Controller with the other IELTS Owners (British Council, IDP Education and Cambridge Assessment English) for the purposes of conducting the IELTS Test.
If you apply to take, or have taken, an IELTS Test we will collect personal details such as name, postal and email addresses, date of birth, gender, contact details, languages, country of nationality, residency status, education qualifications, national identity card number, passport details, employment history occupation, information required for visa application and professional memberships.
This will include copies of documents you provide to us as proof of identity – e.g. copies of your passport or national identity card, proof of citizenship or photographs of you when you apply to sit the IELTS test.
We will also hold results achieved by you.
We may also hold Sensitive Information about you including:

medical information (which you may provide to obtain special consideration in relation to your testing);
biometric information for IELTS test candidates who provide a finger scan as part of the Identity Authentication Management (IAM) for the IELTS test (a copy of - your finger scan is held in the form of a Binary Large Object (BLOB); and 
racial or ethnic origin (from information on your country of origin).

An IELTS Owner may share any relevant information from your IELTS application or test with the other IELTS Owners for purposes such as administering the IELTS test, reviewing IELTS test results for statistical and fraud detection purposes, or accommodating changes in your preference as to test centre location;
You can refer to the privacy policies for them by following these links:
The British Council’s is found at:
www.britishcouncil.org/privacy-cookies/data-protection
Cambridge Assessment English ’s is found at:
ww.cambridgeenglish.org/footer/data-protection/
The IELTS Privacy Policy and the Test Terms and Conditions provide more details on Privacy and Personal Information handling practices related to IELTS Testing.
www.ielts.org/policy/privacy-policy

IELTS Test Practice and Preparation

We provide services to support you in preparing to take the IELTS test including support tools, practice tests, seminars and IDP Masterclasses.
Collection of your Personal Information allows us to:

provide you with test practice and preparation services you request
develop new and improved test practice and preparation tools
provide you with your test practice results and give you general or personalised feedback
with your consent, inform you of and provide you with information on the services we provide (including events and opportunities to participate in projects/programs);
manage our internal business operations; and
respond to your questions.

Retention
When the Personal Information that we collect is no longer required, we will destroy, delete it in a secure manner, or ensure that the information is de-identified in accordance with our information destruction and de-identification policy, unless we are required by law to retain a copy of the Personal Information or the information is contained in a Commonwealth record.
If we collect your information for IELTS testing or test preparation purposes, we will retain it for up to 3 years after taking the IELTS test or participating in the service, or after your last activity with us. 
Finger scan data is only retained as a binary large object file for 60 days after your test date. 
We may also retain certain records for other legitimate reasons (including after your relationship with the IELTS Owner(s) has ended), for example to resolve any potential disputes, to comply with other reporting and retention obligations, or for the prevention of fraud.  

Other Dealings with IELTS Australia

Other ways you may deal with IELTS Australia include: 

registering your interest in being engaged as consultants or employees
submitting a research proposal
 registering with a service offered by IELTS Australia; or
being the representative of a Recognising Organisation.

We will provide specific notice at the point your information is collected.  Any unsolicited personal information will be securely destroyed.

How we collect Personal Information

We will only collect your Personal Information by lawful and fair means. Personal Information may be collected directly from you or your authorised representative, or may be collected from a third party such as a licensee or representative authorised by us to provide services to you. You may supply your Personal Information to us when communicating with us via social networks and other online channels, e.g. through your Facebook or Twitter identity.
We do not collect Personal and Sensitive information unless the information is reasonably necessary for our business functions or activities. We will obtain your consent before collecting any Sensitive Information.
When you deal with us, you will need to identify yourself in order for us to provide our services to you, as such we do not accept the use of pseudonyms.
If you access our websites, we may collect additional personal information about you in the form of your IP address and/or domain name. For more details please see our Website Cookie Policy for further information.

Disclosure to Others

Information disclosed by us to overseas recipients for the purposes of providing IELTS Testing services to you may be transferred offshore to the other IELTS Owners (British Council and Cambridge Assessment English). These recipients are United Kingdom based entities, subject to the Data Protection Act (UK). Where we transfer data to offshore recipients, the data is subject to use restrictions and safeguards against unauthorised access.
Some IELTS Australia IT systems and data are hosted on servers in countries outside Australia or the UK and we may send your information to one of those servers as part of our business. 
IELTS Australia may hold or transfer the data we collect in or to the following countries (in addition to your home country):
This information is stored and accessed only as required to provide our services to you. Where we transfer data to offshore servers, the data is subject to use restrictions and safeguards against unauthorised access.
The information disclosed to overseas recipients consists of any information you have given us to provide the relevant services. By accepting services from us, you consent to us providing your Personal Information to the relevant overseas recipient as mentioned above. IELTS Australia takes appropriate steps to ensure there are adequate safeguards in place for the transfer of your personal information including standard protection clauses in our contracts and an IDP Group Data Transfer agreement. 

Your rights

You have the right to seek access to any of your Personal Information held by us unless there is a valid reason under the Privacy Act or Data Protection Act for us to withhold the information.
Valid reasons include:

We have reason to suspect that unlawful activity or misconduct of a serious nature has been engaged in and giving access would be likely to prejudice the taking of appropriate action in relation to the matter; or
Giving access would reveal evaluative information generated within us in connection with a commercially sensitive process.

If your personal details change, or you believe the information we hold about you is incorrect, incomplete or out-of-date, please contact us so that we can correct our records.
The right to erasure
At your request, we will remove all the data we have for you which we are not required legally to retain. 
The right to restrict processing
You have the right to restrict processing when you have exercised one of the above rights and it may take some time to process that request. For example: when you contact us to update your details you have the right to request restriction until your details are being updated.
The right to withdraw consent
For the processing activities where we have asked you for consent, you have the right to withdraw this consent at any point by unsubscribing from a message from us, or by contacting privacyofficer@idp.com.
The right to object
You can object to the processing of your personal data. 
The right to not be subject to automated decisions and profiling
None of our processing activities are purely automated and no decisions are made without human intervention. There are instances where we process data to analyse or predict behaviour but we will ask you for explicit consent when this processing will involve your personal data.
The right to data portability
If you have provided us with your personal data with consent or under the contact obligations, you have the right to request the data you have provided to us in a machine-readable format should you decide to move to another data controller.
Making a request
Requests for access or correction to your Personal Information or other privacy rights described above should be addressed to The IDP Privacy Officer by emailing privacyofficer@idp.com. All requests will be responded to in writing within a reasonable period of time. As part of this process we will verify the identity of the individual requesting the information prior to providing access or making any changes. If we cannot fulfil your request, we will respond provide you with an explanation. 

Making a complaint if you are unhappy about how we have dealt with or are dealing with your Personal Information

We have procedures in place to deal with your inquiries or complaints.
If you have any questions about our policy or any complaint regarding the treatment of your privacy by us, please contact privacyofficer@idp.com. 
If you feel we have intruded on your privacy or misused your data, you are able to complain.
In Australia
Office of the Australian Information Commissioner’s (OAIC) website at www.oaic.gov.au
The contact details for the OAIC are:
Office address: Level 3, 175 Pitt Street, Sydney 2000
Postal address (Sydney): GPO Box 5218 Sydney NSW 2001
Postal address (Canberra): GPO Box 2999 Canberra ACT 2601
Phone: 1300 363 992
Fax: + 61 2 9284 9666
Email: enquiries@oaic.gov.au
In the UK
Information Commissioners Office
https://ico.org.uk/concerns/ 
Updates to this Policy
This version of the policy was published on Friday 20 July, 2018. From time to time we may update this privacy policy. We encourage you to take the time to review it any time you provide us with personal information.
We aim for there to be ‘no surprises’ in our dealings with your personal information. If we make significant changes to the policy and we have your email address, we will send you an email information you of these changes, how they may impact you, and remind you of your privacy rights.

Glossary

 




1. Applicable Privacy laws means: 

the Privacy Act 
GDPR
any other applicable laws relating to privacy or the use or protection of personal data anywhere in the world





2. IDP refers to IDP Education Limited (ACN 117 676 463).
 




3. GDPR means the European General Data Protection Regulation 2016/679.
 




4. IELTS refers to the International English Language Testing System.
 




5. IELTS Australia refers to IELTS Australia Pty Ltd (ACN 008 664 766) which is a subsidiary of IDP.
 




6. IELTS Owners refers to the British Council, CambridgeAssessment English and IDP Education Ltd (through its subsidiary IELTS Australia Pty Ltd), who together own the IELTS test. The British Council and Cambridge Assessment English are United Kingdom based entities.
 




7. Personal Information means information or an opinion about an identified individual, or an individual who is reasonably identifiable, whether the information or opinion is true or not, and whether the information or opinion is recorded in a material form or not.
 




8. Privacy Act means the Privacy Act 1988 (Cth).
 




9. Sensitive Information includes information or an opinion about an individual’s racial or ethnic origin, political opinions, membership of a professional or trade association or criminal record that is also personal information, or health information about an individual, or biometric information used for the purpose of automated biometric verification or identification, or biometric templates.
 




 
Information collected on this website
Google Analytics
We use Google Analytics to measure and analyse the site’s internet usage to ensure the site meets IDP's business objectives with advertisers and users. Individual privacy is protected but we gain insights on how to make the site more useful for advertisers as well as our users.
Data collected from this analysis include:

the number of page views (or page impressions) that occur on our sites;
the number of unique visitors;
how long these unique visitors (on average) spend on our sites;
common entry and exit points to our sites;
files downloaded from the site; and
forms filled in on the site
Audience and device analysis
Referral source

Hotjar
We use Hotjar to better understand our users’ needs and to optimize this service and experience. Hotjar is a technology service that helps us better understand our users experience (e.g. how much time they spend on which pages, which links they choose to click, what users do and don’t like, etc.) and this enables us to build and maintain our service with user feedback. Hotjar uses cookies and other technologies to collect data on our users’ behaviour and their devices (particularly, device IP address (captured and stored only in anonymized form), device screen size, device type (unique device identifiers), browser information, geographic location (country only), preferred language used to display our website). Hotjar stores this information in a pseudonymized user profile. Neither Hotjar nor we will ever use this information to identify individual users or to match it with further data on an individual user. For further details, please see Hotjar’s privacy policy by clicking on this link.
You can opt-out to the creation of a user profile, Hotjar’s storing of data about your usage of our site and Hotjar’s use of tracking cookies on other websites by following this opt-out link.
Google AdWords
Google AdWords is an online advertising service by Google, which allows advertisers to display ads on the Google display network. We use the Google AdWords remarketing feature to show you ads about IELTS preparation, products and support. Google Customer Match allows advertisers to target custom audiences based on a list of email address on the Google display network. We may use Google Customer Match to target IELTS preparation, product and support ads to you.  
If you wish to opt-out of Google ads online, please visit https://support.google.com/ads/answer/2662922?hl=en 
Facebook Custom Audience
The Facebook Custom Audience program allows advertisers to target custom audiences based on email address on Facebook. We may participate in the Facebook Custom Audience program to target IELTS preparation, product and support ads to you.
If you wish to opt-out of Facebook Custom Audience ads, you may control this via your Facebook privacy settings, or by clicking on the ad and selecting ‘Hide all from this advertiser’.
Cookies
Cookies are small data files sent from a website and stored in a user’s web browser. We use cookies to personalise content and ads, to provide social media features and to analyse our traffic. We also share information about your use of our site with our social media, advertising and analytics partners.
If you wish to disable the cookies on this website you will need to refer to the privacy and security settings of your preferred browser.
Below is a summary of the types of cookies used on our sites and how they are used.
Strictly Necessary Cookies
These cookies are necessary for the website to function and cannot be switched off in our systems. They are usually only set in response to actions made by you which amount to a request for services, such as setting your privacy preferences, logging in or filling in forms. You can set your browser to block or alert you about these cookies, but some parts of the site will not then work. These cookies do not store any personally identifiable information.
Performance Cookies
These cookies allow us to count visits and traffic sources so we can measure and improve the performance of our site. They help us to know which pages are the most and least popular and see how visitors move around the site. All information these cookies collect is aggregated and therefore anonymous. If you do not allow these cookies we will not know when you have visited our site, and will not be able to monitor its performance.
Functional Cookies 
These cookies enable the website to provide enhanced functionality and personalisation. They may be set by us or by third party providers whose services we have added to our pages. If you do not allow these cookies then some or all of these services may not function properly.
Targeting Cookies
These cookies may be set on our site by our advertising partners, Google and Addthis. They may be used by those companies to build a profile of your interests and show you relevant adverts on other sites. They do not store directly personal information, but are based on uniquely identifying your browser and internet device. If you do not allow these cookies, you will experience less targeted advertising.
If you prefer that we not share your non-personal information with partners, including third parties that use tracking technologies to deliver targeted display advertising based on that information, please visit http://optout.networkadvertising.org/?c=1#!/ to access the Network Advertising Initiative opt-out.
Social Media Cookies
These cookies are for a range of social media services including Linkedin and Facebook that we have added to the site to enable you to share our content with your friends and networks. They are capable of tracking your browser across other sites and building up a profile of your interests. This may impact the content and messages you see on other websites you visit. If you do not allow these cookies you may not be able to use or see these sharing tools. 
 
 
