
Privacy Statement
Effective Date: December, 2017
This privacy statement applies to www.astonesthrowaway.com owned and operated by A Stone's Throw Away. This privacy statement describes how we collect and use the personal information you provide on our web site: www.astonesthrowaway.com. It also describes the choices available to you regarding our use of your personal information and how you can access and update this information. We may amend this privacy statement from time to time, so visit this page regularly to keep abreast of the updates
The type of personal information we collect.
The types of personal information that we collect include:

Your first name, last name, email address, phone number and home address;
Credit card details (type of card, credit card number, name on card, expiration date and security code);
Guest stay information, including date of arrival and departure, special requests made, observations about your service preferences (including room preferences, facilities or any other services used);
Information you provide regarding your marketing preferences or in the course of participating in surveys, contests or promotional offers;

When you visit our website, even if you do not make a reservation, we may collect certain information, such as your IP address, which browser you’re using, and information about your computer’s operating system, application version, language settings and pages that have been shown to you. If you are using a mobile device, we might also collect data that identifies your mobile device, device-specific settings and characteristics and latitude/longitude details. When you make a reservation, our system registers through which means and from which websites you have made your reservation. If this data can identify you as a natural person, this data is considered personal information which is subject to this privacy statement.
You may always choose what personal information (if any) you wish to provide to us. If you choose not to provide certain details, however, some of your transactions with us may be impacted.
Why do we collect, use and share your personal information?

Reservations: we use your personal information to complete and administer your online reservation.
Customer service: we use your personal information to provide customer service. 
Guest reviews: we may use your contact information to invite you by email to write a guest review after your stay. This can help other travellers to choose the accommodation that suits them best.

Marketing activities: we also use your information for marketing activities, as permitted by law. 

Other communications: there may be other times when we get in touch by email, by post, by phone or by texting you, depending on the contact information you share with us. There could be a number of reasons for this:

					
We may need to respond to and handle requests you have made.

If you have not finalised a reservation online, we may email you a reminder to continue with your reservation. We believe that this additional service is useful to you because it allows you to carry on with a reservation without having to search for the accommodation again or fill in all the reservation details from scratch.
When you use our services, we may send you a questionnaire or invite you to provide a review about your experience with our website.


Legal purposes: In certain cases, we may need to use your information to handle and resolve legal disputes, for regulatory investigations and compliance.

Fraud detection and prevention: we may use your personal information for the detection and prevention of fraud and other illegal or unwanted activities.
Improving our services: finally, we use your personal information for analytical purposes, to improve our services, to enhance the user experience, and to improve the functionality and quality of our online travel services.


To process your information as described above, we rely on the following legal basis:

Performance of a Contract: The use of your information may be necessary to perform the contract that you have with us. For example, if you use our services to make an online reservation, we'll use your information to carry out our obligation to complete and administer that reservation under the contract that we have with you.
Legitimate Interests: We may use your information for our legitimate interests, such as providing you with the best appropriate content for the website, emails and newsletters, to improve and promote our products and services and the content on our website, and for administrative, fraud detection and legal purposes.
Consent: We may rely on your consent to use your personal information for certain direct marketing purposes. You can withdraw your consent anytime by contacting us at any of the addresses at the end of this Privacy Statement.

How do we share your personal information with third parties?


Booking.com: we have teamed up with Booking.com B.V., located at Herengracht 597, 1017 CE Amsterdam, The Netherlands (www.booking.com) (hereafter Booking.com) to offer you our online reservation services. While we provide the content to this website and you make a reservation directly with us, the reservations are processed through Booking.com. The information you enter into this website will therefore also be shared with Booking.com and its affiliates. This may include personal information such as your name, your contact details, your payment details, the names of guests traveling with you and any preferences you specified when making a booking. To find out more about the Booking.com corporate family, visit About Booking.com.
Booking.com will send you a confirmation email, a pre-arrival email and provide you with information about the area and our accommodation. Booking.com will also provide you with international customer service 24/7 from its local offices in more than 20 languages. Sharing your details with Booking.com’s global customer service staff allows them to respond when you need assistance. Booking.com may use your information for technical, analytical and marketing purposes as further described in the Booking.com privacy policy. This may include that your information might also be shared with other members of the Priceline Group (Agoda.com, Rentalcars.com and Kayak.com) for analysis to provide you with travel-related offers that may be of interest to you and to offer you customised service. If you have questions about the processing of your personal information by Booking.com, please contact customer.service@booking.com.
BookingSuite: Your personal information may be shared with BookingSuite B.V. located at Herengracht 597, 1017 CE Amsterdam, the Netherlands (suite.booking.com), the company which operates this website. BookingSuite will provide your personal information to Booking.com for the purposes described above. 
Third-party service providers: We may use service providers to process your personal information strictly on our behalf. This processing would be for purposes such as facilitating reservation payments, sending out marketing material or for analytical support services. These processors are bound by confidentiality clauses and are not allowed to use your personal data for their own purposes or any other purpose.

Competent authorities: We disclose personal information to law enforcement and other governmental authorities insofar as it is required by law or is strictly necessary for the prevention, detection or prosecution of criminal acts and fraud.


Retention of Personal Information
We will retain your personal information for as long as we deem it necessary to provide services to you, comply with applicable laws (including those regarding document retention), resolve disputes with any parties and otherwise as necessary to allow us to conduct our business. All personal information we retain will be subject to this privacy statement.

What security procedures are put in place to safeguard your personal data?
In accordance with applicable data protection laws, we observe reasonable procedures to prevent unauthorised access to, and the misuse of, personal data. We use appropriate business systems and procedures to protect and safeguard the personal data you give us. We also use security procedures and technical and physical restrictions for accessing and using the personal data on our servers. Only authorised personnel are permitted to access personal data in the course of their work.
How can you control your personal information?

You always have the right to review the personal information we keep about you. You can request an overview of your personal data by emailing us to the email address stated below. Please write 'Request personal information' in the subject line of your email to speed things along a bit.
You can also contact us if you believe that the personal information we have for you is incorrect, if you believe that we are no longer entitled to use your personal data, or if you have any other questions about how your personal information is used or about this Privacy Statement. Please email or write to us using the contact details below. We will handle your request in accordance with the applicable data protection law.
Who is responsible for the processing of your personal data?
A Stone's Throw Away, Tropical Gardens Road & Gambier Heights off West Bay Street, 00000 Nassau, Bahamas. If you have any suggestions or comments about this privacy statement, please send an email to reservations@astonesthrowaway.com.

