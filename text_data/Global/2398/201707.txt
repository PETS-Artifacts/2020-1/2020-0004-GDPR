
Introduction
Tickets for this website are provided by London Theatre Direct Ltd we are committed to safeguarding your privacy when and fully comply with the Data Protection Act 1998.
Privacy policy
We only collect personal information about you (for example your name, address, telephone number, email address and credit card details) when you specifically provide us with such information on a voluntary basis, for example when purchasing or enquiring about event tickets online. We also collect information about the frequency of visits to the website, in addition to the pages you visited and the length of time spent on the LondonTheatreDirect.com website.
Any personal information we collect via our web site will never be released or sold to any external companies or individuals. We will not share your personal information for marketing or any other purposes without your consent unless where required by law. If you have registered your interest with us and thereby given your consent, you may be contacted with relevant promotions, offers or information that you have expressed an interest in or that might be of interest to you.
What we do with your information
We use the information you provide us with to sell you the tickets you requested. Certain information, such as your full name and address details may be provided to the venue where you chosen event takes place in order to facilitate the booking. We may occasionally use some of the same information (such as the type of tickets you requested) to tailor the content of this site to the interests of our users. By making a purchase from this site you are consenting to your financial and/or personal information being passed to any third party organisations necessary to process your transactions, such as credit card companies and banks on our behalf. Except for these specific cases, we will never share financial information with third parties. Certain information, such as your full name and address details, may be provided to the venue where you chosen event takes place in order to facilitate the booking.
Opting out
If you do not wish to receive further e-mails from us, please log in to your account and change your mailing preferences. Alternatively, you can e-mail us at contact@londontheatredirect.com. We respect your privacy and any personal communication between you and ourselves. We will always comply with any data protection legislation currently in force.
Information and the law
We may occasionally release your personal or financial information to assist law enforcement, to comply with laws or regulations, to enforce the terms under which you transacted with us or to protect the rights, property or safety of London Theatre Direct Ltd, users of the site or others.
Security measures
We take the security of your personal information very seriously. We use 128 bit Secure Socket Layer (SSL) encryption for all your transactions. This system encrypts all your personal information, including credit card number, name, and address, so that it cannot be read if intercepted by a malicious third party. We may also record you IP address as part of our efforts to combat fraud.
Third party sites
We are not responsible for the privacy policies or practices of any third party sites linked to this site. Consequently we advise you to read their privacy policies before submitting data to such sites.

