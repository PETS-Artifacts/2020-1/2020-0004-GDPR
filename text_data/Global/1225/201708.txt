
The National WWII Museum is committed to protecting the privacy of its users and any personal information that they may share with us or that we hold. We value our users’ trust and recognize that maintaining this trust requires us to be transparent and accountable at all times. The Privacy Policy describes important information about the steps necessary to safeguarding all personal information about our users.
The National WWII Museum collects from users only the personal information necessary to provide the information and services requested by them. Personal information in our possession is maintained and used in ways that respect individual privacy. Employees are educated about the importance of protecting personal information and consumer privacy, and only authorized employees are officially granted access to personal information.
The National WWII Museum will not disclose personal information collected from users to private third parties unless the law requires such disclosure.
Depending on the situation, the Museum may collect the following information about visitors to our website:
the domain name and or IP address
the e-mail addresses of those who communicate with us via e-mail
aggregate information about which pages consumers access or visit
information volunteered by the consumer, such as survey information
This information is used to improve the scope and content of the Museum’s services on the Internet, and, in some cases, to respond to you.
Use and Disclosure of Personal Information
The Museum collects, uses and discloses personal information only for certain purposes that are identified to the user. We may use it to establish and manage our relationship with a user, as well as providing requested information. Personal information can also help us understand users and identify preferences and expectations. We collect user information for internal use only, and just to the extent required for our purposes. We obtain relevant personal information about users lawfully and fairly.
Sharing or Selling of Personal Information
The Museum does not sell, trade or rent the information and contents of active donor and customer files to others. The Museum will not share donor and customer e-mail address to third parties. The Museum may share donor and customer postal mailing lists with other organizations for a single use. An organization that receives the Museum’s mailing list must agree to strict parameters which limit the use of the data. For example:
The names and mailing addresses will only be used for a specific project;
The Museum must approve the contents of the mailing piece before it is sent to the members of our list;
The data file containing the mailing list will not be forwarded to any other person or entity except when necessary to complete the specific project;
No copies of any type of the data file will be made by the receiving organization; and
The data file of the names and addresses will be returned or destroyed when the project is completed.
Cookies
Any web page or application at this site that uses cookies will identify itself as such. Cookies are short and simple text files that are stored on a user's computer hard drive by websites. They are used to keep track of and store information so the user does not have to supply the information multiple times. The information that is collected through cookies at this site is handled in the same manner as other information collected here. You can configure your web browser to refuse cookies or to notify you when a website attempts to send you a cookie. You can also check your hard drive for cookie files and delete them from your computer.
Email Correspondence
E-mail messages will be treated the same as any other written communication. They may be subject to public inspection or legal disclosure. Please do not put confidential and personal information such as social security number, date of birth, bank account and credit card information, mother's maiden name or medical information in your non-secure e-mail communications to The National WWII Museum. Your privacy cannot be protected or insured by The National WWII Museum during email or unsecured web form transmission.
Any personally identifying information which you choose to provide (for example, your mailing address) in an electronic mail message or web form requesting information, any comments on policies or issues, or other information collected in this manner, is used solely for information and, in some cases, to respond to you. An email automatically includes your email address and certain routing information, including your internet server.
Online Purchases, Donations and Catalog Requests
If you make an online purchase, online donation or request a catalog, we may also collect your e-mail and/or your mailing address to contact you for marketing purposes. We may share the mailing addresses we collect with other reputable publishers to help them contact consumers for marketing purposes.
If you do not want to receive e-mail from The National WWII Museum in the future, or do not want us to share your mailing address with other publishers, please let us know by e-mailing us at interactive@nationalww2museum.org or by sending a letter to:
The National WWII Museum
945 Magazine Street
New Orleans, LA 70130
Please provide your exact name and address and tell us if you want your name removed from our mailing list, the list we may share with other companies, or both.
Links to Other Websites
This website has links to other websites. These include links to websites operated by other governmental agencies, nonprofit organizations and private businesses. Many of these sites may operate under different privacy standards. Visitors to those websites are advised to review the privacy statements of those sites. The National WWII Museum is not responsible for any content on a linked site.
Contact Information
The National WWII Museum welcomes comments regarding this privacy statement. Please convey any questions or concerns to:
The National WWII Museum
945 Magazine Street
New Orleans, LA 70130

