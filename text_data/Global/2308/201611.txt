Privacy

        Booksure™ retains and uses your personal information to provide you with the
        best online e-commerce experience by providing you with a personalised service and
        to give you details of offers which we think will be of interest to you.  We may
        also use the information to process any transactions you undertake with us and for
        internal administration and analysis.

        We treat your personal details very seriously.  Booksure™ will keep all personal
        information you give us as strictly confidential and no personal information will
        be made available to third parties, unless obliged to do so by law or legal process.

        When you forward personal information including your name, e-mail address, mailing
        address and credit card details to us, Booksure™ will use a secure server
        to encrypt such information before it is received by us.  Once we receive it, we
        will undertake all reasonable measures to protect such information from unauthorised
        access and use.  These measures include, but are not limited to preventing unauthorised
        access or disclosure, maintaining data accuracy, and ensuring the appropriate use
        of the information. Registered Booksure™ users will be provided with online
        access to their information.  They will be able to keep this information up to date
        and accurate.  Booksure™ will take reasonable steps to verify your identity,
        such as a password and user ID, before granting access to your data.  It is the users
        responsibility to keep the userID and password confidential.  As per Booksure™’s
        compliance with The Payment Card Industry Data Security Standard (PCI DSS), no
        credit card information is stored in our database.
        Unless we have your express consent we will only disclose personal data to third
        parties if this is required for the purpose of completing your transaction with
        us.   This is of course subject to the proviso that we may disclose your data to certain
        permitted third parties, such as members of our own group, our own professional
        advisers who are bound by confidentiality codes, and when we are legally obliged
        to disclose your data.

        Booksure™ may send e-mail messages to our customers with news and special
        offers and users may, by return mail, indicate whether they would prefer not to
        receive any such mail in future.

        By disclosing your personal information to us using this website or over the telephone,
        you consent to the collection, storage and processing of your personal information
        by Booksure™ in the manner set out in this privacy policy.  Booksure™
        may collect your computer's Internet Protocol (IP) address, domain name, the identity
        of your internet service provider, the type and version of web browsing software
        you use, your computers operating system, the URL of the web page from which you
        came to visit our Site and your preferred web browsing language.  We use these technologies
        to identify our users, to help with statistical reporting and reduce the risk of
        unauthorized users attempting to fraudulently use or hack our system.

        This site may use “cookies” to customise it and make your visit to us
        more user friendly.  Cookies are small text files and cannot harm your computer in
        any way. They are an industry standard way to store general information provided
        by you on our website.

Two types of cookies are used:

Permanent cookies: We use these cookies to identify you between visits.  They
                are not required for the site to work but may enhance your experience. We do not
                store any personal data in these cookies, just a unique number that identifies you
                to us. Furthermore, you can configure their browsers to not accept our cookies,
                however certain non-core functionality such as the ‘enquiry basket’ will not be
                able to function properly.
Session cookies: These are used to maintain something called session state.  These are required for the site to function but are not used in any way to identify
                you personally.

Booksure™ retains and uses your personal information to provide you with the best online e-commerce experience by providing you with a personalised service and to give you details of offers which we think will be of interest to you. We may also use the information to process any transactions you undertake with us and for internal administration and analysis.We treat your personal details very seriously. Booksure™ will keep all personal information you give us as strictly confidential and no personal information will be made available to third parties, unless obliged to do so by law or legal process.When you forward personal information including your name, e-mail address, mailing address and credit card details to us, Booksure™ will use a secure server to encrypt such information before it is received by us. Once we receive it, we will undertake all reasonable measures to protect such information from unauthorised access and use. These measures include, but are not limited to preventing unauthorised access or disclosure, maintaining data accuracy, and ensuring the appropriate use of the information. Registered Booksure™ users will be provided with online access to their information. They will be able to keep this information up to date and accurate. Booksure™ will take reasonable steps to verify your identity, such as a password and user ID, before granting access to your data. It is the users responsibility to keep the userID and password confidential. As per Booksure™’s compliance with The Payment Card Industry Data Security Standard (PCI DSS), no credit card information is stored in our database.Unless we have your express consent we will only disclose personal data to third parties if this is required for the purpose of completing your transaction with us. This is of course subject to the proviso that we may disclose your data to certain permitted third parties, such as members of our own group, our own professional advisers who are bound by confidentiality codes, and when we are legally obliged to disclose your data.Booksure™ may send e-mail messages to our customers with news and special offers and users may, by return mail, indicate whether they would prefer not to receive any such mail in future.By disclosing your personal information to us using this website or over the telephone, you consent to the collection, storage and processing of your personal information by Booksure™ in the manner set out in this privacy policy. Booksure™ may collect your computer's Internet Protocol (IP) address, domain name, the identity of your internet service provider, the type and version of web browsing software you use, your computers operating system, the URL of the web page from which you came to visit our Site and your preferred web browsing language. We use these technologies to identify our users, to help with statistical reporting and reduce the risk of unauthorized users attempting to fraudulently use or hack our system.This site may use “cookies” to customise it and make your visit to us more user friendly. Cookies are small text files and cannot harm your computer in any way. They are an industry standard way to store general information provided by you on our website.