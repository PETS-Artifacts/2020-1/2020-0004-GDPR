
Missing or Broken Files
When you get a 404 error be sure to check the URL that you are attempting to use in your browser.This tells the server what resource it should attempt to request.
http://example.com/example/Example/help.html
In this example the file must be in public_html/example/Example/
Notice that the CaSe is important in this example. On platforms that enforce case-sensitivity example and Example are not the same locations.
For addon domains, the file must be in public_html/addondomain.com/example/Example/ and the names are case-sensitive.
Broken Image
When you have a missing image on your site you may see a box on your page with with a red X where the image is missing. Right click on the X and choose Properties. The properties will tell you the path and file name that cannot be found.
This varies by browser, if you do not see a box on your page with a red X try right clicking on the page, then select View Page Info, and goto the Media Tab.
http://example.com/cgi-sys/images/banner.PNG
In this example the image file must be in public_html/cgi-sys/images/
Notice that the CaSe is important in this example. On platforms that enforce case-sensitivity PNG and png are not the same locations.

