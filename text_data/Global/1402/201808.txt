
Last updated: October 26, 2014.
This privacy policy attempts to outline how Curious Concept websites collect and use information from their users.
Logs
Like most other websites, Curious Concept websites collect non-personal information about users whenever they access them. These logs may include the browser name, the type of computer and technical information about the means of connection to the site, such as the operating system, the Internet service providers utilized and other similar information.
Cookies
Cookies are files with small amount of data which are sent to your browser from a website and stored on your computer's hard drive. They can be used for tracking purposes, however Curious Concept websites specifically uses them to enhance user experiences by remembering preferences. You can instruct your browser to refuse all cookies or to indicate when a cookie is being sent.
Forms
With the exception of contact forms, Curious Concept websites do not collect any information submitted to forms.  Any information submitted to a contact form will be kept confidential.
Third Party
Curious Concept websites use reputable third party vendors to provide traffic analytics, security, and advertising services.
Traffic Analytics
Traffic analysis and monitoring of Curious Concept websites is provided by Google Analytics and Clicky Web Analtyics.  This information is used to direct resources, plan maintenance windows and improve usability.  Both Google and Clicky use cookies, in addition Google may use the data it collects to contextualize and personalize the ads of its own advertising network.
Security
Additional security measures on Curious Concept websites are enabled by CloudFlare.  To achieve this CloudFlare, like Curious Concept websites, collects non-personal information about users whenever they access Curious Concept websites and/or other sites on the Internet. These logs may include the browser name, the type of computer and technical information about the means of connection to the site, such as the operating system, the Internet service providers utilized and other similar information.
Advertising
Ads appearing on Curious Concept websites are delivered to you by Google Adsense and VigLink. Through it's DoubleClick cookie, Google and its partners are able recognize user's computers and serve targeted advertisements based on their prior visitation to Curious Concept websites and/or other sites on the Internet. For more information about Google Advertising, cookies, and how to opt-out, please visit Google's Advertising Privacy & Terms.  For more information about VigLink, including how to opt-out, please visit What’s VigLink.
Changes
This privacy policy may be revised at any time without notice.  We encourage users to frequently check this page for any changes.  Your continued use Curious Concept websites after any changes or revisions to this policy shall indicate your agreement with the revised privacy policy.
Questions?
If you have any questions about this Privacy Policy, or anything else related to Curious Concept web sites, please contact us.

