
Information we collect from you
We will collect personal information when you voluntarily provide it to us. For example, we collect personal information when you order, register for (or to use), or request information about VMware products or services, subscribe to marketing communications, request support, complete surveys, provide in product feedback, or sign up for a VMware event or webinar. We may also collect personal information from you offline, such as when you attend one of our events, during phone calls with sales representatives, or when you contact customer support. We may use this information in combination with other information we collect about you, as set forth in this Notice.

The personal information we collect may include contact information such as your name, address, telephone number or email address, user IDs and passwords, and contact preferences. It may also include professional information, such as your job title, department or job role, as well as the nature of your request or communication. We also collect information you choose to provide to us when completing any 'free text' boxes in our forms (for example, for event sign-up, product feedback or survey requests). In addition, we may collect personal information disclosed by you on our message boards, chat features, blogs and our other services to which you are able to post information and materials. Any information that is disclosed in those forums becomes public information and may therefore appear in public ways, such as through search engines or other publicly available platforms, and may be “crawled” or searched by third parties. It could also be read, collected or used by other users to send you unsolicited messages. Please do not post any information that you do not want to reveal to the public at large.

When purchasing VMware products or services, we may also collect billing and transactional information.

Examples of how we may use this information include:

To provide you with VMware products and services, and to process transactions.
To respond to your requests or provide information requested by you.
To send administrative or account-related information to you.
To manage your account and provide support or other services, such as product updates, fixes and product and service recommendations.
To obtain and understand your feedback or user experience.
To better understand our customers and end-users and the way they use and interact with VMware websites, mobile apps, products and services.
To provide a personalized experience, and implement the preferences you request.
To improve service reliability or improve features and functionality.
To enhance security, monitor and verify identity or service access, combat spam or other malware or security risks.
To provide you with marketing and promotional communications (where this is in accordance with the law).
To determine the effectiveness of marketing and promotional campaigns.
To post testimonials (with your prior consent).
To interact with you on third party social networks (subject to that network's terms of use).
To administer product downloads and to confirm your compliance with our licensing and other terms of use.
To communicate with you about our events or our partner events.
For quality control and staff training.



Information we automatically collect
When using VMware websites, online advertisements or marketing emails (collectively “Online Properties”) or mobile apps:
We automatically collect certain information when you use or access Online Properties or mobile apps. This information does not necessarily reveal your identity directly but may include information about the specific device you are using, such as the hardware model, operating system version, web-browser software (such as Firefox, Safari, or Internet Explorer) and your Internet Protocol (IP) address/MAC address/device identifier. In some countries, including the European Economic Area, this information may be considered personal information under applicable data protection laws.

We also automatically collect and store certain information in server logs such as: statistics on your activities on the Online Properties or mobile apps; information about how you came to and used the Online Property or mobile app; your IP address; device type and unique device identification numbers, device event information (such as crashes, system activity and hardware settings, browser type, browser language, the date and time of your request and referral URL), broad geographic location (e.g. country or city-level location) and other technical data collected through cookies, pixel tags and other similar technologies that uniquely identify your browser. We may also collect information about how your device has interacted with our website, including pages accessed and links clicked. In some countries, including the European Economic Area, this information may be considered personal information under applicable data protection laws.

For additional information about our use of cookies and other similar tracking technologies, please see our Cookie Notice.

Examples of how we may use this information include:

To provide you with the Online Property or mobile app.
To respond to your requests or provide information requested by you.
To obtain and understand your feedback or user experience.
To better understand our customers and end-users and the way they use and interact with the Online Property or mobile app.
To provide a personalized experience, and implement the preferences you request.
To improve service reliability or improve features and functionality.
To enhance security, monitor and verify identity or service access, combat spam or other malware or security risks.
To provide you with marketing and promotional communications (where this is in accordance with the law).
To determine the effectiveness of marketing and promotional campaigns.
To interact with you on third party social networks (subject to that network's terms of use).
To communicate with you about our events or our partner events.


When using VMware products and services:
In connection with your organization’s deployment of certain VMware products and services, we may automatically collect certain data relating to the performance and configuration of the VMware products and services, and our customer’s and their end-user's consumption of and interaction and use of such products and services ("Usage Data"). Usage Data is generally technical information obtained from product software or systems hosting the services, devices accessing these products and services and/or log files generated during such use which typically does not directly identify an end-user.

Certain VMware products and services participate in VMware’s customer product experience improvement programs and, depending on the program, our customers may have the ability to limit or control the extent of information collected. Where information is collected in these programs, the information is used by VMware for three core purposes:

to facilitate the delivery of that VMware product or service and this may include tracking entitlements, verifying compliance, monitoring services and infrastructure performance, ensuring integrity and stability of the services or simply monitoring or addressing technical issues so we can deliver the product or service to the standards VMware has committed to;

to make recommendations for and to support the use of VMware products and services; and

to diagnose issues with and improve VMware products and services and related user experience.


Some VMware products and services automatically collect and store certain Usage Data through cookies, pixel tags and other similar tracking technologies that uniquely identify a browser or device. For additional information about our use of cookies and other similar tracking technologies, please refer to our Cookie Notice.

Where applicable, customer agreements (including service descriptions), just-in-time notices, or other disclosures contained within the VMware products or services may describe more details about the collection and use of Usage Data by VMware for its own purposes. The technical information and identifiers collected are further described in our Trust & Assurance Center and, in some cases, will depend on the level of program participation selected by the customer.

Usage Data does not include any of a customer's content uploaded to VMware products or services. If you are an individual user of VMware products or services and you require more information regarding our Usage Data collection practices for products and services deployed by your organization, please contact your IT administrator.



Information from third party sources and affiliated companies
From time to time, we may obtain information about you from third party sources as permitted by applicable law, such as public databases, resellers and channel partners, joint marketing partners, social media platforms. Examples of the information we may receive from other sources include: account information, job role and public employment profile, service and support information, information about your product or service interests or preferences, page-view information from some business partners with which we operate co-branded services or joint offerings, and credit history information from credit bureaus. We may use the information we receive from you to better understand you and/or maintain and improve the accuracy of the records we hold about you as well as to position, promote or optimise our products and services. We may also use this information in conjunction with your browsing habits / preferences (as obtained from our data partners) and your contact details, professional information and VMware transaction history (as obtained from our customer relationship management database) to deliver targeted advertising and marketing to you, where permitted by applicable law and in accordance with your advertising / marketing preferences.

We may also obtain information about you from our affiliated companies, including Dell Technologies, Dell EMC, Pivotal, RSA, SecureWorks and VirtuStream (the Dell Family”) and, to the extent involved in the sales and marketing of our collective products and services, their appointed partners.

We will use this information to provide support as well as to enhance our ability to provide relevant marketing, products, services and recommendations to you, and also to help monitor, prevent and detect fraud.



Information we process on behalf of our customers
In the course of using our products and services, VMware customers and their end-users may submit or upload information to VMware's services for hosting or storage. This customer content is processed by VMware on behalf of the customer, and our privacy practices will be governed by the contract that we have in place with our customers, and this Privacy Notice will not apply to any personal information contained in such customer content. In accordance with such terms, VMware will process such customer content for the purposes of providing the relevant VMware services and in accordance with our customer's instructions. If you have any questions or concerns about how such information is handled, you should contact the person or entity who has contracted with VMware to use the VMware service to host or process this information (e.g. your employer). We will provide assistance to our customers to address any concerns you may have in accordance with the terms of our contract with them.

