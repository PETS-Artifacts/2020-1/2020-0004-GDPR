



Privacy Policy


Last Updated: September 1, 2010






GENERAL POLICY REGARDING CUSTOMER INFORMATION

FREE CONFERENCING CORPORATION DOES NOT SELL, RENT OR DISTRIBUTE CUSTOMERS PERSONAL INFORMATION INCLUDING NAME, EMAIL ADDRESS, PHONE NUMBER, ADDRESS OR COMPANY. We periodically issue technical and marketing information and are committed to an "opt in/opt out" permission based philosophy. If you voluntarily submit your e-mail address to receive information, we may subsequently issue information via e-mail. However, we respect the right to opt out and will remove e-mails from our list on request.



CUSTOMER INFORMATION COLLECTION

Registration Pages- Each of our products requires the customer to register with at least a name and email address. For internal purposes, we use this information to identify accounts and monitor usage. This information is also used to communicate with our customers (See Section 2: Customer Communications for full details). Our paid services including SimpleEvent, SimpleFlatRate, SimpleTollFree, SimpleVoiceBox and SimpleVoiceCenter require customer credit card information. This includes name, card number and expiration date, which is used for our or our agents' billing purposes only, and is not otherwise shared.
Telemarketing- Some customer information is obtained through telemarketing and marketing lists. Potential customer information is not registered into the Free Conferencing Corporation database unless express permission is given.
Customer Service- Additional customer information given through our customer service group is not currently appended to the customer account information.
Surveys and Contests- Customer information including phone number, address, email, title and company may be collected during a survey or contest. Since prizes may be awarded during these events, it is critical that Free Conferencing Corporation collect full customer details. This information is stored in a separate database and may be used for further marketing activities.



CUSTOMER COMMUNICATIONS

Email Contact- Free Conferencing Corporation uses email to make contact with its customers. Customers receive an initial registration email with all account details listed. After each call, customers receive call detail reports via email. Finally, Free Conferencing Corporation uses email to communicate promotions, product updates and technical advisories with their customers.
Opting Out of Contact- Opting out of contact with Free Conferencing Corporation can be done by unsubscribing from email lists. There are 3 main lists that a customer will need to unsubscribe from; call detail reports, email marketing and technical advisories. Unsubscribing from technical advisories will result in the cancellation of the customers account.



SECURITY

We use reasonable precautions to protect our customers' personal information and to store it securely. Sensitive information that is transmitted to us online (such as credit card number) is encrypted and is transmitted to us securely. In addition, access to all of our customers' information, not just the sensitive information mentioned above, is restricted. Only employees who need the information to perform a specific job (for example, a billing clerk or a customer service representative) are granted access to personally identifiable information. Finally, the servers on which we store personally identifiable information are kept in a secure environment.



LINKS

Our web sites contain links to other sites. Free Conferencing Corporation is not responsible for the privacy practices or content of such other sites. We encourage our users to be aware when they leave our site and to read the privacy statements of each web site to which we may link that may collect personally identifiable information.



CHILDREN

Free Conferencing Corporation products, services and websites are not directed at individuals under thirteen years of age, and Free Conferencing Corporation does not intend to collect any personally-identifiable information from such individuals.



CALIFORNIA PRIVACY RIGHTS

Beginning on January 1, 2005, California Civil Code Section 1798.83 permits those customers that are California residents to request that Free Conferencing Corporation not share your personal information with third parties for their direct marketing use. To make such a request, please write to:

California Privacy Rights

Free Conferencing Corporation

4300 E. Pacific Coast Hwy

Long Beach, CA 90804



CONTACT AND QUESTIONS


For any additional information or to contact Free Conferencing Corporation you can email
info@freeconferencecall.com
or call
(844) 844-1322.










