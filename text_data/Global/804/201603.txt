



	St John Ambulance - the nation’s leading first aid charity



var _gaq = _gaq || [];
var pluginUrl = '//www.google-analytics.com/plugins/ga/inpage_linkid.js';
_gaq.push(['_require', 'inpage_linkid', pluginUrl]);
_gaq.push(['_setAccount', 'UA-956871-1']);
_gaq.push(['_setDomainName', 'sja.org.uk']);
_gaq.push(['_trackPageview']);
(function() {
var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();











//<![CDATA[
var theForm = document.forms['aspnetForm'];
if (!theForm) {
    theForm = document.aspnetForm;
}
function __doPostBack(eventTarget, eventArgument) {
    if (!theForm.onsubmit || (theForm.onsubmit() != false)) {
        theForm.__EVENTTARGET.value = eventTarget;
        theForm.__EVENTARGUMENT.value = eventArgument;
        theForm.submit();
    }
}
//]]>






  window.fbAsyncInit = function() {
    FB.init({
      appId      : '344860018939025', // App ID
      status     : true, // check login status
      cookie     : true, // enable cookies to allow the server to access the session
      xfbml      : true  // parse XFBML
    });
  if (typeof facebookLikeInit == 'function') {
        facebookLikeInit();
    }
  if (typeof facebookUnlikeInit == 'function') {
        facebookUnlikeInit();
    }
  };

  // Load the SDK Asynchronously
  (function(d){
     var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement('script'); js.id = id; js.async = true;
     js.src = "//connect.facebook.net/en_GB/all.js";
     ref.parentNode.insertBefore(js, ref);
   }(document));

(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_GB/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

window.twttr = (function (d,s,id) {
      var t, js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return; js=d.createElement(s); js.id=id;
      js.src="//platform.twitter.com/widgets.js"; fjs.parentNode.insertBefore(js, fjs);
      return window.twttr || (t = { _e: [], ready: function(f){ t._e.push(f) } });
    }(document, "script", "twitter-wjs"));
    

            window.___gcfg = { lang: 'en-GB' };

            (function () {
                var po = document.createElement('script'); 
                po.type = 'text/javascript'; 
                po.async = true;
                po.src = 'https://apis.google.com/js/plusone.js';
                var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
            })();
        

//<![CDATA[
function facebookLikeInit() {
  FB.Event.subscribe('edge.create', function(targetUrl) {
    _gaq.push(['_trackSocial', 'facebook', 'like', targetUrl]);
  });
}function facebookUnlikeInit() {
  FB.Event.subscribe('edge.remove', function(targetUrl) {
    _gaq.push(['_trackSocial', 'facebook', 'unlike', targetUrl]);
  });
}function trackTwitter(intent_event) {
    if (intent_event) {
      var opt_pagePath;
      if (intent_event.target && intent_event.target.nodeName == 'IFRAME') {
            opt_target = extractParamFromUri(intent_event.target.src, 'url');
      }
      _gaq.push(['_trackSocial', 'twitter', 'tweet', opt_pagePath]);
    }
  }

  //Wrap event bindings - Wait for async js to load
  twttr.ready(function (twttr) {
    //event bindings
    twttr.events.bind('tweet', trackTwitter);
  });

function extractParamFromUri(uri, paramName) {
  if (!uri) {
    return;
  }
  var regex = new RegExp('[\\?&#]' + paramName + '=([^&#]*)');
  var params = regex.exec(uri);
  if (params != null) {
    return unescape(params[1]);
  }
  return;
}//]]>









St John Ambulance









Site search








Font size:

Regular font size
Medium font size
Large font size 



Additional navigation
Jobs
Media
Contact
SJA Connect



As the nation's leading first aid charity, we want to teach everyone simple, life saving skills.
Donate


Site Navigation

Home
What we do
Volunteer
Support us
Training courses
First aid advice
Young people
Schools
Shop
DONATE





facebook
Twitter
LinkedIn
YouTube










Learn the song, in case things go wrong. Click here
for more











            Tweet #babyCPR















First aid courses
Learn the skills to save lives

  var CourseSearchFormId = "Template_CourseSearchBox1_";

  try {
    addControlToValidate(CourseSearchForm);
  } catch (err) {
    //console.log(err);
  }

  function GetCourseCode(item) {
    var CourseCode = document.getElementById(item.id);
    var StoredCourseCode = document.getElementById("Template_CourseSearchBox1_StoredCourseCode");
    StoredCourseCode.value = item.value;
  }

  function CourseSearchForm() {
    var objPostcode = new getObj(CourseSearchFormId + "Postcode");
    var objCourseCode = new getObj(CourseSearchFormId + "CourseCode");
    var objGo = new getObj(CourseSearchFormId + "goTC");
    var PassValidation = true;
    var strErr = "";
    objPostcode.obj.className = 'NormField';
    if (ButtonPressed != "CourseSearch") {
      // do something
    } else {
      //a postcode would start like this
      var pc = objPostcode.obj.value.toUpperCase().replace(/\s/g, "");
      var pcExp = /^[A-Z]{1,2}[0-9]{1,2}[A-Z]?([0-9]{1}[A-Z]{2})?$/i
      var townExp = /^[A-Za-z\s',]{3,}$/i
      if (pcExp.test(pc)) {
        pcExp = /^[A-Z]{1,2}[0-9]{1,2}[A-Z]?[0-9]{1}[A-Z]{2}$/i
        if (pcExp.test(pc)) {
          pc = pc.substr(0, pc.length - 3) + " " + pc.substr(pc.length - 3)
        }
        objPostcode.obj.value = pc
      } else if (townExp.test(objPostcode.obj.value)) {
        //we're going to assume a town was entered
      } else {
        strErr += "\n- The location you have entered does not appear to be valid as a UK postcode or town, please re-enter or use the town name if it is correct";
        objPostcode.obj.className = 'ErrField';
        PassValidation = false;
      }
      if (objCourseCode.obj[objCourseCode.obj.selectedIndex].value == 'none') {
        strErr += "\n- Please select the course you would like to do";
        objCourseCode.obj.className = 'ErrField';
        PassValidation = false;
      }
    }
    if (!PassValidation) {
      alert("We had a problem with your information for the course search, please check the following and try again:\n" + strErr);
    }
    return PassValidation
  }
  function UpdateCourses(courseType) {
    $('.courseSelects select').hide();
    $('.courseSelects select').val('none');
    $('.courseSelects select[id$="' + courseType + '"]').show();
  }






Find a course






              Workplace
            


              Public
            





Please select a course -->First aid at work (initial)First aid at work (requalification)Emergency first aid at work (previously 'Appointed Persons')Automated external defibrillation (AED)AED - requalificationAnnual refresherProfessional drivers first aid (Driver CPC)Cardiopulmonary Resuscitation (CPR)Anaphylaxis first aidPaediatric first aidSchools first aidFire marshalFire marshal (refresher)Managing safely - IOSHWorking safely - IOSHRisk assessment - BSC level 2Health and safety basicsMoving and handling principlesDSE Risk Assessment - BSC Level 2Sports first aidHealthcare professional resuscitation


Please select a course -->Anaphylaxis first aidPaediatric first aidEssential first aidEssential first aid (all ages)Essential first aid (child and infant)Sports first aidBasic first aidEssential and basic first aidBaby first aid












Calculate your workplace requirements
View all of our courses at a glance





What we do



First aid training
Equipping thousands of people with the skills to save a life



First aid at events
Providing advanced first aiders at public events





Emergency response
Supporting the emergency services



Volunteer programmes
A wide range of opportunities




Learn first aid


Baby first aid course
Get first aid advice
Nursery Rhymes Inc.
Download our free app
Play our game
Watch our videos



Watch: How to give CPR to a child







First aid skills finder


Choking advice for parents
Using a defibrillator (AED)
Resuscitation (CPR)
Cardiac arrest
Hot and cold conditions
Broken bones and fractures
Head injuries
Heart attack
The recovery position
Poisoning
Allergic reactions
Shock
Stroke
Unconscious and not breathing
Unconscious and breathing
Severe bleeding

Go








Support our activities

Donate now
£10 allows one young person to attend a first aid course


Fundraise
Join our growing team of runners, skydivers and supporters


Join us
Search for job opportunities or
volunteer vacancies that help save
lives



Latest news



Top charities support Bill to make first aid compulsory for UK driving licence



All parents should learn first aid say couple whose toddler's life was saved by St John Ambulance 'guardian angel'



St John Ambulance provides support for Europe's biggest ever disaster response drill




Everyday Heroes - last chance to nominate!




Help us celebrate the nation's life savers - nominate your
first aid heroes today!




Big First Aid
Lesson
Watch the latest Big First
Aid Lesson with your class
        

Download our training brochure
 Download our workplace training brochure for
first aid, health and safety, and fire training courses
        

Been
treated or transported by us?
 We value your feedback. Tell us about your
experience in our patient survey.
        









Stay connected
Facebook
Twitter
LinkedIn
YouTube









What we do

Ambulance services
Event services
Emergency response
Community first responders
Community projects



Volunteer

Find your volunteer role

Volunteer vacancies

Supporting our volunteers

Volunteer recruitment process
Under
18



Support us

Make a donation
Fundraise
Our campaigns
Nursery
Rhymes Inc. 
Corporate supporters



Training
courses

First aid courses
Healthy and safety courses
Requirements calculator
Download our brochure
First aid in schools



First aid
advice

Unconscious and not breathing
Unconscious but breathing
Heart attack
Choking
Wounds and bleeding



Young people

Badgers
Cadets
Schools
RISE










St John Ambulance Supplies
Everyday Heroes






						  © 2015 St John Ambulance. Please read our
						  legal and site information
						  and
						  privacy policy. 
              

              St John Ambulance, 27 St John's Lane, London EC1M 4BU
              

              Registered charity no. 1077265/1. A company registered in England no. 3866129
					  












//<![CDATA[
detectBrowser();processButtonLinks();//]]>



var _wow = _wow || [];
(function () {
try{
_wow.push(["setClientId", "5c01480c-6e0b-406f-a245-ff493b7cff08"]);
_wow.push(["enableDownloadTracking"]);
_wow.push(["trackPageView"]);
var d = document, g = d.createElement("script"), s = d.getElementsByTagName("script")[0];
g.type = "text/javascript"; g.defer = true; g.async = true;
g.src = "//t.wowanalytics.co.uk/Scripts/tracker.js";
s.parentNode.insertBefore(g, s);
}catch(err){}})();


 
var _qevents = _qevents || [];

(function() {
var elem = document.createElement('script');
elem.src = (document.location.protocol == "https:" ? "https://secure" : "http://edge") + ".quantserve.com/quant.js";
elem.async = true;
elem.type = "text/javascript";
var scpt = document.getElementsByTagName('script')[0];
scpt.parentNode.insertBefore(elem, scpt);
})();

_qevents.push(
{qacct:"p-39tUQCyLv-bFA",labels:"_fp.event.Homepage"}
);









