



        Norton Rose Fulbright | Global law firm        






























    window.jQuery || document.write('<script src="/_resources/nortonrosefulbright/_include/js/jquery-1.8.3.min.js"><\\/script>')
  

























 Use of cookies byNorton Rose Fulbright
We use cookies to deliver our online services. Details and instructions on how to disable those cookies are set out at  nortonrosefulbright.com/cookies-policy. By continuing to use this website you agree to our use of our cookies unless you have disabled them.   














Search our site



	$("#global_search").click(function () {
		
		return redirect(this);
		
		
	});

	//$('#srch_box').keyup(function (e) {
	//	if (e.keyCode == 13) {
			
	//		$("#global_search").click();
			
	//	}
	//});

	//$('#srch_box').keydown(function (e) {
	//	if (e.keyCode == 13) {

	//		$("#global_search").click();

	//	}
	//});

	
	

	

		


Location
Global 









About us

Diversity and inclusion
Clients
Business principles
Awards and accolades
Global coverage
Corporate partnership
The magazine Re:
Alumni
Governance structure
Risk management
Contact us



People

Search people
Browse people



Our services


Knowledge




Watch 
Videos



Read 
Publications



Participate 
Events
Learning and development 
Meet the knowledge team




Online services, resources, and tools
Technical resources
Stay connected







News

Press releases
Press coverage
Media information
Press contacts



Careers

Working Start



Corporate responsibility

Pro bono and volunteering
Sustainability policy
Charitable activities
Special Olympics – Global Charitable Initiative 2015-16









	
		$(document).ready(function(){
		var cookieLoc = $.cookie('currentLocation');
		if (cookieLoc == null ){
			$.colorbox({href: "/locations.aspx div#locations", opacity:0.7, onComplete:setLoc});
		} else if (window.location.href != "http://www.nortonrosefulbright.com" + cookieLoc)  {
			window.location.href = "http://www.nortonrosefulbright.com" + cookieLoc;
		} 
	}); 






 
Inside Africa

A business resource for organisations operating across Africa
Find out moreabout Inside Africa






 
About us

Norton Rose Fulbright is a global law firm. We provide a full business law service.
Find out moreabout About us






 
COP21

The Paris Agreement
Read moreabout COP21






 
Legalflyer

Topical issues and law reforms in the aviation sector
Read moreabout Legalflyer






 
Capital Markets Union

Your complete resource
Read moreabout Capital Markets Union









 
Insurance focus

Analysis of key legal developments in the insurance industry
Read moreabout Insurance focus






 
2015 Litigation Trends Annual Survey


Download the reportabout 2015 Litigation Trends Annual Survey






 
Legalseas

Topical issues and law reforms in the shipping sector
Read our shipping newsletterabout Legalseas






 
Working Start

Giving people with disabilities a Working Start
Read moreabout Working Start






 
Cryptocurrency

Chapter 4: Cryptocurrency litigation risks
Register for our global guide to cryptocurrenciesabout Cryptocurrency






 
Cultivate

Delivering market insight into the global food and agribusiness sector

Download the newsletterabout Cultivate









 
Brand protection blog


Learn about the blogabout Brand protection blog






 
Hydraulic fracking blog


Read our US blogabout Hydraulic fracking blog






 
Financial services: Regulation tomorrow


Subscribe to our global blogabout Financial services: Regulation tomorrow






 
Norton Rose Fulbright'sblog network

Follow the latest trends in niche legalareas, including fracking, brandingand financial regulation.
about Norton Rose Fulbright'sblog network






 
Financial institutions legal snapshot


Read our South African blogabout Financial institutions legal snapshot





 









			#Result
			{
				cursor: pointer;
			}
		

		var hdnJurisdiction = $('#hdnJurisdiction').val();
		var hdnLanguage = $('#hdnLanguage').val();
		var hdnOtherLanguagePrefix = $('#hdnOtherLanguagePrefix').val();

		

People



Search our people for contact information or use our advanced search.

Search by Name


	$('#gl_search_name').keyup(function (e) {
		if (e.keyCode == 13) {
			
			SubmitPS("1");

		}
	});
	


All locations

All locations
Abu DhabiAfricaAlmatyAmsterdamAthensAustinBahrainBangkokBeijingBogotáBrisbaneBrusselsBujumburaCalgaryCape TownCaracasCasablancaDallasDar es SalaamDenverDubaiDurbanFrankfurtHamburgHarareHong KongHoustonIndiaIsraelJakartaJohannesburgKampalaLondonLos AngelesMelbourneMilanMinneapolisMontréalMoscowMunichNew YorkNordic regionOttawaParisPerthPiraeusPittsburgh-SouthpointeQuébecRio de JaneiroRiyadhSan AntonioShanghaiSingaporeSt. LouisSydneyTokyoTorontoTurkeyWarsawWashington, DC





Search our people









Global coverage




View our global coverage







Key industries

Discover more about our key industry sectors:

Financial institutions
Energy
Infrastructure, mining and commodities
Transport
Technology and innovation
Life sciences and healthcare



 












Explore our site

About us
Search our people
Our services
Knowledge
Search news
Global careers
Corporate responsibility
Online services, resources, and tools



Key industries

Financial institutions
Energy
Infrastructure, mining and commodities
Transport
Technology and innovation
Life sciences and healthcare



Offices

Contact us






	Abu Dhabi • Almaty • Amsterdam • Athens • Austin • Bahrain • Bangkok • Beijing • Bogotá • Brisbane • Brussels • Bujumbura** • Calgary • Cape Town • Caracas • Casablanca • DallasDar es Salaam • Denver • Dubai • Durban • Frankfurt • Hamburg • Harare** • Hong Kong • Houston • Jakarta* • Johannesburg • Kampala** • London • Los Angeles • Melbourne • MilanMinneapolis • Montréal • Moscow • Munich • New York • Ottawa • Paris • Perth • Piraeus • Pittsburgh-Southpointe • Québec • Rio de Janeiro • Riyadh* • San Antonio • ShanghaiSingapore • St. Louis • Sydney • Tokyo •Toronto • Warsaw • Washington DC *associate office **alliance


 Sitemap 
 Legal notices 
 Privacy notice 
 Cookies policy 
 Remote access 
 Alumni 
 Suppliers 



LinkedIn
Twitter
Stay connected


More Sharing Services



RSS Feed 




    <!-- AddThis Button END-->
  








  var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
  document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
  

  var pageTracker = _gat._createTracker("UA-2452429-1");
  pageTracker._trackPageview();
  
 nrcheckcookie(); 



