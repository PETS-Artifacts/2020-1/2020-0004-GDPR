





  




Cedars-Sinai – A Non-Profit Hospital in Los Angeles   







 if (!window.console) console = {log: function() {}}; 







dataLayer = [];
#findaDoctor{float:left;}



#jsAdvice {display: block;}


		$(document).ready(function() {

			var bordervalue = $('table[border]').attr('border');
			if (bordervalue == 1){
				$("table").css("border", "1px solid #000");
				$("td").css("border", "1px solid #000");
			}
		
			//Detailed insurance info expand and collapse
			//$(".insuranceDetailInfo").hide();
			$(".insuranceDetailTop").addClass("hand");
			$(".insuranceDetailTop").find("img").attr("src","/resources/images/white_plus.gif");
			$(".insuranceDetailTop").click(function(){
				if($(this).next(".insuranceDetailInfo").css("display")=="none"){
					$(this).next(".insuranceDetailInfo").show();
					$(this).find("img").attr("src","/resources/images/white_minus.gif");
				}
				else{
					$(this).next(".insuranceDetailInfo").hide();
					$(this).find("img").attr("src","/resources/images/white_plus.gif");
				}
			});
		var ua = navigator.userAgent.toLowerCase();
		var isAndroid = ua.indexOf("android") > -1; //&& ua.indexOf("mobile");
		if(isAndroid) {
			$('#androidAppBanner').css('display','block');
		}
		$('#demo').moodular({
					/* core parameters */
						// effects separated by space
						effects: 'fade',
						// controls separated by space
						controls: 'pagination stopOnMouseOver',
						// if you want some yummy transition
						easing: '',
						// step 
						step: 1,
						// selector is to specify the children of your element (tagName)
						selector: 'li',
						// if timer is 0 the carrousel isn't automatic, else it's the interval in ms between each step
						timer: 7000,
						// speed is the time in ms of the transition
						speed: 2000,
						// queuing animation ?
						queue: false,
					/* parameters for controls or effects */
						// pagination control
						pagination: jQuery('#index')
				});
				

		$("#callback a").click(function (){
					$("#callBackForm").attr("src","http://cti.connextions.com/?company=cedarssinai&track=MDFIND1");
					$("#overlayContainer").height($("body").height());
					var overlayTop = $(window).scrollTop() + ($(window).height() - $("#callbackWrapper").height()) / 2;
					$("#callbackWrapper").css("top",overlayTop);
					$("#overlayContainer").show();
					$("#callbackWrapper").show();
					$("#colorbox").hide();
					$("#cboxOverlay").hide();
                    
					return false;
				});
		$("#rightColumnVideoImage a").click(function (){
					$("#overlayContainer").height($("body").height());
					var overlayTop = $(window).scrollTop() + ($(window).height() - $("#videoWrapper").height()) / 2;
					$("#videoWrapper").css("top",overlayTop);
					if(isNaN($(this).attr("data-ytID"))){
						$('#im_box_ep').hide();
						$('#im_player_ep').remove();
						$("#ytVideo").show().attr("src","http://www.youtube.com/embed/"+$(this).attr("data-ytID")+"?autoplay=1");
					}
					else{
						$("#ytVideo").hide();
						$('#im_box_ep').show();
						im_is_loaded_ep({ video_id:$(this).attr("data-ytID") });
					}
					
					$("#overlayContainer").show();
					$("#videoWrapper").show();
					return false;
				});
	    $("#overlayClose,.overlayClose2,#overlayContainer").click(function () {
                    $("#overlayContainer").hide();
                    $("#videoWrapper").hide();
                    $("#ytVideo").attr("src","about:blank");
                    $('#im_player_ep').remove();
                    $("#callbackWrapper").hide();
                    $("#callBackForm").attr("src","about:blank");
					$("#mapWrapper").hide();
                });
				
				
				
                $("#researchWrapper .learnMoreInfo").hide();
                $("#researchWrapper .learnMoreLabMember").addClass("hand"); //.learnMore
                $("#researchWrapper .learnMoreLabMember").click(function(event){ //.learnMore
                    // prevents scrolling when clicking read more link
                    if(document.all){
                        event.returnValue = false;
                    }
                    else{
                        event.preventDefault();
                    }

                    if($(this).next("#researchWrapper .learnMoreInfo").css("display") == "none"){

                        $(this).next("#researchWrapper .learnMoreInfo").show();
                        $(this).css("background-image", "url(/resources/images/tiny_arrow_opened.png)");
                    }
                    else{

                        $(this).next("#researchWrapper .learnMoreInfo").hide();
                        $(this).css("background-image", "url(/resources/images/tiny_arrow.png)");
                    }
                });
				
				$('#top_nav li').click( function() {
    				$(this).siblings().removeClass('selected');
  					$(this).addClass('selected');
 				 });
 				$(".youtube").colorbox({iframe:true, innerWidth:640, innerHeight:390,maxWidth:'90%',maxHeight:'90%'});
				$(".cbl a.137135").colorbox({rel:'137135', transition:"none",maxWidth:'90%',maxHeight:'90%'});
				$(".cbr a.137135").colorbox({rel:'137135', transition:"none",maxWidth:'90%',maxHeight:'90%'});
				$(".cbc a.137135").colorbox({rel:'137135', transition:"none",maxWidth:'90%',maxHeight:'90%'});

				
    			var nav_items = new Array("Patients/Health","Patients/Programs","edu/Patients","edu/Medical","edu/Research","edu/Education");
    			var the_url_path = location.href;
    			for (var i=0;i<nav_items.length;i++)
    			{
    			     if (the_url_path.match(nav_items[i] ,'gi')){
    			          $('*[data-path="'+nav_items[i]+'"]').addClass("selected");
						  break;
    			     }
    			} 
				
				$('#conditions_submit').mouseleave(function() {
 					$("#greyBoxSearchErrorMsg").html('&nbsp;');
				}); 
				$(".videoLink").colorbox({inline:true,innerWidth:800,innerHeight:452});
					

					$('#resFindDoctor, #resHomeFindDoctor').colorbox({inline:true});
					
					$('#showLocate .more').toggle(function(){
						$('#showLocate').css('border-color','#000');
						$('#locate').toggle();
						$('#showLocate .more').html('Less');
					}, function() {
						$('#locate').toggle();
						$('#showLocate .more').html('More');
						$('#showLocate').css('border-color','#fff');
					});
					$('#searchIcon').toggle(function(){
					        $(this).css('background-position', '-162px -54px');
							$('#search #suggestion_form').toggle();
							$('#resMenu #resFindDoctor').toggle();
							$('#search .searchform-text').focus();
					    }, function() {
					        $(this).css('background-position', '-129px -54px');
							$('#search #suggestion_form').toggle();
							$('#resMenu #resFindDoctor').toggle();
							$('#search .searchform-text').focus();
					});
					$('#resMainNav').toggle(function(){
				        $(this).css('background-position', '-162px -54px');
						$('#top_nav').toggle();
						$('#locate').toggle();
					}, function() {
					        $(this).css('background-position', '9px -226px');
							$('#top_nav').toggle();
							$('#locate').toggle();
					});
					
					if( $('#tertiaryContainer').length )         // use this if you are using id to check
					{
					     $('#resLeftMenu').addClass('leftMenuBB');
					     $('#contentTitle').addClass('indentContentTitle');
					}
				
				    var $window = $(window);
				    var $pane = $('#pane1');
				
				    function checkWidth() {
				        var windowsize = $window.width();
				        if ((windowsize > 1000)&&($('#locate').css('display') == 'none')) {
				            $('#locate').css('display','block');
				        }
				        if ((windowsize > 600)&&($('ul#top_nav').css('display') == 'none')) {
				        	$('ul#top_nav').css('display','block');
				        }
				
				    }
				    checkWidth();
				    $(window).resize(function(){
						checkWidth();
				    });
			});
	




(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-BQDX');



(function(){var g=function(e,h,f,g){
this.get=function(a){for(var a=a+"=",c=document.cookie.split(";"),b=0,e=c.length;b<e;b++){for(var d=c[b];" "==d.charAt(0);)d=d.substring(1,d.length);if(0==d.indexOf(a))return d.substring(a.length,d.length)}return null};
this.set=function(a,c){var b="",b=new Date;b.setTime(b.getTime()+6048E5);b="; expires="+b.toGMTString();document.cookie=a+"="+c+b+"; path=/; "};
this.check=function(){var a=this.get(f);if(a)a=a.split(":");else if(100!=e)"v"==h&&(e=Math.random()>=e/100?0:100),a=[h,e,0],this.set(f,a.join(":"));else return!0;var c=a[1];if(100==c)return!0;switch(a[0]){case "v":return!1;case "r":return c=a[2]%Math.floor(100/c),a[2]++,this.set(f,a.join(":")),!c}return!0};
this.go=function(){if(this.check()){var a=document.createElement("script");a.type="text/javascript";a.src=g+ "&t=" + (new Date()).getTime();document.body&&document.body.appendChild(a)}};
this.start=function(){var a=this;window.addEventListener?window.addEventListener("load",function(){a.go()},!1):window.attachEvent&&window.attachEvent("onload",function(){a.go()})}};
try{(new g(100,"r","QSI_S_ZN_a5CJAKTSOthct9P","//zna5cjaktsothct9p-perficient.siteintercept.qualtrics.com/WRSiteInterceptEngine/?Q_ZID=ZN_a5CJAKTSOthct9P&Q_LOC="+encodeURIComponent(window.location.href))).start()}catch(i){}})();







Follow us:
Follow Us on Twitter
Like Us on Facebook
Follow Us on Google+
Watch videos on our Youtube channel








Find a Doctor
Main Menu
Search





More



Contact Us
Locations & Directions
Medical Staff Directory
Careers
Giving
International Patients



A
A
A























Find a Doctor
Conditions & Treatments
Programs & Services
Patients & Visitors Guide
For Medical Professionals
Research
Education
















Marina Del Rey Hospital is now a Cedars-Sinai affiliate.

Expanding access to high-quality healthcare throughout the Los Angeles region. Learn More>> 












Healthy Heartbeats for Life

Their hearts were in danger. Learn how they got back into rhythm. >> 














By sharing your unique perspective, you can help improve the healthcare experience for yourself and others. Learn More>> 














Search Clinical Trials
Explore Cedars-Sinai's database of active clinical trials.
Learn More







My CS-Link
A secure online tool that connects you to your personal health information.
Learn More







Patient & Visitor Information
Maps, directions and visitor resources.
Learn More







Newsroom
Learn about the latest in research and patient care at Cedars-Sinai.
Learn More






Find a Doctor

Call
1-800-CEDARS-1
(1-800-233-2771)
Available 24 Hours A Day
or
Online Referral
Schedule a Callback



Quick Links

Career Opportunities 

About Us

International Patients

Community Benefit

Locations & Directions

Mission, Vision and Values

Medical Network

Quality Measures
 








 About Us
Newsroom
Career Opportunities 
Community Benefit
International Patients
 Website Terms and Conditions
Privacy Policy
Staff Link
Sitemap
 
2016 © Cedars-Sinai. All Rights Reserved. A 501(c)(3) non-profit organization





close







close






 FmRmYmRmYHgHZ2VseAFkbWRgeAZmY2F4E2RnYm1kYg==

We noticed JavaScript is turned off in your browser settings.  For a better viewing experience, enable JavaScript.  If you're unsure of how to do this, click here to download a new browser.


