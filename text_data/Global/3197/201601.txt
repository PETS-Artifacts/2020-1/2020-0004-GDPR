



    [ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak], .ng-cloak, .x-ng-cloak {display: none !important;}
    



    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-4245914-66', {cookieDomain: 'none', });
    ga('set', 'page', '/page/privacy-policy');
    ga('set', 'title', '');
    ga('set', 'dimension1', 'Basic Page');
    ga('send', 'pageview', '/page/privacy-policy');
    window._gaSent = true;

    ga('create', 'UA-1778701-17', {name: '_tempestTracker', cookieDomain: 'none', });
    ga('_tempestTracker.set', 'page', '/page/privacy-policy');
    ga('_tempestTracker.set', 'title', '');
    ga('_tempestTracker.set', 'dimension1', 'Basic Page');
    ga('_tempestTracker.send', 'pageview', '/page/privacy-policy');

    




    /*globals _sf_startpt, window*/
    'use strict';
    var _sf_startpt = (new Date()).getTime();
    window._pageLoadStart = new Date();
    window._pageRunId = Math.random();
    





































































      (function(d) {
        var config = {
          kitId: window.phxSiteConfig.typekitId,
          scriptTimeout: 3000,
          async: true
        },
        h=d.documentElement,t=setTimeout(function(){h.className=h.className.replace(/\bwf-loading\b/g,"")+" wf-inactive";},config.scriptTimeout),tk=d.createElement("script"),f=false,s=d.getElementsByTagName("script")[0],a;h.className+=" wf-loading";tk.src='https://use.typekit.net/'+config.kitId+'.js';tk.async=true;tk.onload=tk.onreadystatechange=function(){a=this.readyState;if(f||a&&a!="complete"&&a!="loaded")return;f=true;clearTimeout(t);try{Typekit.load(config)}catch(e){}};s.parentNode.insertBefore(tk,s)
      })(document);
    




































    require(['main'], function (main) {

    });
    




