



    [ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak], .ng-cloak, .x-ng-cloak {display: none !important;}
    



    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
    

        window.googleAnalyticsConfig = {
    "trackers": [
        {
            "name": "_tempestTracker",
            "id": "UA-1778701-17",
            "cookieDomain": "www.maxim.com",
            "sendEvents": true,
            "sendPerformanceMetrics": true,
            "customDimensionMap": {
                "pageType": 1,
                "sectionName": 2,
                "authorName": 3,
                "publicationQuarter": 4,
                "channelName": 11
            }
        },
        {
            "id": "UA-4245914-66",
            "cookieDomain": "www.maxim.com",
            "sendEvents": true,
            "sendPerformanceMetrics": false,
            "customDimensionMap": {
                "channelName": "4"
            }
        }
    ],
    "initialModel": {
        "pageType": "Basic Page",
        "path": "\/page\/privacy-policy",
        "channelName": "Web"
    },
    "experimentId": null
} || {trackers: []};
        (function (googleAnalyticsConfig) {

        var experimentVariant = null;
        if (window.cxApi) {
            experimentVariant = cxApi.chooseVariation(googleAnalyticsConfig.experimentid);
        }

        
        googleAnalyticsConfig.sendPageView = function (model) {
            model.experimentVariant = experimentVariant;
            for (var i = 0; i < googleAnalyticsConfig.trackers.length; i++) {
                var tracker = googleAnalyticsConfig.trackers[i];
                if (tracker.experimentOnly && ! experimentVariant) {
                    continue;
                }
                var config = {};
                var prefix = '';
                if (tracker.name) {
                    config.name = tracker.name;
                    prefix = tracker.name + '.';
                }
                if (tracker.cookieDomain) {
                    config.cookieDomain = tracker.cookieDomain;
                }
                if (tracker.sampleRate) {
                    config.sampleRate = tracker.sampleRate;
                }
                ga('create', tracker.id, config);
                ga(prefix + 'set', 'page', model.path);
                ga(prefix + 'set', 'title', model.title);
                var cd = tracker.customDimensionMap;
                for (var k in cd) {
                    if (! cd[k] || ! cd.hasOwnProperty(k)) {
                        continue;
                    }
                    var val = model[k];
                    if (val !== undefined && val !== null) {
                        ga(prefix + 'set', 'dimension' + cd[k], '' + val);
                    }
                }
                ga(prefix + 'send', 'pageview', model.path);
            }
        };

        var model = googleAnalyticsConfig.initialModel;
        model.positionInSession = 'initial';
        googleAnalyticsConfig.sendPageView(model);
        window._gaSent = true;

        })(window.googleAnalyticsConfig);
    

    /*globals _sf_startpt, window*/
    'use strict';
    var _sf_startpt = (new Date()).getTime();
    window._pageLoadStart = new Date();
    window._pageRunId = Math.random();
    



































































        (function(d) {
            var config = {
                kitId: window.phxSiteConfig.typekitId,
                scriptTimeout: 3000,
                async: true
            },
            h=d.documentElement,t=setTimeout(function(){h.className=h.className.replace(/\bwf-loading\b/g,"")+" wf-inactive";},config.scriptTimeout),tk=d.createElement("script"),f=false,s=d.getElementsByTagName("script")[0],a;h.className+=" wf-loading";tk.src='https://use.typekit.net/'+config.kitId+'.js';tk.async=true;tk.onload=tk.onreadystatechange=function(){a=this.readyState;if(f||a&&a!="complete"&&a!="loaded")return;f=true;clearTimeout(t);try{Typekit.load(config)}catch(e){}};s.parentNode.insertBefore(tk,s)
        })(document);
    




































    require(['main'], function (main) {
        // unbuilt DEV code
    });
    


