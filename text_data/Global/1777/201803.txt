INFORMATION HELPS MAKE CONSERVATION HAPPEN
PRIVACY POLICY
At WWF-UK we rely on the generosity and support of individuals like you to carry out our vital conservation work. That is why we want to be completely transparent about why we need the personal details we request when you engage with us and how we will use them.
As you browse our website and whenever you communicate with us, we collect information. It deepens our understanding of what works and what doesn’t, which helps make our communications more effective. The more information we have, the more efficiently we are able to operate and the more funds we can direct into conservation.
We take protecting your privacy very seriously and will always take all reasonable steps within our power to make sure your information is safe.
Please read this policy carefully, along with our Terms and Conditions and any other documents referred to in this policy, to understand how we collect, use and store your personal information.
By providing us with your personal information, you consent to the collection and use of any information you provide in accordance with this privacy policy.
We may update this policy from time to time without notice to you, so please check it regularly, particularly if you are sending personal information to us.
If you have any questions please contact our Supporter Care team on 01483 426333, supportercare@wwf.org.uk, or at The Living Planet Centre, Brewery Road, Woking, Surrey, GU21 4LL.
CONTENTS
Who we are
Why we collect information
What information we collect, how we collect it and what we use it for
What we communicate with you about
Who we share your data with
How we keep your data safe
Keeping your information up to date
How long we store your data for
How to find out what information we have about you
How to change the way we contact you
What to do if you don’t want us to collect information about you
Changes to this policy
Further information
 
WHO WE ARE
WWF-UK is part of the WWF global network which operates in over 100 countries around the world. All country offices work collaboratively with a common mission of creating a future where people and nature thrive. We are known as WWF-UK to distinguish ourselves from our sister organisations from other countries. This privacy policy relates to the information which is obtained and processed by WWF-UK only.
WWF-UK is comprised of both a charity and a trading company. In this policy, whenever you see the words ‘we’, ‘us’ or ‘our’, it refers to both our charity and our trading company.
Our trading company is wholly owned and controlled by our charity. Any information we collect may be used by both entities. Our trading company exists so that we can support our charitable activities including running our online shop and allowing others to use our brand, in support of our charity.
Our charity is WWF-UK (company number 04016725 and registered charity no. 1081247 in England and no. SC039593 in Scotland). Our trading company is WWF-UK Trading Ltd (company number 892812).
Both our charity and our trading company are registered as data controllers, with the Information Commissioner’s Office under The Data Protection Act 1998:


Data controller
Registration number
WWF-UK
Z6158630 
WWF UK (Worldwide Fund for Nature) TRADING LTD
Z6159804

 
 
 
 

WHY WE COLLECT INFORMATION
FOR A FUTURE WHERE PEOPLE AND NATURE THRIVE
To build a future where people live in harmony with nature we need to have effective communications and fundraising activity. By understanding more about how people use the information we provide, we can improve how we communicate the most important messages that you need to hear.
TO DELIVER IMPORTANT INFORMATION ABOUT WHAT YOU CAN DO FOR YOUR PLANET
We can broadcast information on TV, in the news and on our website, but if we can talk to you directly or deliver it straight to your doorstep or to your inbox, we can be sure that you get to know exactly what you can do to help as soon as it happens.
TO SAVE MONEY
We never do anything without carefully considering how much it costs. Collecting information about you and what grabs your attention, and comparing it to others, allows us to work out the most efficient way to do things so that more funds can be directed into conservation.
WHAT INFORMATION WE COLLECT, HOW WE COLLECT IT AND WHAT WE USE IT FOR
We collect information every time you interact with us. The type and quantity of information we collect and how we use it depends on why you are providing it.
 
For example, we may collect information when you:
Become a member of WWF- UK
Join Team Panda or participate in an event
Adopt an animal
Become a tiger protector
Make a donation to us
Subscribe to our e-newsletters
Respond to our mailings
Request information from us
Become involved with one of our campaigns
Volunteer with us
Make comments on message boards or discussion forums
Contact us or become involved with us in any other way than as stated above
 
The information we may collect from the above interactions may include, but is not limited to:
Your name, address, telephone number, mobile number and/or email address, along with your preferences as to how we should contact you in the future
Financial and credit card information which you give to us, including your gift aid status
Information you enter onto our website
Records of your donation history and correspondence with us
Images, photographs or video if you take part in an event with us
Details of your visit to the website, including technical information such as the IP address you use to access the website, your browser type and version
Other details relevant to your communications with us including your reasons for supporting us
In addition to information you give us directly, we may also collect information about you from publicly available sources.
 
We will mainly use the data we collect to:
Provide you with the services, products or information you asked for – for example adoption updates or membership magazines
Fundraise in accordance with our policies and procedures
Administer your donation or support your fundraising, including processing gift aid
Keep a record of your relationship with us
Analyse the personal information we collect about you and use publicly available information to better understand your interests, preferences and level of potential donations so that we can contact you more effectively
Manage your communication preferences, including marketing preferences
Understand how we can improve our services, products or information
Keep you up to date with the work you are supporting and to ask for financial and non-financial support
Support and further our mission (for example we may with your consent use images, photographs or video in marketing or promotional materials)
Carry out our obligations arising from any contracts entered into by you and us
Process a job application
 
We make it easy for you to tell us how you want us to communicate, in a way that suits you. Our communications have clear marketing preference questions and we include information on how to opt out when we send you marketing or fundraising materials. If you don’t want to hear from us, that’s fine. Just tell us so when you provide your data or contact us on 01483 426333 or email us at supportercare@wwf.org.uk. You can also update your personal details and contact preferences online using our self-service portal.
Where you have provided us with your telephone number, please note that we may use that telephone number to call you. If you have already told us you prefer not to receive telephone calls from us then we will not call you. If you would like to tell us now that you would prefer not to receive telephone calls from us then please contact Supporter Care on 01483 426333 or email supportercare@wwf.org.uk.
We may use desk research, profiling and screening techniques to analyse your personal information to create a profile of your interests and preferences so that we can contact you with information relevant to you or to help us find others like you who might like to show their support. We do this because it allows us to target our resources more effectively, which donors consistently tell us is a key priority for them. We may make use of additional information about you, including geo-demographic information and measures of affluence, when it is available from third party sources. We may on occasion use third party suppliers to undertake these activities on our behalf and provide them with your information to the extent required.
Such information is compiled using publicly available data about you, for example addresses, listed Directorships, social media posts, newspaper articles or typical earnings in a given industry. This helps us understand the background of the people who do or may support us and helps us to make appropriate requests for gifts to supporters who may be able and willing to give more than they already do and/or leave us a gift in their will. Collating this publicly available information helps us better understand your motivations and preferences enabling us to deliver a more targeted and relevant donor experience. Additionally, it enables us to raise more funds, sooner, and more cost-effectively than we otherwise would.
Note: If you’re adopting an animal or buying membership as a gift for someone else, we’ll need their name and address to send their pack and updates to, but we won’t contact them about anything else unless they ask us to. And we’ll never contact children except where we’ve had parental permission to do so.
If you enter your details onto one of our online forms, and you don’t ‘send’ or ‘submit’ the form, we may contact you via email to see if we can help with any problems you may be experiencing with the form or our websites.
Similarly if you receive an email, open it, don’t open it, select a link, browse our website, we collect this information so we can see which stories are popular and which aren’t. And next time we’ll do better so that more people will be spurred into action.
When we’re seeing what people do online like this, we’re using cookies. Accepting cookies from us helps direct more funds into conservation. If you don’t want cookies, you can set your browser to notify you when you receive one, then choose to decline it. Please read our Cookies Policy for more information.
Depending on your settings or the privacy policies for social media and messaging services like Facebook and Twitter, you might give us permission to access information from those accounts or services. For example, we occasionally participate in Facebook’s “Custom Audience” program which enables us to display ads to our existing or potential supporters via Facebook. We provide personal information such as your email address to Facebook to enable them to determine if you are a registered account holder with Facebook. Our adverts may then appear when you access Facebook and on your Facebook feed. Your data is sent in an encrypted format that is deleted by Facebook if it does not match with a Facebook account. For more information please read our Facebook Business page about Custom Audiences and Facebook’s Data Policy.
WHAT WE COMMUNICATE WITH YOU ABOUT
We have reviewed the communications we send our supporters, including what we send, how we send it, and what the communication is about. Along with conservation news and updates on our work, we may also contact you about five other things - fundraising, campaigning, events, trading and educational activities. We will always ask for your consent if we want to message you by email or text message, however we do not normally ask for consent to make telephone calls to you or write to you about any of these five things. This is because, as a charity, each of these activities is fundamental to how we work, so we have a legitimate interest to contact you. When you give us your details we will tell you what we are going to do with them, and when you give us your telephone or address details you will always either have an option not to give us them, or to give us them in order for us to provide the service you have requested but to opt-out of receiving our marketing communications by telephone or post.
While we don’t normally ask, there are some instances where we may have expressly asked you for consent to receive telephone calls or letters from us (for example, if you gave us your details through our fundraisers in the street). If you have consented for us to contact you in these ways, we will continue to do so unless you tell us not to. We are developing a policy on how often we will seek to confirm your consent and once we have determined this, we will write it here in this policy.
For further information on the types of communications we may send under each of these categories, please see below for examples of typical communications, although note that this is not the full list:
Campaigns – we’d love to tell you about our campaigning activities so that you can get involved by writing to your MP, signing a petition, contacting business or sharing campaign communications to influence for positive change for the environment.
Events – we’d love to tell you about our Team Panda challenge events such as Ride London or the Brighton Marathon which we’d love you to get involved with. We’d also like to tell you about volunteering opportunities where you can get join us for a fun day out, community fundraising where we can support you with your own events, or days where you can join a movement like Wear It Wild.
Trading – we’d love to show you what you can buy on our online shop. We’d also like to let you know about any special offers – a good excuse to treat family, friends, or yourself. All our products are eco-friendly and inspired by our natural world.  
Fundraising – your support is vital to our work and without you we would not be able to continue our conservation efforts. We may ask you to make a donation, contribute to an appeal, upgrade your support or change its type. We may also as you to renew or restart your support, and we may invite you to special supporter events.
Educational activities – we’d love to tell you about our schools and youth programmes and how you or your school can get involved and join us in the many activities.
In addition, we also want to keep you updated about our work, and tell you the latest conservation news.
WHO WE SHARE YOUR DATA WITH
We do not sell or share personal details to third parties for the purposes of marketing.
In other cases we will not disclose any of your personal data except in accordance with this policy, or when we have your permission, or under special circumstances, such as when we believe in good faith that the law requires it or to protect the rights, property and safety of WWF, or others. This includes disclosing your details if required to the police, regulatory bodies or legal advisors.
We may allow our suppliers to access and use your personal data to allow them to perform services on our behalf (e.g. payment processing). In these circumstances we will not give these organisations any rights to use your personal information except to provide services to us and in accordance with our instructions.
HOW WE KEEP YOUR DATA SAFE
We aim to ensure that there are appropriate physical, technical and managerial controls in place to protect your personal details; for example our online forms are always encrypted and our network is protected and routinely monitored. Within our offices only those trained in handling data securely will have access to your information.
When we use external companies to collect or process personal data on our behalf we do comprehensive checks on these companies before we work with them, and put a contract in place that sets out our expectations and requirements. We may need to transfer your personal data to the US or other countries outside of the European Economic Area to allow them to perform services on our behalf (such as when you sign up for a petition to help safeguard the natural environment). In doing so, your data may be stored or otherwise processed outside of the EEA. Where we engage with such organisations we will endeavour to ensure that the processing of your data is subject to appropriate security measures and by submitting your details in such circumstances you agree to this transfer.
Our website may from time to time contain links to third party websites. If you follow a link to any of these websites, please note that these websites will have their own privacy policies and that we do not accept any responsibility or liability for these policies. Please check these policies before you submit any personal data to these websites.
Despite all of our precautions however, no data transmission over the internet can be guaranteed to be 100% secure. So, whilst we strive to protect your personal information, we cannot guarantee the security of any information which you disclose to us and so wish to draw your attention that you do so at your own risk.
Where we have given you (or you have chosen) a password which enables you to access certain parts of our site, you are responsible for keeping this password confidential, and we ask you not to share this with anyone
KEEPING YOUR INFORMATION UP TO DATE
Where possible we use publicly available sources to keep your records up to date; for example, the Post Office’s National Change of Address database. We really appreciate it if you let us know if your contact details, or circumstances change you can update your details online at any time via our self-service portal.
HOW LONG WE STORE YOUR DATA FOR
We will hold your personal information on our systems for as long as is necessary for the relevant activity. If you request that we stop sending you marketing materials we will keep a record of your contact details and the appropriate information to enable us to comply with your request not to be contacted by us. We won’t store your credit/debit card details once we’ve processed your donation for instance.
Legacy income is vital to the running of the charity. We may keep data you provide to us indefinitely to carry out legacy administration and communicate effectively with the families of people leaving us legacies. This also enables us to identify and analyse the source of legacy income we receive.
HOW TO FIND OUT WHAT INFORMATION WE HAVE ABOUT YOU
You can request the details of the personal information we hold about you under the Data Protection Act 1998. We may ask you for an administration fee of £10.00.
If you want to access your information, send a description of the information you want to see and proof of your identity by post to Supporter Care, WWF-UK, The Living Planet Centre, Rufford House, Brewery Road, Woking GU21 4LL. We do not accept these requests by email.
If you just need to update your contact details you can also do so online via our self-service portal.
HOW TO CHANGE THE WAY WE CONTACT YOU
If at any time you’d like us to change the way we contact you, please call Supporter Care on 01483 426333 or email supportercare@wwf.org.uk, or you can visit our self-service portal to change your details or contact preferences online at any time.
You can also now register your details with the Fundraising Preference Service if you want to tell us through the Fundraising Regulator that you would prefer us not to contact you. 
We will only email or text you if we believe you have consented for us to, so if you receive anything you haven’t asked for please do let us know. In every email or text we send there will be instructions on how to unsubscribe.
During any phone conversation you have with us please feel free to let us know how you prefer to be contacted.
If we’ve sent you something in the post that you don’t really want just drop us a line to let us know. Our Supporter Care team’s contact details will always be included in the pack. Or you can visit our self-service portal to change your details or contact preferences online at any time
WHAT TO DO IF YOU DON’T WANT US TO COLLECT INFORMATION ABOUT YOU
If you don’t want us to collect information about you as you browse our website you’ll need to set your browser to notify you when you receive a cookie, then choose to decline it.
If you don’t want us to hold any personal details about you, it’s best just not to give them to us. If you want us to stop collecting information about you or processing that information then please let us know.
CHANGES TO THIS POLICY
This policy was last updated in November 2017. It was re-worded to make it easier to understand, and to clarify what we communicate with you about..
We may amend or update this policy at any time to take account of any changes to data protection law or other legislation. When further updates to the policy are made they will be posted on this page, but rest assured we won’t ever make any other changes to the way we use your data without asking you first. Your subsequent submission of personal information to WWF will be deemed to signify your acceptance to the updates, so you may wish to check this policy each time you submit personal information to us.
FURTHER INFORMATION
The laws that dictate how your personal information can be used are:
At WWF-UK we do our best to go beyond the minimum requirements and also follow the best practice code set out by the Fundraising Regulator: Code of Fundraising Practice.
You can also contact the Information Commissioner’s Office at ico.org.uk to find out more or report a concern. They are the UK’s independent authority set up to uphold information rights in the public interest, promoting openness by public bodies and data privacy for individuals. We work with them to make sure that we collect, store and use your information appropriately and don’t do anything you wouldn’t expect us to.
 

