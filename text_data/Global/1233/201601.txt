

Cheapflights.co.uk, which is operated by Momondo Group Limited has created this privacy statement in order to demonstrate its commitment to privacy. The following discloses our information gathering and dissemination practices for Cheapflights.co.uk as well as email newsletters. It is important for you to understand what information we collect about you during your visit and what we do with that information.
This Privacy Policy only applies to information collected on Cheapflights.co.uk and not to information that is collected by Cheapflights through any other means. Your visit to Cheapflights.co.uk is subject to this Privacy Policy and our Terms of Use.


1. Personal Information Collection and Use

            We do not collect personally identifiable information about you, except when you provide it to us. For example, if you submit a question to us, sign up for our weekly newsletters or a sweepstakes on Cheapflights.co.uk, you may be asked to provide certain information such as your contact information (name, email address, mailing address, etc.).
        

            When you submit your personally identifiable information through Cheapflights.co.uk, you are giving your consent to the collection, use and disclosure of your personal information as set forth in this Privacy Policy. If you would prefer that we not collect any personally identifiable information from you, please do not provide us with any such information. We will not sell or rent your personally identifiable information to third parties without your consent, except as otherwise disclosed in this Privacy Policy.
        

            Except as otherwise disclosed in this Privacy Policy, we will use the information you provide us only for the purpose of responding to your inquiry or in connection with the service for which you provided such information. For example, if you opt-in to our email alert, we may use your email address to send you periodic emails. We may forward your contact information and inquiry to our affiliates and other divisions of our company that we feel can best address your inquiry or provide you with the requested service. We may also use the information you provide in aggregate form for internal business purposes, such as generating statistics and developing marketing plans. We may share or transfer such non-personally identifiable information with or to our affiliates, licensees, agents and partners.
        

            We may retain other companies and individuals to perform functions on our behalf. Examples include, without limitation, customer support and fulfillment companies (e.g., companies that coordinate mailings). Such third parties may be provided with access to personally identifiable information needed to perform their functions, but may not use such information for any other purpose.
        

            In addition, we may disclose any information, including personally identifiable information, we deem necessary, in our sole discretion, to comply with any applicable law, regulation, legal proceeding or governmental request.
        


2. Email addresses

            We do not want you to receive unwanted email from us. We try to make it easy to opt-out of any service you have asked to receive. If you sign-up to our email newsletters we do not sell, exchange or give your email address to a third party.
        

            Email addresses are collected via the Cheapflights.co.uk Web site, co-registration partners and third-party marketing lists and promotions. Users have to physically opt-in to receive the Cheapflights.co.uk newsletter and a verification email is sent. Cheapflights is clearly and conspicuously named at the point of collection.
        

            Cheapflights uses email addresses to send its newsletters and occasional mailings to assist in market research.
        

            We sometimes run sweepstakes with our partners and sweepstakes entries may require you to provide an email address. These email addresses may be shared with the partner running the competition. We try to make it very clear when this is the case and we have no responsibility for subsequent use or distribution of email addresses by our partners.
        

            If you no longer wish to receive our newsletter and promotional communications, you may opt-out of receiving them by following the instructions included in each newsletter. Alternatively, please visit the Newsletter Unsubscribe page to be removed from future communications.
        

            The security of your personal information, including your email address and zip code, is important to us. We follow generally accepted industry standards to protect the personal information submitted to us, both during registration and once we receive it. No method of transmission over the Internet, or method of electronic storage, is 100 per cent secure, however. Therefore, though we strive to use commercially acceptable means to protect your personal information, we cannot guarantee its absolute security.
        

            If you have any questions about the security on our Web site, please visit our "Contact Us" page and a designated Cheapflights correspondent will respond to your query.
        

            If we decide to change our email practices, we will post those changes to this privacy statement, the homepage, and other places we think appropriate so that you are aware of what information we collect, how we use it, and under what circumstances, if any, we disclose it.
        

            We reserve the right to modify this privacy statement at any time, so please review it frequently. If we make material changes to our email practices, we will notify you here, by email, and by means of a notice on our home page.
        


3. Access to Personally Identifiable Information

            If your email address changes, or if you no longer desire our service, you may correct, update, delete or deactivate your details by visiting the Contact Us at http://www.cheapflights.co.uk/contact-us/



4. Tracking Technologies
Tracking technologies are set up via the Cheapflights newsletter to monitor open and click performance. Cookies are only used once a user enters the Cheapflights.co.uk Web site in order to identify, discriminate and count the unique users visiting our Web site.


5. Cookies
Cheapflights uses cookies to improve user experience such as improving navigation and relevancy of product and information on the site. Your web browser should allow you to choose whether or not to allow cookies - you can still use our site if you have them disabled. For more information on cookies, visit www.cookiecentral.com.


6. Third Party Sites and Booking Tools

            Cheapflights does not sell you travel products or services, but directs you to third party Web sites that do. These sites may collect personally identifiable information from you, such as your name, address, credit card details or details pertaining to other methods of payment. These sites may have their own privacy policies which will govern how such sites use and share your personally identifiable information. In addition, Cheapflights.co.uk displays third party booking tools. When you utilize these third party booking tools, you are subject to the terms, conditions and privacy policies of those third parties.
        

            This Privacy Policy applies only to the information we collect on Cheapflights.co.uk. Please note that Cheapflights has no input into, and is not responsible for, the privacy practices of other Web sites. We encourage you to read the privacy policies of other web sites you link to from Cheapflights.co.uk.
        


7. Third Party Advertisers

            The advertising banners and other forms of advertising appearing on this Web site are sometimes delivered to you, on our behalf, by a third party. In the course of serving advertisements to this site, the third party may place or recognize a unique cookie on your browser. For more information on cookies, visit www.cookiecentral.com.
        


8. Business Transfers

            As we continue to develop our business, we might sell certain of our entities or assets. In such transactions, user information, including personally identifiable information, generally is one of the transferred business assets, and by submitting your personal information on Cheapflights.co.uk you agree that your data may be transferred to such parties in these circumstances.
        


9. Notification of Changes

            We may revise this Privacy Policy from time to time. If we decide to change our Privacy Policy, we will post the revised policy here. As we may make changes at any time without notifying you, we suggest that you periodically consult this Privacy Policy. Please note that our rights to use your personally identifiable information will be based on the privacy policy in effect at the time the information is used.
        


10. Electronic Privacy Policy (P3P)
In addition to this standard Privacy Policy, Cheapflights uses an electronic privacy policy (P3P) that can be automatically retrieved and interpreted by certain Web browsers. Our P3P policy states that it is valid for 52 weeks from the time it is loaded by a user.


11. Comments
We have taken great measures to ensure that your visit to Cheapflights.co.uk is an excellent one and that your privacy is respected. If you have any questions, comments or concerns about our privacy practices, please contact us by email via our Feedback link or by writing to the following address:
Content Editor
Alfred Place 
London
WC1E 7EB
United Kingdom
May 1, 2015


