

How to make a complaint
Please tell us if you’re not happy with how we’ve handled your personal information. Find our more about our complaints process and how to make a complaint.

Privacy policy review
We regularly review this Privacy policy. If we make changes they’ll be updated here and we will place a prominent notice on our website.

This Privacy policy was prepared to be as concise as possible. It does not provide exhaustive detail of all aspects of our collection and use of your personal information. However, we are happy to provide any additional information or explanation needed.  Any requests for this should be sent to the address below.

Date of this review: October 2016.
 
How to contact us
Please contact us if you have any question about our Privacy policy or information we hold about you. You can email us or write to: Data Protection Department, Legal Services, RSPCA, Wilberforce Way, Southwater, Horsham, West Sussex, RH13 9RS.

