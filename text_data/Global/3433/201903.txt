
特許庁のホームページは、2019年2月25日にリニューアルしました。
お手数をお掛けしますが、トップページ（https://www.jpo.go.jp/index.html）からお探し下さい。
The Japan Patent Office renewed our website on February 25, 2019.
Please go to our new top page(https://www.jpo.go.jp/e/index.html). Apologies for the inconvenience.

