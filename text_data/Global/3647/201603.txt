We’re actively looking for stories to feature on the site, on the stage and on The Moth Radio Hour. Read more »
  Privacy Policy 

Last Updated: January 1, 2016
The Moth (the “Company”) operates themoth.org (the “Site”). This page informs you of our policies regarding the collection, use and disclosure of Personal Information we receive from users of the Site. We use your Personal Information only for providing content and improving the Site. By using the Site, you agree to the collection and use of information in connection with this policy.
Personal Information Collection and Use
While using our Site, we may ask you to provide us with certain personally identifiable information that can be used to contact or identify you. Personally identifiable information may include, but is not limited to your name (“Personal Information”).
Log Data
Similar to many site operators, we collect information that your browser sends whenever you visit our Site (“Log Data”). This Log Data may include information such as your computer’s Internet Protocol (“IP”) address, bowser type, browser version, the pages of our Site that you visit, the time and date of your visit, and other statistics. In addition, we may use third party services such as Google Analytics, that collect, monitor and analyze this data.
Communications
We may use your Personal Information to contact you with newsletters, marketing or promotional materials and other information which is voluntarily provided during the use of this Site. This includes information provided when signing up for our Newsletter, Donating to The Moth, and when buying tickets for The Moth events.
Cookies
Cookies are files with small amount of data, which may include an anonymous unique identifier. Cookies are sent to your browser from a web site and stored on your computer’s hard drive. Like many sites, we use “cookies” to collect information. You can instruct your browser to refuse all cookies or to indicate when a cookie is being sent. However, if you do not accept cookies, you may not be able to use some portions of our Site.
Security
The security of your Personal Information is important to us, but remember that no method of transmission over the Internet, or method of electronic storage, is 100% secure. While we strive to use commercially acceptable means to protect your Personal Information, we cannot guarantee its absolute security.
Changes to Privacy Policy
This Privacy Policy is effective as of January 1, 2016 and will remain in effect except with respect to any changes in its provisions in the future, which will be in effect immediately after being posted on this page.We reserve the right to update or change our Privacy Policy at any time. Your continued use of the Service after we post any modification to the Privacy Policy on this page will constitute your acknowledgment and consent to abide by and be bound by the modified Privacy Policy. Notice of any material changes to this Privacy Policy will be provided through the email address you have provided to us, or by placing a prominent notice on our website.
Contact Us
If you have any questions about this Privacy Policy, please contact us.
 
