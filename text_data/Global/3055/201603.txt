



  










Privacy Policy | NIWA














<!--//--><![CDATA[//><!--
(function(i,s,o,g,r,a,m){i["GoogleAnalyticsObject"]=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,"script","//www.google-analytics.com/analytics.js","ga");ga("create", "UA-18439856-1", {"cookieDomain":"auto"});ga("send", "pageview");
//--><!]]>




<!--//--><![CDATA[//><!--
jQuery.extend(Drupal.settings, {"basePath":"\/","pathPrefix":"","ajaxPageState":{"theme":"niwa_d7","theme_token":"37psJPvT43Ad12Mmriu9SHQALMPXJ6ddH3oandEwOjI","js":{"sites\/all\/modules\/contrib\/addthis\/addthis.js":1,"misc\/jquery.js":1,"misc\/jquery.once.js":1,"misc\/drupal.js":1,"sites\/all\/modules\/contrib\/lightbox2\/js\/lightbox.js":1,"sites\/all\/modules\/contrib\/google_analytics\/googleanalytics.js":1,"0":1,"sites\/all\/themes\/niwa_d7\/js\/plugins.js":1,"sites\/all\/themes\/niwa_d7\/js\/vendor\/modernizr-2.6.2.min.js":1,"sites\/all\/themes\/niwa_d7\/js\/vendor\/jquery.hint.min.js":1,"sites\/all\/themes\/niwa_d7\/js\/utils.js":1,"sites\/all\/themes\/omega\/omega\/js\/jquery.formalize.js":1,"sites\/all\/themes\/omega\/omega\/js\/omega-mediaqueries.js":1,"public:\/\/cpn\/page.js":1},"css":{"modules\/system\/system.base.css":1,"modules\/system\/system.menus.css":1,"modules\/system\/system.messages.css":1,"modules\/system\/system.theme.css":1,"sites\/all\/modules\/contrib\/ldap\/ldap_user\/ldap_user.css":1,"modules\/aggregator\/aggregator.css":1,"modules\/book\/book.css":1,"sites\/all\/modules\/contrib\/date\/date_api\/date.css":1,"sites\/all\/modules\/contrib\/date\/date_popup\/themes\/datepicker.1.7.css":1,"modules\/field\/theme\/field.css":1,"modules\/node\/node.css":1,"modules\/search\/search.css":1,"modules\/user\/user.css":1,"sites\/all\/modules\/contrib\/views\/css\/views.css":1,"sites\/all\/modules\/contrib\/ctools\/css\/ctools.css":1,"sites\/all\/modules\/contrib\/ldap\/ldap_servers\/ldap_servers.admin.css":1,"sites\/all\/modules\/contrib\/lightbox2\/css\/lightbox.css":1,"sites\/all\/modules\/contrib\/panels\/css\/panels.css":1,"sites\/all\/modules\/contrib\/node_embed\/plugins\/node_embed\/node_embed.css":1,"sites\/all\/themes\/niwa_d7\/ds_layouts\/twocol_66_33_stacked\/twocol_66_33_stacked.css":1,"sites\/all\/themes\/omega\/alpha\/css\/alpha-reset.css":1,"sites\/all\/themes\/omega\/alpha\/css\/alpha-mobile.css":1,"sites\/all\/themes\/omega\/alpha\/css\/alpha-alpha.css":1,"sites\/all\/themes\/omega\/omega\/css\/formalize.css":1,"sites\/all\/themes\/omega\/omega\/css\/omega-forms.css":1,"sites\/all\/themes\/niwa_d7\/css\/global.css":1,"ie::normal::sites\/all\/themes\/niwa_d7\/css\/niwa-d7-alpha-default.css":1,"ie::normal::sites\/all\/themes\/niwa_d7\/css\/niwa-d7-alpha-default-normal.css":1,"ie::normal::sites\/all\/themes\/omega\/alpha\/css\/grid\/alpha_default\/normal\/alpha-default-normal-12.css":1,"narrow::sites\/all\/themes\/niwa_d7\/css\/niwa-d7-alpha-default.css":1,"narrow::sites\/all\/themes\/niwa_d7\/css\/niwa-d7-alpha-default-narrow.css":1,"sites\/all\/themes\/omega\/alpha\/css\/grid\/alpha_default\/narrow\/alpha-default-narrow-12.css":1,"normal::sites\/all\/themes\/niwa_d7\/css\/niwa-d7-alpha-default.css":1,"normal::sites\/all\/themes\/niwa_d7\/css\/niwa-d7-alpha-default-normal.css":1,"sites\/all\/themes\/omega\/alpha\/css\/grid\/alpha_default\/normal\/alpha-default-normal-12.css":1,"wide::sites\/all\/themes\/niwa_d7\/css\/niwa-d7-alpha-default.css":1,"wide::sites\/all\/themes\/niwa_d7\/css\/niwa-d7-alpha-default-wide.css":1,"sites\/all\/themes\/omega\/alpha\/css\/grid\/alpha_default\/wide\/alpha-default-wide-12.css":1}},"lightbox2":{"rtl":0,"file_path":"\/(\\w\\w\/)public:\/","default_image":"\/sites\/all\/modules\/contrib\/lightbox2\/images\/brokenimage.jpg","border_size":10,"font_color":"000","box_color":"fff","top_position":"","overlay_opacity":"0.8","overlay_color":"000","disable_close_click":1,"resize_sequence":0,"resize_speed":400,"fade_in_speed":400,"slide_down_speed":600,"use_alt_layout":0,"disable_resize":0,"disable_zoom":0,"force_show_nav":1,"show_caption":1,"loop_items":1,"node_link_text":"View Image Details","node_link_target":0,"image_count":"Image !current of !total","video_count":"Video !current of !total","page_count":"Page !current of !total","lite_press_x_close":"press \u003Ca href=\u0022#\u0022 onclick=\u0022hideLightbox(); return FALSE;\u0022\u003E\u003Ckbd\u003Ex\u003C\/kbd\u003E\u003C\/a\u003E to close","download_link_text":"","enable_login":false,"enable_contact":false,"keys_close":"c x 27","keys_previous":"p 37","keys_next":"n 39","keys_zoom":"z","keys_play_pause":"32","display_image_size":"original","image_node_sizes":"()","trigger_lightbox_classes":"","trigger_lightbox_group_classes":"","trigger_slideshow_classes":"","trigger_lightframe_classes":"","trigger_lightframe_group_classes":"","custom_class_handler":0,"custom_trigger_classes":"","disable_for_gallery_lists":true,"disable_for_acidfree_gallery_lists":true,"enable_acidfree_videos":true,"slideshow_interval":5000,"slideshow_automatic_start":true,"slideshow_automatic_exit":true,"show_play_pause":true,"pause_on_next_click":false,"pause_on_previous_click":true,"loop_slides":false,"iframe_width":600,"iframe_height":400,"iframe_border":1,"enable_video":0},"googleanalytics":{"trackOutbound":1,"trackMailto":1,"trackDownload":1,"trackDownloadExtensions":"7z|aac|arc|arj|asf|asx|avi|bin|csv|doc(x|m)?|dot(x|m)?|exe|flv|gif|gz|gzip|hqx|jar|jpe?g|js|mp(2|3|4|e?g)|mov(ie)?|msi|msp|pdf|phps|png|ppt(x|m)?|pot(x|m)?|pps(x|m)?|ppam|sld(x|m)?|thmx|qtm?|ra(m|r)?|sea|sit|tar|tgz|torrent|txt|wav|wma|wmv|wpd|xls(x|m|b)?|xlt(x|m)|xlam|xml|z|zip"},"urlIsAjaxTrusted":{"\/privacy-policy":true},"omega":{"layouts":{"primary":"normal","order":["narrow","normal","wide"],"queries":{"narrow":"all and (min-width: 740px) and (min-device-width: 740px), (max-device-width: 800px) and (min-width: 740px) and (orientation:landscape)","normal":"all and (min-width: 980px) and (min-device-width: 980px), all and (max-device-width: 1024px) and (min-width: 1024px) and (orientation:landscape)","wide":"all and (min-width: 1220px)"}}}});
//--><!]]>





Jump to Navigation
Skip to main content









NIWA



 








Search form

Search 





 




MenuMain navigation 




ScienceAquaculture
Atmosphere
Climate
Coasts and Oceans
Environmental Information
Fisheries
Freshwater and Estuaries
Natural Hazards
Pacific Rim
Te Kūwaha
Vessels
Antarctica
Voyages

ServicesOnline services
Instruments
Vessels
Primary sector
Energy sector
Oil and gas sector
Seafood sector
High Performance Computing Facility
Localised event forecasting
Software tools

News & publicationsMedia centre
News
Publications
Photo gallery
Videos
EventsPast Events

Library
New Zealand-Australia Antarctic Ecosystems Voyage
Blogs

Education & trainingInstitutes we work with
Schools
Training courses
Science and Technology Fairs
Scholarships
Post-Doctoral Fellowships

About usOur company
Our mission
Our people
Statement of Core Purpose
Statement of Corporate Intent
Our partners & funders
Annual reports
Careers at NIWA
Organisational responsibility

Contact usNIWA offices

Careers
 

 















Privacy Policy 


Information transmitted over the Internet is inherently insecure, with the possible exception of those pages that use encryption to protect the information during transmission. Once NIWA receives any personal information from you, NIWA will hold that information securely and will only use such information to:  provide you with products and services at your request; charge you for provision of those products and services; communicate with you regarding payment or other aspects of those products and services.  





 
 









NIWA
Copyright 2013, NIWA. All rights reserved 



National science centres


Aquaculture
Atmosphere
Climate
Coasts and Oceans


Environmental Information
Fisheries
Freshwater and Estuaries
Natural Hazards


Pacific Rim
Te Kūwaha
Vessels
 



Useful Links

Careers with NIWA
Contact us
Staff login
Search 
 



Contact

Free phone within New Zealand: 0800 RING NIWA (0800 746 464) Contact form 

 
 
  


