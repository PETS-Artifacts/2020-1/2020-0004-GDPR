Aconvert.com is dedicated to making your online experience as interesting and enjoyable as possible. We truly appreciate your presence on our site and at all times understand that you are our guest and should be treated accordingly. To ensure this, we have listed the components of our Privacy Policy below. If you have any questions regarding the processing of your personal data, as well as your rights regarding data privacy, please contact: support@aconvert.com.
 
Use of cookies

To make this site work properly, we sometimes place small data files called cookies on your device. Most big websites do this too.
 
What are cookies?

A cookie is a small text file that a website saves on your computer or mobile device when you visit the site. It enables the website to remember your actions and preferences (such as login, language, font size and other display preferences) over a period of time, so you don’t have to keep re-entering them whenever you come back to the site or browse from one page to another. 
 
How do we use cookies?

Aconvert.com does not use any first-party cookies, this includes both session cookies and persistent cookies. Our webiste uses third party advertising services from Google Adsense that may use cookies. 
 
Cookies for Advertising

This type of services allows User Data to be utilized for advertising communication purposes displayed in the form of banners and other advertisements on this Application, possibly based on User interests.
This does not mean that all Personal Data are used for this purpose. Information and conditions of use are shown below.
Some of the services listed below may use Cookies to identify Users or they may use the behavioral retargeting technique, i.e. displaying ads tailored to the User’s interests and behavior, including those detected outside this Application. For more information, please check the privacy policies of the relevant services.
Google AdSense (Google Inc.)
Google AdSense is an advertising service provided by Google Inc. This service uses the “Doubleclick” Cookie, which tracks use of this Application and User behavior concerning ads, products and services offered. Users may decide to disable all the Doubleclick Cookies by clicking on: google.com/settings/ads/onweb/optout.
Personal Data collected: Cookies and Usage Data.
Place of processing: US – Privacy Policy – Opt Out.
 
How to control cookies?

You can control and/or delete cookies as you wish – for details, see aboutcookies.org. You can delete all cookies that are already on your computer and you can set most browsers to prevent them from being placed. If you do this, however, you may have to manually adjust some preferences every time you visit a site and some services and functionalities may not work.
You can delete all cookies that are already on your computer and you can set most browsers to prevent them from being placed. If you do this, however, you may have to manually adjust some preferences every time you visit a site and some services and functionalities may not work. 
Right of objection

You have the right to object.
In your browser settings you can restrict or completely prevent the use of cookies. You can also prompt the automatic deletion of cookies when a browser window is closed.
You can find out how to delete and change the cookie settings of the most common browsers here:

Google Chrome: Website
Mozilla Firefox: Website
Apple Safari: Website
Microsoft Internet Explorer: Website

 
Apps for Android

Aconvert.com built apps as an Ad Supported apps. This SERVICE is provided by Aconvert.com at no cost and is intended for use as is. For a better experience, while using our Service, we may require you to provide us with certain personally identifiable information. The information that we request will be retained on your device and is not collected by us in any way.
Our apps use third party services that may collect information used to identify you. Link to privacy policy of third party service providers used by our apps:
Google Play Services, AdMob
Our JPG to PDF Converter app uses permissions that are associated with accessing camera. This permission allows this application to capture images in order to be converted to a PDF file within this application. This permission can be rejected. Our online file conversion services may require you to upload files to convert. Your files will be for only you to access and will be automatically removed permanently on our server in 2 hours.
 
Data protection
Personal information

When visiting Aconvert.com, the IP address used to access the site will be logged along with the dates and times of access. This information is purely used to analyze trends, administer the site, track users movement and gather broad demographic information for internal use. Most importantly, any recorded IP addresses are not linked to personally identifiable information.
Aconvert.com doesn't require registration to use. That means we DO NOT collect your name, email address, company name or password etc.
Aconvert.com will never share your personal information with any Third Party business organisation that intends to use it for direct marketing purposes unless you have given us specific permission to do so.
 
Storage of files

All the files uploaded for processing on Aconvert.com are stored on an appropriate server infrastructure for processing and the download afterwards. All user-uploaded files as well as the converted output files will be deleted in two hours after upload or conversion respectively. We keep the files for the sole purpose of giving you enough time to download them. During that time, we don’t look at the files or mine any data from them. No backups are made of any uploaded files nor their processed output neither are the contents monitored without the explicit permission of user. 
 
Third party websites

Our website may contain links to other websites of interest. However, once you have used these links to leave our site, you should note that we do not have any control over that other website. Therefore, we cannot be responsible for the protection and privacy of any information which you provide whilst visiting such sites and such sites are not governed by this privacy statement. You should exercise caution and look at the privacy statement applicable to the website in question.
 
General Data Protection Regulation (GDPR)

The General Data Protection Regulation (GDPR) is a regulation in EU law on data protection and privacy for all individuals across the EU and within the European Economic Area. It becomes enforceable on 25 May 2018.
In the terms of the GDPR, Aconvert.com act as the data controller and data processor.
 
Aconvert.com acts as a data controller when it directly collects or processes personal data providing services to end users. It means that Aconvert.com acts as a data controller when you upload files, which may contain your personal data. We do collect your IP address and access times information. We do not share this data with anyone. Aconvert.com irreversibly deletes all your files according to “Storage of files” section of this policy.
Aconvert.com acts as a data processor if it processes data on behalf of its customer, i.e. when you use REST API or any other way to process files of your own customers. As a data processor, Aconvert.com will treat and manage your data in accordance with strict security standards.
If you have any questions regarding our privacy policy and GDPR compliance, feel free to contact us at support@aconvert.com.
 
