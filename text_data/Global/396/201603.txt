Data Privacy
Data relating to visitors browsing this Web site is not collected. The confidentiality of visitors browsing this Web site, including their identity, is respected. The National Kidney Foundation, owners of this Web site, undertake to honor or exceed the legal requirements of the Health Insurance Portability and Accountability Act (HIPAA) and all other requirements of medical/health information privacy in the U.S.
Donor Privacy
National Kidney Foundation, Inc. will not sell, share or trade our donors' names or personal information with any other entity, nor send mailings to our donors on behalf of other organizations, unless the donors have given National Kidney Foundation, Inc. specific permission to do so.
This policy applies to all information received by National Kidney Foundation, Inc. both online and offline, on any Platform ("Platform", includes the National Kidney Foundation, Inc. website and mobile applications), as well as any electronic, written, or oral communications.
To the extent any donations are processed through a third-party service provider, the donors’ information will only be used for purposes necessary to process the donation.
E-mail
The National Kidney Foundation will not send you unsolicited e-mail (Spam), will not sell, trade or share your personal information with anyone else, nor will we send e-mailings on behalf of other organizations. E-mail communication from the National Kidney Foundation adheres to the CAN-SPAM Act and contains an opt-out link. If you have any questions or concerns about your privacy on this site, please contact us.
Information Collected Online and Elsewhere
When you apply for membership, donate funds or a vehicle, register for an event, or sign up for a newsletter, we may ask for basic information that may include name, address, city, state, zip code, phone number and e-mail address. The National Kidney Foundation will not sell, trade or share your personal information with anyone else, nor will we send mailings to you on behalf of other organizations. Online payment information, i.e. credit card numbers, is not stored. Should you voluntarily disclose personal information online, e.g. on message boards, through e-mail or in chat areas, that information may be collected and used by others.
Online Transactions
National Kidney Foundation Web sites employ Secure Socket Layer (SSL) security for data encryption and firewall technology. System passwords are routinely changed. All credit card information transmitted during online transactions is processed with Secure Socket Layer (SSL) security, an established protocol for transmitting information via the Internet which uses server authentication and data encryption to ensure that your data is safe and secure. Once processed, credit card information is not stored. Access to National Kidney Foundation Web sites is restricted to authorized staff only. Despite use of these safeguards, we cannot guarantee and do not represent that your information will always be secure from unauthorized access.
Web Site Cookies
A cookie is a small amount of data which often includes an anonymous unique identifier that is sent to your browser from a Web site and is stored on your computer's hard drive. Each Web site can send its own cookie to your browser if your browser's preferences allow it, but to protect your privacy your browser only permits a Web site to access the cookies it has already sent to you, not the cookies sent to you by other sites. You can configure your browser to accept all cookies, reject all cookies, or notify you when a cookie is set. (Each browser is different, so check the "Help" menu of your browser to learn how to change your cookie preferences.)
How NKF Uses Cookies
NKF does not use cookies to store or transmit personal information.
Advertising
This Web site does not accept advertising.
Policy on Discontinuing Contact
Revised 8/18/08
 
