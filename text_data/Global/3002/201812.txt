
Information  We Disclose
The  above-described types of personal and other types of   information about our  current, former, or potential customers, may be   disclosed, but only as  permitted or required by law, or as permitted or   directed by the customer or  consumer. 
We  may disclose such information to affiliated and   nonaffiliated insurance  companies, insurance agents, reinsurance   companies, insurance support  organizations, such as consumer reporting   agencies, government entities, and  claims administrators. We may also   disclose information to nonaffiliated third  parties that perform   services or functions on our behalf including the  marketing of our   products or services. Further, we may also disclose  information as   necessary to effect, administer or enforce a transaction that  you   request or authorize. 
The  following are examples of situations in which we may disclose information: 

Your information may be passed  between us and our   agents, appraisers, attorneys, investigators, and others who  may be   involved in the sales and marketing of Empower Ins products and    services, processing of applications, and servicing of policies or   claims. 
We provide your policy information  to adjusters and other appropriate business entities when you are involved in a  claim. 
We may provide your information to  others whom we   determine need such information to perform insurance or other    professional functions for us. These may include parties helping us with    administrative services and consumer reporting agencies. 
We may supply information as required  by search   warrant, subpoena, or legal process, with state insurance  departments,   or other law enforcement or government authorities when required  by   law, or to protect our own legal interests in the event of suspected   illegal  or fraudulent activities. 
If we collect your information as an  agent for one of   our business partners, we may use it to contact you or make  you an   offer regarding insurance that may be of interest to you. 

All  of the nonaffiliated entities with which we may share   your information are  required to keep the information confidential and   use it only for the purposes  for which it was shared, except as   otherwise permitted by law. 

