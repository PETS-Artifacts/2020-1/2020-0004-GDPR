 About Us
We are a group of IT professionals enthusiastic in creating quality free tools and content on the internet. The main purpose of this website is to provide a comprehensive collection of free online calculators for the ease of public use. This site was launched on calculators.info first in 2007. In 2008, we purchased and migrated to calculator.net.
More than 99% of the calculators on this site were developed in-house. Some calculators use open-source JavaScript components under different open-source licenses. More than 80% of the calculators are based on well-known formulas or equations from textbooks, such as the mortgage calculator, BMI calculator, etc. If formulas are controversial, we provide the results of all popular formulas, as can be seen in the Ideal Weight Calculator. Calculators such as the love calculator that are solely meant for amusement are based on internal formulas. The results of the financial calculators were reviewed by our financial advisors, who work for major personal financial advising firms. The results of the health calculators were reviewed and approved by local medical doctors. More than 95% of the descriptive content was developed in-house with a small amount of content taken from wikipedia.org under the GNU Free Documentation License. The descriptive content of the financial calculators was created and reviewed by our financial team. The descriptive content of the health calculators was reviewed by local medical doctors. The rest of the descriptive content was created by our own team. In addition to this site, we also have free apps for mobile visitors.
This site is owned and operated by Maple Tech. International LLC. This site is mainly financially supported by the income from Google Adsense advertisements.
Contact Us
We can be reached via the following form. Normally, we will respond within 2 business days. However, please do NOT contact for advertising purposes, especially text link ads. If needed, you can place targeted ads on our site via Google Adwords.
This site needs JavaScript enabled to run properly. Please change your browser settings!

Mailing Address:
10210 Grogans Mill Road
Suite 193
The Woodlands, TX 77380






Copyright
The logos, images, scripts, and individual pages are copyrighted material. The materials on this site cannot be copied/modified without permission.

Privacy Policy
We respect and take your privacy seriously. We may collect information via cookies or web logs. This is to customize services and enhance customer satisfaction.
The data we collect include: 

The information entered into and submitted by the web forms on our sites.
Standard web log entries including your IP address, page URL, timestamp, etc.

We use third-party advertising companies to serve ads when you visit our website. These companies may use information (not including your name, address, email address, or telephone number) about your visits to this and other websites in order to provide advertisements about goods and services of interest to you. If you would like more information about this practice and to know your choices about not having this information used by these companies, click here.

Google, as a third party vendor, uses cookies to serve ads on your site.
Google's use of the DART cookie enables it and its partners to serve ads to your users based on their visit to your sites and/or other sites on the Internet.
Users may opt out of the use of the DART cookie by visiting the Google ad and content network privacy policy.

We reserve the right to update this privacy policy with or without notice.

Terms of Use
By using our services, you agree to be bound by the following terms and conditions (the "Terms of Use").
We have no control over any potential changes to content that we link to. As such, we  provide no guarantees and take no responsibility regarding changes to, or the accuracy of content that we link to, as it is possible that changes that do not align with our original intent could be made after our initial link to the site. If any inaccuracies or broken links are found, feel free to contact us using the form above, and any issues will be addressed as necessary.
THE MATERIALS ON THIS SITE ARE PROVIDED "AS IS" AND WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED.
We reserve the right to update the Privacy Policy and Terms of Use with or without notice.

