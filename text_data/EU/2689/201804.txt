Privacy - Ululewindow.jQuery || document.write('<script src="https://d1yggf0lcx0ykm.cloudfront.net/site/js/lib/jquery.min.e0e0559014b2.js"><\/script>')
                jQuery.migrateTrace = false;
                jQuery.migrateMute = true;

                $.support.cors = true;
            
                var media_url = 'https://d2homsd77vx6d2.cloudfront.net/',
                    static_url = 'https://d1yggf0lcx0ykm.cloudfront.net/',
                    local_media_url = '',
                    lang = 'en',
                    field_required = "This field is required.",
                    CKEDITOR_BASEPATH = 'https://d1yggf0lcx0ykm.cloudfront.net/site/js/lib/ckeditor-4.4/',
                    CKEDITOR_TRANSLATIONS = {
                      uploader: {
                        ui: {
                          imageOptions: "Images options",
                          replacementText: "Replacement text",
                          width: "Width",
                          height: "Height",
                          ratio: "Keep ratio",
                          add: "Add",
                          separator: "Or",
                          flashErrorMessage: "Oops... you need to choose an image from your computer or your favorite website",
                        },
                        uploader: {
                          InputLabel: "Choose a file from your computer",
                          AcceptedFiles: "Accepted files: JPG, JPEG, PNG and GIF",
                          IconDeleteTitle: "Delete image",
                          GlobalError: "Oops... An error occurred during the processing of your image file. Please retry",
                          ErrorFileType: "Only jpg/jpeg/png/gif files are allowed",
                        },
                        externalImage: {
                          label: "Choose an image from your favorite website:",
                          placeholder: 'https://media.giphy.com/media/OjJ93PklG2gCs/giphy.gif',
                          notFoundErrorMessage: "The image has not been found. Please, enter another URL address",
                        },
                      },
                      oembed: {
                        title: "Add media",
                        placeholder: "Media URL address",
                        button: "Add media",
                        helper: "Accepted providers: &lt;b&gt;YouTube&lt;/b&gt;, &lt;b&gt;Dailymotion&lt;/b&gt;, &lt;b&gt;Vimeo&lt;/b&gt;, &lt;b&gt;Soundcloud&lt;/b&gt;, ...",
                        bad_request_error: "No embed code found, or site is not supported!",
                        internal_error: "Oops... An internal error has occured. Please, retry.",
                      },
                      validator: {
                        required: "This field is required",
                        url: "Please enter a valid URL address",
                      }
                    };

                    window.LANGUAGE_CODE = "en";
                    window.SUPPORT_LANGUAGE_CODE = "en-us";
                    window.STATIC_URL = "https://d1yggf0lcx0ykm.cloudfront.net/";
                    window.User = {
                        isAuthenticated: false,
                        isStaff: false
                    };

                    

                    
            
        
        window.CURRENCIES = {
            SYMBOLS: {"AUD": {"separator": "", "symbol": "$", "trigram": "AUD"}, "CAD": {"separator": "", "symbol": "$", "trigram": "CAD"}, "CHF": {"separator": "", "symbol": "CHF", "trigram": ""}, "DKK": {"separator": " ", "symbol": "kr", "trigram": "DKK"}, "EUR": {"separator": "", "symbol": "\u20ac", "trigram": ""}, "GBP": {"separator": "", "symbol": "\u00a3", "trigram": ""}, "NOK": {"separator": " ", "symbol": "kr", "trigram": "NOK"}, "USD": {"separator": "", "symbol": "$", "trigram": "USD"}},
            RATES: {"AED": 3.67, "AFN": 69.64, "ALL": 106.3, "AMD": 480.61, "ANG": 1.79, "AOA": 214.36, "ARS": 20.17, "AUD": 1.3, "AWG": 1.77, "AZN": 1.7, "BAM": 1.59, "BBD": 2.0, "BDT": 84.09, "BGN": 1.59, "BHD": 0.38, "BIF": 1766.05, "BMD": 1.0, "BND": 1.31, "BOB": 6.92, "BRL": 3.34, "BSD": 1.0, "BTC": 0.0, "BTN": 65.05, "BWP": 9.58, "BYN": 1.96, "BYR": 20026.25, "BZD": 2.01, "CAD": 1.28, "CDF": 1613.32, "CHF": 0.96, "CLF": 0.02, "CLP": 602.8, "CNH": 6.29, "CNY": 6.29, "COP": 2806.05, "CRC": 567.19, "CUC": 1.0, "CUP": 25.5, "CVE": 90.2, "CZK": 20.62, "DJF": 177.0, "DKK": 6.07, "DOP": 49.58, "DZD": 114.14, "EEK": 14.16, "EGP": 17.6, "ERN": 15.0, "ETB": 27.44, "EUR": 0.81, "FJD": 2.03, "FKP": 0.71, "GBP": 0.71, "GEL": 2.42, "GGP": 0.71, "GHS": 4.43, "GIP": 0.71, "GMD": 47.39, "GNF": 9030.4, "GTQ": 7.42, "GYD": 206.55, "HKD": 7.85, "HNL": 23.71, "HRK": 6.05, "HTG": 64.64, "HUF": 254.02, "IDR": 13763.81, "ILS": 3.53, "IMP": 0.71, "INR": 64.92, "IQD": 1196.07, "IRR": 37636.52, "ISK": 99.02, "JEP": 0.71, "JMD": 126.12, "JOD": 0.71, "JPY": 106.56, "KES": 101.1, "KGS": 68.29, "KHR": 4044.44, "KMF": 401.05, "KPW": 900.0, "KRW": 1057.97, "KWD": 0.3, "KYD": 0.83, "KZT": 320.87, "LAK": 8296.6, "LBP": 1516.17, "LKR": 155.95, "LRD": 131.75, "LSL": 11.87, "LTL": 3.09, "LVL": 0.63, "LYD": 1.33, "MAD": 9.22, "MDL": 16.43, "MGA": 3228.35, "MKD": 50.14, "MMK": 1332.86, "MNT": 2388.92, "MOP": 8.09, "MRO": 355.0, "MRU": 35.39, "MTL": 0.68, "MUR": 33.75, "MVR": 15.4, "MWK": 725.12, "MXN": 18.22, "MYR": 3.87, "MZN": 61.2, "NAD": 11.87, "NGN": 360.34, "NIO": 31.2, "NOK": 7.83, "NPR": 104.08, "NZD": 1.37, "OMR": 0.39, "PAB": 1.0, "PEN": 3.23, "PGK": 3.26, "PHP": 52.02, "PKR": 115.69, "PLN": 3.42, "PYG": 5545.35, "QAR": 3.64, "RON": 3.79, "RSD": 96.27, "RUB": 57.63, "RWF": 866.49, "SAR": 3.75, "SBD": 7.79, "SCR": 13.46, "SDG": 18.18, "SEK": 8.38, "SGD": 1.31, "SHP": 0.71, "SLL": 7664.01, "SOS": 578.41, "SRD": 7.47, "SSP": 130.26, "STD": 19930.46, "STN": 20.06, "SVC": 8.76, "SYP": 514.99, "SZL": 11.87, "THB": 31.2, "TJS": 8.83, "TMT": 3.51, "TND": 2.44, "TOP": 2.23, "TRY": 3.99, "TTD": 6.74, "TWD": 29.17, "TZS": 2259.4, "UAH": 26.32, "UGX": 3694.45, "USD": 1.0, "UYU": 28.33, "UZS": 8107.4, "VEF": 40241.51, "VND": 22802.6, "VUV": 105.58, "WST": 2.53, "XAF": 534.24, "XAG": 0.06, "XAU": 0.0, "XCD": 2.7, "XDR": 0.69, "XOF": 534.24, "XPD": 0.0, "XPF": 97.19, "XPT": 0.0, "YER": 250.31, "ZAR": 11.82, "ZMK": 5253.08, "ZMW": 9.46, "ZWL": 322.36},
            COUNTRIES: {"AD": "EUR", "AE": "USD", "AF": "USD", "AG": "USD", "AI": "USD", "AL": "USD", "AM": "USD", "AN": "USD", "AO": "USD", "AQ": "USD", "AR": "USD", "AS": "USD", "AT": "EUR", "AU": "AUD", "AW": "USD", "AX": "EUR", "AZ": "USD", "BA": "USD", "BB": "USD", "BD": "USD", "BE": "EUR", "BF": "USD", "BG": "USD", "BH": "USD", "BI": "USD", "BJ": "USD", "BL": "EUR", "BM": "USD", "BO": "USD", "BR": "USD", "BS": "USD", "BT": "USD", "BV": "NOK", "BW": "USD", "BY": "USD", "BZ": "USD", "CA": "CAD", "CC": "AUD", "CD": "USD", "CF": "USD", "CG": "USD", "CH": "CHF", "CI": "USD", "CK": "NZD", "CL": "USD", "CM": "USD", "CN": "USD", "CO": "USD", "CR": "USD", "CU": "USD", "CV": "USD", "CX": "AUD", "CY": "EUR", "DE": "EUR", "DJ": "USD", "DK": "DKK", "DM": "USD", "DO": "USD", "DZ": "USD", "EC": "USD", "EE": "EUR", "EG": "USD", "EH": "USD", "ER": "USD", "ES": "EUR", "ET": "USD", "FI": "EUR", "FJ": "USD", "FK": "GBP", "FM": "USD", "FO": "DKK", "FR": "EUR", "GA": "USD", "GB": "GBP", "GD": "USD", "GE": "USD", "GF": "EUR", "GG": "GBP", "GH": "USD", "GI": "GBP", "GL": "DKK", "GM": "USD", "GN": "USD", "GP": "EUR", "GQ": "USD", "GR": "EUR", "GS": "GBP", "GT": "USD", "GU": "USD", "GW": "USD", "GY": "USD", "HM": "AUD", "HN": "USD", "HR": "USD", "HT": "USD", "ID": "USD", "IE": "EUR", "IM": "GBP", "IN": "USD", "IO": "USD", "IQ": "USD", "IR": "USD", "IS": "USD", "IT": "EUR", "JE": "GBP", "JM": "USD", "JO": "USD", "KE": "USD", "KG": "USD", "KH": "USD", "KI": "AUD", "KM": "USD", "KN": "USD", "KP": "USD", "KR": "USD", "KW": "USD", "KY": "USD", "KZ": "USD", "LA": "USD", "LB": "USD", "LC": "USD", "LI": "CHF", "LK": "USD", "LR": "USD", "LS": "USD", "LT": "USD", "LU": "EUR", "LV": "USD", "LY": "USD", "MA": "USD", "MC": "EUR", "MD": "USD", "ME": "EUR", "MF": "EUR", "MG": "USD", "MH": "USD", "MK": "USD", "ML": "USD", "MM": "USD", "MN": "USD", "MO": "USD", "MP": "USD", "MQ": "EUR", "MR": "USD", "MS": "USD", "MT": "EUR", "MU": "USD", "MV": "USD", "MW": "USD", "MZ": "USD", "NA": "USD", "NC": "USD", "NE": "USD", "NF": "AUD", "NG": "USD", "NI": "USD", "NL": "EUR", "NO": "NOK", "NP": "USD", "NR": "AUD", "NU": "NZD", "NZ": "NZD", "OM": "USD", "PA": "USD", "PE": "USD", "PF": "USD", "PG": "USD", "PK": "USD", "PM": "EUR", "PN": "NZD", "PR": "USD", "PT": "EUR", "PW": "USD", "PY": "USD", "QA": "USD", "RE": "EUR", "RO": "USD", "RS": "USD", "RU": "USD", "RW": "USD", "SA": "USD", "SB": "USD", "SC": "USD", "SD": "USD", "SE": "SEK", "SH": "GBP", "SI": "EUR", "SJ": "NOK", "SK": "EUR", "SL": "USD", "SM": "EUR", "SN": "USD", "SO": "USD", "SR": "USD", "ST": "USD", "SV": "USD", "SY": "USD", "SZ": "USD", "TC": "USD", "TD": "USD", "TF": "EUR", "TG": "USD", "TJ": "USD", "TL": "USD", "TM": "USD", "TN": "USD", "TO": "USD", "TR": "USD", "TT": "USD", "TV": "AUD", "TZ": "USD", "UA": "USD", "UG": "USD", "UM": "USD", "US": "USD", "UY": "USD", "UZ": "USD", "VA": "EUR", "VC": "USD", "VE": "USD", "VG": "USD", "VI": "USD", "VN": "USD", "VU": "USD", "WF": "USD", "WS": "USD", "YE": "USD", "YT": "EUR", "ZA": "USD", "ZM": "USD", "ZW": "USD"},
            CURRENT: 'EUR'
        }
        

        window.UFE = {
            locale: document.querySelector('html').getAttribute('lang') || 'en',
            
                user: {
                    is_authenticated: false,
                },
            
            cookies: {
                access_token: 'ul_access_token',
                refresh_token: 'ul_refresh_token',
                tracking_token: 'gclid'
            },
            
            raven_public_dsn: 'https://60875ed89e7043cb980be5a2aa5916f6@sentry.io/182314',
            
            api: {
                ulule: {
                    url: 'https://api.ulule.com/v1',
                    headers: {
                        "Ulule-Version": '2017-09-19'
                    }
                },
                discussions: {
                    url: 'https://discussions.ulule.com/api',
                }
            },
            urls: {
                refresh_token: 'https://connect.ulule.com/refresh/',
                join: 'https://connect.ulule.com/join/',
                signin: 'https://connect.ulule.com/signin/',
                forbidden: 'https://connect.ulule.com/forbidden/'
            }
        }
    
        /*<![CDATA[*/
        window.zEmbed || function(e, t) {
            var n, o, d, i, s, a = [],
                r = document.createElement("iframe");
            window.zEmbed = function() {
                a.push(arguments)
            }, window.zE = window.zE || window.zEmbed, r.src = "javascript:false", r.title = "", r.role = "presentation", (r.frameElement || r).style.cssText = "display: none", d = document.getElementsByTagName("script"), d = d[d.length - 1], d.parentNode.insertBefore(r, d), i = r.contentWindow, s = i.document;
            try {
                o = s
            } catch (e) {
                n = document.domain, r.src = 'javascript:var d=document.open();d.domain="' + n + '";void(0);', o = s
            }
            o.open()._l = function() {
                var o = this.createElement("script");
                n && (this.domain = n), o.id = "js-iframe-async", o.src = e, this.t = +new Date, this.zendeskHost = t, this.zEQueue = a, this.body.appendChild(o)
            }, o.write('<body onload="document._l();">'), o.close()
        }("https://assets.zendesk.com/embeddable_framework/main.js", "ulule.zendesk.com");
        /*]]>*/
        zE(function() {
            zE.setLocale(SUPPORT_LANGUAGE_CODE);
            
        });

        window.zESettings = {
            webWidget: {
                helpCenter: {
                    searchPlaceholder: {
                        '*': "Need help for..."
                    }
                }
            }
        };
    
                
                    var dataLayer = [
                        {
                            'siteCategory': 'Ulule'
                        }
                    ];
                    
                
            (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
                })(window,document,'script','dataLayer','GTM-PX3JCRM');
        $(document).ready(function() {
            $('.js_create_project_gtm_tag').click(function() {
                dataLayer.push({
                    'event': 'GAevent',
                    'eventCategory': 'Project proposal',
                    'eventAction': 'click'
                });
            });
        });
    
            // for script tags with defer attribute
            function whenAvailable(name, callback) {
                var interval = 10; // ms
                window.setTimeout(function() {
                    if (window[name]) {
                        callback(window[name]);
                    } else {
                        window.setTimeout(arguments.callee, interval);
                    }
                }, interval);
            }
        BackBackMenuCloseClosePlusPlusSearchUluleUlule<![CDATA[
                    .st0{fill:#AD0000;}
                    .st1{fill:#ADADAD;}
                    .st2{fill:#111111;}
                    .st3{fill:#949494;}
                    .st4{fill:#FFFFFF;}
                    .st5{fill:#00608B;}
                    .st6{fill-rule:evenodd;clip-rule:evenodd;fill:#FF0000;}
                    .st7{fill-rule:evenodd;clip-rule:evenodd;fill:#00A6F0;}
                ]]>UluleUluleChatFacebookInstagramLinkedInTwitterYouTubefacebooktwitterB Corporation<![CDATA[.st-b{fill:#060708;}.st-w{fill:#fff;}]]><![CDATA[
                    .icon-egg-op {opacity: 0.4;}
                ]]>MenuAll projectsHow it worksStart a projectAbout UluleForumChange language and currencyEnglishEnglishDeutschFrançaisEspañolItalianoPortuguêsNederlandsCatalà€€Euro$USDUnited States Dollar£British Pound$CADCanadian Dollarkr DKKDanish Krone$AUDAustralian Dollarkr SEKSwedish Krona$NZDNew Zealand Dollarkr NOKNorwegian KroneCHFSwiss FrancStart a projectSign inMake good things happenAboutTeamPressIdentityContactMoreOnline helpVox's User AgreementPrivacyTerms of UseJobsGuidelinesBecause Ulule is a French company, the Data is collected and maintained in France. For your information, this processing has been reported to the relevant local authority (Commission Nationale de l'Informatique et des Libertés), under the number 1427763.
 
Let's keep it simple: we work for you. To offer you the best service possible, to make it easy for you to organize a fundraising, to share it with your networks, to promote, to manage your project and your supports.
 
We do not work to collect every data we can about you, to know your social status and your sexual preferences, to save all your friends' birthdays et send it all to advertising agencies searching for stories to tell to their clients.
 
When you create your Ulule account, we save only the datas you want to give us - and they rarely go further than your e-mail or website address. You keep a total control over these datas, you can edit or delete them anytime you want.
 
A word about Facebook, which gathers (often with good reasons) a lot of complaints about its privacy policy : we use Facebook Connect to ease the process for the users who'd rather login with their usual ID. We don't transmit any data to Facebook, except the ones that you'd specifically authorized (notifications on your wall for example).
 
As every website, we're using our own utilization stats to improve the service. We use Google Analytics to collect these datas, which once again aren't nominatives. But if you don't wish to communicate this type of information, you can ask for an opt-out from Google Analytics.
 
The only nominatives infos that we transfer to a third party, punctually and if you allow it, are your Ulule coordinates to the project owner you did support, so he can receive his funds et send you (if there's one) your reward.
 
This is our commitment. We're a stickler for your privacy. Would you have a question, an inquiry, something on your mind, don't hesitate to mail us at : privacy@ulule.com.
 
Then, if you'd rather use good old fashionned paper mail, you can write us here : Ulule, Privacy Service, 8 rue Saint-Fiacre, 75002 Paris. We'll answer you briefly.About UluleHow it worksNewsletterStatisticsHall of famePressTeamFinance your projectOnline helpForumTerms of usePrivacyContact usUlule proOfficial usersDeveloper APIMake good things happenOur ambition: empower creators and entrepreneurs. Ulule enables you to discover and give life to unique projects.They are by our sideEngaged companyCrafted with loveEnglishEnglishDeutschFrançaisEspañolItalianoPortuguêsNederlandsCatalà€€Euro$USDUnited States Dollar£British Pound$CADCanadian Dollarkr DKKDanish Krone$AUDAustralian Dollarkr SEKSwedish Krona$NZDNew Zealand Dollarkr NOKNorwegian KroneCHFSwiss Franc
        $(document).ready(function() {
            $('a[data-next]').on('click', function(e) {
                var elem = $(this);
                var character = elem.attr('href').indexOf('?') === -1 ? '?':'&';
                var parameters = ['lang=en', 'next=' + $(this).data('next')].join('&');
                elem.attr('href', elem.attr('href') + character + parameters);
            });

            $('.b-shutter').prev('.b-nav__link').click(function(e){
                e.preventDefault();
                var shutter = $(this).parent().find('.b-shutter');
                shutter.show();
                shutter.find('.b-shutter__body').animate({
                    left: 0,
                }, 'fast');
                shutter.find('.b-shutter__mask').fadeIn('fast');
                $('body').addClass('is-unscrollable');
            });

            $('.b-shutter__close, .b-shutter__mask').click(function(e){
                e.preventDefault();
                var shutter = $(this).closest('.b-shutter');
                shutter.find('.b-shutter__body').animate({
                    left: -300,
                }, 'fast', function() {
                    shutter.hide();
                });
                shutter.find('.b-shutter__submenu').removeClass('is-open');
                shutter.find('.b-shutter__mask').fadeOut('fast');
                $('body').removeClass('is-unscrollable');
            });

            var toggleMenus = function(menus) {
                menus.each(function(i){
                    var menu = $(this);
                    if (menu.hasClass('js-slider')) {
                        menu.children('.js-menu__panel').slideToggle('fast')
                    }
                    menu.toggleClass('is-open');
                });
                closeMenus($('.js-menu.is-open').not(menus).not(menus.parents()));
            };

            var closeMenus = function(menus) {
                menus.each(function(i){
                    var menu = $(this);
                    if (menu.hasClass('js-slider')) {
                        menu.children('.js-menu__panel').slideUp('fast')
                    }
                    menu.removeClass('is-open');
                });
            };

            $(window).on('location:loaded', function(e, locationData) {
                $('#search-desktop').closest('.js-menu__loader-opener').data('post-data', {
                    country: locationData.country_code
                });

                $(document).on('click', 'a.category-link', function(e) {
                    e.preventDefault();
                    try {
                        var url = new URL(this.href);
                        url.searchParams.set('country', locationData.country_code);
                        window.location.href = url.href;
                    } catch(err) {
                        window.location.href = this.href + "?country=" + locationData.country_code;
                    }
                });

            });

            $(document).on('click', '.js-menu__loader-opener', function (e) {
                e.preventDefault();

                var opener = $(this),
                    targetMenu = opener.closest('.js-menu');

                // if open: do nothing
                if (targetMenu.hasClass('is-open')) {
                    return;
                }

                // if (not open) and (loaded): open
                if (targetMenu.hasClass('is-loaded')) {
                    toggleMenus(targetMenu);
                    return;
                }

                // if (not open) and (not loaded): async load, open on success
                if (opener.data('url') && opener.data('post-data') && !targetMenu.hasClass('is-loading')) {
                    targetMenu.addClass('is-loading');

                    $.post(opener.data('url'), opener.data('post-data')).done(function(data) {
                        if (!data) { return; }

                        targetMenu.addClass('is-loaded');
                        $($.parseHTML(data)).insertAfter(opener);
                        toggleMenus(targetMenu);

                    }).always(function() {
                        targetMenu.removeClass('is-loading')
                    })
                }
            });

            $(document).on('click', '.js-menu__toggler', function (e) {
                e.preventDefault();
                toggleMenus($(this).closest('.js-menu'));
            });

            $(document).click(function(e) {
                if(!$(e.target).closest('.js-menu').length) {
                    closeMenus($('.js-menu.is-open'));
                }
            });

            var submitForm = function (formClass, valueInputClass, submitButtonClass) {
                return function(e){
                    e.preventDefault();

                    $(formClass + ' .selected').removeClass('selected');
                    $(this).addClass('selected');

                    $('input' + valueInputClass).val($(this).data('code'));
                    $(submitButtonClass, $(this).closest(formClass)).trigger('click');
                };
            };

            $('.js-language-switcher-form .b-selector__link').click(
                submitForm('.js-language-switcher-form', '.js-pref-language-code', '.js-language-submit')
            );

            $('.js-currency-switcher-form .b-selector__link').click(
                submitForm('.js-currency-switcher-form', '.js-pref-currency-code', '.js-currency-submit')
            );
        });
    
                if ($.browser === undefined || !$.browser.msie) {
                    document.domain = 'ulule.com';
                }
                window.fbAsyncInit = function() {
                    FB.init({
                        appId      : '120670887943016',
                        xfbml      : true,
                        version    : 'v2.8'
                    });
                    
                };
                (function(d, s, id){
                    var js, fjs = d.getElementsByTagName(s)[0];
                    if (d.getElementById(id)) {return;}
                    js = d.createElement(s); js.id = id;
                    js.src = "//connect.facebook.net/en_US/sdk.js";
                    fjs.parentNode.insertBefore(js, fjs);
                }(document, 'script', 'facebook-jssdk'));
                $(document).ready(function() {
                    $('body').tooltip({
                        selector: '[data-toggle=tooltip]'
                    }).tooltip({
                        selector: '[data-toggle=tooltip-html]',
                        html: true
                    }).addClass('js');
                    $('#wrapper').click(function () {
                        $('.openable').hide();
                        $('.nav-link').removeClass('active');
                    });
                    $('a[data-rel*=facebox],a[rel*=facebox]').facebox({
                        faceboxHtml  : '\
                        <div id="facebox" style="display:none;"> \
                            <div class="popup"> \
                            <div class="content"> \
                            </div> \
                            </div> \
                        </div>',
                        loadingImage: 'https://d1yggf0lcx0ykm.cloudfront.net/site/build/img/loading.gif',
                        loadingHtml  : '<div class="popup-overlay facebox-overlay widget"> \
                            <section> \
                                <header class="see-project"> \
                                    Loading \
                                </header> \
                                <div class="p"> \
                                    <i class="icon icon-spin icon-spinner"></i> \
                                </div> \
                            </section> \
                        </div>'
                    });

                    
                    

                    $('.flash-close').on('click', function(e) {
                        e.preventDefault();
                        $(this).parent().remove();
                    });

                    $(document).on('click', '.facebox-overlay .close-link', function(e) {
                        e.preventDefault();

                        $(document).trigger('close.facebox');
                    });

                    $("#header").on("unfix", function() {
                        $(this).removeClass('fixed');
                    }).on("fix", function() {
                        $(this).addClass('fixed');
                    });

                    if (window.location.hash) {
                        $('#header').trigger('unfix');
                    }
                });
            
            $(document).ready(function() {
                function initializeCurrencies(lang, currency) {
                    new Currency({
                        lang: lang,
                        currency: currency
                    }, {
                        regex: /(<span class="amount-trigram">\s?\w+<\/span>)/,
                        formatTrigram: function (trigram, separator) {
                            return '<span class="amount-trigram">' + separator + trigram +'</span>'
                        },
                        formatValue: function (val) {
                            return '<span class="amount-text">' + val + '</span>'
                        },
                        currencies: window.CURRENCIES.SYMBOLS,
                        rates: window.CURRENCIES.RATES
                    });
                }

                $(document).on('initCurrencies', '.b-list', function(e) {
                    if (currency) {
                        window.CURRENCIES.CURRENT = currency;
                        initializeCurrencies('en', currency);
                    }
                });

                var currency = null;

                
                    $(window).on('location:loaded', function(e, data) {
                        currency = '' + window.CURRENCIES.COUNTRIES[data['country_code']];
                        $('.b-list').trigger('initCurrencies');
                    });
                

            });
        