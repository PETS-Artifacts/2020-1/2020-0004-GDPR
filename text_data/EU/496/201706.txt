
Privacy Policy - maltasupermarket.com
Credit Card Security Notice
Your privacy is important to us. To better protect your privacy, maltasupermarket.com only uses the information collected to process orders. maltasupermarket.com will not sell, trade or rent your information to others and no credit card information is stored in our database.
All transactions are done over a secure server to offer the highest level of security for your shopping. maltasupermarket.com uses industry-standard SSL (Secure Socket Layer) encryption.
Data Protection
1.0	Privacy Policy
We are committed to protecting our visitors' privacy and we will not collect any personal information about you as a visitor unless you provide it voluntarily. Any personal information you communicate to us is kept within our own records in accordance with the Data Protection Act 2001. We therefore consider that we have a legal duty to respect and protect any personal information we collect from you and we will abide by such duty. We take all safeguards necessary to prevent unauthorised access and we do not pass on your details collected from you as a visitor, to any third party unless you give us your consent to do so.
2.0 When you fill the form on the "CONTACT US", we use the personal information submitted in the form only to respond to your message. This personal information will not be kept longer than necessary and will be deleted once the feedback requirement is met.
3.0 If you read or download information from our site, we automatically collect and store the following non-personal information:
3.1 The requested web page or download; 
3.2 Whether the request was successful or not; 
3.3 The date and time when you accessed the site;
3.4 The Internet address of the web site or the domain name of the computer from which you accessed the website; and
3.5 The operating system of the machine running your web browser and the type and version of your web browser.
4.0 Links to other Web Sites
Our site has a number of hyperlinks to other local and international organisations and agencies. In some cases, for the benefit of the visitor, it may be required that we link to other web sites of other organisations after permission is obtained from them respectively. It is important for you to note that upon linking to another site, you are no longer on our website and you become subject to the privacy policy of the new website you are visiting.
5.0 Access to your information
You may request us at any time what information is effectively held on you if any, at that particular time. You have the right to have any inaccuracies corrected and where applicable erased, if they are not already deleted.
6.0 Changes to this Privacy Policy
If there are any changes to this privacy policy, we will replace this page with an updated version. It is therefore in your own interest to check the "Privacy Policy" page any time you access our web site so as to be aware of any changes which may occur from time to time.


Version 2.0
Last Updated: March 2015

