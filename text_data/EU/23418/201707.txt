Terms & Conditions of Use
SoftServe, Inc. (herein referred to as "SoftServe"), maintains this Internet Website as a service to the Internet user community. Access to and use of this is subject to the terms and conditions set forth herein and all applicable laws. SoftServe may revise these terms and conditions from time to time by updating this statement. SoftServe invites the Internet community to browse and peruse this Website for the purpose of personal information, education and communication with SoftServe.
ATTENTION: PLEASE READ THIS STATEMENT CAREFULLY BEFORE USING THIS WEBSITE. USING THIS WEBSITE INDICATES THAT YOU ACCEPT THESE TERMS. IF YOU DO NOT ACCEPT THESE TERMS, DO NOT USE THIS WEBSITE.
Copyright & Trademarks
Copyright Notice
Copyright 1993-2011 by SoftServe, Inc. All rights reserved. While SoftServe maintains copyright protection in all publications it places on the Internet, SoftServe consents to normal downloading, copying and distribution of the information for non-commercial purposes within the user’s organization only. In consideration of this, users agree that copies of the information will retain all copyright and other proprietary notices.
Trademarks and Service marks owned by SoftServe
The SoftServe logo, the SoftServe brand, N-Tier+TM Foundation, CommEntSTM, SSMarketTM, SoftReportTM, Notification Delivery AgentTM, RT Foundation ClassesTM, Software Manufacturing CycleSM, Software Localization SequenceSM, Legacy System Conversion SequenceSM, and product names referenced herein are either registered trademarks, trademarks of SoftServe. All other brand and product names mentioned herein are trademarks of their respective owners.
Trademarks special attributions
Java and all Java-based trademarks are trademarks of Sun Microsystems, Inc. in the United States, other countries, or both. Microsoft, Windows, Windows NT, SQL Server, Office, BackOffice and the Windows logo are trademarks of Microsoft Corporation in the United States, other countries, or both. Other product and company names mentioned herein may be trademarks and/or service marks of their respective owners.
Contacting Us
If you have any questions or comments about our Copyright and Trademarks statement or practices, please contact us via email at info@softserveinc.com. SoftServe reserves the right to modify or update this Copyright Notice without notice.
Privacy Practices
Personal information
Our privacy practices are designed to provide a high level of protection for your personal data.
Information security and quality
We intend to protect the quality and integrity of your personally identifiable information. We have implemented appropriate organizational and technical measures to help us keep your information secure, current, accurate, and complete. We will make a sincere effort to respond in a timely manner to your requests to correct inaccuracies in your personal information. To correct inaccuracies in your personal information, please send the message containing the inaccuracies to the author with details of the correction requested.
Customers Pages
Certain restricted information is available on this service only to registered customers of SoftServe that are registered to receive information via passwords issued by SoftServe. This restricted information is considered confidential and proprietary information of SoftServe. SoftServe authorizes registered customers to download, copy, and use the restricted information only within the customer’s organization, and only for the intended purposes authorized by SoftServe. Issuance of a registration password is conditioned on the customer’s use of the information in accordance with the terms of their software license agreement with SoftServe. Customers shall not transfer their passwords to unauthorized parties.
Cookies
We sometimes collect anonymous information from visits to our Website to help us provide better customer service. For example, we keep track of the domains from which people visit and we also measure visitor activity on the SoftServe Website, but we do so in ways that keep the information anonymous. This anonymous information is sometimes known as "click stream data". SoftServe may use this data to analyze trends and statistics and to help us provide better customer service. Cookies can be used to provide you with tailored information from a Website. A cookie is an element of data that a Website can send to your browser, which may then store it on your system. Some SoftServe pages use cookies so that we can better serve you when you return to our site. You can set your browser to notify you when you receive a cookie, giving you the chance to decide whether to accept it.
Business Relationships
This Website contains links to other Websites. SoftServe is not responsible for the privacy practices or the content of such Websites.
Contacting Us
If you have any questions or comments about our Privacy statement or practices, please contact us via email at info@softserveinc.com. SoftServe reserves the right to modify or update this Privacy statement at any time without notice.
Legal Terms
This Website may contain other proprietary notices and copyright information, the terms of which must be observed and followed. Information on this Website may contain typographical errors or technical inaccuracies. Information may be changed or updated without notice. SoftServe may also make improvements and/or changes in the products and/or the programs described in this information at any time without notice. SoftServe does not want to receive confidential or proprietary information from you through our Website.
Limitation of Liability
SoftServe’s obligations with respect to its products and services are governed solely by the agreements under which they are provided. If you obtain a product or service from SoftServe via this Website that is provided without an agreement, that product or service is provided "AS-IS" with no warranties whatsoever, express or implied, and your use of that product or service is at your own risk. In addition, a link to a non-SoftServe Website does not mean that SoftServe endorses or accepts any responsibility for the content, or the use, of such Website. It is up to you to take precautions to ensure that whatever you select for your use is free of such items as viruses, worms, Trojan horses and other items of a destructive nature.
ATTENTION: IN NO EVENT WILL SOFTSERVE BE LIABLE TO ANY PARTY FOR ANY DIRECT, INDIRECT, SPECIAL OR OTHER CONSEQUENTIAL DAMAGES FOR ANY USE OF THIS WEBSITE, OR ON ANY OTHER HYPER LINKED WEBSITE, INCLUDING, WITHOUT LIMITATION, ANY LOST PROFITS, BUSINESS INTERRUPTION, LOSS OF PROGRAMS OR OTHER DATA ON YOUR INFORMATION HANDLING SYSTEM OR OTHERWISE, EVEN IF WE ARE EXPRESSLY ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
Careers Opportunity
SoftServe, Inc. is an Equal Opportunity Employer.
Children’s data
While almost any one of the Web pages may be directed towards children, SoftServe is committed to comply with applicable laws and requirements, such as the United States’ Children’s Online Privacy Protection Act (COPPA).
Contacting Us
If you have any questions or comments about our Terms of Use, please contact us via email at info@softserveinc.com. SoftServe reserves the right to modify or update this and Terms of Use at any time without notice.

