

Privacy Policy
The Tetris Company ("we" or "us") recognizes the importance of respecting the privacy of those who visit this web site. This Privacy Policy describes how we collect and use your information and gives you choices as to how we use it. We suggest that you review this Privacy Policy periodically as we may update it from time to time.

What do we collect?
Every time you visit our Web sites, our Web servers automatically collect the following non-personally identifiable information: your Internet service provider's address, the referring URL and, information on the pages you access on our sites.a We may also collect personally identifiable information, such as names, addresses and e-mail addresses that you provide when you make a purchase or contact us.

How do we use your information?
The information we collect is used:

to generally improve the content and layout of our Web site
to improve the content and layout of our Web site for individual visitors
to notify you about updates to our Web site
to respond to your questions or comments
to contact you for marketing purposes
for internal review

When you supply us with personal information, and indicate your consent to receive promotional information, the information you provide will be added to our customer file. You may receive periodic mailings from us. If you do not want to receive such mailings from us, then please use our Contact Us form at www.tetris.com to contact us to remove yourself from our contact list. We may send you e-mails with information on new products. If you do not wish to receive such e-mails in the future, please use our Contact Us form at www.tetris.com to contact us to remove yourself from our e-mail list. Please note that when you provide us with your e-mail address in order to make a purchase, we will send you confirmation of your order via e-mail even if you have chosen not to receive future e-mail from us about our products and services. We may also contact you by email, mail or telephone if we have questions about your order.

Who do we share your information with?
We will not share your personal information with any companies that are not part of the Tetris family of companies except third party vendors or consultants who provide services or functions on our behalf, including credit card processing. These vendors or consultants have access to personal information necessary to perform their functions but they are not permitted to disclose or use the information for any other purpose. We may share information with governmental agencies or other companies assisting us in fraud prevention or investigation. We may do so when: (1) permitted or required by law; or, (2) trying to protect against or prevent actual or potential fraud or unauthorized transactions; or, (3) investigating fraud which has already taken place. The information is not provided to these companies for marketing purposes. In the event that there is a change in control of The Tetris Company, or, if all, or substantially all, of the assets of The Tetris Company are sold to a third party, we may transfer the information on our customer file to the third party. This information may include your name, mailing address and e-mail address among other information. Please know that if such a transfer takes place, the third party acquiring the information will be advised of your preferences about how we use your information. After the transfer takes place, use of your information is subject to the Privacy Policy of the third party that acquires it. The acquiring company may continue to operate our Web sites. If so, you should check this Privacy Policy periodically to determine how the acquiring entity will use your information.

What are "cookies"?
Most Internet sites, including ours, collect data about visitors through the use of "cookies". Cookies are small software programs that help sites recognize repeat visitors by assigning a random number to each desktop for tracking content preferences and traffic patterns. Cookies may be used to "connect" your computer to information stored in a separate location. At tetris.com, we use cookies to connect your computer with the information we store for you in our database. Some examples of this information include items added to your shopping cart and user specific information on pages accessed and previous visits to our site. Your information is stored in a safe and secure database. Through cookies we may also: alert you to new products that we think might be of interest to you when you return to our site; record past activity at our site in order to provide you better service when you return to our site; or, customize Web page content based on your browser type or other information that you send us. We do not use cookies to store any of your personal or financial information on your computer. The Tetris Company recognizes that you have a choice to refuse cookies. However, if you wish to purchase from our site, you must accept cookies so that we can securely process your order. Cookies keep track of the items that you order during your shopping session so that when you're ready to check out, all of the items you've placed in your shopping cart will be there and ready for purchase.

Links to Third Party Web sites
Links from our Sites to third party Web sites are provided for your convenience. Any personal information you provide on the linked pages is provided directly to that third party and is subject to that third party's privacy policy, not ours.  Tetris.com is a participant in the Amazon Services LLC Associates Program, an affiliate advertising program designed to provide a means for sites to earn advertising fees by advertising and linking to Amazon.com.

How can you update your information?
You can update your information by sending us an e-mail via our Contact Us form. We will evaluate your request and reply in an appropriate manner and form of communication that ensures the security of the information you have requested. If applicable, you may also log into your account to update your contact information.

How do you know your information is secure?
When you see either a solid key icon or a locked padlock icon in your browser window, then your information is secured through Secure Sockets Layer technology. We use industry-standard encryption technologies when transferring and receiving your information on our sites. If you do not see one of these icons, please contact your Internet service provider. When we transfer and receive certain types of sensitive information, we will redirect you to a secure server. We have appropriate security measures in place in our facilities to protect against the loss, misuse or alteration of information that we have collected from you at our sites so you can feel comfortable and secure. Please note that e-mail is not encrypted and is not considered a secure means of transmitting credit card numbers.

Children's Policy
The Tetris Company does not seek to solicit information from anyone under the age of thirteen (a "Child"). If a Child submits information to us through any part of our Web sites, and we know that the person submitting the information is a Child, we will not use it for any purpose. We will delete the information as soon as we discover it and we will not disclose it to third parties.


Changes To This Privacy Policy Statement
From time to time, we may use the information you provide us for new, unanticipated uses not previously addressed in our Privacy Policy. If our information practices change at some time in the future we will post the policy changes on this page to notify you of these changes. You should check our Web sites periodically for more information on how we use the data we collect. If there is a material change in the manner we use data we collect, we will update this Privacy Policy to reflect these changes.

Contact Us
To contact us for any reason, including a request to be added or taken off a list, or if you have any concerns or questions about this Privacy Policy, please use our Contact Us form at www.tetris.com to contact us.

