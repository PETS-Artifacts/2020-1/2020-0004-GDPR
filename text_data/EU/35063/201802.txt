
What information do we collect?
Google, as a third party vendor, uses cookies to serve ads on the site. Google's use of the DART cookie enables it to serve ads to the users based on their visit to websites on the Internet. Users may opt out of the use of the DART cookie by visiting the Google ad and content network privacy policy..
What do we use your information for?
Any of the information we collect from you may be used in one of the following ways:

to personalize your experience (your information helps us to better respond to your individual needs)
to improve our website (we continually strive to improve our website offerings based on the information and feedback we receive from you)
to improve customer service (your information helps us to more effectively respond to your customer service requests and support needs)
to administer a contest, promotion, survey or other site feature

How do we protect your information?
We implement a variety of security measures to maintain the safety of your personal information when you enter, submit, or access your personal information.
Do we use cookies?
Yes (Cookies are small files that a site or its service provider transfers to your computers hard drive through your Web browser (if you allow) that enables the sites or service providers systems to recognize your browser and capture and remember certain information.
Do we disclose any information to outside parties?
We do not sell, trade, or otherwise transfer to outside parties your personally identifiable information. This does not include trusted third parties who assist us in operating our website, conducting our business, or servicing you, so long as those parties agree to keep this information confidential. We may also release your information when we believe release is appropriate to comply with the law, enforce our site policies, or protect ours or others rights, property, or safety. However, non-personally identifiable visitor information may be provided to other parties for marketing, advertising, or other uses.
Third party links
Occasionally, at our discretion, we may include or offer third party products or services on our website. These third party sites have separate and independent privacy policies. We therefore have no responsibility or liability for the content and activities of these linked sites. Nonetheless, we seek to protect the integrity of our site and welcome any feedback about these sites.
California Online Privacy Protection Act Compliance
Because we value your privacy we have taken the necessary precautions to be in compliance with the California Online Privacy Protection Act. We therefore will not distribute your personal information to outside parties without your consent.
Childrens Online Privacy Protection Act Compliance
We are in compliance with the requirements of COPPA (Childrens Online Privacy Protection Act), we do not collect any information from anyone under 13 years of age. Our website, products and services are all directed to people who are at least 13 years old or older.
Online Privacy Policy Only
This online privacy policy applies only to information collected through our website and not to information collected offline.
Your Consent
By using our site, you consent to our web site privacy policy.
Changes to our Privacy Policy
If we decide to change our privacy policy, we will post those changes on this page.This policy was last modified on June 30, 2015.
Contacting Us
If there are any questions regarding this privacy policy you may contact us using the information below.
Paul Bradbury21465 JelsaCroatia
+385 21 717755 This email address is being protected from spambots. You need JavaScript enabled to view it. 
  
