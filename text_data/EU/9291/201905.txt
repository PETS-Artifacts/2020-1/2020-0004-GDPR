Privacy Policy for www.offer.com.cyLatest Update: 20/05/2018
If you require any more information or have any questions about our privacy policy, please feel free to contact us by email at info@offer.com.cy.
This privacy policy ("Policy") describes how we collect, protect and use the personally identifiable information ("Personal Information") you ("User", "you" or "your") provide on the http://www.offer.com.cy website and any of its products or services (collectively, "Website" or "Services"). It also describes the choices available to you regarding our use of your personal information and how you can access and update this information. This Policy does not apply to the practices of companies that we do not own or control, or to individuals that we do not employ or manage.
Collection of personal information
We receive and store any information you knowingly provide to us when you create an account, publish content, fill any online forms on the Website. When required this information may include your email address, name, phone number, or other Personal Information. You can choose not to provide us with certain information, but then you may not be able to take advantage of some of the Website`s features.
We collect the following personal information when you create an account on our Website, when you post a classified and when you using the Contact Form:

Your Full Name
Email address
Phone(s) numbers
District

How do we use your Personal Data
To investigate and follow up on any comments, inquiries and complaints you may have and to resolve any disputes and problems which may have arisen with respect to the use of our services.
Full Name, District, Phone number(s): To display it at contact information section in your classified page. Please note that when you delete or mark as SOLD your classified then phone number(s) will no longer be visible in your classified page.
Email Address: to send email for account verification, notifications about your classifieds, notifications about business account expiration, promotional email about future Website upgrades.
We do not sell or share your email address or phone number with anyone else.
No third party providers have access to your data, unless specifically required by law.
Managing personal information
You are able to access, add to, update and delete certain Personal Information about you. The information you can view, update, and delete may change as the Website or Services change. When you update information, however, we may maintain a copy of the unrevised information in our records. We will retain your information for as long as your account is active or as needed to provide you Services. Some information may remain in our private records after your deletion of such information from your account. We will retain and use your information as necessary to comply with our legal obligations, resolve disputes, and enforce our agreements. We may use any aggregated data derived from or incorporating your Personal Information after you update or delete it, but not in a manner that would identify you personally.
Collection of non-personal information
When you visit the Website our servers automatically record information that your browser sends. This data may include information such as your computer`s IP address, browser type and version, operating system type and version, language preferences or the webpage you were visiting before you came to our Website, pages of our Website that you visit, the time spent on those pages, information you search for on our Website, access times and dates, and other statistics.
Use of collected information
Any of the information we collect from you may be used to personalize your experience; improve our website; send notification emails such as password reminders, updates, email notifications about your classifieds, etc; run and operate our Website and Services. Non-personal information collected is used only to identify potential cases of abuse and establish statistical information regarding Website traffic and usage. This statistical information is not otherwise aggregated in such a way that would identify any particular user of the system.
Controlling your Personal Data.
If you believe that any information we are holding on you is incorrect or incomplete please contact us as soon as possible. We will promptly correct or remove any information found to be incorrect
Log Files
Like many other Web sites, our Website makes use of log files. The information inside the log files includes internet protocol ( IP ) addresses, type of browser, Internet Service Provider ( ISP ), date/time stamp, referring/exit pages, and number of clicks to analyze trends, administer the site, track user`s movement around the site, and gather demographic information. IP addresses, and other such information are not linked to any information that is personally identifiable.
Cookies and Web Beacons
The Website uses "cookies" to help personalize your online experience. A cookie is a text file that is placed on your hard disk by a web page server. Cookies cannot be used to run programs or deliver viruses to your computer. Cookies are uniquely assigned to you, and can only be read by a web server in the domain that issued the cookie to you. We may use cookies to collect, store, and track information for statistical purposes to operate our Website and Services. You have the ability to accept or decline cookies. Most web browsers automatically accept cookies, but you can usually modify your browser setting to decline cookies if you prefer. If you choose to decline cookies, you will not be able to use and experience the features of the Website and Services.
Types of cookies
There are three different types of cookies:
Session cookies: These cookies expire when the browser is closed
Permanent cookies: these remain in operation, even when you have closed the browser. Our Website use the following permanent cookies:

ocyUsrAuth*: This cookie remember your login details and password so you don’t have to type them in every time you use our Website. This cookie is only created if you check the option `Remember me` during Sign In or Sign Up process (expire after 6 months).
ocyLang: This cookie remember preferable language (expire after 6 months).

Third-party cookies: these are installed by third parties with the aim of collecting certain information to carry out various research into behaviour, demographics etc.
*: We encourage you to NOT choose to stay signed in if you are using a public or shared computer.
Analytics
We like to keep track of what pages and links are popular and which ones don`t get used so much to help us keep our sites relevant and up to date. It`s also very useful to be able to identify trends of how people navigate (find their way through) our Website and if they get `error messages` from web pages.
This group of cookies, often called `analytics cookies` are used to gather this information. These cookies don`t collect information that identifies you. The information collected is anonymous and is grouped with the information from everyone else’s cookies. We can then see the overall patterns of usage rather than any one person’s activity. Analytics cookies only record activity on the site you are on and they are only used to improve how a website works.
Some of our websites and some of the emails you might get from us also contain small invisible images known as `web beacons` or `tracking pixels`. These are used to count the number of times the page or email has been viewed and allows us to measure the effectiveness of its marketing and emails. These web beacons are anonymous and don`t contain or collect any information that identifies you.
We also use `affiliate` cookies. Some of our web pages will contain promotional links to other companies’ sites. If you follow one of these links and then register with or buy something from that other site, a cookie is sometimes used to tell that other site that you came from one of our sites. That other site may then pay us a small amount for the successful referral. This works using a cookie.
Google Analytics
Google Analytics is Google’s analytics tool that helps our website to understand how visitors engage with their properties. It may use a set of cookies to collect information and report website usage statistics without personally identifying individual visitors to Google.
In addition to reporting website usage statistics, Google Analytics can also be used, together with some of the advertising cookies, to help show more relevant ads on Google properties (like Google Search) and across the web and to measure interactions with the ads Google shows.
Learn more about Analytics cookies and privacy information.
Advertisement
We may display online advertisements and we may share aggregated and non-identifying information about our customers that we collect through the registration process or through online surveys and promotions with certain advertisers. We do not share personally identifiable information about individual customers with advertisers. In some instances, we may use this aggregated and non-identifying information to deliver tailored advertisements to the intended audience.
DoubleClick DART Cookie
.:: Google, as a third party vendor, uses cookies to serve ads on our Website. .:: Google`s use of the DART cookie enables it to serve ads to users based on their visit to our Website and other sites on the Internet. .:: Users may opt out of the use of the DART cookie by visiting the Google ad and content network privacy policy at the following URL: http://www.google.com/privacy_ads.html
Some of our advertising partners may use cookies and web beacons on our site. Our advertising partners include:
Google Adsense
These third-party ad servers or ad networks use technology to the advertisements and links that appear on www.offer.com.cy send directly to your browsers. They automatically receive your IP address when this occurs. Other technologies ( such as cookies, JavaScript, or Web Beacons ) may also be used by the third-party ad networks to measure the effectiveness of their advertisements and / or to personalize the advertising content that you see.
Our Website has no access to or control over these cookies that are used by third-party advertisers.
You should consult the respective privacy policies of these third-party ad servers for more detailed information on their practices as well as for instructions about how to opt-out of certain practices. www.offer.com.cy`s privacy policy does not apply to, and we cannot control the activities of, such other advertisers or web sites.
If you wish to disable cookies, you may do so through your individual browser options. More detailed information about cookie management with specific web browsers can be found at the browsers` respective websites.
Links to other websites
Our Website contains links to other websites that are not owned or controlled by us. Please be aware that we are not responsible for the privacy practices of such other websites or third parties. We encourage you to be aware when you leave our Website and to read the privacy statements of each and every website that may collect personal information.
Information security
We secure information you provide on computer servers in a controlled, secure environment, protected from unauthorized access, use, or disclosure. We maintain reasonable administrative, technical, and physical safeguards in an effort to protect against unauthorized access, use, modification, and disclosure of personal information in its control and custody. However, no data transmission over the Internet or wireless network can be guaranteed. Therefore, while we strive to protect your personal information, you acknowledge that (i) there are security and privacy limitations of the Internet which are beyond our control; (ii) the security, integrity, and privacy of any and all information and data exchanged between you and our Website cannot be guaranteed; and (iii) any such information and data may be viewed or tampered with in transit by a third party, despite best efforts.
PayPal
If you choose to use PayPal to finalize and pay for services we provide (like promoting a classified), you will provide your credit card number, directly to PayPal. PayPal`s privacy policy will apply to the information you provide on the PayPal Web site.
Data breach
In the event we become aware that the security of the Website has been compromised or users Personal Information has been disclosed to unrelated third parties as a result of external activity, including, but not limited to, security attacks or fraud, we reserve the right to take reasonably appropriate measures, including, but not limited to, investigation and reporting, as well as notification to and cooperation with law enforcement authorities. In the event of a data breach, we will make reasonable efforts to notify affected individuals if we believe that there is a reasonable risk of harm to the user as a result of the breach or if notice is otherwise required by law. When we do we will post a notice on the Website.
Legal disclosure
We will disclose any information we collect, use or receive if required or permitted by law, such as to comply with a subpoena, or similar legal process, and when we believe in good faith that disclosure is necessary to protect our rights, protect your safety or the safety of others, investigate fraud, or respond to a government request.
Changes and amendments
We reserve the right to modify this privacy policy relating to the Website or Services at any time, effective upon posting of an updated version of this privacy policy on the Website.
Acceptance of this policy
You acknowledge that you have read this Policy and agree to all its terms and conditions. By using the Website or its Services you agree to be bound by this Policy. If you do not agree to abide by the terms of this Policy, you are not authorized to use or access the Website and its Services.