
Terms of use 
Responsibility for the learning materials generated and published using 
LearningApps.org lies with the respective authors. LearningApps.org may only be used 
for generating and publishing content if no rights of third parties (e.g. copyright) are 
violated. 

We are unable to check the legality of content that is generated and published by third 
parties using LearningApps.org before its publication. Therefore, LearningApps.org 
cannot be held responsible for the content of individual Apps. If you find inappropriate 
content that is in any way illegal, defamatory, sexually harassing, infringing on copyright 
etc., please 

By creating and publishing content on LearningApps.org you agree that other users can use, copy and adapt your Apps without reference to you for free.

Data Protection
Personal data are treated with confidentiality. They are protected from access by 
unauthorized third parties within the scope of legal provisions, and will not be passed 
on. Exception: By entering personal data in your personal profile, you agree to have these data 
published on LearningApps.org.


Without Guarantee
All services of LearningApps.org are offered without any kind of guarantee. No 
guarantee is given for availability, reliability, completeness or security of the service. 
Any and all liability is declined.


Applied law and jurisdiction
Swiss law applies.

LearningApps.org is a platform to support teaching and learning processes and is developed and maintained by the LearningApps Association . The use of LearningApps.org and any content produced with help of our authroing tools is free of charge for educational use. We reserve the right to limit or prohibit extensive use of LearningApps outside educational context.Responsibility for the learning materials generated and published using LearningApps.org lies with the respective authors. LearningApps.org may only be used for generating and publishing content if no rights of third parties (e.g. copyright) are violated.We are unable to check the legality of content that is generated and published by third parties using LearningApps.org before its publication. Therefore, LearningApps.org cannot be held responsible for the content of individual Apps. If you find inappropriate content that is in any way illegal, defamatory, sexually harassing, infringing on copyright etc., please let us know so that we can remove it immediately.By creating and publishing content on LearningApps.org you agree that other users can use, copy and adapt your Apps without reference to you for free.Personal data are treated with confidentiality. They are protected from access by unauthorized third parties within the scope of legal provisions, and will not be passed on. Exception: By entering personal data in your personal profile, you agree to have these data published on LearningApps.org.All services of LearningApps.org are offered without any kind of guarantee. No guarantee is given for availability, reliability, completeness or security of the service. Any and all liability is declined.Swiss law applies.