
This Privacy and Cookie Policy is valid for everyone, in accordance with the General Terms and Conditions of Business and Use of Website www.visitljubljana.com, considered as a user, and the website in its entirety, including its subpages.A user is considered any legal entity or a natural person visiting the www.visitljubljana.com website (hereinafter referred to as: the User).A buyer is any legal entity or a natural person, who concludes one of the agreements in accordance with the General Terms and Conditions of Business and Use of Website (hereinafter referred to as: the Buyer).The User consents to the Privacy and Cookie Policy by using the website. The User agrees to any changes made by continuing the use of the website.I - Personal information protectionThe Ljubljana Tourism Public Institute (javni zavod Turizem Ljubljana) respects your privacy and undertakes to use your personal information exclusively for the intended use. Your personal information shall be protected in due diligence in pursuant to the legislation in the field of personal information protection (General Data Regulation Protection (GDPR) - Regulation EU 2016/679 and Personal Data Protection Act – ZVOP-1), Rules on Protection of Personal Data and internal acts of our institute.1. Controller of Personal InformationThe Ljubljana Tourism Public Institute (javni zavod Turizem Ljubljana), Krekov trg 10, 1000 Ljubljana, Slovenia is the Controller of Personal Information of the information processed in pursuant to the Rules on Protection of Personal Data.2. Authorised Person for Personal Information ProtectionThe Authorised Person for Personal Information Protection at the Ljubljana Tourism Public Institute (javni zavod Turizem Ljubljana) is Katja Hrvatin Rakar, available at the following e-mail: katja.hrvatin(at)visitljubljana.si.3. Processor of Personal InformationThe contractual Processor of Personal Information of the Ljubljana Tourism Public Institute (javni zavod Turizem Ljubljana) is the Innovatif d.o.o. Company, Poljanski nasip 6, 1000 Ljubljana, Slovenia. The Personal Information Controller and Processor have determined the mutual relationship and the obligations with a written contract.4. Transmission of Personal Information to Third Parties or to Third CountriesThe Ljubljana Tourism Public Institute (javni zavod Turizem Ljubljana) does not forward any Personal Information to third parties or to third countries.The two exceptions as follows:We are using the online platform MailChimp for sending an e-newsletter, where personal information is saved for forwarding such news (name, surname, e-mail, company name, country). The MailChimp on-line platform guarantees in its General Terms and Conditions as well as the Privacy Policy to respect the rules of the General Data Protection Regulation (GDPR) (Decision EU 2016/679) and confirms to have received the compliance certificate with the protection shield EU-ZDA (EU-U.S. Privacy Shield Framework).We use the services of the Survey Monkey platform for the execution of the post/sales activities, such as checking for the satisfaction of the purchases in the online shop. This online platform guarantees to respect the rules of the General Data Protection Regulation (GDPR) (Decision EU 2016/679) and confirms to have received the compliance certificate with the protection shield EU-ZDA (EU-U.S. Privacy Shield Framework).5. Types of Personal Information and Intention of UseThe Ljubljana Tourism Public Institute (javni zavod Turizem Ljubljana) collects and processes personal information according to the consent of individuals and the concluded agreement for the delivery of goods or agreement, or any other method, on the execution of the tourist arrangement.5.1 Treatment Based on ConsentThe Ljubljana Tourism Public Institute (javni zavod Turizem Ljubljana) collects and processed personal information of individuals for the intention of forwarding e-newsletter, information on business opportunities and the possibilities of collaboration in the field of business and congress tourism in Ljubljana, and public announcements (press releases), additional material for media and invitations to presentations and events, intended to notify the public on the condition and development of tourism in Ljubljana. The aforementioned notifications are forwarded exclusively via e-mail.The Ljubljana Tourism Public Institute (javni zavod Turizem Ljubljana) collects and processes from the individuals the following personal information: name, surname, e-mail.For the intention of business communication, the Ljubljana Tourism Public Institute (javni zavod Turizem Ljubljana) collects and processes the following personal information: name, surname, e-mail, company name and address.5.2. Treatment Based on AgreementThe Ljubljana Tourism Public Institute (javni zavod Turizem Ljubljana) collects and processes personal information according to the concluded agreement for the delivery of goods or agreement, or any other method, on the execution of the tourist arrangement, as determined by the General Business Terms and Conditions.The personal information, collected accordingly, are necessary for the execution of contractual obligation on the delivery of purchased goods or the implementation of the ordered service within the tourist arrangement offer or the post-sales activities, such as reviewing satisfaction on the purchase. The Ljubljana Tourism public institution executes the post-sales activities via e-mail and shares your personal information to trustworthy third parties, who collaborate in the organisation.The Ljubljana Tourism Public Institute (javni zavod Turizem Ljubljana) collects and processes from the individuals the following personal information:name
surname
e-mail
address
post code
city
country.
The Ljubljana Tourism Public Institute (javni zavod Turizem Ljubljana) does not collect numbers or any other information regarding the credit cards, or the buyer forwards via a website. This information is collected solely by the providers of credit card payment.5.3. Analysis Based on Legal InterestThe Public Institute Ljubljana Tourism (Javni zavod Turizem Ljubljana) collects and analyses data of individuals based on legal interest in the following cases:Photo Library: Based on personal information from a form (name, surname, e-mail, purpose of use of the photographs) the Ljubljana Tourism will enable access to the photographs to an individual. The Public Institute Ljubljana Tourism (Javni zavod Turizem Ljubljana) shall archive the request data for 3 (three) years due to a possibility of identifying the purposes of use of the forwarded photographs.
Requests for Ordering Guided Tours or Other Programmes, available at the following online links Tours and Trips and Incentive Programmes. The Public Institute Ljubljana Tourism (Javni zavod Turizem Ljubljana) shall use the information from the submitted request solely for the purposes of preparing an offer. In case of providers being a third party, the Ljubljana Tourism shall forward this information to a third party. The Public Institute Ljubljana Tourism (Javni zavod Turizem Ljubljana) shall archive the request data for 3 (three) years.
Request for Proposal. The Public Institute Ljubljana Tourism (Javni zavod Turizem Ljubljana) shall use the information from the submitted request solely for the purposes of preparing an offer. In case of providers being a third party, the Ljubljana Tourism shall forward this information to a third party. The Public Institute Ljubljana Tourism (Javni zavod Turizem Ljubljana) shall archive the general request data for 3 (three) years.
6. Personal Information ProtectionThe Ljubljana Tourism Public Institute (javni zavod Turizem Ljubljana) executes all necessary legal, organisational and adequate logistics and technical procedures and measure to guarantee the personal information protection, via their internal acts or their contractual partners by:protecting the facilities, equipment and system software, including the entry-exit units;
protecting the applicative software, which is used for information processing;
prevents any unauthorised access to the personal information during their transmission, including the transmission using telecommunication means and networks;
guarantees an effective method of blocking, disposing, deletion and anonymising personal information;
enables the information of the time individual personal information has been used and introduced to the data collection and by whom for the period of storing personal information.
7. Personal Information Storage DeadlineThe personal information of an individual, acquired by consent, can be processed and stored until the intention is present or until the cancellation of the individual’s consent.The personal information of an individual, acquired by an agreement, can be processed and stored 5 (five) years after the fulfilment of contractual obligations of the both parties. The invoices are archived for 10 (ten) years after the year of the invoice in accordance with the legislation form the area of value added tax.8. Rights of IndividualIn accordance with the valid legislation in the field of personal information protection the individual has the right to revoke their consent at all times, request a review, a correction, limitation of the treatment or deletion of their personal information by informing us in written via mail at Javni zavod Turizem Ljubljana, Krekov trg 10, 1000 Ljubljana, Slovenia or e-mail at info(at)visitljubljana.si. The aforementioned address can also be used for an individual to submit their complaint regarding the treatment of their personal information.An individual has the right to submit a complaint regarding their personal information directly to the competent authority, i.e. Information Commissioner of the Republic of Slovenia.9. Behaviour in Case of InfringementIn case of infringement of the personal information protection the Public Institute of Tourism Ljubljana (javni zavod Turizem Ljubljana) shall perform any internal and external measures (technical, organisational) to protect the rights and interest of an individual, notify the involved persons and notify the competent authority in the Republic of Slovenia.II - Use of cookiesCookies are small text files, which are used by websites to enable more efficient user experience.Pursuant to the legislation the cookies are stored on your device in case, they are necessary for displaying the website. We need your consent for any other type of cookies.This website uses various types of cookies. Some are used on request by other websites, which appear on your website.Following cookies are used to guarantee the function of the entire website:Strictly necessary cookies enable a functional website, basic operations, such as website navigation and access to the safe areas of the website. A website does not function properly without these cookies.
Cookies for monitoring statistics are the cookies, we need to monitor visits to the website with the purpose of improving the user experience.
Advertising Cookies are used for tracking users on the websites. Their purpose is to display ads, which are appropriate and interesting to an individual user.
An accurate description of an individual cookie and the time of their installation is clear from the table below:Strictly Necessary CookiesStrictly necessary cookies enable a functional website, basic operations, such as website navigation and access to the safe areas of the website. A website does not function properly without these cookies.Cookie name
Service
Validity
Description
CookieConsent
Visit Ljubljana
1 year
Stores the user's cookie consent state for the current domain.
MyVisit
Visit Ljubljana
1 year
This cookie saves information about non-registered users' itineraries.
__cfduid [x3]
blueskytraveler.com, four-magazine.com, twoscotsabroad.com
1 year
Used by the content network, Cloudflare, to identify trusted web traffic.
incap_ses_#
greengopost.com
session
Preserves users states across page requests.
visid_incap_#
greengopost.com
1 year
Preserves users states across page requests.
___utmvbfEuvEzYB
greengopost.com
session
Preserves users states across page requests.
___utmvbFIuvEzYB
greengopost.com
session
Preserves users states across page requests.
___utmvmfEuvEzYB
greengopost.com
session
Preserves users states across page requests.
___utmvmFIuvEzYB
greengopost.com
session
Preserves users states across page requests.
Cookies for Statistics MonitoringCookies for statistical purposes allow the owners of websites understand the way the visitors use their website by anonymously collect and report information.Cookie name
Service
Validity
Description
_ga
Google Analytics
2 year
Registers a unique ID that is used to generate statistical data on how the visitor uses the website.
_gat
Google Analytics
session
Used by Google Analytics to throttle request rate.
_gid
Google Analytics
session
Registers a unique ID that is used to generate statistical data on how the visitor uses the website.
_pinterest_cm
pinterest.com
1 year
Used by Pinterest to track the usage of services.
Advertising CookiesThe Marketing Cookies are used for tracking users on the websites. Their purpose is to display ads, which are appropriate and interesting to an individual user.Cookie name
Service
Validity
Description
ads/ga-audiences
Google Analytics
session
Used by Google AdWords to re-engage visitors that are likely to convert to customers based on the visitor's online behaviour across websites.
Collect
Google Analytics
session
Used to send data to Google Analytics about the visitor's device and behaviour. Tracks the visitor across devices and marketing channels.
Fr
Facebook
3 months
Used by Facebook to deliver a series of advertisement products such as real time bidding from third party advertisers.
impression.php/#
Facebook
session
Used by Facebook to register impressions on pages with the Facebook login button.
PREF
YouTube
8 months
Registers a unique ID that is used by Google to keep statistics of how the visitor uses YouTube videos across different websites.
VISITOR_INFO1_LIVE
YouTube
179 days
Tries to estimate the users' bandwidth on pages with integrated YouTube videos.
YSC
YouTube
session
Registers a unique ID to keep statistics of what videos from YouTube the user has seen.
III - ValidityThis Privacy and Cookie Policy of the Public Institute of Tourism Ljubljana (javni zavod Turizem Ljubljana) shall be supplemented if needed, which you will be notified about.This Policy is published at the www.visitljubljana.com website and is valid from 15 May 2018. It was last updated 12 September 2018.

Change settings


