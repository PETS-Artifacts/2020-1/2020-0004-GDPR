
Privacy Policy
This statement relates to the Environmental Protection Agency's (EPA) privacy practices in connection with its website(s). The EPA is not responsible for the content or privacy practices of other websites that are/may be linked to this site. Any external links to other websites are clearly identifiable as such. Some technical terms used in this statement are explained in the glossary at the end of this page. 
General statement
The EPA fully respects your right to privacy, and will not collect any personal data about you through this website that you do not give voluntarily. Any personal data that you volunteer to the EPA will be treated with the highest standards of security and confidentiality, strictly in accordance with the Data Protection Acts, 1988 & 2003.
Collection and use of personal information
The EPA does not collect any personal data about you on this website, apart from information which you volunteer (for example by e-mailing us, by using our online feedback form, or online publications shop). Any information which you provide in this way is not made available to any third parties, and is used by the EPA only for the purpose for which you provided it.
Collection and use of technical information  
We use cookies to collect non-personal information about how visitors use our website.  These cookies are used as part of Google Analytics, a web analytics service that enables us to track how visitors use our website.  The goal of this application is to help us improve the quality of our site.  Google Analytics uses first-party cookies.  
Technical details in connection with visits to this website are logged for statistical purposes. No information is collected that could be used by us to identify website visitors.  The EPA will make no attempt to identify individual visitors, or to associate the technical details with any individual.  You should note that technical details, which we cannot associate with any identifiable individual, do not constitute "personal data" for the purposes of the Data Protection Acts, 1988 & 2003.
Glossary of technical terms
Web browser - The piece of software you use to read web pages. Examples are Microsoft Internet Explorer, Netscape Navigator, Mozilla Firefox and Opera. 
IP address - The identifying details for your computer (or your internet company’s computer), expressed in "internet protocol" code (for example 192.168.72.34). Every computer connected to the web has a unique IP address, although the address may not be the same every time a connection is made. 
Cookies - Small pieces of information, stored in simple text files, placed on your computer by a web site. Cookies can be read by the web site on your subsequent visits. The information stored in a cookie may relate to your browsing habits on the web page, or a unique identification number so that the web site can 'remember' you on your return visit. Generally speaking, cookies do not contain personal information from which you can be identified, unless you have furnished such information to the web site. 


