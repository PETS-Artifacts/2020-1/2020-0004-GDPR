
    Welche Daten werden an der Universität Bielefeld verarbeitet?
  

Generell werden bei dem Besuch einer Webseite aus technischen Gründen automatisch Informationen in Server Log Files gespeichert, die Ihr Browser an uns übermittelt. Das sind:

IP-Adresse
Dateinamen
Zugriffsstatus (http-Statuscode)
abgerufene Datenmenge
Datum und Uhrzeit der Serveranfrage
Referrer-URL (die zuvor besuchte Seite wird nur im Fehlerfalle (Error 404) im Log gespeichert)

Diese gespeicherten Daten werden ausschließlich zu Zwecken der Identifikation und Nachverfolgung unzulässiger Zugriffsversuche und Zugriffe auf den Web-Server 
   verwendet und sind nicht einer bestimmten Person zuordenbar. Ein Abgleich mit anderen Datenbeständen oder eine Weitergabe an Dritte erfolgt nicht. Die Daten 
   werden für eine Dauer von längstens sieben Tagen gespeichert und nach einer anonymisierten, statistischen Auswertung gelöscht. Daten, deren weitere Aufbewahrung 
   zu Beweiszwecken erforderlich ist, sind bis zur endgültigen Klärung des jeweiligen Vorfalls von der Löschung ausgenommen.
Formulare und webbasierte Dienste
Das Webangebot der Universität Bielefeld enthält an einigen Stellen die Möglichkeit, weitere personenbezogen Daten mitzuteilen in Form von Formularen und der 
   Nutzung von webbasierten Diensten. So können über Webformulare Informationen angefordert und Newsletter abonniert werden. Die Nutzung dieser Dienste seitens der 
   Nutzer erfolgt ausdrücklich auf freiwilliger Basis. Welche Daten im Einzelnen erhoben und verarbeitet werden, darüber wird an den entsprechenden Formularen bzw. 
   Webdiensten informiert. Die Nutzung dieser zusätzlichen Möglichkeiten und die damit verbundene Eingabe von personenbezogenen Daten erfolgen ausschließlich auf 
   freiwilliger Basis. Die Daten werden ausschließlich zu dem dort angegebenen Zweck verarbeitet. Erfolgt eine Weitergabe an Dritte, so erfolgt dies nur auf Basis 
   einer gesetzlichen Erlaubnis oder wird explizit durch eine informierte Einwilligung legitimiert.
Cookies
Ihr Browser speichert zudem sogenannte Cookies. Cookies sind Dateien, die von dem Anbieter einer Webseite im Verzeichnis des Browserprogramms auf dem Rechner der 
   Nutzerin / des Nutzers abgelegt werden können. Diese Dateien enthalten Textinformationen und können bei einem erneuten Seitenaufruf vom Anbieter wieder gelesen werden. 
   Der Anbieter kann diese Cookies bspw. dazu nutzen, Seiten immer in der von der Nutzerin / dem Nutzer gewählten Sprache auszuliefern.
Das Speichern von Cookies kann in den Einstellungen Ihres Browsers ausgeschaltet oder mit einer Verfallszeit versehen werden. Durch das Deaktivieren von Cookies 
   können allerdings einige Funktionen, die über Cookies gesteuert werden, dann nur noch eingeschränkt oder gar nicht genutzt werden.
Das Webangebot der Universität Bielefeld verwendet für folgende Zwecke Cookies:

Wiedererkennung einer Nutzerin / eines Nutzers während einer Session, um Sprachenauswahl und weitere persönlich gemachte Einstellungen zuordnen zu können bspw. mit einem Session-Cookie).
Anonyme Wiedererkennung einer Nutzerin / eines Nutzers durch die Webanalysesoftware Matomo (vormals: Piwik). Das genutzte Cookie "PIWIK_SESSID" o. ä. ist 
        mit einer Lebensdauer von 1 Woche versehen und wird danach von Ihrem Browser gelöscht. Bei erneutem Besuch des Webangebots wird die Gültigkeit wieder auf 1 Woche gesetzt.
Speicherung des Widerspruchs gegen Tracking durch die Webanalysesoftware Matomo (vormals: Piwik) im permanenten Cookie "piwik_ignore".

Darüber hinaus können einzelne über die Webseiten verlinkte Webanwendungen (Lernplattform, Foren, Blogs etc.) ggf. weitere Cookies setzen. Diese werden in den zu den 
   jeweiligen Diensten gehörenden Datenschutzerklärungen beschrieben, sofern sie abweichen von dieser Datenschutzerklärung.
Webanalyse mittels Matomo (vormals Piwik)
Zur Verbesserung des Webangebots setzt die Universität Bielefeld die Webanalysesoftware des Open-Source-Projekts Matomo ein.
Die Webanalysesoftware wertet für die Erstellung von Statistiken folgende Daten über den Besuch einer Webseite aus:

abgerufene Webseite/-adresse
anonymisierte IP-Adresse
Datum und Uhrzeit des Seitenaufrufs
abgerufene Datenmenge
Webseite oder Suchmaschine, von der aus Sie auf eine Seite zugegriffen haben (Referrer)
Browsertyp und-version / verwendetes Betriebssystem

Diese Daten werden anonymisiert (nicht personenbezogen) gespeichert und nur für die Verbesserung des Webangebots verwendet. Die Anonymisierung erfolgt durch Kürzen der 
   IP-Adresse, sodass diese keinem Gerät und keiner Person mehr zugeordnet werden kann.
Sie können der Aufzeichnung von Tracking-Informationen widersprechen. Dazu wird in Ihrem Browser ein sogenannter Deaktivierungs-Cookie abgelegt. Wenn Sie den 
   Deaktivierungs-Cookie aus ihrem Browser löschen, müssen Sie den Prozess wiederholen.
Sie können sich im Folgenden entscheiden, ob in Ihrem Browser ein eindeutiger Webanalyse-Cookie abgelegt werden darf, um dem Betreiber des Webangebots die Erfassung 
   und Analyse verschiedener statistischer Daten zu ermöglichen.
Wenn Sie sich dagegen entscheiden möchten, klicken Sie den folgenden Link an, um den Matomo-Deaktivierungs-Cookie in Ihrem Browser abzulegen. Ihr Besuch dieser Website 
   wird aktuell von der Matomo-Webanalyse erfasst.

Wenn Sie das Opt-Out Cookie aus ihrem Browser löschen, müssen Sie den Prozess wiederholen.
Suche in den Internetseiten der Universität Bielefeld
Die Suche im Webangebot der Universität Bielefeld erfolgt mittels Uni-Suche (https://search.uni-bielefeld.de/).
Die Suche ist ein Angebot im Verantwortungsbereich der Universitätsbibliothek Bielefeld. Beim Aufruf von Webseiten der Universität Bielefeld werden keine Daten an Fremde übermittelt.
Bei einer Suchanfrage werden die Suchbegriffe, Ihre IP-Adresse und Browserinformationen in einer Server Log-Datei gespeichert (siehe Abschnitt Speicherung von Browserdaten). 
   Beim Aufrufen der Uni-Suche wird ein Cookie mit dem Namen "SearchUniBI" gesetzt und beim Verlassen der Seite gelöscht.
Nutzung von Sozialen Netzwerken
Facebook, Twitter, Instagram
Das Webangebot der Universität Bielefeld verwendet keine direkten Einbindungen von Social Media Plugins (bspw. Like-Buttons) der sozialen Netzwerke Facebook,
Twitter oder Instagram. Somit werden beim Aufruf der Webseiten keine Daten an externe soziale Netzwerke übermittelt. Erst wenn Sie Icons/Symbole/Links der sozialen 
Netzwerke auf den Webseiten der Universität anklicken und die darüber verlinkten Seiten aufrufen, verlassen Sie die Webseiten, die sich in der Verantwortung der Universität Bielefeld 
befinden, und es erfolgen Aufrufe externer Webseiten entsprechend den Bedingungen des jeweiligen Anbieters / sozialen Netzwerks.
YouTube
Die Universität Bielefeld verwendet für die Einbindung von Videos den Anbieter YouTube LLC , 901 Cherry Avenue, San Bruno, CA 94066, USA, vertreten durch Google Inc., 
1600 Amphitheatre Parkway, Mountain View, CA 94043, USA. Normalerweise wird bereits bei einem Aufruf einer Seite mit eingebetteten Videos Ihre IP-Adresse an 
YouTube gesendet und Cookies auf Ihrem Rechner installiert. Wir haben unsere YouTube-Videos jedoch mit dem erweiterten Datenschutzmodus eingebunden
 (in diesem Fall nimmt YouTube immer noch Kontakt zu dem Dienst Double Klick von Google auf, doch werden dabei laut der Datenschutzerklärung von 
Google personenbezogene Daten nicht ausgewertet). Dadurch werden von YouTube keine Informationen über die Besucher mehr gespeichert, es sei denn, 
sie sehen sich das Video an. Wenn Sie das Video anklicken, wird Ihre IP-Adresse an YouTube übermittelt und YouTube erfährt, dass Sie das Video angesehen haben. 
Sind Sie bei YouTube eingeloggt, wird diese Information auch Ihrem Benutzerkonto zugeordnet (dies können Sie verhindern, indem Sie sich vor dem Aufrufen des Videos bei YouTube ausloggen).
Die Universität Bielefeld hat von der dann möglichen Erhebung und Verwendung Ihrer Daten durch YouTube weder Kenntnis noch hat sie  darauf Einfluss. 
Nähere Informationen können Sie der Datenschutzerklärung von YouTube  unter  www.google.de/intl/de/policies/privacy/ entnehmen. Generelle Informationen zum Umgang mit und die 
Deaktivierung von Cookies entnehmen Sie bitte dem Abschnitt Cookies auf dieser Seite.
Panopto
Auf den Seiten der Universität sind außerdem Videos der Videoplattform Panopto eingebunden. Die Videoplattform wird von dem Anbieter Panopto EMEA Limited, Highgate Studios, 53-79
 Highgate Road, London NW5 1TL, UK angeboten und betrieben. Die bei einem Aufruf einer Seite mit eingebetteten Panopto-Videos erhobenen Daten (IP-Adresse, 
Inhalt des Panopto-Sitzungscookies in Ihrem Browser) können von der Universität Bielefeld nicht und von Panopto EMEA Limited ausschließlich mit ausdrücklicher Genehmigung durch 
die Universität Bielefeld eingesehen werden. Weitere generelle Datenschutzhinweise zu Panopto erhalten Sie unter 

 https://www.panopto.com/privacy/
