
Privacy of your information
StarNow (owned and operated by StarNow Limited) collects personal information about you through your use of the services and the web site, including your registration details; and information relating to your use of the web site and the content you access.
You agree that StarNow may use this personal information to assist us to provide the services to you, for internal research purposes, to verify your identity, for promoting and marketing other StarNow products and services to you, and for any other use that you authorize.
You agree that StarNow may use your images, profile text and/or media files for the promotion of the website. You also agree when you choose to make your profile public or include your photo in the Talent Directory that StarNow will publish your personal information and photos, and allow other members to contact you.
We will not sell your personal information without your consent, however you agree that StarNow may disclose your personal information, including your name and contact details, to the relevant authorities, parties and/or the applicable intellectual property right holders (or their representatives) where the law requires or allows us to do so.
You can access and correct your personal information at any time.
Cookies
A cookie is a piece of information that is stored on your computer’s hard drive, or on your mobile device. StarNow offers many features that are only accessible when cookies are enabled. Cookies also help us to remember your membership details and preferences.
If you visit StarNow with your browser settings adjusted to accept cookies, we understand that you want to use our products and services. However, you can disable cookies at any time.
Find out more about cookies and learn how to adjust your cookie settings.
Third Party Services
StarNow may contain links to other websites ("Third Party Services") and may post your information on other websites (including, but not limited to, Facebook, Twitter, Instagram and YouTube). If you choose to use a Third Party Service they may use and share your data in accordance with their privacy policy and your privacy settings on that service. You should always review the policies of Third Party Services and websites to make sure you are comfortable with the ways in which they use information you share with them.
StarNow may use third party providers to provide certain services to you in connection with your membership on StarNow. StarNow may disclose personally identifiable information to these third party service providers for the sole purpose of the provision of services to you.
Remarketing
StarNow also uses third party remarketing cookies, in particular, Google AdWords. These remarketing services may place cookies on web browsers in order to serve ads based on past visits to our website. This means you may see ads about our service across the Internet, specifically on the Google Content Network (GCN). This allows us to make special offers and market our service to those who are interested in our service, and only display ads that are relevant to you.
We respect your privacy and do not collect or store any personally identifiable information through Google AdWords or any other third party remarketing system.
If you do not wish to see ads from StarNow you can opt out in several ways: Opt out of Google’s use of cookies by visiting Google’s Ads Settings.Opt out of a third-party vendor’s use of cookies by visiting the Network Advertising Initiative opt-out page.
Applying for roles
You understand that when you apply for a role, your contact details and any information entered into your personal profile will be sent to the advertiser. This includes any photo you have uploaded.
Spam and your email address
Your email address is used to identify your membership, and to sign in to the site.
After registering with StarNow you will receive regular casting call update emails, containing casting calls and auditions matching your profile. You may also receive our email newsletters, account updates and information about StarNow features.
You can easily unsubscribe from any of our emails, either by using the 'unsubscribe' link provided in the email, changing your email settings on your profile or by contacting us at contact@starnow.co.uk.

