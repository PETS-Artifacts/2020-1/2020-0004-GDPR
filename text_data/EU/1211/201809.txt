
Privacy policy
General
We do not sell, rent or share collected information from this website in ways different from what is written here.
Google analytics
RapidTables uses Google Analytics to monitor site usage in 
		order to improve the quality of the pages. Google Analytics
		provides statistical data only for website mangement. Google Analytics use cookies.
See: Google Analytics cookie usage, Google privacy policy.
Google adsense

RapidTables uses Google Adsense to display ads.
Third party vendors, including Google, use cookies to serve ads based on a user's prior visits to your website or other websites.
Google's use of advertising cookies enables it and its partners to serve ads to your users based on their visit to your sites and/or other sites on the Internet.
Users may opt out of personalized advertising by visiting Ads Settings and www.aboutads.info.
Adsense serves non-personalized ads to EEA users.

Application Data
RapidTables may save user's settings or user's data in the browser's local storage (e.g: Notepad).




