
















window.publicationUrl = 'https://www.belfasttelegraph.co.uk/';
window.publicationTitle = 'BelfastTelegraph.co.uk';
window.breadcrumb = 'Home;Service';



head.js(
{ jquery: '//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js' },
{ jqueryui: '//ajax.googleapis.com/ajax/libs/jqueryui/1.9.0/jquery-ui.min.js' },
{ extras: 'https://cdn-02.belfasttelegraph.co.uk/static/810047bcd886d8acbd9423fa2f7902059ad1922c/js/extras.min.js' },
{ tracking: 'https://cdn-03.belfasttelegraph.co.uk/static/810047bcd886d8acbd9423fa2f7902059ad1922c/js/tracking.min.js' },
{ global: 'https://cdn-03.belfasttelegraph.co.uk/static/810047bcd886d8acbd9423fa2f7902059ad1922c/js/scripts.min.js' },
{ widgets: 'https://cdn-03.belfasttelegraph.co.uk/static/810047bcd886d8acbd9423fa2f7902059ad1922c/js/widgets.min.js' }
);

Privacy Statement - BelfastTelegraph.co.uk









{
"@context": "http://schema.org",
"@type": "NewsArticle",
"headline": "Privacy Statement",
"inLanguage": "en",
"publisher": {
"@type": "Organization",
"name": "BelfastTelegraph.co.uk",
"logo": {
"@type": "ImageObject",
"url": "https://cdn-01.belfasttelegraph.co.uk/img/logo-belfast-b.png"
}
},
"image": {
"@type": "ImageObject",
"url": "",
"height": "",
"width": ""
},
"mainEntityOfPage": {
"@type": "WebPage",
"@id": "https://www.belfasttelegraph.co.uk/"
},
"dateCreated": "2017-05-01T02:27:46+0100",
"dateModified": "2018-11-15T02:27:15+0000",
"datePublished": "2018-05-23T10:30:00+0100",
"keywords": "belfasttelegraph",
"thumbnailUrl": "",
"author": {
"@type": "Person",
"name": ""
}
}












{"publication-id":"3147"}


{"data-collection-id":"4809","audience-extraction-id":"4299"}


{"account":"mediaforce","sub-account":"independentnews"}


if (!window.inmConsent || (inmConsent.getPublisherConsents().standardPurposeConsents[5] === true)) {
var comscore_pv = {
c1: "2",
c2: "8097916"
};
var _comscore = _comscore || [];
_comscore.push(comscore_pv);
(function() {
var s = document.createElement("script"), el = document.getElementsByTagName("script")[0];
s.async = true;
s.src = (document.location.protocol == "https:" ? "https://sb" : "http://b") + ".scorecardresearch.com/beacon.js";
el.parentNode.insertBefore(s, el);
})();
}



digitalData = {};
digitalData.page = {"pageInfo":{"pageName":"Bt:Service:Privacy Statement","pageTitle":"Privacy Statement","publication":"belfasttelegraph.co.uk"},"category":{"pageType":"Article","primaryCategory":"Service","subCategory1":"n\/a","subCategory2":"n\/a","subCategory3":"n\/a","subCategory4":"n\/a"},"attributes":{"inmproduct":"website","virtualview":"false"},"content":{"author":"","tags":"","articleID":35669791,"articleType":"news"}};
(function() {
var getCookie = function(key) {
var cookies = document.cookie.split('; ');
for (var i = 0, parts; (parts = cookies[i] && cookies[i].split('=')); i++) {
if (parts.shift() === key) {
var cookieValue = parts.join('=');
if (cookieValue) cookieValue = decodeURIComponent(cookieValue);
return cookieValue;
}
}
};
var userId = getCookie('guid');
if (userId) {
digitalData.user = {
profile: {
profileInfo: {
profileID: userId
}
}
};
}
})();


window.gigyaCmd = [];
window.onGigyaServiceReady = function(serviceName) {
var cmds = gigyaCmd;
gigyaCmd = {
push: function(cmd) {
cmd();
}
};
for (var i = 0, ii = cmds.length; i < ii; i++) {
cmds[i]();
}
};
var s = document.createElement('script');
s.type = 'text/javascript';
s.async = true;
s.src = 'https://cdns.gigya.com/js/gigya.js?apiKey=3_wjMJ4AQSA7Z1Va3mHT7NjHkjrdsooComEsn9qSQqL0duJPMJAaadqpyiiJEh4hK-';
s.text = '{}';
document.getElementsByTagName('head')[0].appendChild(s);


window.gigyaIntegration = {
screenSet: 'BelTel-RegistrationLogin',
mobileScreenSet: 'BelTel-DefaultMobile-RegistrationLogin',
myAccountUrl: 'https://member.belfasttelegraph.co.uk/',
signinLabel: "Sign In",
signoutLabel: "Sign Out",
command: []
};





!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '221345138391096'); // Insert your pixel ID here.
fbq('track', 'PageView');






{"targeting":{"art_id":"35669791","kw":["Privacy","Statement"]}}


{"js":"https:\/\/cdn-02.belfasttelegraph.co.uk\/static\/810047bcd886d8acbd9423fa2f7902059ad1922c\/js\/prebid.min.js","timeout":"1000","bidders":{"rubicon":{"defaultDealValue":"0"}}}


var articleReadMoreEnabled = true;




dataLayer = [{"pageType":"article","pageCategory":"Service","pageTitle":"Privacy Statement","pageAttributes":{"comments":"no","relatedArticles":"no","videos":0,"source":"Belfast Telegraph Digital","readMore":"yes","photos":0},"articleId":35669791,"sectionId":1431}];
window.dataLayerEvUpdate && window.dataLayerEvUpdate(dataLayer);
window.dataLayerACUpdate && window.dataLayerACUpdate(dataLayer);



(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-CQ3M');


(function() {
var ARTICLE_URL = window.location.href;
var CONTENT_ID = 'everything';
document.write(
'<scr'+'ipt '+
'src="//survey.g.doubleclick.net/survey?site=_nuhppxxejursg5tgjyabkogr64'+
'&amp;url='+encodeURIComponent(ARTICLE_URL)+
(CONTENT_ID ? '&amp;cid='+encodeURIComponent(CONTENT_ID) : '')+
'&amp;random='+(new Date).getTime()+
'" type="text/javascript">'+'\x3C/scr'+'ipt>');
})();

_satellite.pageBottom();


(function() {
window.facebookInitHandlers = [];
var initFacebook = function() {
FB.init({
appId : '321217294648139',
status : true,
cookie : true,
xfbml : true
});
var handlers = window.facebookInitHandlers;
window.facebookInitHandlers = undefined;
for (var i = 0; i < handlers.length; i++) {
handlers[i].call(this);
}
};
window.onFacebookInit = function(handler) {
if (window.facebookInitHandlers) {
window.facebookInitHandlers.push(handler);
if (window.FB) {
console && console.log && console.log('window.FB loaded');
initFacebook();
}
} else {
handler.call(this);
}
};
window.fbAsyncInit = function() {
console && console.log && console.log('fbAsyncInit called');
initFacebook();
};
})();
(function(d){
var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
if (d.getElementById(id)) {return;}
js = d.createElement('script'); js.id = id; js.async = true;
js.src = "//connect.facebook.net/en_US/all.js";
ref.parentNode.insertBefore(js, ref);
}(document));






{"id":"gpt-ad-29045163-1431","sizes":[[169,1086],[340,1086]],"adUnit":"8216\/belfasttelegraph.co.uk\/Service","targeting":{"pos":["roadblock"]}}






{"id":"gpt-ad-29045162-1431","sizes":[[171,1086],[341,1086]],"adUnit":"8216\/belfasttelegraph.co.uk\/Service","targeting":{"pos":["roadblock"]}}












Property




Propertynews.com




Property For Sale




Property For Rent




Agent Search






Jobs




niJobfinder




Recruit NI






Classifieds




Belfast Telegraph Classifieds




For Sale




Family Notices 



Place your ad 





Travel




Travel






Competitions




Cars




Autotrader




Buying New & Used




Sell My Car




Car Reviews









Subscribe



Home Delivery




Digital Edition




Newsletters













{"id":"gpt-ad-35609517-1431","sizes":[[728,90],[970,90],[940,90],[940,200],[940,60],[970,200],[970,250]],"adUnit":"8216\/belfasttelegraph.co.uk\/Service","prebid":{"bids":[{"bidder":"rubicon","params":{"accountId":"11022","siteId":"129174","sizeId":"57","zoneId":"610148"}},{"bidder":"appnexus","params":{"placementId":"11055336"}},{"bidder":"criteo","params":{"zoneId":"1030884"}},{"bidder":"openx","params":{"unit":"538992766","delDomain":"mediaforce-d.openx.net"}}],"customPriceBuckets":[{"precision":2,"min":0,"max":0.2,"increment":0.01},{"precision":2,"min":0.2,"max":2,"increment":0.05},{"precision":2,"min":2,"max":5,"increment":0.1},{"precision":2,"min":5,"max":8,"increment":0.2},{"precision":2,"min":8,"max":15,"increment":0.5},{"precision":2,"min":15,"max":99,"increment":1}]},"targeting":{"pos":["top","roadblock"]}}









Follow

Facebook
Twitter
Google+








var locations=new Array();
locations['Armagh'] = [['14'], ['7'], ['4'], ['armagh-united_kingdom.html']];
locations['Bangor'] = [['12'], ['8'], ['4'], ['bangor-north_down-united_kingdom.html']];
locations['Belfast'] = [['14'], ['8'], ['4'], ['belfast-united_kingdom.html']];
locations['Belfast'] = [['14'], ['8'], ['4'], ['belfast-united_kingdom.html']];
locations['Dublin'] = [['13'], ['10'], ['7'], ['dublin-ireland.html']];
locations['Enniskillen'] = [['14'], ['7'], ['4'], ['enniskillen-fermanagh-united_kingdom.html']];
locations['Larne'] = [['12'], ['8'], ['6'], ['larne-antrim-united_kingdom.html']];
locations['Londonderry'] = [['12'], ['7'], ['4'], ['londonderry-united_kingdom.html']];
locations['Newcastle'] = [['12'], ['9'], ['7'], ['newcastle-down-united_kingdom.html']];
locations['Omagh'] = [['14'], ['7'], ['4'], ['omagh-tyrone-united_kingdom.html']];
locations['Portrush'] = [['12'], ['8'], ['4'], ['portrush-coleraine-united_kingdom.html']];
head.ready(function() {
var loc = $.cookie('weather_loc');
loc = loc != null ? loc : 'Belfast';
if(typeof window.weather_location== 'function') {
// function exists, so we can now call it
weather_location(locations, loc);
}	});

.change{
font: 600 13px/20px Open Sans,Trebuchet MS,Helvetica Neue,Helvetica,sans-serif !important;
padding:0px !important; margin: 0px !important;
}
.location{
text-transform: capitalize !important;
}


 Hi °C | Lo °C  | WEATHER 


Athlone
Bray
Cork
Drogheda
Dundalk


Galway
Kilkenny
Limerick
Navan
Waterford









Home




Brexit




Courts






News




Northern Ireland




UK




Republic of Ireland




World




Politics




RHI Scandal






Brexit




Health




Sunday Life




Education




Love to learn




Graduations






Courts






Sport




Football




Premier League




Irish League




Scottish




International




FA and League Cups




Billy on the box






Rugby




Ulster Rugby




Ulster Rugby Round Up podcasts




PRO14




Six Nations




European Champions Cup




Club Rugby




Schools Cup






GAA




Antrim




Armagh




Cavan




Derry




Donegal




Down




Fermanagh




Monaghan




Tyrone






Golf




Motor Sport




Motorcycling




Other Sports




Belfast Giants




Horse Racing




Boxing




Cricket




Tennis




Hockey




Cycling






Running




Declan Bogue






Business




Northern Ireland




Commercial Property






Food, Drink and Hospitality




UK & World




Brexit




Jobs




Agri




Balmoral Show






Tech




Analysis




Help and Advice






Top 100




Business Awards






Entertainment




News




Music and Gigs




Film and TV




Theatre and Arts




Belfast Festival




Horoscopes






Life




Fashion & beauty




Health




House and Home




Property Awards






Restaurant Reviews




Food & Drink




Recipes






Features




Books




Family Life




Countdown To Christmas




Homes & Interiors








Cars




Opinion




News Analysis




Editor's Viewpoint




Columnists




Alban Maginness




Alf McCreary




Fionola Meredith




Gail Walker




Lindy McDowell




Mary Kenny




Nelson McCausland




Ruth Dudley Edwards




Suzanne Breen








Archive




People




Events




Places




Titanic




Ulster Covenant






Travel




Holiday World




Holiday Guide 2018-2019




Your Hotel & Tour Guide






Video




Events




Property Awards




Print Supplement






Sports Awards




Business Awards




Spirit of N. Ireland Awards






Search

Go






Privacy Statement








0 Comments


Privacy Statement
BelfastTelegraph.co.uk

https://www.belfasttelegraph.co.uk/service/privacy-statement-35669791.html


Email




 

.clearfix {
clear: both;
}
.privacy-navigation-container {
width: 33%;
float: left;
}
#priv_updated {
margin-bottom: 15px;
}
#priv_body,
#priv_updated {
width: 66%;
float: right;
}
.privacy-navigation {
list-style: none;
padding: 0;
}
.navigation-item {
margin-bottom: 10px;
font-weight: 600;
}
.navigation-link {
text-decoration: none !important;
display: block !important;
padding-left: 15px !important;
color: inherit !important;
text-decoration: none !important;
text-transform: capitalize;
font-family: "Open Sans";
}
.navigation-link.active {
color: #0046b4 !important;
box-shadow: inset 3px 0 !important;
}
.MsoTocHeading,
.MsoToc3,
.MsoToc2 {
display: none;
}
#priv_body h1,
#priv_updated {
display: none;
}
a[href="#privacy_top"] {
display: none;
}




Overview


Content


Registration


Newsletters


Subscriptions


Site Function


Mobile Apps


Push Notifications


Belfast Telegraph Travel


Your Rights


Your Data


Changes To This Information


Acceptance Of This Privacy Statement


Contact the Data Protection Office




links = document.getElementsByClassName("navigation-link");
Array.from(links).forEach(function (element) {
element.onclick = function(e) {
e.preventDefault();
active = document.getElementsByClassName("navigation-link active")[0];
active.classList.remove('active');
this.classList.add('active');
elementToScroll = document.getElementsByName(this.getAttribute('href').replace('#', ''))[0];
scrollTo(elementToScroll);
return;
};
});
function scrollTo(element) {
window.scroll({
behavior: 'smooth',
left: 0,
top: (element.offsetTop - 50)
});
}






var inmpriv = {
pub: "belfasttelegraph",
doc: "privacy-statement",
/*
/ Set those values for an extra title or extra containers.
/ title_id: "priv_title",
/ body_id: "priv_body",
/ updated_id: "priv_updated"
*/
};













Christmas: It’s Tradition 


Every December 25th Northern Ireland families come together to feast on a plate of festive classics but have you ever wondered how the yuletide meal we love so well made its way onto the table?

Sponsored by 












Get discounts and cashback on your Christmas gift shopping 


On the hunt for a special gift for a loved one? Or an office secret Santa pressie for under a tenner?

Sponsored by 












Break the rules this Christmas 


When the roast turkey and all the trimmings lose their appeal, what’s next on the menu?

Sponsored by 












Gifts to make her sparkle 


In this special feature PANDORA breaks down the who and what of ideal gifting for the woman in your life, whether that’s a devoted mum, style-astute sister, perfect partner or a forever friend…

Sponsored by 







{"id":"gpt-ad-35613326-1431","sizes":[[728,90],[940,91]],"adUnit":"8216\/belfasttelegraph.co.uk\/Service","prebid":{"bids":[{"bidder":"rubicon","params":{"accountId":"11022","siteId":"129174","sizeId":"2","zoneId":"610160"}},{"bidder":"appnexus","params":{"placementId":"11055351"}},{"bidder":"criteo","params":{"zoneId":"948171"}},{"bidder":"openx","params":{"unit":"538992783","delDomain":"mediaforce-d.openx.net"}}]},"targeting":{"pos":["bottom"]}}






News




Northern Ireland




UK




Rep of Ireland




World




Politics




Brexit






Sport




Football




Rugby




GAA




Golf




Motor Sport




Motorcycling






Business




Northern Ireland




Food & Drink




UK and World




Brexit




Jobs




Agri






Entertainment




News




Film and TV




Music and Gigs




Theatre and Arts




Belfast Festival




Horoscopes






Life




Fashion




Health




House




Eating Out




Food




Features






Opinion




News Analysis




Editor's Viewpoint




Columnists








Follow

Facebook
Twitter
Google+









Search

Go






Mobile Site




App




Sitemap




Contact




Terms & Conditions




Privacy Statement




Cookie Policy




Media Pack




Group Websites




Complaints



© Belfast Telegraph 




{"id":"gpt-ad-30485225-1431","oop":true,"sizes":[1,1],"adUnit":"8216\/belfasttelegraph.co.uk\/Service"}









Home




Brexit




Courts






News




Northern Ireland




UK




Republic of Ireland




World




Politics




RHI Scandal






Brexit




Health




Sunday Life




Education




Love to learn




Graduations






Courts






Sport




Football




Premier League




Irish League




Scottish




International




FA and League Cups




Billy on the box






Rugby




Ulster Rugby




Ulster Rugby Round Up podcasts




PRO14




Six Nations




European Champions Cup




Club Rugby




Schools Cup






GAA




Antrim




Armagh




Cavan




Derry




Donegal




Down




Fermanagh




Monaghan




Tyrone






Golf




Motor Sport




Motorcycling




Other Sports




Belfast Giants




Horse Racing




Boxing




Cricket




Tennis




Hockey




Cycling






Running




Declan Bogue






Business




Northern Ireland




Commercial Property






Food, Drink and Hospitality




UK & World




Brexit




Jobs




Agri




Balmoral Show






Tech




Analysis




Help and Advice






Top 100




Business Awards






Entertainment




News




Music and Gigs




Film and TV




Theatre and Arts




Belfast Festival




Horoscopes






Life




Fashion & beauty




Health




House and Home




Property Awards






Restaurant Reviews




Food & Drink




Recipes






Features




Books




Family Life




Countdown To Christmas




Homes & Interiors








Cars




Opinion




News Analysis




Editor's Viewpoint




Columnists




Alban Maginness




Alf McCreary




Fionola Meredith




Gail Walker




Lindy McDowell




Mary Kenny




Nelson McCausland




Ruth Dudley Edwards




Suzanne Breen








Archive




People




Events




Places




Titanic




Ulster Covenant






Travel




Holiday World




Holiday Guide 2018-2019




Your Hotel & Tour Guide






Video




Events




Property Awards




Print Supplement






Sports Awards




Business Awards




Spirit of N. Ireland Awards







Search

Go




Back to top



try {
$(document).ready(function(){
_402_Show();
});
} catch(e) {}


var gdpr = {
links: {
settings: 'https://www.belfasttelegraph.co.uk/service/settings/',
cookie: 'https://www.belfasttelegraph.co.uk/service/cookie-policy-36824393.html',
privacy: 'https://www.belfasttelegraph.co.uk/service/privacy-and-cookie-policy-35669791.html',
},
cookie: {
domain: '.belfasttelegraph.co.uk',
version: 2
},
theme: 'bt'
}



var captify_kw_query_11271 = "";
(function(c,a,p,t,i,f,y){i=c.createElement(a);t=c.getElementsByTagName(a)[0];i.type='text/javascript';i.async=true;i.src=p;t.parentNode.insertBefore(i,t);})(document,'script','https://p.cpx.to/p/11271/px.js');





window.com = window.com || {};
com.inmplc = com.inmplc || {};
com.inmplc.adb = com.inmplc.adb || [];
com.inmplc.adb.push({
infoLink: 'https://www.belfasttelegraph.co.uk/service/how-to-disable-ad-blockers-on-belfasttelegraphcouk-35415498.html',
imageA: 'w141-icon-a2.png',
whitelistSitename: 'BelfastTelegraph.co.uk'
});



