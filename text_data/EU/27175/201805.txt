Kontentino.com privacy policy
This privacy policy has been compiled to better serve those who are concerned with how their 'Personally Identifiable Information' (PII) is being used online. PII, as described in US privacy law and information security, is information that can be used on its own or with other information to identify, contact, or locate a single person, or to identify an individual in context. Please read our privacy policy carefully to get a clear understanding of how we collect, use, protect or otherwise handle your Personally Identifiable Information in accordance with our website.
What personal information do we collect from the people that visit our blog, website or app?
When ordering or registering on our site, as appropriate, you may be asked to enter your name, email address, phone number, credit card information or other details to help you with your experience.
When do we collect information?
We collect information from you when you register on our site, place a subscription order or enter information on our site.
How do we use your information?
We may use the information we collect from you when you register, make a purchase, sign up for our newsletter, respond to a survey or marketing communication, surf the website, or use certain other site features in the following ways:


                To personalize your experience and to allow us to deliver the type of product offerings in which you are most interested.
            

                To allow us to better service you in responding to your customer service requests.
            

                To quickly process your transactions.
            

                To follow up with them after correspondence (live chat, email or phone inquiries)
            

How do we protect your information?


                Our website is scanned on a regular basis for security holes and known vulnerabilities in order to make your visit to our site as safe as possible.
            

                Your personal information is contained behind secured networks and is only accessible by a limited number of persons who have special access rights to such systems, and are required to keep the information confidential.
            

                All sensitive information you supply (your phone, credit card, paypal authorization) is encrypted via Secure Socket Layer (SSL) technology.
            

How do we protect your billing information? (credir cards, PayPal authorization... etc)


                    We do not store or process any credit card information or PayPal authorizations on our servers.
                    
                    All credit card information and PayPal authorizations are stored and processed within  Braintree Payments service, known as credit card payment gate maintained by PayPal.
                

                    Kontentino website complies with Payment Card Industry Data Security Standard (PCI DSS) and is eligible for SAQ A, i.e. credit card processing is full outsourced to the Braintree Payments service and in Kontentino, it is provided through an iFrame.
                

Do we use 'cookies'?
Yes. Cookies are small files that a site or its service provider transfers to your computer's hard drive through your Web browser (if you allow) that enables the site's or service provider's systems to recognize your browser and capture and remember certain information. For instance, we use cookies to help us remember and process the items in your shopping cart. They are also used to help us understand your preferences based on previous or current site activity, which enables us to provide you with improved services. We also use cookies to help us compile aggregate data about site traffic and site interaction so that we can offer better site experiences and tools in the future.
We use cookies to:

Understand and save user's preferences for future visits.
Compile aggregate data about site traffic and site interactions in order to offer better site experiences and tools in the future. We may also use trusted third-party services that track this information on our behalf.


                You can choose to have your computer warn you each time a cookie is being sent, or you can choose to turn off all cookies. You do this through your browser settings. Since browser is a little different, look at your browser's Help Menu to learn the correct way to modify your cookies.
            
If users disable cookies in their browser:
If you turn cookies off, Some of the features that make your site experience more efficient may not function properly.Some of the features that make your site experience more efficient and may not function properly.
OAuth authorizations

                Kontentino uses OAuth authorization to publish content only to the social media profiles, that you allow.
            
Facebook authorizations
Kontentino requests the following permissions:

manage_pages
publish_pages
user_photos
publish_actions
ads_read


                        Any authorizations made from your Facebook account can be revoked in Facebook's App settings here: https://www.facebook.com/settings?tab=applications>
                    
Twitter authorizations
Kontentino requests read-write authrization from your Twitter profile.

                        Any authorizations made from your Twitter accounts can be revoked in Twitter's App settings here: https://twitter.com/settings/applications.
                    
LinkedIn authorizations
Kontentino requests the following permissions:

r_basicprofile
rw_company_admin
w_share


                        Any authorizations made from you LinkedIn accounts can be rework in LinkedIn's Privacy settings here: https://www.linkedin.com/psettings/third-party-applications.
                    
Google+ authorizations
Kontentino requests the following permissions:

https://www.googleapis.com/auth/plus.login
https://www.googleapis.com/auth/plus.pages.manage
https://www.googleapis.com/auth/plus.stream.write
https://www.googleapis.com/auth/plus.media.readwrite
https://www.googleapis.com/auth/plus.me


                        Any authorizations made from you Google accounts can be rework in Google Third party application settings here: https://myaccount.google.com/permissions.
                    
Third-party disclosure
We do not sell, trade, or otherwise transfer to outside parties your Personally Identifiable Information unless we provide users with advance notice. This does not include website hosting partners and other parties who assist us in operating our website, conducting our business, or serving our users, so long as those parties agree to keep this information confidential. We may also release information when it's release is appropriate to comply with the law, enforce our site policies, or protect ours or others' rights, property or safety.
However, non-personally identifiable visitor information may be provided to other parties for marketing, advertising, or other uses.
Third-party links
We do not include or offer third-party products or services on our website.
California Online Privacy Protection Act
CalOPPA is the first state law in the nation to require commercial websites and online services to post a privacy policy. The law's reach stretches well beyond California to require any person or company in the United States (and conceivably the world) that operates websites collecting Personally Identifiable Information from California consumers to post a conspicuous privacy policy on its website stating exactly the information being collected and those individuals or companies with whom it is being
             shared. - See more at: http://consumercal.org/california-online-privacy-protection-act-caloppa/#sthash.0FdRbT51.dpuf
According to CalOPPA, we agree to the following:

Users can visit our site anonymously.
Once this privacy policy is created, we will add a link to it on our home page or as a minimum, on the first significant page after entering our website.
Our Privacy Policy link includes the word 'Privacy' and can easily be found on the page specified above.

You will be notified of any Privacy Policy changes:

Via Email



Can change your personal information:


                        By logging in to your account
                    



COPPA (Children Online Privacy Protection Act)
When it comes to the collection of personal information from children under the age of 13 years old, the Children's Online Privacy Protection Act (COPPA) puts parents in control. The Federal Trade Commission, United States' consumer protection agency, enforces the COPPA Rule, which spells out what operators of websites and online services must do to protect children's privacy and safety online.
We do not specifically market to children under the age of 13 years old.
Fair Information Practices
The Fair Information Practices Principles form the backbone of privacy law in the United States and the concepts they include have played a significant role in the development of data protection laws around the globe. Understanding the Fair Information Practice Principles and how they should be implemented is critical to comply with the various privacy laws that protect personal information.

In order to be in line with Fair Information Practices we will take the following responsive action, should a data breach occur:

            
                We will notify you via email
            
        

            We also agree to the Individual Redress Principle which requires that individuals have the right to legally pursue enforceable rights against data collectors and processors who fail to adhere to the law. This principle requires not only that individuals have enforceable rights against data users, but also that individuals have recourse to courts or government agencies to investigate and/or prosecute non-compliance by data processors.
        
CAN SPAM Act
The CAN-SPAM Act is a law that sets the rules for commercial email, establishes requirements for commercial messages, gives recipients the right to have emails stopped from being sent to them, and spells out tough penalties for violations.
We collect your email address in order to:

Process orders and to send information and updates pertaining to orders.
Send you additional information related to your product and/or service

To be in accordance with CANSPAM, we agree to the following:

Not use false or misleading subjects or email addresses.
Identify the message as an advertisement in some reasonable way.
Include the physical address of our business or site headquarters.
Monitor third-party email marketing services for compliance, if one is used.
Honor opt-out/unsubscribe requests immediatedly.
Allow users to unsubscribe by using the link at the bottom of each email.

If at any time you would like to unsubscribe from receiving future emails, you can email us at


                Follow the instructions at the bottom of each email and we will promptly remove you from ALL correspondence.
            

Contacting Us
If there are any questions regarding this privacy policy, you may contact us using the information below.

            kontentino.com 
            Brigadnicka 27 
            Bratislava, Slovakia 84101 
            Slovakia 
            info@kontentino.com 

            Last Edited on the 24th August, 2017
        
