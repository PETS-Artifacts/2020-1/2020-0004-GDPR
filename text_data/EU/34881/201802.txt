


















 !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '1956290191319367');
  fbq('track', 'PageView');



            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date(); a = s.createElement(o),
                m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            window.ga('create', 'UA-105571553-4', 'auto');

            function getParameterByName(name, url) {
                if (!url) url = window.location.href;
                name = name.replace(/[\[\]]/g, "\\$&");
                var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
                    results = regex.exec(url);
                if (!results) return null;
                if (!results[2]) return '';
                return decodeURIComponent(results[2].replace(/\+/g, " "));
            }

            var action = getParameterByName("action");
            console.log("ACTION!", action);
            if (action === "registered_google") {
                ga('send', 'event', 'Register', 'success', 'google');
            }
            if (action === "logged_in_google") {
                ga('send', 'event', 'Login', 'success', 'google');
            }
            if (action === "registered_facebook") {
                ga('send', 'event', 'Register', 'success', 'facebook');
            }
            if (action === "logged_in_facebook") {
                ga('send', 'event', 'Login', 'success', 'facebook');
            }

            function removeParam(parameter) {
                var url=document.location.href;
                var urlparts= url.split('?');

                if (urlparts.length>=2)  {
                    var urlBase=urlparts.shift(); 
                    var queryString=urlparts.join("?"); 

                    var prefix = encodeURIComponent(parameter)+'=';
                    var pars = queryString.split(/[&;]/g);
                    for (var i= pars.length; i-->0;)               
                      if (pars[i].lastIndexOf(prefix, 0)!==-1)   
                          pars.splice(i, 1);
                    url = urlBase+'?'+pars.join('&');
                    window.history.pushState('',document.title,url); // added this line to push the new url directly to url bar .
                }
                return url;
            }

            removeParam("action");
        


          (adsbygoogle = window.adsbygoogle || []).push({
            google_ad_client: "ca-pub-8131648498075630",
            enable_page_level_ads: true
          });
        











        #app {
            min-height: 100vh;
        }

        /* way the hell off screen */
        .scrollbar-measure {
            width: 100px;
            height: 100px;
            overflow: scroll;
            position: absolute;
            top: -9999px;
        }

        [ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak], .ng-cloak, .x-ng-cloak {
            display: none !important;
        }
        



(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.10&appId=279120312589607';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));








 Login
 Register


 {{user.email}}
Logout



 Watchlist 



{{coins[symbol].display_symbol}}

{{coins[symbol].price_change_1D_percent | number : 2}}%



                                No Items in Watchlist
                            




{{user.settings.currency}}

{{currency}}

{{currency}}





BTC Dominance
{{btc_dominance() | number : 1}}%


Total Market Cap



Total Volume








All Coins
ICO Calendar
Exchanges
News

Guides







Coins


{{coin.name}} ({{coin.display_symbol}})


ICOs


{{coin.name}} ICO



                                        No Results
                                    
















Home

{{crumb.name}}
 {{crumb.name}}








CoinCodex





				loading...
			








Active ICOs (View All)
22


Currencies
{{num_currencies()}}


Gainers 
{{gainers() | number : 1}}%


Losers 
{{losers() | number : 1}}%








CoinCodex

About Us
Contact Us
Advertising
FAQ



Menu

All Coins
ICO Calendar
News
Exchanges

Guides




New to Crypto?
 Start Here



What is CoinCodex?
Complete cryptocurrency market coverage with real-time coin prices, charts and crypto market
                            cap featuring over {{roundto(num_currencies(), 100)}} coins on more than {{roundto(num_exchanges, 10)}} exchanges.














CoinCodex © 2017. All rights reserved.


By using this site you agree to Terms of Service, Privacy Policy and Cookie Policy.






			var coincodex = {
                "rates" : {"AUD":1.2495,"BGN":1.5698,"BRL":3.1694,"CAD":1.2319,"CHF":0.93129,"CNY":6.2967,"CZK":20.28,"DKK":5.9738,"EUR":0.80263,"GBP":0.70246,"HKD":7.82,"HRK":5.9656,"HUF":248.86,"IDR":13418,"ILS":3.4284,"INR":64.023,"ISK":100.34,"JPY":109.66,"KRW":1071.1,"MXN":18.566,"MYR":3.904,"NOK":7.6816,"NZD":1.36,"PHP":51.633,"PLN":3.3344,"RON":3.7361,"RUB":56.282,"SEK":7.8682,"SGD":1.3125,"THB":31.36,"TRY":3.7464,"ZAR":11.898,"USD":1},
                "focused" : true,
                "version" : "261",
                "all_coins_changed" : "1517511837",
                "all_buysell_coins" : {"binance":["ADA","ADX","AION","AMB","APPC","ARK","ARN","AST","BAT","BCD","BCH","BCPT","BNB","BNT","BQX","BRD","BTC","BTG","BTM","BTS","CDT","CM","CND","CTR","DASH","DGD","DLT","DNT","EDO","ELC","ELF","ENG","ENJ","EOS","ETC","ETH","EVX","FUEL","FUN","GAS","GTO","GVT","GXS","HCC","HSR","ICN","ICX","INS","IOST","IOT","IOTA","KMD","KNC","LEND","LINK","LLT","LRC","LSK","LTC","LUN","MANA","MCO","MDA","MOD","MTH","MTL","NAV","NEBL","NEO","NULS","OAX","OMG","OST","PIVX","POE","POWR","PPT","QSP","QTUM","RCN","RDN","REQ","RLC","SALT","SNGLS","SNM","SNT","STORJ","STRAT","SUB","TNB","TNT","TRIG","TRX","USDT","VEN","VIB","VIBE","WABI","WAVES","WINGS","WTC","XLM","XMR","XRP","XVG","XZC","YOYO","YOYOW","ZEC","ZRX"],"hitbtc":["","1ST","8BT","ADX","AE","AEON","AIR","AMB","AMM","AMP","ANT","ARDR","ARN","ART","ATB","ATL","ATM","ATS","AVT","B2X","BAS","BCH","BCN","BET","BKB","BMC","BMT","BNT","BOS","BQX","BTC","BTCA","BTG","BTM","BTX","BUS","C20","CAPP","CCT","CDT","CDX","CFI","CL","CLD","CND","CNX","COSS","CPAY","CRS","CSNO","CTR","CTX","CVC","DASH","DATA","DBIX","DCN","DCT","DDF","DENT","DGB","DGD","DICE","DIM","DLT","DNT","DOGE","DRT","DSH","EBET","EBTC","ECAT","EDG","EDO","EKO","ELM","EMC","EMGO","ENG","ENJ","EOS","ERO","ETBS","ETC","ETH","ETP","EUR","EVX","EXN","FCN","FRD","FUEL","FUN","FYN","FYP","GAME","GNO","GRPH","GUP","GVT","HAC","HDG","HGT","HPC","HRB","HSR","HVN","ICN","ICO","ICOS","ICX","IDH","IGNIS","IML","IND","INDI","IPL","IXT","KBR","KICK","KMD","LA","LAT","LEND","LIFE","LOC","LRC","LSK","LTC","LUN","MAID","MANA","MCAP","MCO","MIPS","MNE","MRV","MSP","MTH","MYB","NDC","NEBL","NEO","NET","NGC","NTO","NXC","NXT","OAX","ODN","OMG","OPT","ORME","OTN","PAY","PBKX","PING","PIX","PLBT","PLR","PLU","POE","POLL","PPC","PPT","PQT","PRE","PRG","PRO","PST","PTOY","QAU","QCN","QTUM","QVT","REP","RKC","RLC","ROOTS","RVT","SAN","SBD","SBTC","SC","SHIT","SISA","SKIN","SMART","SMS","SNC","SNGLS","SNM","SNT","SPF","STAR","STEEM","STORM","STRAT","STU","STX","SUB","SUR","SWT","TAAS","TFL","TGT","TIME","TIX","TKN","TKR","TNT","TRST","TRX","UET","UGT","USDT","UTT","VEN","VERI","VIB","VIBE","VOISE","WAVES","WEALTH","WINGS","WMGO","WRC","WTC","WTT","XAUR","XDN","XDNCO","XEM","XLC","XMR","XRP","XTZ","XUC","XVG","YOYOW","ZAP","ZEC","ZRC","ZRX","ZSC"],"livecoin":["ABN","ACN","ADZ","AMM","ANT","ARC","ASAFE2","ATM","ATX","B2B","BAT","BCC","BCH","BIO","BIT","BLK","BLU","BNT","BPC","BQX","BSD","BTA","BTB","BTC","BTS","BURST","CCRB","CDX","CLD","CLOAK","CPC","CRBIT","CREVA","CTR","CURE","CVC","DANC","DASH","DAY","DBIX","DGB","DGD","DIBC","DIME","DMC","DMD","DOGE","DOLLAR","DTR","EDG","EDR","eETT","EL","EMC","ENJ","ENT","EOS","ERO","ESC","ETH","ETHP","EUR","EVC","FirstBlood","FLIXX","FNC","FORTYTWO","FRST","FST","FU","FUN","FUNC","GAME","GB","GNO","GNT","GOLOS","GRS","GRX","GUP","GYC","HNC","HST","HVN","ICN","ICOS","INCNT","INSN","IPL","ITI","KICK","KNC","KPL","KRB","LDC","LEO","LSK","LTC","LUNA","MAID","MCO","MCR","MGO","MLN","MNE","MNX","MOIN","MOJO","MONA","MSCN","MTCoin","MTL","NEO","NMC","NVC","NXT","OBITS","OD","OMG","OTN","OXY","PAY","PIPL","PIVX","PLBT","POST","POSW","PPC","PPY","PRES","PRG","PRO","PUT","QAU","QTUM","RBIES","REE","REP","RLC","RLT","RUR","SHIFT","SIB","SLR","SNGLS","SNT","SOAR","SPF","STEEM","STORJ","STRAT","SUMO","SXC","SYS","TAAS","TFL","THS","TIME","TKN","TRST","TRUMP","TX","UAH","UNC","UNRC","UNY","UQC","USD","VIB","VLTC","VOISE","VOX","VRC","VRM","VRS","VSL","WAVES","wETT","WIC","WINGS","XAUR","XEM","XMR","XMS","XRC","XRL","XSPEC","YOC","ZBC","ZRX"],"kucoin":["ACT","AGI","AION","AIX","AMB","BCD","BCH","BCPT","BHC","BNTY","BTC","BTG","BTM","CAG","CAN","CFD","COFI","CV","CVC","DASH","DAT","DBC","DENT","DGB","DNA","DRGN","ELIX","ENJ","EOS","ETC","ETH","EVX","FLIXX","FOTA","GAS","GVT","HSR","HST","INS","KCS","KEY","KNC","LA","LEND","LTC","MOD","MTH","NEBL","NEO","NULS","OMG","ONION","PAY","PBL","POE","POLL","POWR","PPT","PRL","PURA","QLC","QSP","QTUM","R","RDN","REQ","RHOC","RPX","SNM","SNOV","SNT","STX","SUB","TEL","TFL","TIO","TNC","UKG","USDT","UTK","VEN","WTC","XAS","XLR","XRB","ZPT"]},
                "blocking_ads" : "unknown",
                "is_mobile" : false
			};
            (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) coincodex.is_mobile = true;})(navigator.userAgent||navigator.vendor||window.opera);

            window.onfocus = function() {
                coincodex.focused = true;
            };

            window.onblur = function() {
                //coincodex.focused = false;
            };
/*
 * FuckAdBlock 3.2.1
 * Copyright (c) 2015 Valentin Allaire <valentin.allaire@sitexw.fr>
 * Released under the MIT license
 * https://github.com/sitexw/FuckAdBlock
 */

(function(window) {
    var FuckAdBlock = function(options) {
        this._options = {
            checkOnLoad:        false,
            resetOnEnd:         false,
            loopCheckTime:      50,
            loopMaxNumber:      5,
            baitClass:          'pub_300x250 pub_300x250m pub_728x90 text-ad textAd text_ad text_ads text-ads text-ad-links',
            baitStyle:          'width: 1px !important; height: 1px !important; position: absolute !important; left: -10000px !important; top: -1000px !important;',
            debug:              false
        };
        this._var = {
            version:            '3.2.1',
            bait:               null,
            checking:           false,
            loop:               null,
            loopNumber:         0,
            event:              { detected: [], notDetected: [] }
        };
        if(options !== undefined) {
            this.setOption(options);
        }
        var self = this;
        var eventCallback = function() {
            setTimeout(function() {
                if(self._options.checkOnLoad === true) {
                    if(self._options.debug === true) {
                        self._log('onload->eventCallback', 'A check loading is launched');
                    }
                    if(self._var.bait === null) {
                        self._creatBait();
                    }
                    setTimeout(function() {
                        self.check();
                    }, 1);
                }
            }, 1);
        };
        if(window.addEventListener !== undefined) {
            window.addEventListener('load', eventCallback, false);
        } else {
            window.attachEvent('onload', eventCallback);
        }
    };
    FuckAdBlock.prototype._options = null;
    FuckAdBlock.prototype._var = null;
    FuckAdBlock.prototype._bait = null;
    
    FuckAdBlock.prototype._log = function(method, message) {
        console.log('[FuckAdBlock]['+method+'] '+message);
    };
    
    FuckAdBlock.prototype.setOption = function(options, value) {
        if(value !== undefined) {
            var key = options;
            options = {};
            options[key] = value;
        }
        for(var option in options) {
            this._options[option] = options[option];
            if(this._options.debug === true) {
                this._log('setOption', 'The option "'+option+'" he was assigned to "'+options[option]+'"');
            }
        }
        return this;
    };
    
    FuckAdBlock.prototype._creatBait = function() {
        var bait = document.createElement('div');
            bait.setAttribute('class', this._options.baitClass);
            bait.setAttribute('style', this._options.baitStyle);
        this._var.bait = window.document.body.appendChild(bait);
        
        this._var.bait.offsetParent;
        this._var.bait.offsetHeight;
        this._var.bait.offsetLeft;
        this._var.bait.offsetTop;
        this._var.bait.offsetWidth;
        this._var.bait.clientHeight;
        this._var.bait.clientWidth;
        
        if(this._options.debug === true) {
            this._log('_creatBait', 'Bait has been created');
        }
    };
    FuckAdBlock.prototype._destroyBait = function() {
        window.document.body.removeChild(this._var.bait);
        this._var.bait = null;
        
        if(this._options.debug === true) {
            this._log('_destroyBait', 'Bait has been removed');
        }
    };
    
    FuckAdBlock.prototype.check = function(loop) {
        if(loop === undefined) {
            loop = true;
        }
        
        if(this._options.debug === true) {
            this._log('check', 'An audit was requested '+(loop===true?'with a':'without')+' loop');
        }
        
        if(this._var.checking === true) {
            if(this._options.debug === true) {
                this._log('check', 'A check was canceled because there is already an ongoing');
            }
            return false;
        }
        this._var.checking = true;
        
        if(this._var.bait === null) {
            this._creatBait();
        }
        
        var self = this;
        this._var.loopNumber = 0;
        if(loop === true) {
            this._var.loop = setInterval(function() {
                self._checkBait(loop);
            }, this._options.loopCheckTime);
        }
        setTimeout(function() {
            self._checkBait(loop);
        }, 1);
        if(this._options.debug === true) {
            this._log('check', 'A check is in progress ...');
        }
        
        return true;
    };
    FuckAdBlock.prototype._checkBait = function(loop) {
        var detected = false;
        
        if(this._var.bait === null) {
            this._creatBait();
        }
        
        if(window.document.body.getAttribute('abp') !== null
        || this._var.bait.offsetParent === null
        || this._var.bait.offsetHeight == 0
        || this._var.bait.offsetLeft == 0
        || this._var.bait.offsetTop == 0
        || this._var.bait.offsetWidth == 0
        || this._var.bait.clientHeight == 0
        || this._var.bait.clientWidth == 0) {
            detected = true;
        }
        if(window.getComputedStyle !== undefined) {
            var baitTemp = window.getComputedStyle(this._var.bait, null);
            if(baitTemp && (baitTemp.getPropertyValue('display') == 'none' || baitTemp.getPropertyValue('visibility') == 'hidden')) {
                detected = true;
            }
        }
        
        if(this._options.debug === true) {
            this._log('_checkBait', 'A check ('+(this._var.loopNumber+1)+'/'+this._options.loopMaxNumber+' ~'+(1+this._var.loopNumber*this._options.loopCheckTime)+'ms) was conducted and detection is '+(detected===true?'positive':'negative'));
        }
        
        if(loop === true) {
            this._var.loopNumber++;
            if(this._var.loopNumber >= this._options.loopMaxNumber) {
                this._stopLoop();
            }
        }
        
        if(detected === true) {
            this._stopLoop();
            this._destroyBait();
            this.emitEvent(true);
            if(loop === true) {
                this._var.checking = false;
            }
        } else if(this._var.loop === null || loop === false) {
            this._destroyBait();
            this.emitEvent(false);
            if(loop === true) {
                this._var.checking = false;
            }
        }
    };
    FuckAdBlock.prototype._stopLoop = function(detected) {
        clearInterval(this._var.loop);
        this._var.loop = null;
        this._var.loopNumber = 0;
        
        if(this._options.debug === true) {
            this._log('_stopLoop', 'A loop has been stopped');
        }
    };
    
    FuckAdBlock.prototype.emitEvent = function(detected) {
        if(this._options.debug === true) {
            this._log('emitEvent', 'An event with a '+(detected===true?'positive':'negative')+' detection was called');
        }
        
        var fns = this._var.event[(detected===true?'detected':'notDetected')];
        for(var i in fns) {
            if(this._options.debug === true) {
                this._log('emitEvent', 'Call function '+(parseInt(i)+1)+'/'+fns.length);
            }
            if(fns.hasOwnProperty(i)) {
                fns[i]();
            }
        }
        if(this._options.resetOnEnd === true) {
            this.clearEvent();
        }
        return this;
    };
    FuckAdBlock.prototype.clearEvent = function() {
        this._var.event.detected = [];
        this._var.event.notDetected = [];
        
        if(this._options.debug === true) {
            this._log('clearEvent', 'The event list has been cleared');
        }
    };
    
    FuckAdBlock.prototype.on = function(detected, fn) {
        this._var.event[(detected===true?'detected':'notDetected')].push(fn);
        if(this._options.debug === true) {
            this._log('on', 'A type of event "'+(detected===true?'detected':'notDetected')+'" was added');
        }
        
        return this;
    };
    FuckAdBlock.prototype.onDetected = function(fn) {
        return this.on(true, fn);
    };
    FuckAdBlock.prototype.onNotDetected = function(fn) {
        return this.on(false, fn);
    };
    
    window.FuckAdBlock = FuckAdBlock;
    
    if(window.fuckAdBlock === undefined) {
        window.fuckAdBlock = new FuckAdBlock({
            checkOnLoad: true,
            resetOnEnd: true
        });
    }
})(window);


		














































			/*$('.open-menu').click(function() {
                $('body').addClass('no-scroll');
                $('html').addClass('no-scroll');
                $('#menu-mobile').addClass('open');
            });

            $("#menu-mobile").click(function (data, handler) {
                if (data.target == this) {
                    $(this).removeClass('open');
                    $('body').removeClass('no-scroll');
                    $('html').removeClass('no-scroll');
                }
            });*/

            setTimeout(function() {
                // https://davidwalsh.name/detect-scrollbar-width
                // Create the measurement node
                var scrollDiv = document.createElement("div");
                scrollDiv.className = "scrollbar-measure";
                document.body.appendChild(scrollDiv);

                // Get the scrollbar width
                var scrollbarWidth = scrollDiv.offsetWidth - scrollDiv.clientWidth;
                console.warn("scrollbarWidth", scrollbarWidth); // Mac:  15

                // Delete the DIV 
                document.body.removeChild(scrollDiv);

                //https://stackoverflow.com/questions/1409225/changing-a-css-rule-set-from-javascript
                function getCSSRule(ruleName) {
                    ruleName = ruleName.toLowerCase();
                    var result = null;
                    var find = Array.prototype.find;

                    find.call(document.styleSheets, styleSheet => {
                        result = find.call(styleSheet.cssRules, cssRule => {
                            return cssRule instanceof CSSStyleRule 
                                && cssRule.selectorText.toLowerCase() == ruleName;
                        });
                        return result != null;
                    });
                    return result;
                }

                getCSSRule('.modal-open').style["margin-right"] = scrollbarWidth + "px"; 
            }, 100);
        

        .chart .highcharts-tooltip > span{ width: 175px;float: left; margin: 0 }
        .chart .highcharts-tooltip .header{ font-weight: bold; width: 154px!important; height: auto!important; float: left; border-bottom: 1px solid #bebebe;  margin: 1px 0 5px 1px!important; padding: 5px 10px;background-color: rgba(255,255,255,1);
            -webkit-border-top-left-radius: 5px;
            -webkit-border-top-right-radius: 5px;
            -moz-border-radius-topleft: 5px;
            -moz-border-radius-topright: 5px;
            border-top-left-radius: 5px;
            border-top-right-radius: 5px;}
        .chart .highcharts-tooltip .circle{width: 12px; height: 12px; border-radius: 24px; float: left;  margin: 2px 5px 5px 10px;clear: left}
        .chart .highcharts-tooltip p.country{float: left; margin-right: 0}
        .chart .highcharts-tooltip p{float: right; font-size: 12px; margin-right: 10px;}
    


