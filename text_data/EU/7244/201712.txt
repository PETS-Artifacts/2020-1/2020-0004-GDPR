
                Here at the HomeServe Group we know how important it is to keep your personal data safe, 
                that’s why we’re committed to making sure that you receive the service you’d expect and 
                that your privacy is protected every step of the way.
            

                In this notice you’ll discover exactly what information we collect from you and how we 
                then use this to deliver our services, as well as your rights. It might not be something 
                you’re interested in, but it’s really important you have a read and of course, let us 
                know if you have any questions.
            

                The HomeServe Group ('we' and 'us') means HomeServe plc, together with any entity in 
                which HomeServe plc directly or indirectly has at least a 50% shareholding. HomeServe plc 
                uses a variety of well-known brand and trading names including (but not limited to): 
                HomeServe Membership Ltd, Checkatrade, Help-Link, HomeServe Heating, HomeServe Connect 
                and HomeServe Labs. We may transfer your personal information among the members of the 
                HomeServe Group for the purposes contemplated in this privacy notice.
            


So, how do we use your information?

                            When you buy a product or service through us, we’ll collect some of your personal 
                            details, to make sure you have everything you need to make the most of our services. 
                            Please rest assured that we’ll only use this for administering your contract and to 
                            support the delivery of the services or products you’ve asked for.
                        

                            Examples of the types of personal information we usually collect are:
                        

Name
Address
Telephone number (including mobile)
Email address
Date of birth
Payment details (Direct Debit, credit/debit card number)
Property type (flat, house etc.)
Boiler details (make and model)
Any additional requirements such as large print or Braille documentation
Details of any additional authorised parties who can manage your requirements on your behalf
I.P. address for your device (if visiting our website or using our app)


                            The information requested during the application process is required to complete 
                            the sale of the product on offer and forms part of your contract with us. Unless 
                            otherwise stated, the information you provide will never be used for any other 
                            purpose without your permission. If any of the data is missing from the application 
                            form or is incorrect, we may not be able to process your request.
                        


Using information provided by third parties

                            We sometimes use data from third parties or publicly available sources such as 
                            Experian, the edited electoral roll or the deceased preference service. Using this 
                            data helps us to ensure that our records are accurate and up to date by filling in 
                            any gaps on an address or clarifying vanity addresses and house numbers. It also 
                            allows us to remove anyone from our mailings that has passed away.
                        

                            We also use data for marketing purposes. Any data we use in this way is thoroughly 
                            checked by both the supplier and our own internal teams to ensure that the correct 
                            marketing permissions are in place and that the data is being used fairly.
                        


Keeping and storing your data

                            If you’re a current or past customer, we’ll keep a copy of your personal details 
                            for no longer than 6 years, from the time your active relationship with us ends. 
                            Holding on to data allows us to keep accurate records for tax purposes and to handle 
                            any future complaints. All other personal data used for prospecting and quotation 
                            requests is kept for a maximum of 90 days, unless otherwise required by law.
                        

                            Some of the data that we collect may be transferred to and stored at a destination 
                            outside the European Economic Area ('EEA'), for example some of our IT systems are 
                            run on servers hosted in the USA. We take all steps reasonably necessary to ensure 
                            that your data is treated in accordance with this privacy notice and applicable 
                            privacy laws.
                        


Sharing your data with third parties

                            We often work with a number of carefully selected third parties, who introduce us 
                            to their customers, so we can promote the products and services we offer.
	                    

                            If you’ve been introduced to us through one of our partners, we may share details 
                            of the products and services you’ve purchased with them. Sharing data in this way 
                            helps us resolve individual complaints and helps us ensure that we’re offering the 
                            right products, to the right people.
                        

                            We respect your privacy and that’s why we don’t give your data to any third parties 
                            for marketing purposes. However, on occasion and in addition to the above, we may 
                            pass your information to a limited number of third parties for the following reasons:
                        

To deliver the services you’ve asked for, which might include giving information 
                                to members of your family, household, or other people who have an interest in the 
                                property, for instance, landlords or letting agents
For legal or regulatory purposes including fraud prevention
If we buy or sell any business or company assets



We’ll always keep you in the loop

                            Whenever we collect your personal information we’ll give you the opportunity to let 
                            us know how you’d like us to get in touch in the future. We promise not to inundate 
                            you with marketing messages, but we also understand if you’d prefer not to receive 
                            anything from us.
                        

                            If you’ve asked us to send you marketing material, you can change your mind at any 
                            time by contacting us using the details in the Contact Us section. You’ll also find 
                            an unsubscribe link at the bottom of every marketing email we send to you. 
                        

                            If you’ve given us an email address you may receive messages related to the management 
                            of your policy via email, which include policy and renewal documents. If you’d prefer 
                            not to receive these messages in this way, just let us know and we’ll be happy to 
                            provide them in paper form instead.
                        


Your rights matter

                            If you’d like to see the personal information that we hold about you, you can request 
                            a copy any time. If you find that this information is incorrect you can ask for it to 
                            be updated. Or, if you believe the information is being processed without a legal 
                            basis, you can ask us to stop or request that it’s deleted from our systems.
                        

                            To action any of the above, send an email to: [email protected] or alternatively you can write to us at:
                        
Checkatrade Head Office
                        5 - 6 Sherrington Mews, Ellis Square,
                        Selsey,
                        West Sussex,
                        PO20 0FJ

                            We won’t ever charge you for a copy of your personal data but we may ask you for proof 
                            of your identity before we disclose any information. Once we’ve seen this, we’ll send 
                            you a copy of the personal data we hold within 30 days. In addition, if you decide to 
                            move away from us for any reason, you can also request for your personal data to be 
                            transferred to a new provider on your behalf.
                        


Customer profiles
We carry out something called ‘profiling’ so we can understand the needs, behaviours 
                            and socio-demographic characteristics of our customers. This helps us make sure that 
                            we‘re offering the right products and services to the right people, for example, not 
                            offering gas products to people without a gas supply or products to people living in 
                            sheltered accommodation, who don’t need our services. Profiling also means we can 
                            tailor our offerings to current customers, enhancing the potential benefits of being 
                            a customer.


Contact us
General Enquiries/Data Controller
Checkatrade Head Office
                        5 - 6 Sherrington Mews, Ellis Square,
                        Selsey,
                        West Sussex,
                        PO20 0FJ
Tel: 0333 0146 190
www.checkatrade.com/Contact

Data Protection Officer
Legal Services
                        HomeServe PLC
                        Cable Drive
                        Walsall
                        WS27BN
Tel: 0800 247 999
                        Email: [email protected]


Got any worries?
If, at any time, you feel that we haven’t processed your data fairly or you’re not 
                            satisfied with how we’ve handled your personal information, you can contact the 
                            Information Commissioners Office, who will look into this for you. For full 
                            details about how to share any concerns you may have, visit 
                            www.ico.org.uk/concerns


Links to other Websites
Any links to other websites are provided solely as pointers to information on 
                            topics that may be useful to the users of our website. Please remember that when 
                            you use a link to go from our website to another website, this privacy notice will 
                            no longer apply. Your browsing and interaction on any other website, including 
                            those which have a link on our website, are subject to that site's own rules and 
                            policies. We recommend that you read the rules and policies relating to that 
                            website before submitting any personal information.


Updates
This notice will be updated from time to time and we recommend that you check back 
                            regularly but we will notify you of any changes through our website. The version 
                            number and date released will always be listed below:
Version number: 1.0
Date released: December 2017


