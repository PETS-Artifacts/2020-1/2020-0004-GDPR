
Welcome to the Search Encrypt Privacy Policy

    Search Encrypt (“we” or “us”) respects and protects the privacy of consumers; and is committed to delivering a privacy-safe search environment.  When you use Search Encrypt, you trust us with your searches and we understand and value this trust.  We have provided this Privacy Policy to explain how Search Encrypt protects your privacy while searching, what information we collect through Search Encrypt and some of your privacy rights regarding that information.

Effective February 1, 2016
This Policy may change from time to time. Please check back frequently for updates.

    1. Search Encrypt does not track search history in any user identifiable way.


    The most popular search engines create search profiles of specific users in order to retarget ads based on those search queries as the user navigates the internet.  Search Encrypt does not track your search history in any user identifiable way.

2. Search Encrypt does not retain server logs connected to personally identifiable information (PII).

    Search Encrypt does not use tracking tool or maintain logs that compromise your privacy.

3. Search Encrypt prevents your ISP from tracking your search terms.
Some search engines do not encrypt your queries, which means your Internet Service Provider (ISP) can intercept and access them and connect them to your web and/or cable account. When you use Search Encrypt, your search queries are encrypted so that your ISP cannot read them.
4. Search Encrypt does not associate your searches from your online accounts.
The most popular search engines offer other services which often collect sensitive personal information such as email (Gmail, Outlook Webmail, and Yahoo! Mail) and social web services (Google+ and YouTube). When you search the web and remain logged into these accounts, your search queries can be easily connected to your profile.
Search Encrypt completely disconnects your searches from being tied to your email accounts and social profiles.
5.  Search Encrypt does not request, log or share your personal information.
Search Encrypt doesn't collect or maintain any personal information from users who use SearchEncrypt.com.
6.  Search Encrypt uses cookies to determine the effectiveness of our own marketing campaigns.
Because we don’t track any personally identifiable information, we can’t do more than determine that a specific user searched for whatever search terms they searched for. We know nothing about that particular user and we don’t know anything identifiable about the user. Using cookies also allows us to encrypt your search queries on the browser to help hide the history of what you searched for on the machine.
7.  Search Encrypt helps hide your search history on your device.
Search Encrypt uses proprietary technology to hide you search history from others who may use your device after you search.  This may save you from some embarrassing situations.
8.  More Information.
For more information about protecting your privacy, you may wish to visit http://www.netcoalition.com and http://www.privacyalliance.org. If you wish to be notified of any updates to this Privacy Policy, please check back frequently as any updates will be posted to SearchEncrypt.com with a new effective date.
9.  Contacting Search Encrypt
If you have questions about this Privacy Policy and your rights under this policy, please write to us at the address below.
support@searchencrypt.com

