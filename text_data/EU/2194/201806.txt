
Ettevõtte isikuandmete kaitse põhimõtted
SIA INBOKSS isikuandmete kaitse põhimõtted käsitlevad andmete kasutamist ja Teie kui INBOX kasutaja privaatsuse kaitsmist ettevõtte SIA INBOKSS, registrikood 40003560720 (edaspidi - INBOX), poolt.
INBOX’i teenuste kasutamisel usaldate Te meile oma andmed. Käesolevate isikuandmete kaitse põhimõtted on koostatud, aitamaks kasutajal mõista, milliseid andmeid ja mis eesmärgil me kogume ning kuidas me neid kasutame. See on äärmiselt tähtis ja me loodame, et leiate aega tutvuda käesoleva dokumendiga.
Andmehalduse, konfidentsiaalsuse ja turvalisuse tagamise võimalusega saate tutvuda jaotises „Kasutaja profiil“.
Kasutame Teie esitatud andmeid Teile sobivaima reklaami pakkumiseks, teiste inimestega suhtlemise võimaldamiseks ning kiirema ja lihtsama ühiskasutuse tagamiseks.
1. Kogutud andmed
INBOX kogub teatud andmeid, et pakkuda klientidele kvaliteetsemaid teenuseid.
1.1. Isikuandmed
Registreerimisel peate esitama järgmised andmed:
1.1.1        Eesnimi
1.1.2        Perekonnanimi
1.1.3        Sünnikuupäev
1.1.4        Sugu
Meie teenuste kasutamise käigus kogutavad andmed. Me kogume andmeid selle kohta, milliseid teenuseid ja kuidas Te kasutate. Nende andmete kogumine hõlmab järgmist teavet:
1.2. IP-aadress
INBOX kogub andmeid loodud internetiühenduste kohta
1.2.1. IP-aadress, millelt ühendus toimus
1.2.2. Ühenduse kuupäev ja kellaaeg
1.3. Asukoha andmed
INBOX’i teenuste kasutamise ajal võime koguda ja töödelda andmeid Teie tegeliku asukoha kohta. INBOX kasutab asukoha IP-aadressi järgi tuvastamise tehnoloogiaid.
1.4. Andmed tasumise kohta
INBOX’i teenuste eest tasumise hetkel võib INBOX koguda ja töödelda Teie pangakonto, panga, telefoninumbri ja PayPali konto andmeid. Maksekaardi andmeid ei koguta ega töödelda.
1.5. Kohalik andmehoidla 
INBOX võib koguda ja hoida andmeid (sh isikuandmeid) Teie seadmes, kasutades selleks nt brauseri veebipõhist andmehoidlat.
1.6. Küpsised ja sarnased tehnoloogiad
Kui Te külastate INBOX’i teenuseid, kasutame me (ja ka meie koostööpartnerid) erinevaid andmete kogumise ja hoiustamise tehnoloogiaid, mis hõlmavad küpsiste või sarnaste tehnoloogiate kasutamist Teie brauseri või seadme tuvastamiseks. INBOX kasutab neid tehnoloogiaid andmete kogumiseks ja hoiustamiseks reklaami edastamise eemärgil.
2. Kogutud andmete kasutamine
INBOX’i teenustest kogutud andmeid kasutab INBOX olemasolevate teenuste tagamiseks, haldamiseks, kaitsmiseks ja arendamiseks, uute teenuste väljatöötamiseks ja INBOX’i kasutajate turvalisuse tagamiseks. Samuti kasutab INBOX kogutud andmeid oma klientidele isikustatud sisu ja reklaami pakkumiseks.
Kõigi selliste teenuste pakkumisel, mis nõuavad INBOX’i konto olemasolu, võib INBOX kasutada INBOX’i kasutaja poolt INBOX’i profiili registreerimisel esitatud ees- ja perekonnanime ning profiili nime.
Kui Te võtate ühendust INBOX‘iga, talletab INBOX sellega seotud andmed, aitamaks Teil lahendada mis tahes võimalikke probleeme. Me võime kasutada Teie e-posti aadressi, et teavitada Teid meie teenustest ning näiteks kavandatud muudatustest või parendustest.
Küpsiste ja muude tehnoloogiate kasutamisel kogub INBOX andmeid, mis aitavad meil parendada kasutajakogemust (nt keele-eelistuste kohta andmete kogumine) ja teenuste üldist kvaliteeti.
Isikustatud reklaamide edastamisel ei seo INBOX küpsiste või sarnaste tehnoloogiate tuvastuselemente tundlike isikuandmetega, nt rassi, usu, seksuaalse orientatsiooni või tervise seisundiga.
INBOX töötleb isikuandmeid oma serverites, mis asuvad Lätis.
3. Läbipaistvus ja valikuvõimalus
INBOX’i eesmärk on selgelt teavitada kasutajat INBOX‘i poolt kogutud andmetest, et kasutaja saaks teha põhjendatud valiku kogutud andmete kasutamise kohta. Te saate teha järgmist:
3.1. INBOX’i profiili ülevaates saate vaadata üle ja kontrollida teatud tüüpi andmeid, mis on seotud Teie INBOX’i kontoga.
3.2. INBOX’i profiili ülevaates olevates reklaamiseadetes saab kasutaja vaadata üle ja muuta INBOX’i keskkonnas pakutud reklaamide eelistusi.
Kasutaja võib oma brauseris blokeerida kõik küpsised, sealhulgas ka INBOX’i teenustega seotud küpsised. Kuid võtke arvesse, et küpsiste keelamine võib takistada teatud INBOX’i teenuste kättesaadavust.
4. Juurdepääs isikuandmetele ja isikuandmete uuendamine
Olenemata INBOX’i teenuse kasutamise ajast, on INBOX’i eesmärk tagada kasutajale juurdepääs tema isikuandmetele. INBOX teeb kõik võimaliku valede isikuandmete kiireks uuendamiseks või kustutamiseks.
INBOX’i eesmärk on pakkuda oma teenuseid viisil, mis välistab andmete juhusliku või tahtliku hävitamise. Pärast andmete kustutamist INBOX’i teenuste keskkonnast võib kuluda teatud aeg vastavate andmete lisakoopiate kustutamiseks meie aktiivsetest serveritest.
4.1. INBOX’i ja kasutajate vahel jagatud teave
4.1.1. Te nõustute Teie esitatud andmete töötlemisega INBOX’i andmebaasis.
4.1.2. Te saate andmete töötlemisest keelduda jaotises „Kasutaja profiil“.
4.1.3. Me säilitame Teie andmeid seni, kuni Te kasutate INBOX’i teenuseid.
4.1.4. Teil on õigus esitatud andmeid parandada ja kustutada. Sellisel juhul hoiame Teie andmeid senikaua, kui on mõistlikult vajalik veendumaks, et andmeid pole parandanud või kustutanud kolmas isik, kes on ebaseaduslikult saanud juurdepääsu Teie e-postile ja teinud Teie nimel toimingud.
4.1.5. INBOX kohustub Teie nõudmisel kustutama Teie esitatud eraisikuandmed.
4.2. INBOX’i teenuste kasutamise eelduseks on isiku tuvastamist võimaldavate andmete esitamine, kusjuures e-posti sisu kuulub Teile ja vaidluse korral peate suutma oma õigusi tõendada.
4.2.1. Vastavalt EUROOPA PARLAMENDI JA NÕUKOGU MÄÄRUSE (EL) nr 2016/679 artiklile 13 on Teil õigus esitada kaebus järelevalveasutusele.
4.2.2. Kasutajal on õigus nõuda hoiustatud isikuandmete koopia väljastamist. Selleks peab ta isiklikult tulema INBOX’i kontorisse ja tuvastama end profiili omanikuna. Taotletud teave hoiustatud isikuandmete kohta esitatakse kahe nädala jooksul alates taotluse kättesaamisest.
5. Isikuandmete kustutamise ja taastamise kord
Kasutaja isikuandmeid saab kustutada järgmistel juhtudel:
5.1. Kasutajal on õigus ise oma isikuandmed kustutada, kustutades oma profiili jaotises „Kasutaja profiil“. Juurdepääs profiilile blokeeritakse ja kasutaja andmed kustutatakse INBOX’i serverist ühe aasta jooksul.
5.2. Vastavalt INBOX’i portaali kasutustingimustele võib INBOX kustutada kasutaja isikuandmed tema profiili kustutamise teel. Sellisel juhul kustutatakse kasutaja andmed INBOX’i serverist ühe aasta jooksul.
Mainitud ooteaeg on vajalik, et välistada olukord, kus kolmandad isikud teevad kasutaja sisselogimisandmeid kasutades loata toiminguid.
Punktis 5.1 ja 5.2 sätestatud andmete säilitamise ajal on kasutajal õigus oma profiili taastada. Selleks peab ta isiklikult tulema INBOX’i kontorisse ja tuvastama end profiili omanikuna. Isikuandmed taastatakse kahe päeva jooksul alates taotluse kättesaamisest.
Punktis 5.1 ja 5.2 sätestatud andmete säilitamise ajal on kasutajal õigus oma profiil kustutada. Selleks peab ta isiklikult tulema INBOX’i kontorisse ja tuvastama end profiili omanikuna. Isikuandmed kustutatakse hiljemalt kahe päeva jooksul vastava taotluse kättesaamisest.
6. Isikuandmete turvalisuse tagamise meetmed
INBOX rakendab järgmisi turbemeetmeid, et kaitsta ettevõtet INBOX ja kasutajaid loata juurdepääsu eest INBOX’i käsutuses olevale teabele ja selle võimaliku muutmise, avalikustamise või hävitamise eest.
6.1. Selleks rakendab INBOX järgmisi meetmeid:
6.1.1. andmete krüpteerimine vastavalt SSL-standardile
6.1.2. kaheastmeline tuvastamine INBOX’i kontole sisselogimisel
6.1.3. INBOX‘i kogutud andmete, nende töötlemise ja hoiustamise, sh füüsilise turvalisuse tagamise meetmete kontroll, välistamaks loata juurdepääsu süsteemidele
6.1.4. juurdepääs isikuandmetele on lubatud üksnes INBOX’i töötajatele, töövõtjatele ja esindajatele, kes teostavad andmetöötlust INBOX’i korraldusel ja kes on kohustatud järgima lepingus sätestatud rangeid konfidentsiaalsuse nõudeid ja kellele nimetatud nõuete rikkumise korral võidakse määrata distsiplinaarkaristus või vallandamine.
7. Teenused, mille suhtes kohaldatakse käesolevaid eraelu puutumatuse põhimõtteid
INBOX’i eraelu puutumatuse põhimõtteid kohaldatakse kõigi INBOX’i pakutavate teenuste suhtes, välja arvatud teenused, mille kohta kehtivad eraldi eraelu puutumatuse põhimõtted, mida käesolevad eraelu puutumatuse põhimõtted ei sisalda.
INBOX’i eraelu puutumatuse põhimõtted ei laiene teiste ettevõtete või eraisikute pakutavatele teenustele, kaasa arvatud toodetele või keskkondadele, mis võivad hõlmata INBOX’i teenuseid, või muudele INBOX’i teenustega seotud keskkondadele. INBOX’i eraelu puutumatuse põhimõtted ei laiene nende teiste ettevõtete ja organisatsioonide sätestatud korrale, kes reklaamivad INBOX’i teenuseid ja võivad kasutada küpsiseid, veebimajakaid ja muid reklaamide edastamiseks ja pakkumiseks ettenähtud tehnoloogiaid.
8. Põhimõtete järgimine ja koostöö reguleerivate asutustega
INBOX kontrollib regulaarselt oma tegevuste vastavust käesolevate eraelu puutumatuse põhimõtete nõuetele. Ametliku kirjaliku kaebuse või taotluse kättesaamisel võtame ühendust selle esitajaga, et otsustada edaspidiste toimingute üle. Isikuandmete edastamisega seotud mis tahes kaebuse lahendamiseks, mida ei õnnestu lahendada konkreetse isikuga läbi rääkides, teeb INBOX koostööd asjaomaste reguleerivate asutustega, muu hulgas kohalike andmekaitseasutustega.
9. Muudatused
INBOX‘il on õigus käesolevat eraelu puutumatuse põhimõtteid aeg-ajalt muuta. INBOX ei piira käesolevatest eraelu puutumatuse põhimõtetest tulenevaid kasutaja õigusi, kui selleks ei ole saadud kasutaja selget nõusolekut. Kõik eraelu puutumatuse põhimõtete muudatused avaldatakse käesoleval lehel. Oluliste muudatuste korral teavitab INBOX neist eraldi (muu hulgas saadab e-posti teel teavituse konkreetsete teenuste kohta kehtivate eraelu puutumatuse põhimõtete muutmisest).
10. Muu kasulik teave eraelu puutumatuse ja turvalisuse kohta
INBOX’i kasutajatoe kodulehelt leiate kasulikku teavet eraelu puutumatuse ja turvalisuse kohta, sealhulgas allpool toodud teemade kohta:
10.1. küpsiste kasutamine INBOX‘i poolt;
 



Kui te ei suutnud leida vastust oma küsimusele, palun kirjutage support@inbox.lv

