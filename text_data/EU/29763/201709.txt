
This page was last modified on: 2016-12-03. View changes here.
Information stored via web servers

  HTTP access logs for cock.li are retained for 2 days. Anonymized analytics information is collected by the self-hosted libre software Piwik and is periodically anonymized and scrubbed. Information compiled from this non-identifying information may be made public at any time. Visitors who have the Do Not Track header set are not tracked by Piwik.


  HTTP access logs for mail.cock.li (webmail) are completely disabled.

Information stored via xmpp servers

  Only the bare info is stored, meaning your roster list and any info like mood or whatever you provide to the server. All logging is disabled.

Information stored via mail servers

  Here's the juicy stuff. There are 3 classes of information here: Public, confidential, and private. They are listed and explained below.


Public (May be made public at any time)

Your e-mail address (existence of your email address can be checked during registration and password changing)
Anonymized data including the time you registered, GeoIP maps, etc.
Anything posted to any lists.cock.li mailing lists
Anything E-mailed to me

Confidential (Used in abuse investigations and when complying with legal orders for user information)

User-specific disk space statistics
Sieve rules
Data stored permenantly:

Registering IP address
Time registered
Password changes (only user ID and time are recorded)

Data stored for 2 days:

IMAP logs are disabled
SMTP logs, including who you send E-mail to and at what time, and who you receive E-mail from and at what time. IPs are logged with these.


Private (Only looked at when complying with legal orders for user information)

IMAP folder names and structure
E-mail content
E-mail headers



