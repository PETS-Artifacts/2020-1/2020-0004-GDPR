



(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5SSFZ2Z');








window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,SI:p.setImmediate,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1044.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);



















The SlottyVegas Casino Online: the Place Where Jackpots Live







$(document).ready(function(){
});












  
	
	All games
	



Login




Welcome back to Slotty Vegas








           
        
        Forgot password?
        




$(document).ready(function() {
	$(".login-form").on("submit",function(){
		var isValid 	= validateForm($(this));
				
		if(isValid)
		{	
			$("input[name=login_submit]").hide();
			$(".login-form .submit-container a").hide();
			$("#form-loading").fadeIn();
		}
		else
		{			
			$("#form-loading").hide();
			$("input[name=login_submit]").fadeIn();
			$(".login-form .submit-container a").fadeIn();
		}
		return isValid;
	});
});
















* x30B wagering requirement applies - Read terms













or use:









$(document).ready(function() {
	$(".quick-reg-form input[name=field_email]").keyup(function()
	{
		checkQuickRegLogin($(this),"Login","SIGN UP","Enter your password","Choose a secure password with more than 6 characters","https://slottyvegas.com/en/welcome/register","https://slottyvegas.com/en/");
	});
});








SIGN UP






















Show all games


$(document).ready(function() {
	$(window).smartresize(function(e){
		var element = $(".show-more-games");
		if($(document).width() > 640)
		{
			element.fadeIn();
			element.html("Show all games");
		}
		else
		{
			if($("#games-non-logged-in .col:nth-child(n+5)").css("display") == "block")
			{
				element.fadeOut();
			}
			else
			{
				element.html("Show more games");
			}
		}
	});
	$(".show-more-games").on("click",function(event){
		event.preventDefault();
		if($(document).width() > 640)
		{
			loadBlindWindow($(this));
		}
		else
		{
			$("#games-non-logged-in .col:nth-child(n+5)").css("display","block");
			$("#games-non-logged-in").addClass("noMargin");
			$(this).fadeOut();
			$("#games-non-logged-in").trigger("resize");
		}
	});
});



Our games pay more withSupercharged™ Wins









We will boost each and every winning spin with extra funds, on the house! Play any game with the  logo to keep boosting your winnings. It really is as simple as that!

Read more







$(document).ready(function() {
	var carousel = $(".supercharged-carousel.three");
	updateSuperchargedFeed(carousel,3,1);
	animateSuperchargedFeed(carousel,3);
});
















Join the casino










or use:









$(document).ready(function() {
	$(".quick-reg-form input[name=field_email]").keyup(function()
	{
		checkQuickRegLogin($(this),"Login","SIGN UP","Enter your password","Choose a secure password with more than 6 characters","https://slottyvegas.com/en/welcome/register","https://slottyvegas.com/en/");
	});
});















Great casino on your mobile!
Did you know that Slotty Vegas can also be found on mobile?
You can enjoy your favorite games straight from your smart phone or tablet with an entire casino at your finger tips.
Psssst, our welcome offer is also playable on mobile games so create an account and start winning now!











function addLoadEvent(func) {
var oldonload = window.onload;
if (typeof window.onload != "function") {
window.onload = func;
} else {
window.onload = function() {
if (oldonload) {
oldonload();
}
func();
}
}
}
ddLoadEvent(function(){
outdatedBrowser({
bgColor: "#f68933",
color: "#ffffff",
lowerThan: "transform",
languagePath: "scripts/outdatedbrowser/lang/en.html"
})
});



(function(d, s, id) {
var js, fjs = d.getElementsByTagName(s)[0];
if (d.getElementById(id)) return;
js = d.createElement(s); js.id = id;
js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.8";
fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));













			 $(window).on("load",function(){
				 $(".jcarousel-footer").jcarousel({
					 wrap:	  "circular",
					 transitions: true,
					 animation: {
						duration: 500,
						easing:   "linear",
						complete: function() {}
					 }
				 }).jcarouselAutoscroll({
					 interval: 1000,
					 target: "+=1",
					 autostart: true
				 });
				 $(window).trigger("resize");
			 });
			 












NRR Entertainment is a Limited Liability company registered under the laws of Malta in the European Union with registration number C47261 and a registered address "JPR Buildings, Level 2, Triq Taz-Zwejt, San Gwann, SGN3000, MALTA". NRR Entertainment holds a remote gaming license number MGA/CL1/785/2011 & MGA/CL1/965/2014 granted by the Malta Gaming Authority under the Subsidiary Legislation 438.04 "Remote Gaming Regulations" of the Laws of Malta.
Licensed in the U.K by the Gambling Commission.
Underage gambling is an offence - Bet Responsibly.
Filtering software to be used by adults (such as parents or withing schools) in order to restrict access to the relevant pages of our website.
Slotty Vegas incorporates the most advanced security and encryption technologies on the market to ensure that all your financial details are entirely secure at all times.




Follow @SlottyVegas




	Our Games
	



	Responsible Gaming
	



	Terms of service
	



	Privacy
	



	Complaints
	



	Facebook
	



	Affiliates
	













































Available onMobile

	$(document).ready(function() {
		$("#available-on-mobile").velocity("fadeIn",{duration:1700,loop:5,delay:2000});
	});
	

    window.__lc = window.__lc || {};

    window.__lc.chat_between_groups = false;
    window.__lc.group = 3;

    window.__lc.license = 7249321;

        (function() {
			var lc      = document.createElement('script');
			var att     = document.createAttribute('data-cfasync');
			att.value   = 'false';
			
			lc.type = 'text/javascript';
			lc.async = true;
			lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
			var s = document.getElementsByTagName('script')[0];
			
			s.setAttributeNode(att);
			s.parentNode.insertBefore(lc, s);
        })();
    


	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,"script","https://www.google-analytics.com/analytics.js","ga");
	
	ga("create", "UA-47579410-1", "slottyvegas.com");
	ga("require", "displayfeatures");
	ga("require", "linkid", "linkid.js");
	ga("send", "pageview");
	
	function ga_heartbeat(){
	ga("send","event","Heartbeat","Heartbeat","");
	setTimeout(ga_heartbeat, 5*60*1000);
	}
	ga_heartbeat();


	if(window.self == window.top)
	{
	   var _mfq = _mfq || [];
	   (function() {
	       var mf = document.createElement("script"); mf.type = "text/javascript"; mf.async = true;
	       mf.src = "//cdn.mouseflow.com/projects/42288bef-daea-4f16-b4cd-e140aa706483.js";
	       document.getElementsByTagName("head")[0].appendChild(mf);
	   })();
	}
	
window.NREUM||(NREUM={});NREUM.info={"beacon":"bam.nr-data.net","licenseKey":"3a69c55044","applicationID":"7554248","transactionName":"NFUDMEsEX0NQBhEPCw0fNBZQSlheVQAdSBQLQA==","queueTime":0,"applicationTime":162,"atts":"GBIARgMeTE0=","errorBeacon":"bam.nr-data.net","agent":""}
