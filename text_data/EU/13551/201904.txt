
INTRODUCTION
Our privacy policy will help you understand what information we collect at Fexco, how Fexco uses it, and what choices you have.
When we talk about “Fexco,” “we,” “our,” or “us” in this policy, we are referring to Fexco Unlimited Company, registered in Ireland under number 83934, with a registered address at Fexco Centre, Iveragh Road, Killorglin, Co. Kerry and our Affiliates, the company which provides the Services. “Affiliates” in this context means any entity that directly or indirectly controls, is controlled by, or is under common control with us, and “Control” means direct or indirect ownership or control of more than 50% of the voting interests of the subject entity).
When we talk about the “Services” in this policy, we are referring to all our products and services. Your product or service terms and conditions will specify which of our businesses is providing the relevant product or service to you. Some of our businesses as listed below have their own data privacy notices reflecting the particular services they provide. If you are a customer of these businesses, please also read their privacy notices:
There are certain circumstances when Fexco acts as a data processor rather than a data controller, and we have been instructed to process your Personal Data by the data controller of your Personal Data. In such circumstances, we may only process your Personal Data in line with such data controller’s instructions. To the extent that you wish to exercise any of the rights in respect of Personal Data referred to in this Privacy Policy in respect of circumstances when we are the data processor rather than the data controller, you should contact the relevant data controller in respect of same. We are not responsible for the privacy policies and practices of third party data controllers, and we disclaim all liability in relation to same.
If you have any questions about how your information gathered, shared or used, please contact our data protection team via the ‘How To Contact Us’ facility referred to below
This Privacy Policy sets out the basis on which any Personal Data which we collect from you, or that you provide to us, will be processed by us. In this Privacy Policy, the term “Personal Data” means data relating to a living individual who is or can be identified either from the data or from the data in conjunction with other information that is in, or is likely to come into, our possession, and includes personal data as described in Data Protection Legislation (as defined below).
We will handle your Personal Data in accordance with Data Protection Legislation. “Data Protection Legislation” means the Data Protection Acts 1988 and 2003 and Directive 95/46/EC, any other applicable law or regulation relating to the processing of personal data and to privacy (including the E-Privacy Directive and the European Communities (Electronic Communications Networks and Services) (Privacy and Electronic Communications) Regulations 2011 (“E-Privacy Regulations”), as such legislation shall be amended, revised or replaced from time to time, including by operation of the General Data Protection Regulation (EU) 2016/679 (“GDPR”) (and laws implementing or supplementing the GDPR, and laws amending or supplementing the E-Privacy Regulations).
B. INFORMATION WE COLLECT AND RECEIVE
We fully respect your right to privacy in relation to your interactions with the Services and endeavor to be transparent in our dealings with you as to what information we will collect and how we will use your information. As you use our services, apply for products, make enquiries and engage with us, information is gathered about you. Also, we only collect and use individual’s information where we are legally entitled to do so. Information in relation to Personal Data collected by Irish entities is available on www.dataprotection.ie, the website of the Irish Data Protection Commissioner (“DPC”)
1. Customer Data
Personal Data submitted by users to the Services (the “Customers”) is referred to in this Privacy Policy as “Customer Data.” Where Fexco collects or processes Customer Data, it does so on behalf of the Customer.
Depending on the service being used the customer data we collect can include the following:
Identity & contact information – Name, date of birth, copies of ID, contact details, PPS number (or foreign equivalent), online user identities, internet protocol addresses, cookie, security details to protect identity, nationality, home status and address, email address, work and personal phone numbers, marital status, family details, tax residency and tax related information.
Financial details/circumstances – Bank account details, credit/debit card details, income details, personal guarantees provided, application processing and administration records, employment status and employment details, credit history, credit assessment records, credit data from credit registers, credit agency performance data and authorised signatories details.
We endeavor to keep Customer Data accurate and up-to-date. As such, you must tell us about any changes to such information that you are aware of as soon as possible.
2. Other information
Fexco also collect and receive the following information:

Account creation information. Users provide information such as name, address, e-mail address, contact details and passwords to create an account.
Billing and other information. For Customers that purchase Services, our corporate Affiliates and our third party payment processors may collect and store billing address and credit card information on our behalf [or we may do this ourselves] and we will have appropriate contracts in place with such third parties to safeguard your Customer Data.
Services usage information. This is information about how you are accessing and using the Services, which may include administrative and support communications with us and information about the Services, features, content, and links you interact with.
Log data. When you use the Services our servers automatically record information, including information that your browser sends whenever you visit a website or your mobile app sends when you are using it. This log data may include your Internet Protocol address, the address of the web page you visited before using the Services, your browser type and settings, the date and time of your use of the Services, information about your browser configuration and plug-ins, language preferences, and cookie data.
Telephone recordings. In performing certain roles we have a contractual obligation to record calls on behalf of our clients. Where this is required we will inform you of those who are likely to have accessed the call and reasons for same.
CCTV Data. The organisation has closed circuit television cameras located at store locations and offices. This is necessary in order to protect against theft or pilferage, for the security of staff and organisation property. Access to the recorded material will be strictly limited to authorised personnel.
Device information. We may collect information about the device you are using the Services on, including what type of device it is, what operating system you are using, device settings, application IDs, unique device identifiers, and crash data. Whether we collect some or all of this information often depends on what type of device you are using and its settings.
Services integrations. If, when using the Services, you integrate with a third party service, we will connect that service to ours. The third party provider of the integration may share certain information about your account with Fexco subject to appropriate contracts being in place with such third parties to safeguard your Customer Data. However, we do not receive or store your passwords for any of these third party services.
Third party data. Fexco may also receive information from Affiliates, our partners, or others that we use to make our own information better or more useful. This might be aggregate level information, such as which IP addresses go with which postal codes, or it might be more specific information, such as about how well an online marketing or email campaign performed.

3. Cookies
A cookie is a small text file that is placed on your device by a web server which enables a website and/or mobile app to recognise repeat users, facilitate the user’s ongoing access to and use of a website and/or mobile app and allows the website and/or mobile app to track usage behaviour and compile aggregate data that will allow content improvements and targeted advertising. We collate information only in relation to the Services which is represented in aggregate format through cookies. They help us to improve our Services and to deliver many of the functions that make your browser experience more user friendly.
By using the Services and accepting the terms of this Privacy Policy you are consenting to the use of cookies as described in this Privacy Policy and our Cookies Policy (i.e. you are agreeing to the placement of cookies on your device unless you specifically choose not to receive cookies).
The ‘Help Menu’ on the menu bar of most browsers will tell you how to prevent your browser from accepting new cookies, how to have the browser notify you when you receive a new cookie and how to disable cookies altogether. You can also disable or delete similar data used by browser add-ons, such as flash cookies, by changing the add-on’s settings or visiting the website of its manufacturer.
For more information about cookies and managing them including how to turn them off, please visit www.cookiecentral.com. However, because cookies allow you to take advantage of some of the Services essential features, we recommend you leave them turned on as otherwise you may not be able to fully experience the interactive features of the Services or other related websites which you visit.
For detailed information on the cookies we use and the purposes for which we use them see our Cookie Policy
4. Aggregated Data
This Privacy Policy is not intended to place any limits on what we do with data that is aggregated and/or de-identified so that it is no longer associated with an identifiable user or Customer of the Services. We may disclose or use aggregate or de-identified information for any purpose. For example, we may share aggregated or de-identified information with our partners or others for business or research purposes.
C. WHY WE NEED CUSTOMER DATA
Fexco needs Customer Data in order to provide the Services you have engaged us to provide. If you do not provide the Customer Data, then we will be unable to provide the Services you have requested. We will not collect any Personal Data from you that we do not need in order to provide and oversee the Services we have agreed to provide you with.
D. HOW WE USE YOUR INFORMATION
We use your information to provide and improve the Services we provide. We will only process your Personal Data where we have a legal basis to do so. In general, our legal basis for processing your Personal Data will be in furtherance of the contract(s) that we have with you and/or where you have provided your consent.
1. Customer Data
Fexco may access and use Customer Data as reasonably necessary to (a) provide, maintain and improve the Services; (b) to prevent or address service, security, technical issues or at a Customer’s request in connection with customer support matters; (c) as required by law and (d) as set forth in our agreement with the Customer or as expressly permitted in writing by the Customer in accordance with Customer’s instructions.
2. Other information
We use other kinds of information in providing the Services. Specifically:

To understand and improve our Services. We carry out research and analyse trends to better understand how users are using the Services and improve them.
To communicate with you by:

Responding to your requests. If you contact us with a problem or question, we will use your information to respond.
Sending electronic communications. We may send you administrative electronic communications relating to the Services. We may also contact you to inform you about changes in our Services, and important Service related notices, such as security and fraud notices. These electronic communications are considered part of the Services and are made in our legitimate interest in accordance with Data Protection Legislation.In addition, we sometimes, with your consent, send electronic communications about new product features or other news about Fexco. You can opt out of these electronic communications at any time by contacting us (see ‘How To Contact Us’) below or by clicking the unsubscribe link in the electronic communication. Opting out of direct marketing will not opt you out of essential communications that we need to send to you in respect of the administration of the Services. If you opt out of our electronic communications to you, we may not be able to fully provide the Services to you. 


Billing and account management. We use account data to administer accounts and keep track of billing and payments.
Communicating with you. We often need to contact you for invoicing, account management and similar reasons.
Protecting Customer Data. We work hard to keep the Services secure and to prevent abuse and fraud, and may contact you in this respect as necessary.

E. HOW LONG WE KEEP CUSTOMER DATA FOR
The length of time Fexco hold your data depends on a number of factors, such as regulatory rules and the type of service we have provided to you.
Those factors include:

The regulatory rules contained in laws and regulations or set by authorities like the Central Bank of Ireland, for example, in the Consumer Protection Code.
The type of financial product we have provided to you. For example, we may keep data relating to a finance product for a longer period compared to data regarding a single payment transaction.
Whether you and us are in a legal or some other type of dispute with another person or each other.
Whether you or a regulatory authority asks us to keep it for a valid reason.

As a general rule, we keep your information for a specified period after the date on which a transaction has completed or you cease to be a customer. In most cases Fexco keep Customer Data for seven (7) years, but may be twelve (12) years where we had a deed in place, after which time it will be destroyed if it is no longer required for the lawful purpose for which it was obtained.
If you consent to marketing, any information we use for this purpose will be kept with us until you notify us that you no longer wish to receive this information.
F. YOUR RIGHTS
As a data subject, you have the following rights under Data Protection Legislation and we, as data controller in respect of Customer Data, will comply with such rights in respect of Customer Data:

the right of access to Personal Data relating to you;
the right to correct any mistakes in your Personal Data;
the right to ask us to stop contacting you with direct marketing;
rights in relation to automated decision taking;
the right to restrict or prevent your Personal Data being processed;
the right to have your Personal Data ported to another data controller;
the right to erasure; and
the right to complain to the DPC if you believe we have not handled your Personal Data in accordance with Data Protection Legislation.

These rights are explained in more detail in Schedule A to this policy, but if you have any comments, concerns or complaints about our use of your Personal Data, please contact us (see ‘How To Contact Us’ below). We will respond to any rights that you exercise within a month of receiving your request, unless the request is particularly complex or cumbersome, in which case we will respond within three months (we will inform you within the first month if it will take longer than one month for us to respond). Where a response is required from us within a particular time period pursuant to Data Protection Legislation, we will respond within that time period.
Withdrawal of consent
If you no longer consent to our processing of Customer Data (in respect of any matter referred to in this Privacy Policy as requiring your consent), you may request that we cease such processing by contacting us via the ‘How To Contact Us’ facility referred to below. Please note that if you withdraw your consent to such processing, it may not be possible for us to provide all/part of the Services to you.
G. SHARING AND DISCLOSURE
There are times when information described in this Privacy Policy may be shared by Fexco. This section discusses only how Fexco may share such information. We may share with third parties certain pieces of aggregated, non-personal information. Such information does not identify you individually. We restrict access to Customer Data to employees, contractors, and agents who need to know that information in order to operate, develop, or improve our Services. Your personal information remains protected when our service providers use it. We only permit service providers to use your information in accordance with our instructions, and we ensure that they have appropriate measures in place to protect your information. These individuals are bound by confidentiality obligations and may be subject to discipline, including termination, civil litigation and/or criminal prosecution, if they fail to meet these obligations.
Customer Data may be transferred to, stored at, or accessed from a destination outside the European Economic Area (“EEA”) for the purposes of us providing the Services. It may also be processed by staff operating outside the EEA who work for us, our Affiliates, or any of our suppliers. By submitting Customer Data, you explicitly consent to this transfer, storing or processing. We will take all steps reasonably necessary to ensure that Customer Data is treated securely and in accordance with this Privacy Policy. The safeguards in place with regard to the transfer of Customer Data outside of the EEA are the entry by us into appropriate contracts with all transferees of such data.
All information you provide to us is stored on our secure servers. Where we have given you (or where you have chosen) a password which enables you to access the Services, you are responsible for keeping this password confidential. We ask you not to share a password with any person not authorised to use the Services.
In particular:
1. Customer Data
Fexco may share Customer Data in accordance with our agreement with the Customer and the Customer’s instructions, including:

With third party service providers and agents. We engage third party companies or individuals to process Customer Data as follows: 

Third parties we need to share your information with in order to facilitate payments you have requested (for example, SWIFT, credit card issuers and merchant banks) and those you ask us to share your information with.
Companies in the Fexco Group.
Companies that provide support services for the purposes of delivering the service. These include, IT and telecommunication service providers, software development contractors, data processors, debit/credit card producers, computer maintenance contractors, document storage and destruction companies, including legal advisors.
Statutory and regulatory bodies (including central and local government) and law enforcement authorities.
Credit reference/rating agencies, including the Central Credit Register and The Irish Credit Bureau.



2. Other types of disclosure
Fexco may share or disclose Customer Data and other information as follows:

During changes to our business structure. If we engage in a merger, acquisition, bankruptcy, dissolution, reorganisation, sale of some or all of Fexco’s assets, financing, acquisition of all or a portion of our business, a similar transaction or proceeding, or steps in contemplation of such activities (e.g. due diligence).
To comply with laws. To comply with legal or regulatory requirements and to respond to lawful requests, court orders and legal process.
To enforce our rights, prevent fraud and for safety. To protect and defend the rights, property, or safety of us or third parties, including enforcing contracts or policies, or in connection with investigating and preventing fraud.

H. THIRD PARTY WEBSITES
This Privacy Policy applies to websites and services that are owned and operated by Fexco. We do not exercise control over the sites/applications that may be linked from our website. These other sites/applications may place their own cookies or other files on your computer, collect data or solicit personal information from you. You acknowledge that the Services that we provide may enable or assist you to access the website content of, correspond with, and purchase products and services from, third parties via third-party websites and that you do so solely at your own risk. We make no representation or commitment and shall have no liability or obligation whatsoever in relation to the content or use of, or correspondence with, any such third-party website, or any transactions completed, and any contract entered into by you, with any such third party and the use by any such third-party of your Customer Data. We do not endorse or approve any third-party website nor the content of any of the third-party website made available via the Service. We encourage you to carefully familiarize yourself with the terms of use and privacy policies applicable to any websites and/or services operated by third parties. Please be aware that we are not responsible for the privacy practices of any third parties.
I. HOW DO WE PROTECT YOUR PERSONAL INFORMATION
We do our utmost to protect user privacy through the appropriate use of security technology. We restrict access to Customer Data to employees, contractors and agents who need to know such Customer Data in order to operate, develop or improve the services that we provide. We ensure that we have appropriate physical and technological security measures to protect your information; and we ensure that when we outsource any processes that the service provider has appropriate security measures in place. However, the Services may contain hyperlinks to websites owned and operated by third parties. These third party websites have their own privacy policies, including cookies. We do not accept any responsibility or liability for the privacy practices of such third party websites and your use of such websites is at your own risk.
We will implement appropriate technical and organisational measures to ensure a level of security appropriate to the risks that are presented by the processing of Customer Data. In particular, we will consider the risks presented by accidental or unlawful destruction, loss, alteration, unauthorised disclosure of, or access to Customer Data transmitted, stored or otherwise processed.
Unfortunately, the transmission of information via the internet is not completely secure. Although we will do our best to protect Customer Data, we cannot guarantee the security of any data transmitted us and any such transmission is at your own risk. Once we have received your information, we will use strict procedures and security features to try to prevent unauthorised access. To the extent permitted by law, we are not responsible for any delays, delivery failures, or any other loss or damage resulting from (i) the transfer of data over communications networks and facilities, including the internet, or (ii) any delay or delivery failure on the part of any other service provider not contracted by us, and you acknowledge that the Services may be subject to limitations, delays and other problems inherent in the use of such communications facilities. You will appreciate that we cannot guarantee the absolute prevention of cyber-attacks such as hacking, spyware and viruses. Accordingly, you will not hold us liable for any unauthorized disclosure, loss or destruction of Customer Data arising from such risks.
J. BREACH REPORTING
We will notify serious data breaches in respect of Customer Data to the DPC or the equivalent authority in other jurisdictions, without undue delay, and where feasible, not later than 72 hours after having become aware of same. If notification is not made after 72 hours, we will record a reasoned justification for the delay; however, it is not necessary to notify the DPC where the Personal Data breach is unlikely to result in a risk to the rights and freedoms of natural persons. A Personal Data breach in this context means a breach of security leading to the accidental or unlawful destruction, loss, alteration, unauthorised disclosure of, or access to, Personal Data transmitted, stored or otherwise processed.
We will keep a record of any data breaches, including their effects and the remedial action taken, and will notify you of any data breach affecting your Personal Data (which poses a high risk to you) when we are required to do so under Data Protection Legislation. We will not be required to notify you of a data breach where:

We have implemented appropriate technical and organisational measures that render the Personal Data unintelligible to anyone not authorised to access it, such as encryption; or
We have taken subsequent measures which ensure that the high risk to data subjects is not likely to materialise; or
It would involve disproportionate effort, in which case we may make a public communication instead.

K. CHILDREN’S INFORMATION
Our Services are not directed to children under 13 or 16 (in the Republic of Ireland). If you learn that a child has provided us with personal information without consent, please contact us.
L. CHANGES TO THIS PRIVACY POLICY
We may change this Privacy Policy from time to time, and if we do we will post any changes on this page. If you continue to use the Services after those changes are in effect, you agree to the revised Privacy Policy.
M. HOW TO CONTACT US
If you have any questions about this Privacy Policy, your information, our use of this information, or your rights when it comes to Customer Data, please feel free to contact us by emailing dataprotection@fexco.com or by writing to us at Fexco, Data Protection Team, Fexco Centre, Iveragh Road, Killorglin, Ireland.
Schedule A – Data Subject rights under Data Protection Legislation
Right of access to Personal Data relating to you
You may ask to see what Personal Data we hold about you and be provided with:

a summary of such Personal Data and the categories of Personal Data held (see Sections 1 and 2 above);
details of the purpose for which it is being or is to be processed;
details of the recipients or classes of recipients to whom it is or may be disclosed, including if they are overseas and what protections are used for those oversea transfers;
details of the period for which it is held or the criteria we use to determine how long it is held;
details of your rights, including the rights to rectification, erasure, restriction or objection to the processing;
any information available about the source of that data;
whether we carry out automated decision-making, or profiling, and where we do, information about the logic involved and the envisaged outcome or consequences of that decision making or profiling; and
where your Personal Data are transferred out of the EEA, what safeguards are in place.

Requests for your Personal Data must be made to us (see ‘How To Contact Us’ above) specifying what Personal Data you need access to, and a copy of such request may be kept by us for our legitimate purposes in managing the Service. To help us find the information easily, please give us as much information as possible about the type of information you would like to see. If, to comply with your request, we would have to disclose information relating to or identifying another person, we may need to obtain the consent of that person, if possible. If we cannot obtain consent, we may need to withhold that information or edit the data to remove the identity of that person, if possible.
There are certain types of data which we are not obliged to disclose to you, which include Personal Data which records our intentions in relation to any negotiations with you where disclosure would be likely to prejudice those negotiations. We are also entitled to refuse a data access request from you where (i) such request is manifestly unfounded or excessive, in particular because of its repetitive character (in this case, if we decide to provide you with the Personal Date requested, we may charge you a reasonable fee to account for administrative costs of doing so), or (ii) we are entitled to do so pursuant to Data Protection Legislation.
Right to update your Personal Data or correct any mistakes in your Personal Data
You can require us to correct any mistakes in your Personal Data which we hold free of charge. If you would like to do this, please:

email or write to us (see ‘How can you contact us’ above);
let us have enough information to identify you (e.g. name, registration details); and
let us know the information that is incorrect and what it should be replaced with.

If we are required to update your Personal Data, we will inform recipients to whom that Personal Data have been disclosed (if any), unless this proves impossible or has a disproportionate effort.
It is your responsibility that all of the Personal Data provided to us is accurate and complete. If any information you have given us changes, please let us know as soon as possible (see ‘How To Contact Us’ above).
Right to ask us to stop contacting you with direct marketing
We have a legitimate interest to send you electronic communications/direct marketing in connection with the Service and related matters (which may include but shall not be limited to newsletters, announcement of new features etc.). We may also ask you different questions for different services, including competitions. We may also ask you to complete surveys that we use for research purposes, although you do not have to respond to them.
You can ask us to stop contacting you for direct marketing purposes. If you would like to do this, please:

email or write to us (see ‘How can you contact us’ above). You can also click on the ‘unsubscribe’ button at the bottom of the electronic communication. It may take up to 15 days for this to take place; and
let us know what method of contact you are not happy with if you are unhappy with certain ways of contacting you only (for example, you may be happy for us to contact you by email but not by telephone).

We will provide you with information on action taken on a request to stop direct marketing – this may be in the form of a response email confirming that you have ‘unsubscribed’.
Rights in relation to automated decision taking (if applicable)
You may ask us to ensure that, if we are evaluating you, we don’t base any decisions solely on an automated process and have any decision reviewed by a member of staff. Profiling may occur in relation to your Personal Data for the purposes of targeted advertising and de-targeting you from specified advertising. This allows us to tailor our advertising to the appropriate customers and helps to minimise the risk of you receiving unwanted advertising. These rights will not apply in all circumstances, for example where the decision is (i) authorised or required by law, (ii) necessary for the performance of a contract between you and us, or (ii) is based on your explicit consent. In all cases, we will endeavour that steps have been taken to safeguard your interests.
Right to restrict or prevent processing of Personal Data
In accordance with Data Processing Legislation, you may request that we stop processing your Personal Data temporarily if:

you do not think that your Personal Data is accurate (but we will start processing again once we have checked and confirmed that it is accurate);
the processing is unlawful but you do not want us to erase your Personal Data;
we no longer need the Personal Data for our processing, but you need the Personal Data to establish, exercise or defend legal claims; or
you have objected to processing because you believe that your interests should override the basis upon which we process your Personal Data.

If you exercise your right to restrict us from processing your Personal Data, we will continue to process the Personal Data if:

you consent to such processing;
the processing is necessary for the exercise or defence of legal claims;
the processing is necessary for the protection of the rights of other individuals or legal persons; or
the processing is necessary for public interest reasons.

Right to data portability
In accordance with Data Protection Legislation, you may ask for an electronic copy of your Personal Data that you have provided to us and which we hold electronically, or for us to provide this directly to another party. This right only applies to Personal Data that you have provided to us – it does not extend to data generated by us. In addition, the right to data portability also only applies where:

the processing is based on your consent or for the performance of a contract; and
the processing is carried out by automated means.

Right to erasure
In accordance with Data Protection Legislation, you can ask us (please see ‘How To Contact Us’ above) to erase your Personal Data where:

you do not believe that we need your Personal Data in order to process it for the purposes set out in this Privacy Policy;
if you had given us consent to process your Personal Data, you withdraw that consent and we cannot otherwise legally process your Personal Data;
you object to our processing and we do not have any legal basis for continuing to process your Personal Data;
your Personal Data has been processed unlawfully or have not been erased when it should have been; or
the Personal Data have to be erased to comply with law.

We may continue to process your Personal Data in certain circumstances in accordance with Data Protection Legislation (i.e. where we have a legal justification to continue to hold such Personal Data). Where you have requested the erasure of your Personal Data, we will inform recipients to whom that Personal Data have been disclosed, unless this proves impossible or involves disproportionate effort. We will also inform you about those recipients if you request it.
Right to complain to the DPC
If you do not think that we have processed your Personal Data in accordance with this Privacy Policy, please contact us in the first instance. If you are not satisfied, you can complain to the DPC or exercise any of your other rights pursuant to Data Protection Legislation. Information about how to do this is available on the DPC website at https://www.dataprotection.ie

