

As the operators of the Workable website, Workable Software Limited ("We", "Us") is committed to protecting and respecting your privacy. This Privacy Policy ("Policy") sets out the basis on which the personal data collected from you, or that you provide to us will be processed by us. Please read the following carefully to understand our views and practices regarding your personal data and how we will treat it.


For the purpose of the Data Protection Act 1998, the Data Controller is Workable Software Limited, registered in England and Wales with Company Registration Number 08125469 and having its address at 235 Old Marylebone Road, London, United Kingdom, NW1 5QT.


Information we may collect from you


We may collect and process the following information from you:



Information that you provide by filling in forms on our site www.workable.com (Website). This includes information provided at the time of registering to use our site, subscribing to our service, posting material or requesting further information or services. We may also ask you for information when you report a problem with our site.


Specifically if you are a Candidate (as defined in our Candidate Terms and Conditions), information relating to your employment history, skills and experience that you upload to the Website.


Specifically if you are a Customer organisation (i.e. an Employer organisation), information regarding job Openings and other details regarding your organisation and its personnel.


If you contact us, we may keep a record of that correspondence.


We may also ask you to complete surveys that we use for research purposes, although you do not have to respond to them.


Details of transactions you carry out through our site and of the provision of services to you.


Details of your visits to our site including, but not limited to, traffic data, location data, weblogs and other communication data, and the resources that you access.



How we store your personal data


We take appropriate measures to ensure that any personal data are kept secure for the duration of your use of our service.


Unfortunately, the transmission of information via the internet is not completely secure. Although we will do our best to protect your personal data, we cannot guarantee the security of your data transmitted to our site; any transmission is at your own risk. Once we have received your information, we will use strict procedures and security features in order to prevent unauthorised access.


We also note the data that we collect from you may be transferred to, and stored at, a destination outside the European Economic Area ("EEA"). It may also be processed by persons operating outside the EEA who work for us or for one of our suppliers. Such persons maybe engaged in, among other things, the provision of certain services which support our Website and allow us to provide the Services to you. By submitting your personal data, you agree to this transfer, storing or processing. We will take all steps reasonably necessary to ensure that your data is treated securely and in accordance with this privacy policy.


If you have any questions about how we store and use data collected which relates to you, please contact us using the details set out below.


IP Addresses & cookies


We may collect information about your computer, including where available your IP address, operating system and browser type, for system administration and to report aggregate information to our advertisers. This is statistical data about our users' browsing actions and patterns, and does not identify any individual.


In addition, our website uses cookies. A cookie is a small file of letters and numbers that we put on your computer if you agree. These cookies allow us to distinguish you from other users of the Website, which helps us to provide you with a good experience when you browse our website and also allows us to improve the Website.


The cookies we use are "analytical" cookies. Some of the common uses for our cookies are as follows:



to recognise and count the number of visitors and to see how visitors move around the site when they are using it. This helps us to improve the way our website works, for example by ensuring that users are finding what they are looking for easily.


to understand what visitors are interested in while using the site. Our advertising partner, AdRoll, then enables us to present visitors with retargeting advertising on other sites based on their previous interaction with the site. The techniques our partners employ do not collect personal information such as your name, email address, postal address or telephone number. Visitors can visit this page to opt out of AdRoll and their partners’ targeted advertising.


to identify and authenticate a user across different pages of our Website, within our own Website, in a session or across different sessions. This is so that the user does not need to provide a password on every page the user visits; and


to be able to retrieve a user’s previously stored data, for example, information that the user previously submitted to the Website, so as to facilitate reuse of this information by the user.



Uses made of your information

We use information held about you in the following ways:


To ensure that content from our site is presented in the most effective manner for you and for your computer.


To provide you with information, products or services that you request from us or which we feel may interest you, where you have consented to be contacted for such purposes.


To carry out our obligations arising from any contracts entered into between you and us, whether you are a Customer, Candidate or otherwise, including but not limited to the terms of use upon which you access the Website.


To allow you to participate in certain features of our service, when you choose to do so.


To notify you about changes to our service.



Disclosure of your information


We may disclose your personal information to any member of our group, which means our subsidiaries, our ultimate holding company and its subsidiaries, as defined in section 1159 of the UK Companies Act 2006 (where applicable).


We may disclose your personal information to third parties:



in the event that we sell or buy any business or assets, in which case we may disclose your personal data to the prospective seller or buyer of such business or assets; or


if we or substantially all of our assets are acquired by a third party, in which case personal data held by us about our customers will be one of the transferred assets; or


specifically if you are a Candidate, to make your details available to third party Employer organisations to whom you apply for Openings for the purposes of evaluation for job vacancies, as may be submitted by you and in accordance with our Candidate Terms and Conditions of Use; or


if we are under a duty to disclose or share your personal data in order to comply with any legal obligation or in order to enforce or apply our Website Terms and Conditions and other agreements, but we will endeavour to minimise such disclosure to only that reasonably necessary and, where possible, to provide you with notice of such disclosure; or


to protect the rights, property, or safety of Workable Software Limited,  the Website, our users and any third party we interact with the to provide the website.


Your rights

The Website may, from time to time, contain links to and from the websites of our partner networks, advertisers and affiliates. If you follow a link to any of these websites, please note that these websites have their own privacy policies and terms of use and that we do not accept any responsibility or liability for these policies and terms of use. Please check these policies before you submit any personal data to these websites.

Access to information

The Data Protection Act gives you the right to access information held about you. Your right of access can be exercised in accordance with the Data Protection Act. Any access request may be subject to a fee of £10 to meet our costs in providing you with details of the information we hold about you.

Changes to our privacy policy

We reserve the right to modify this Privacy Policy at any time. Any changes we may make to our Policy in the future will be notified and made available to you using the Website. Your continued use of the services and the Website shall be deemed your acceptance of the varied Privacy Policy.

Contact

All questions, comments and requests regarding this Privacy Policy should be addressed to support@workable.com.


