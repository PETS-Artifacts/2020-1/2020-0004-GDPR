
Your Rights, Privacy and Data Protection
Data Protection
Returns
Your right of return or cancellation
We hope you'll be satisfied with all our products but in the unlikely event you're not, you can cancel an order or return any item as explained here.
You can cancel your order or return an item* for any reason from the time you place the order up to and including the 14th day after its delivery day. If you wish to cancel your order, you'll need to tell us within this 14-day period. You may use our cancellation form if you wish. You should take care of the item as we may not be able to give you a full refund if your actions reduce its value. Please return it to us as soon as possible after giving notice of cancellation (and in any event within the next 14 days). If you return all the items in an order, we'll refund the cost of the returned items plus the delivery charge (excluding any additional charges for Nominated or Next Day delivery). However, if the return is made via our courier or through a myHermes ParcelShop, we'll charge you a return fee and deduct it from your refund. The return fee will not exceed the original delivery charge. Returning the item by any other means is entirely at your cost.
If, at our sole discretion, we decide to accept a return received outside of this returns period, we reserve the right to make a charge to cover our costs in processing the 
  		late return - currently this charge is £3.00 per item.
*PLEASE NOTE - Orders for the following items cannot be cancelled or returned unless faulty; bespoke and customised or personalised items, items which have been sealed for 
  		 health or hygiene reasons if unsealed, and sealed audio, video and software recordings if unsealed.
Faulty Items
If anything you order from us fails prematurely because of defective workmanship or materials we'll happily offer you a repair, 
        replacement or full or partial refund. Please contact us to arrange for the return of defective items.
Price policy
We describe every item in our catalogue as accurately as we can. However, occasionally product description or pricing errors occur. If after you have submitted your order we discover such an error or the item ordered becomes unavailable we'll do our best to inform you at the earliest opportunity. You may then choose whether to proceed with your order or to cancel it. If we cannot contact you, we may treat your order as cancelled. Subject to this, we intend that prices remain valid for the life of this catalogue. All orders are subject to our acceptance. We accept your order and form the contract between you and us only when the goods are despatched to you. Finally, all our products are subject to availability - we cannot supply what we do not have.
Protecting your interests and our complaints procedure
We aim to get it right first time, but if we make a mistake we'll try to put it right promptly. If you have a question or complaint, please call our Enquiry Line and one of our advisors will be happy to help you; we'll do our utmost to work with you to reach a satisfactory outcome over the phone. If we can't, or we need to investigate further, we'll confirm receipt of your complaint within 5 working days and, in most cases, resolve the problem within 4 weeks.
Complex complaints can take a little longer. In this case, we'll always let you know when to expect an answer. If, together, you and we have not reached an agreement within 8 weeks, we'll give you information about the Financial Ombudsman Service.
You can contact us at any time while your complaint is being investigated, by writing to Service Resolution Manager, Home Shopping Direct, Customer Contact Centre, 40 Lever Street, Manchester, M1 1BB. Don't forget to include your Customer Number so we can find your records quickly.
We subscribe to the Mailing Preference Service and the Telephone Preference Service.
For staff training and for quality control purposes, your calls may be monitored or recorded.
YOU MAY PRINT OUT AND KEEP A PERMANENT COPY OF THESE TERMS AND CONDITIONS FOR YOUR REFERENCE BY CLICKING HERE

Ways to Pay
Deliveries & Returns
Your Rights, Privacy & Data Protection
Your Personal Account
The Environment & Other Useful Information
Cover Plan Information
Account Protection Plans
FCA Complaints Reporting and Insurance Claims Ratio


