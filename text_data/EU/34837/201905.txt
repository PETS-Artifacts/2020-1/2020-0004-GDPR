

VALAMAR RIVIERA d.d., based in Poreč, Stancija Kaligari 1, OIB: 36201212847 (hereinafter: VALAMAR RIVIERA), respects the privacy of each person whose personal information is collected. We would like to inform you which personal information VALAMAR RIVIERA collects, how we protect this information and what are your rights.
  
VALAMAR RIVIERA holds all copyrights on the use of photographs, texts and other published materials in the sense of positive legal regulations of the Republic of Croatia. Photographs, texts and other materials may not be published, sold, publicly or privately advertised or used in any other way without the consent of VALAMAR RIVIERA. Any failure to abide by the above terms implies the liability and obligation to compensate VALAMAR RIVIERA non-material damages caused by the breach of particular material.
  


Basic information


Scope of application


This Policy applies to any processing of personal data by VALAMAR RIVIERA, unless other VALAMAR RIVIERA policy prescribes otherwise. Exceptionally, with regard to the processing of data of guests and users of VALAMAR RIVIERA services, this Policy prevails over all other policies when such other policies prescribe rights and obligations regarding the processing of data in a different manner.
  


Data controller and legal framework


VALAMAR RIVIERA, as the Controller of your data, respects your privacy and undertakes to protect your personal data. The collection and storage of data is carried out in accordance with the provisions of the Regulation (EU) 2016/679 of the European Parliament and of the Council of 27 April 2016 on the protection of natural persons with regard to the processing of personal data and on the free movement of such data (General Data Protection Regulation), the Act on Implementation of the General Data Protection Regulation (Official Gazette, No. 42/2018) and other regulations governing this area that are applicable in Republic of Croatia.
  


Data protection officer


VALAMAR RIVIERA has appointed a personal data protection officer, who can be contacted, at any time, at the e-mail address  dpo@valamar.com  or by regular mail addressed to Valamar Riviera d.d., Stancija Kaligari 1, Poreč, Republic of Croatia.
  


Implementation of data-protection principles


VALAMAR RIVIERA, within the framework of the implementation of this Policy, pays special attention to respecting the principles of data processing and processes data:
  

Lawfully – data processing is made possible when it is permitted by law and only within the limits permitted by law.
Fairly – by taking into account the specifics of each relationship, applying all appropriate measures for the protection of personal data and privacy in general, and allowing data subjects to exercise their rights.
In a transparent manner – by informing data subjects about the processing of personal data. From the beginning of data collection itself, when data subjects are informed about all aspects of data processing, until the end of data processing, data subjects are in accordance with the provisions of the Regulation granted a simple and quick access to their own data, including the ability to inspect and obtain a copy thereof. The access to certain information may be restricted only when this is required by law or necessary for the protection of third parties.
By ensuring purpose limitation – personal data is processed for the purposes for which they are collected and may be processed for other purposes only when the requirements laid down in the Regulation are fulfilled. Data may be processed for duplicate purposes only by taking into account (a) any link between the purposes for which the personal data have been collected and the purposes of the intended further processing; (b) the context in which the personal data have been collected, in particular regarding the relationship between data subjects and Valamar;
(c) the nature of the personal data, in particular whether special categories of personal data are processed, pursuant to Article 9 of the Regulation, or whether personal data related to criminal convictions and offences are processed, pursuant to Article 10 of the Regulation; (d) the possible consequences of the intended further processing for data subjects; and (e) the existence of appropriate safeguards.
By ensuring storage limitation – data must be stored in a form which permits identification of data subjects for no longer than is necessary for the purposes for which the personal data are processed and may be stored longer only when this is allowed by the Regulation.
By ensuring data minimisation – data is processed only when they are adequate, relevant and limited to what is necessary. A special attention is given not to collect data for which there is no justified need for processing.
By ensuring accountability – data must be accurate and kept up to date, and every reasonable step must be taken to erase inaccurate data.
With integrity and confidentiality – processed in a manner that ensures appropriate security of the personal data, including protection against unauthorised or unlawful processing and against accidental loss, destruction or damage, using appropriate technical or organisational measures. Relevant measures are applied taking into account the risk related to each type of data processing.



Transfer of data to third parties


The access to personal data of guests is also granted to the company Imperial d.d. seated in Rab, Jurja Barakovića 2, Company Identification Number (OIB): 90896496260, whose tourism-based activities are managed by VALAMAR RIVIERA under an entrepreneurial contract, whereby these two companies joined their sales and marketing functions. This means that you, as a guest of VALAMAR RIVIERA, may under certain circumstances form VALAMAR RIVIERA also receive offers containing information about other hotels and facilities managed by VALAMAR RIVIERA.
  
The access to the personal data of guests, where necessary and to a limited extent, may also be granted to third party processors (for example, associates of VALAMAR RIVIERA that provide IT or other services), who store such data in their databases until due processing of such data is completed. We will conclude a detailed contract with such parties regarding their powers and obligations during the processing of personal data, in accordance with the requirements of the Regulation.
  
Under certain circumstances, external parties and VALAMAR RIVIERA may jointly determine the purpose and manner of personal data processing. In that event, such external partners and Valamar will be considered joint data controllers. Joint data controllers, in their mutual relationship, determine their own responsibilities for acting in compliance with obligations prescribed by the Regulation in a transparent manner, especially with regard to the exercise of rights held by data subjects and their duty to process data in a transparent manner, unless their responsibilities are already established by law.
  
Should, within the data processing, data be transferred to third countries, VALAMAR RIVIERA will ensure the compliance with high standards of protection to in order to comply with the highest possible standard of personal data protection in accordance with the strict requirements of the Regulation. In this respect, VALAMAR RIVIERA will for all international transfers of personal data inform the data subject of its intent to transfer personal data to a third country or international organisation and the existence or absence of an adequacy decision by the European Commission. At the same time, the data subject will be referred to the to the appropriate or suitable safeguards and the means by which to obtain a copy of them or where they have been made available, as well as whether due transfer is subject to safeguards in accordance with Article 46 of the Regulation, the application of binding corporate rules in accordance with Article 47 of the Regulation or whether the Article 49 paragraph 1 sub-paragraph 2 of the Regulation applies. Any transfer of personal data to third countries will be carried out in accordance with Chapter V of the Regulation.
  


Purpose of data collection


VALAMAR RIVIERA is required to collect certain personal data in order to execute due accommodation contract and comply with regulations governing hospitality. However, VALAMAR RIVIERA may collect other or the same data for other purposes, primarily maintaining contact. Such purposes include:
  

Execution of accommodation contracts;
Compliance with legal requirements and other applicable positive regulations governing hospitality;
Direct marketing;
Submission of bids;
Organisation of promotional prize draws;
Improvement and personalisation of services offered to guests;
Protection of property and safety of individuals achieved through implementation of video surveillance measures.

VALAMAR RIVIERA guarantees that collected data will be used only for the stated purposes.
  
VALAMAR RIVIERA may use depersonalised data for statistical purposes.
  


Legal basis for collection


The legal basis for the stated collection purposes may be:
  

Legal;
Contractual;
Key interests of data subjects
Legitimate interest overridden by interests of data subjects; or
Consent or explicit consent of data subject, depending on the purpose of processing and the type of personal data.



Points of data collection


VALAMAR RIVIERA collects your data at:
  

Booking of accommodation (booking through website or booking by phone call to our call centre);
Conclusion of accommodation contract – registration at the reception desk, filling in the registration card;
Subscription to our newsletter on the VALAMAR RIVIERA website, including a free Wi-Fi login on the VALAMAR RIVIERA website;
Application for loyalty programme membership;
Completion of survey form for participation in survey prize draw;
Places under video surveillance.



Data storage period


Data that was lawfully collected by VALAMAR RIVIERA is stored for a period of time prescribed by a particular law or other positive regulation.
  
Data that was contractually collected by VALAMAR RIVIERA is stored only for a period of time necessary to fulfil the contract or provide a service.
  
Information about the name, surname and e-mail address collected by VALAMAR RIVIERA on the basis of legitimate interest for direct marketing purposes is stored in its guest database for a period of 10 years.
  
Other information collected by VALAMAR RIVIERA on the basis of guest's explicit consent (mobile phone number, number of children, marital status, pets, interests, manner of travel, accommodation and destination preferences) is stored in its guest database for a period of 5 years.
  
VALAMAR RIVIERA may, based on your explicit consent, also collect your web browsing data (so-called cookies) and store them in its database for a period of 2 years. VALAMAR RIVIERA uses such data to inform you about special and personalised offers, news and events organised through online channels (e-mail, web, internet promotions).
  


Rights of the data subject


Regardless of the basis for data collection, you can, at any time, free of charge, request:
  

Access to data, rectification of data or completion of incomplete data in all databases of personal data, upon which VALAMAR RIVIERA will grant you the access or rectify your data, depending on your request, in all its databases;
Erasure ("right to be forgotten") of personal data from all databases of personal data, upon which VALAMAR RIVIERA will erase such data from all its databases except those that VALAMAR RIVIERA is required to maintain and keep on the basis of positive regulations and when there are no overriding legitimate reasons for processing or when processing is not necessary for the establishment, exercise or defence of legal claims;
Restriction of processing of your data or lodge a complaint regarding the processing of such data;
Transfer of data we have collected about you to you yourself or to third parties ("right to data portability"), in accordance with positive legal regulations;
Withdrawal of consent, when data were given on the basis of consent, without any negative consequences;
To lodge a complaint with a supervisory authority – Personal Data Protection Agency (for more information, please visit www.azop.hr).

Please send your written request to the contact e-mail address of personal data protection officer at  dpo@valamar.com  or by regular mail addressed to Valamar Riviera d.d., Stancija Kaligari 1, Poreč, Republic of Croatia.
  


Personal data collected from persons who booked accommodation and guests


VALAMAR RIVIERA, as the data controller, keeps personal data you are required to submit in order to be provided with accommodation services in its database solely for the purpose of concluding the accommodation contract and complying with legal requirements on provision and collection of personal data governing hospitality, and may use such data for other purposes allowed by positive regulations. In the event that you do not provide VALAMAR RIVIERA with minimum information required for the registration of guests in all relevant registers, VALAMAR RIVIERA will not be able to provide you with accommodation services in accordance with the contract and the law.
  
Personal data recorded by VALAMAR RIVIERA at the time of booking and filling in the registration card at arrival to the facility are collected on the basis of laws regulating hospitality and for the purpose of providing services to guests. These include the following data (subject to change with regard to positive regulations):
  

Name and surname;
Address of residence (Croatian citizens);
Date of birth;
Number, type and place of issue of identification document;
Nationality;
Name of facility;
Number of accommodation unit;
Date of arrival and departure of guest;
Sex.

VALAMAR RIVIERA stores such data in its database of guests and shares them with competent authorities of the Republic of Croatia through the E-visitor system (electronic registration system) through the E-visitor system (electronic registration system) in which such data is required to be stored for 10 years. VALAMAR RIVIERA is also required to store all invoices issued to guests, including their personal information, for 11 years, in accordance with legal regulations.
  
Furthermore, in order to fulfil its contractual obligations, at the time of booking and filling in the registration card at arrival to the facility VALAMAR RIVIERA collects the following information:
  

 E-mail address;
Language;
Telephone number.

Other information related to circumstances of your stay, such as the manner of travel, travel companions, marital status, number of children, pets and other interests, will also be collected when they are directly connected to the provision of accommodation services, but will be deleted after your departure from the accommodation facility.
  
VALAMAR RIVIERA, as the data controller, based on a legitimate interest has the right to store your personal information (name and surname, e-mail address) in its database of guests and use such information for the purpose of direct marketing done solely for the purpose of informing you about VALAMAR RIVIERA offers and news by e- mail. Under these circumstances, you will have the right to request erasure (right to be forgotten) from the database for that purpose at any time and free of charge.
  
During and after your stay, VALAMAR RIVIERA will e-mail you, as our guest, a satisfaction questionnaire which, should you wish to fulfil it, namely, solely with your consent, will be processed by the company (TrustYou), which publicly publishes the data from the questionnaire according to its rules and policies for which VALAMAR RIVIERA is not responsible. The primary purpose of such satisfaction questionnaire is to collect data on services for the purpose of their improvement by VALAMAR RIVIERA, whereby VALAMAR RIVIERA depersonalises such data provided in questionnaires and processes them for statistical purposes.
  
In addition, a person who books accommodation, that is, a guest, can give VALAMAR RIVIERA a special permission, namely, consent, that all his or her information, such as:
  

Name;
Surname;
E-mail address;
Date of birth;
Country;
Other personal information collected during stay (for example, mobile phone number, telephone number, sex, number of children, marital status, language, pets, interests and activities enjoyed during stay, manner of travel, accommodation preferences, destination preferences, etc.);
Other personal data collected during web browsing (so-called cookies), including the IP address;

may be collected and used for further profiling of the data subject and for the purpose of maintaining contact and providing information about special and personalised offers, news and events organised by VALAMAR RIVIERA and/or Imperial through online channels (e-mail, web, internet promotions). In this event, the data subject has the right to at any time and free of charge withdraw his or her consent, including the consent for data processing for the purposes of creating a profile to the extent to which such processing is connected with direct marketing, whether in relation to initial or further processing, as well as the right to rectification of data and the right to be forgotten.
  
Data is stored in the VALAMAR RIVIERA database of guests for 5 years.
  
VALAMAR RIVIERA also collects personal data through the video surveillance system.
  


Personal information collected by subscribing to our newsletter, including a free Wi-Fi login on the Valamar website


When a data subject subscribes to our newsletter, he or she will be given the option to provide the following additional information with his or her consent (in addition to his or her name, surname and e-mail address that VALAMAR RIVIERA collects on the basis of legitimate interest):
  

Sex
Date of birth;
Country;
Language.

While renewing his or her subscription to our newsletter, the data subject will be will be given the option to provide the following additional information with his or her consent:
  

Street, house number, zip code, city;
Telephone number;
Mobile phone number;
Interests;
Manner of travel;
Pets;
Preferred accommodation;
Preference of destination.

Collected data are collected on the basis of an explicit consent for the purpose of providing information about special and personalised offers, news and events organised by VALAMAR RIVIERA and/or Imperial through online channels (e-mail, web, internet promotions).
  
Data is stored in the VALAMAR database of guests for 5 years.
  
Data subject has the right to rectification of data and the right to be forgotten.
  


Personal data collected at application for loyalty programme membership


By joining a loyalty programme, guests can collect points during their stay at VALAMAR RIVIERA, in the manner specified in the loyalty membership general terms and conditions. All terms of joining the loyalty programme, obligations of VALAMAR RIVIERA and rights of data subjects are specified in the in the loyalty membership general terms and conditions available at www.valamar.com/loyalty. A guest can become a loyalty programme member solely upon his or her own request, whereas the guest based on such membership enjoys various privileges and can at any time leave our loyalty programme.
  
Collected data include
  

Name;
Surname;
Sex;
Date of birth;
E-mail address;
Mobile phone number;
Telephone number;
Address (street, house number, zip code, city and country).

VALAMAR RIVIERA, based on a legitimate interest, has the right to collect and use all information about the e-mail address, name and surname of all loyalty programme members for the purpose of direct marketing done solely for the purpose of informing you about VALAMAR RIVIERA offers and news by e- mail. In this event, the data subject has the right to at any time and free of charge lodge a complaint to such processing, to the extent to which such processing is connected with direct marketing, whether in relation to initial or further processing.
  
In addition, a person applying for membership may give VALAMAR RIVIERA a special permission, namely, consent, that all his or her information, including:
  

Other personal information collected during stay (for example, mobile phone number, telephone number, sex, number of children, marital status, language, pets, interests and activities enjoyed during stay, manner of travel, accommodation preferences, destination preferences, etc.);

may be collected and used for further profiling of the data subject and for the purpose of maintaining contact and providing information about special and personalised offers, news and events organised by VALAMAR RIVIERA and/or Imperial through online channels (e-mail, web, internet promotions). In this event, the data subject has the right to at any time and free of charge withdraw his or her consent, including the consent for data processing for the purposes of creating a profile to the extent to which such processing is connected with direct marketing, whether in relation to initial or further processing, as well as the right to rectification of data and the right to be forgotten.
  
The above information is stored in the VALAMAR RIVIERA database of guests for 5 years.
  


Personal data collected at participation in prize draw / completion of survey form


VALAMAR RIVIERA may periodically organize prize draws, in which event it will collect your personal information only should you choose to participate in such prize draw. The information collected in such manner which is necessary to participate in prize draw will be specified in the Prize Draw Rules for each prize draw and may vary depending on the prize draw. The information collected in such manner will represent a type of contractual obligation and be used for the purpose of administering a prize draw in accordance with prize draw rules and will be deleted within 5 years after such prize draw finishes.
  
In the event that you complete a survey form for participation in survey prize draw by providing information which is not necessary for participation in the prize draw (such information may vary depending on the prize draw) VALAMAR RIVIERA will, nonetheless, be able to use all such depersonalised data for statistical purposes.
  
VALAMAR RIVIERA, based on a legitimate interest, has the right to collect and use all information about the name, surname and e-mail address that you provide in a survey form for participation in survey prize draw for the purpose of direct marketing done solely for the purpose of informing you about VALAMAR RIVIERA and/or Imperial offers and news by e- mail. In this event, the data subject has the right to at any time and free of charge lodge a complaint to such processing, to the extent to which such processing is connected with direct marketing, whether in relation to initial or further processing.
  
The above information is stored in the VALAMAR RIVIERA database of guests for 10 years.
  
Data subject has the right to rectification of data and the right to be forgotten.
  
In addition, the person who participates in the prize draw may give VALAMAR RIVIERA a special permission, namely, consent, that all his or her information provided by completing a survey form for participation in survey prize (such information may vary depending on the prize draw) may be collected and used for further profiling of the data subject and for the purpose of maintaining contact and providing information about special and personalised offers, news and events organised by VALAMAR RIVIERA and/or Imperial through online channels (e-mail, web, internet promotions). In this event, the data subject has the right to at any time and free of charge withdraw his or her consent, including the consent for data processing for the purposes of creating a profile to the extent to which such processing is connected with direct marketing, whether in relation to initial or further processing, as well as the right to rectification of data and the right to be forgotten.
  
Data is stored in the VALAMAR RIVIERA database of guests for 5 years.
  


Cookies and online technologies


As with many other sites, our site may use "cookies" (small files that we save to your computer when you access our websites to enable basic or additional functionality of each site) or other technologies to help us deliver content specific to your interests, to process your reservations or requests, and/or to analyse your visiting patterns. Cookies, by themselves, cannot be used to reveal your personal identity. When you access our websites, such data is used by our server to identify features of your browser but cannot identify you personally.
  
We use several types of cookies:
  

Necessary cookies – they are necessary to enable the basic website functionalities and websites cannot function properly without them. This means that a website cannot be opened or displayed without these cookies. These cookies are used for the purposes of communication transmission and are necessary for the provision of IT company services that are explicitly requested by users of such services. These cookies also enable us to conduct a basic website analysis with the aim of improving website functionalities by using completely anonymous data, that is, such cookies are not based on your personal information or data that could be in any way linked with you personally. These cookies do not require consent, so we do not ask for your consent to use them.
Functional cookies – we use them to perform a more advanced website analysis. These cookies are used to analyse user behaviours and based on received anonymous data help us to conclude what website visitors prefer to see and browse, thus enabling us to customise the website and make its content and functionalities easier to use. We will ask for your consent to use these cookies.
Advertising cookies – we use them to analyse your interests and preferences and they help us to provide you with information you are interested in and create customised offers based on your use of VALAMAR RIVIERA website. We will ask for your consent to use these cookies.

VALAMAR RIVIERA stores cookies in the database and keeps them for up to 2 years for the purpose of providing information about special and personalised offers, news and events through online channels (e-mail, web, internet promotions).
  
Should you wish to change your cookie settings on the Valamar website, you can do so at any time by clicking on this link: (link to cookie consent box). You can always delete cookies stored on your computer, thereby disabling further processing of your personal data through such technology. Each web browser uses its own procedure for clearing cookies. Here you can find such procedures for the most popular web browsers:
  
Google Chrome: https://support.google.com/chrome/answer/95647?co=GENIE.Platform%3DDesktop&hl=hr 
Mozilla Firefox: https://support.mozilla.org/hr/kb/Brisanje%20kola%C4%8Di%C4%87a
Microsoft Edge: https://privacy.microsoft.com/en-us/windows-10-microsoft-edge-and-privacy


Video surveillance system


VALAMAR RIVIERA, as the data controller, has the legitimate interest to implement video surveillance measures to protect property and persons in relation to certain workplace positions and statutory duty to install surveillance cameras that record employees and anyone moving within the surveillance camera field of view.
  
VALAMAR RIVIERA indicates all places where video surveillance system is installed in the prescribed manner.
  
VALAMAR RIVIERA is aware that the video recordings contain personal data of all the persons moving within the surveillance camera field of view, and therefore handles them with special care. Furthermore, we have implemented a security system and introduced availability and erasure policy regulated by internal VALAMAR RIVIERA rules on safety.
  
Video recordings are regularly rewritten and thus automatically deleted after a maximum of 15 days after they are recorded. Exceptionally, video recordings are kept longer when they serve as evidence in proceedings before competent state authorities. Extracted video recordings are stored in a centralised messaging system with extremely limited access.
  
In the event of judicial and/or criminal proceedings, VALAMAR RIVIERA may use such video recordings. Access to personal data captured on video recordings may be granted to third parties, data processors and contractual partners of VALAMAR RIVIERA who are registered and qualified to provide services of personal and property protection and who do not use any of these data independently but participate in activities related to the security of central supervisory and alarm systems. All other details regarding video surveillance are subject to special regulations that govern that area.
  


Protecting the personal information of children


VALAMAR RIVIERA advises parents and guardians to teach their children the importance of being responsible when dealing with personal information on the internet. VALAMAR RIVIERA does not wish to collect and has no intention of collecting personal information of children. Personal information of children will be neither used nor divulged to third parties. A child may give his or her consent solely in relation to the provision of IT company services, whereby such child must be older than 16 years of age. VALAMAR RIVIERA may process all other information of children below the stated age limit and children under 18 years of age, except as expressly stated herein, only with the prior consent of the parent.
  


Rectification of data


You can contact us at any time to review your personal information, as well as for the purpose of updating, rectification or erasure of your data. Until such time, we will use your previously recorded data for the aforementioned purposes.
  


Your consent


When you in any way provide VALAMAR RIVIERA with your information (booking, registration card ...), you guarantee that the information you have provided is accurate, that you are legally capable and authorised to dispose of the provided information and that you fully agree that VALAMAR RIVIERA may use and collect your information in accordance with the law and the terms of this privacy policy.
  


Technical and integrated data protection


VALAMAR RIVIERA, as the data controller, takes utmost care to meet the highest organisational and technical data protection standards. We, therefore, taking into account state of the art developments, the cost of implementation and the nature, scope, context and purpose of processing, as well as the risks arising from data processing of various levels of probability and severity that may affect the rights or freedoms of natural persons, at the time of choosing the processing resources and at the time of the processing itself, take appropriate technical and organisational measures to enable the effective application of data protection principles.
  
Furthermore, VALAMAR RIVIERA takes the appropriate technical and organisational measures to ensure that only personal data necessary for each special purpose of processing are processed in an integrated way. VALAMAR RIVIERA imposes this measure to the amount of collected personal data, scope of their processing, storage period and their availability. Specifically, such measures ensure that personal data are not automatically, without personal intervention, made available to an unlimited number of persons.
  


Records of processing activities


VALAMAR RIVIERA, as the data controller, keeps records on processing activities involving the following data:
  

Name and contact details of data controller or, where applicable, joint data controller, and data protection officer;
Processing purposes;
Description of data subject categories and personal data categories;
Categories of recipients to whom personal data are or will be disclosed, including recipients in third countries or international organisations;
Where applicable, the transfer of personal data to a third country or an international organisation, including the identification of such third country or international organisation and, in the event of transfer referred to in Article 49, paragraph 1, sub-paragraph 2, documentation on appropriate safeguards;
When possible, planed deadlines for erasure of different categories of data;
When possible, a general description of technical and organisational safeguards.



Personal data breach


VALAMAR RIVIERA, as the data controller, ensures that in the event of personal data breach, the competent supervisory authority is notified of personal data breach without further delays and, if possible, at least 72 hours after such breach has occurred, unless it is not likely that such personal data breach will pose a risk to rights and freedoms of natural persons.
  
The report submitted to the supervisory authority must contain all information prescribed by the Regulation.
  
In the event of personal data breach that is likely to pose high risk to rights and freedoms of natural persons, VALAMAR RIVIERA, as the data controller, will notify the data subject of such personal data breach without further delays. Data subjects will not be notified where the Regulation stipulates that such notification is not mandatory.
  


Data protection impact assessment


Where a type of processing in particular using new technologies, and taking into account the nature, scope, context and purposes of the processing, is likely to result in a high risk to the rights and freedoms of data subjects, Valamar will, as the data controller, prior to the processing, carry out an assessment of the impact of the envisaged processing operations on the protection of personal data.
  
A single assessment may address a set of similar processing operations that present similar high risks.
  
VALAMAR RIVIERA performs a data protection impact assessment in the event of:
  

Systematic and extensive evaluation of personal aspects relating to natural persons which is based on automated processing, including profiling, and on which decisions are based that produce legal effects concerning the natural person or similarly significantly affect the natural person;
Processing on a large scale of special categories of data referred to in Article 9, paragraph 1, or of personal data relating to criminal convictions and offences referred to in Article 10 of the Regulation;
Systematic monitoring of a publicly accessible area on a large scale;
Any other situation defined by the competent supervisory authority.

VALAMAR RIVIERA ensures an adequate involvement of data protection officers in the performance of impact assessment.
  
In accordance with the provisions of the Regulation and, when necessary, after the performance impact assessment, we will consult the supervisory authority prior to processing.
  


Transparency


This Privacy Policy is available at www.valamar.com and at reception desks of all our facilities.
  
Should we decide to make changes to our privacy policy, we will make available and publish such changes on the website www.valamar.com and at reception desks of all our facilities.
  


