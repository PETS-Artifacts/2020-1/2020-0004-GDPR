


























Privacy policy | Gamehag









        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};
        if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
        n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t,s)}(window, document,'script',
        'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '1662774657323371');
        fbq('track', 'PageView');
    


    {
        "@context": "http://schema.org",
        "@type": "Organization",
        "name": "Gamehag",
        "legalName": "Gamehag",
        "url": "https://gamehag.com",
        "description": "Play games and earn attractive rewards, invite your friends to play with, and explore the enchanted world of witches!",
        "logo": "https://gamehag.com/images/mail-logo.png",
          "contactPoint": [
            {
                "@type": "ContactPoint",
                "email": "support@gamehag.com",
                "areaServed": "PL",
                "url":"https://gamehag.com",
                "contactType": "technical support"
            }
        ],
            "sameAs" : [
                "https://www.instagram.com/gamehagofficial/",
                "https://twitter.com/GamehagOfficial",
                "https://www.facebook.com/gamehag.polska/"
            ]
    }







            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-80652339-1', 'auto');
            ga('set', 'language', 'en');
                        ga('send', 'pageview');
        









Get more






Games





The most of



Contracts





Minigames





More







Collect the rewards






Rewards





Contests





Giveaway





Chests







Community






Articles





Forums





Rankings





About us





FAQ







How it works






About us





FAQ





















Games



Rewards



Chests



Articles



Profiles



Groups



Threads




No matching results



Games




<% game.name %>




No matching results



Rewards




<% reward.name %>

<% reward.subname %>





No matching results



Chests




<% case.name %>
 



No matching results



Articles




<% news.name %>




No matching results



Users




<% user.name%>




No matching results



Forum groups




<% forum.name %>




No matching results



Forum threads




 <% topic.name %>




No matching results












Sign in





Your username

This field is required.


Password Forgot your password?

This field is required.



Log in


or















Sign up













or use your e-mail address



Email Address

Invalid e-mail address!


Username

The username is too short.
The username is too long.
This field is required.


Password

The password is too short.
This field is required.



Sign up






The use of this website, including the creation of an User Account constitutes acceptance of the
Terms of Service and the
Privacy policy, along with the
consent to the processing of my personal data and the
consent to receive commercial information.







Sign in



Sign up







Get more




Games



Contracts



Minigames






Collect the rewards




Rewards



Contests



Chests



Giveaway






Community




Articles



Forums



Rankings






How it works




About us



FAQ



 












<% vm.confirmText %>











 

 Privacy policy 


Privacy Policy





§ 1 Object and principles of the Privacy Policy


This Privacy Policy sets out the principles of processing and protection of personal
information pertaining to Users of the www.gamehag.com Portal. It constitutes an
integral part of the Terms and Conditions of the www.gamehag.com website. 


The www.gamehag.com website respects Users’ right to privacy and makes every effort
to ensure the most comprehensive protection of the personal information entrusted to
it.


The Administrator of the personal information provided by Users is Gaming Sp. z o.o.,
ul. Romana Abrahama 18, 61-615 Poznań, NIP [VAT reg. No.]: 7831749141, REGON
[statistical ID No.]: 365698526, entered in the Register of Entrepreneurs kept by
the District Court for Poznań - Nowe Miasto and Wilda in Poznań, 8th Commercial
Division of the National Court Register under number KRS 0000643377.


The Administrator shall protect the personal information against unauthorised access,
loss, alteration, damage or destruction and shall process the information entrusted
thereto in conformity with the legal regulations in force, and in particular in
conformity with the requirements of the Personal Data Protection Act of 10 May 2018
and all related legal acts




§ 2 Type and purpose of processed personal information


Personal information is processed on the basis of a consent granted by the User upon
registration with the Portal.


Personal information referred to in § 2.1 above includes particulars enabling
identification of the User and determining his identity, i.e.:
– first name and surname,
– age,
– address of residence,
– e-mail address,
– mobile phone number.


Personal information provided by the Portal User shall be processed exclusively for
the purpose of using the Portal.


The User shall have the right of access, monitoring and verification of the processed
information pertaining thereto and the right to obtain information on the purpose,
scope and method of processing of the data contained in the files, the right to demand supplementing,
updating and correcting such data, temporary or permanent suspension of processing thereof,
removal of data, as well as to demand that the data no longer be processed pursuant current
rules of personal data protection. On the User’s request, the Administrator is obligated within 30 days
to inform in writing about the rights and processed data. The User have the right of information no
more than once every 6 months.


The User hereby warrants and represents that all personal information provided
thereby in the course of registration or while using the Portal is true and
complete. Providing true and complete personal information is a prerequisite for the
uninterrupted and correct use of the Portal.




§ 3 Account cancellation


The User may at any time cancel his account with the Website.


Cancellation of the User’s account results in the removal of any and all information
and data pertaining to the User. The Portal Administrator, however, reserves the
right to store the User’s information and to refuse to remove it to the extent and
for a period which is legally permitted, this in order to settle any obligations
that have arisen between the User and the Portal and in order to clarify the
circumstances of an unauthorised use of the Portal, i.e. its use in breach of the
Terms and Conditions and/or legal provisions in force.




§ 4 Cookies

Use of cookies

Cookies are small text files downloaded onto the User’s device while using the
website, used to identify the User or to store the history of activities undertaken
thereby.


Cookies are used to adjust and optimise the contents of the Website to the Users’
preferences, to produce statistics of website usage, to personalise marketing
content and to ensure Website security.


The User may disagree with the use of cookies on his device, among others by changing
browser settings. Refusal to agree to the use of cookies may, however, result in
incorrect functioning of the Website or its incomplete display.




§ 5 Final provisions


Users may send any questions and objections to this Privacy Policy to the following
e-mail address: [email protected]


This Privacy Policy may be amended. The Administrator shall notify Users thereof upon
a seven days’ notice by publishing it on the Portal.



GDPR - Duty to provide information













Our games


All
MMO
RPG
Strategy
Arcade
Puzzle


War
Premium
2D Games
3D Games
Browser games
Downloadable games




Quick access


Articles
Rewards
Rankings
FAQ
Magic Bank


About us
Ranks
Tutorials
Contact us
company





Our Discord channel

 


URL: 
discord.gg/W9DmGjW


Online: 
2313














COPYRIGHT © BY GAMEHAG.COM


GDPR - Duty to provide informationPrivacy policyTerms




    var experiments = {};
    var currentGeo = 'us';
    var currentLang = 'en';
    var cookiesLang = JSON.parse('{"cookies1":"By using this website you consent to the use of cookies.","rodoinformacje":"GDPR - Information","zamknij":"Close"}');
    var mainLangs = JSON.parse('{"tak":"Yes","nie":"No","rozumiem":"I understand"}');
    var domain = 'gamehag.com';
    var socketdomain = "gamehag.com:2096";
    var chatPageOpen = false;
    var currentRoute = "page.privacy";
        

    





                console.log = function() {};
                alert = function() {};
            


Discover Gamehag
in 3 steps












Play games
Choose the game that interests you and play itfor free.



Complete the tasks
Every game has some tasks for which you will receive Soul Gems.



Receive rewards
You can exchange your Soul Gems for Steam Wallet top-ups, game keys, CS:GO Skins and other rewards.


Initiate



 Hide suggestion







        $(document).ready(function() {
            console.log('bede initowac');
            initSidebarInstruction();
            handleSidebarInstruction();
        });
    





consent to the processing of my personal data





I express a voluntary and cancellable consent for the processing of my personal data by the Administrator in order to provide services within the Gamehag website. I was informed that my personal data and consent for its processing have been provided voluntarily and on my right to access and control of transmitted data, their correction and the right to object to data processing and forwarding my personal data to other entities.








consent to receive commercial information





I express a voluntary consent to receive commercial information on own and Gamehag partners services by means of electronic communication. I was given the opportunity to opt out of receiveing it or to revoke the consent at any time.








Wait a moment!




You don't want a chest?
You know that on Gamehag you can get real rewards for free, right? Join us, complete walkthrough and get your chest!


I stay!












GDPR - Information

By using this website you consent to the use of cookies.

Close



