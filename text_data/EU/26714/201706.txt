

READLY'S PRIVACY POLICY
1. GENERAL 

1.1
    Readly AB (Swedish company registration number 556921-1120) (below "Readly") cares about protecting the privacy of our customers and users. We have therefore set out this privacy policy (the "Privacy Policy") which describes how we collect, use, disclose, transfer and retain information, including your personal data, when using our website (http://www.readly.com) (the "Website") and in order to provide our web-based subscription service for newspapers, magazines and books in digital format (the "Service") which is available via the Website and our application for mobile devices (the "Application"). Personal data means all kinds of information that directly or indirectly may identify a living person (the "Personal Data").
    1.2
    This Privacy Policy incorporates by reference the Readly's Terms and Conditions of Use https://readly.com/eula, including applicable limits on damages and dispute resolution. This version of the Privacy Policy shall apply unless there is a localized version of the Policy. In such a case the local version of the Privacy Policy shall apply instead. The same terms are used below as in the Terms and Conditions of Use, unless otherwise is stated.
    1.3
    By creating a User Account and using the Service, you acknowledge and accept the terms and conditions of this Privacy Policy and also give your express consent to the processing of your Personal Data as outlined below.
    
2. COLLECTION OF INFORMATION AND PERSONAL DATA 

When Creating a User Account
2.1
              When you create a User Account, we collect information regarding your name, email address and country of origin. If you use your Facebook account via Facebook Connect to access the Service you authorize us to collect your user name and other information that may be available through your Facebook account, including your name, country, e-mail address, date of birth and gender. Readly only collects such information which you have made available to us according to your Facebook privacy settings. We may store and use this information for the purposes outlined below in section 3 below.

              Usage of the Service
2.2
              When you use the Service we automatically collect certain information, including what type of Subscription you have and how you use the Service, such as: (i) information regarding which newspapers, magazines or books you have opened and read; (ii) details of queries you make in the Service; and (iii) technical information, including e.g. the visited URLs, your IP-address, unique device ID (UID), browser type and version, operating system and which version of the Application you use. We also use cookies and similar technologies to collect this information, please see section 7 below.

            Payment Information
2.3
            If you have a signed up for a Subscription to the Service, we also collect such information necessary in order to process your payment, including name, address and credit card information. We may also retain information regarding your transaction history. In order to process payments we may use third party service providers, please see section 5.5 below. Such third party service providers may only process your Personal Data in accordance with our instructions and they are also liable to take technical and organisational measures to protect the personal data as we are committed to.

            Surveys, Contests and Similar
2.4
            If you participate in surveys, contests and similar activities which we conduct, or third parties conduct on our behalf, we may collect additional information as set out in or required for those activities. This additional information may be combined with e.g. information you have provided when you created your User Account or otherwise provided as a result of your usage of the Service and with information from public sources.
            
3. HOW WE USE THE COLLECTED INFORMATION 

3.1
              We may use the collected information, including your Personal Data, in order to give you access to the Service and the Website, in order to manage the customer relationship, to carry out customer analysis, to ensure that the Service and the Website operates properly, to develop and improve our Service and Website, and to understand how users access and use our Service and Website. Furthermore, we may use the information in order to communicate with you regarding our Service and Website. We may also use the information for direct marketing purposes and for other purposes set out in this Privacy Policy.
              3.2
              Moreover, we may process your Personal Data if it is necessary in order to: (a) comply with applicable law and regulations and lawful regulatory requests or relevant orders from competent courts; (b) protect and defend our rights, our property or our interests; or (c) protect our customers and users.
              
4. SECURITY 

              We are committed to protecting our customers' and users' information and Personal Data. Readly takes appropriate technical and organisational security measures in accordance with the provisions of the Swedish Personal Data Act (Sw: personuppgiftslagen (1998:204)) in order to protect your Personal Data against loss, theft, unauthorised access to or processing of your Personal Data as well as against accidental destruction, loss, alteration and disclosure of your Personal Data. The password you have chosen in connection with the creation of your User Account for use of the Service protects your User Account. You should therefore choose a unique and strong password, as well as log out from the Service when you are no longer using the Service.
              
5. DISCLOSURE OF INFORMATION TO THIRD PARTIES 

5.1
                Readly may disclose the information we have collected, including Personal Data, to various third parties as follows.
                Publishers
5.2
                In order to provide the Service, Readly has entered into agreements with a number of publishers. In general, Readly only discloses certain limited information to the publishers, such as statistics on an aggregated level. The publishers are data controllers for their potential processing of your Personal Data. We are not responsible for the Publishers' use of your Personal Data, including for marketing purposes.
                Sharing with Facebook and Similar Services
5.3
                If you use your Facebook credentials to connect to and access the Service, you may also share information with Facebook, including information on which newspapers, magazines or books you have read. In the Service, you may also share information with other services such as Twitter, LinkedIn and similar.

                You acknowledge that your information in such situations may be published to these services. In case you do not want us to publish information to or restrict the information that we collect from these services that are connected to your User Account you can customize the information Readly and other services have access to in the privacy settings for respective service.
                Business Partners
5.4
                We may disclose certain information, including your Personal Data, to our business partners in order to administrate the Service and handle payments through our business partners.
                Third Party Service Providers
5.5
                We may disclose your Personal Data to various third party service providers in order to e.g. communicate with you through newsletters and similar (e.g. for direct marketing purposes), to process your payments, to provide customer service or in order to conduct customer surveys or satisfaction surveys. These third party service providers may only process your Personal Data in accordance with our instructions. They are also liable to take technical and organisational security measures to protect your Personal Data to the same extent as Readly.
                Others
5.6
                Readly may also disclose your Personal Data in circumstances other than those listed above if it is necessary in order to: (a) comply with applicable law and regulations and lawful regulatory requests or relevant orders from competent courts; (b) protect and defend our rights, our property or our interests, such as in a legal dispute; or (c) protect our customers and users.

                Additionally, we may transfer collected Personal Data to third parties in the event of a reorganization, merger or transfer of business.
                
6. CHILDREN 

6.1
                We do not deliberately collect Personal Data from children under the age of 18. If you are under 18 years old you must have your parents' guardians' consent in order to use the Service. If it comes to our knowledge that we have collected Personal Data from a child under the age of 18, without the parents' or guardians' consent, we will take measures to delete the collected Personal Data collected.
                
7. USE OF COOKIES AND OTHER TRACKING TECHNOLOGIES 

What Are Cookies?
7.1
                            We use so called cookies on our Website and in our Service. A cookie is a text file which is sent from a web server to your browser and stored on your computer, smart phone or tablet when you visit a web site. Cookies are used for example to show custom web pages to visitors or to collect statistics. If your web browser is set to accept cookies, the information will be stored on your device until the specified expiration date of respective cookie. Currently, our systems do not recognize browser “DO-NOT-TRACK” requests. You may, however, disable certain tracking as discussed in this section (e.g., by disabling cookies).
                            The Cookies That We Use
Strictly necessary cookies
7.2
                            Some cookies are strictly necessary in order to enable you to move around the website and use its features. Without these cookies, we will not be able to determine the number of unique users of the site or provide certain features, such as automatic sign in to the Readly services.
                            Statistics
7.3
                            These cookies are used to measure and collect information regarding how the users use the Website and the Service. Readly uses this information to produce reports and in order to continuously improve and develop our Website and Service. For this purpose we use Google Analytics, a web analytics service. The information collected will be transmitted to Google for producing visitor reports and stored on Google's servers in the U.S. in accordance with Google Analytic's data privacy policy (https://support.google.com/analytics/answer/6004245?hl=en). Google may also transfer this information to third parties where required by law, or where such third parties process the information on Google's behalf.
                            Functional cookies
7.4
                            These cookies allow us to customize the Website and the Service in accordance with your preferences, for example to remember your username or country of origin.
                            7.5
                            Further technical information regarding the cookies that Readly use on the Website and in the Service is available on the following page (http://www.readly.com/cookie).
                            Other similar technologies
7.6
                            In addition, similar to cookies, we may use clear GIFs (a.k.a. web beacons, web bugs or pixel tags), in connection with our Website and Service to, among other things, track the activities of site visitors, help us manage content, and compile statistics about site usage. We and our third party service providers may also use clear GIFs in HTML e-mails to our customers, to help us track e-mail response rates, identify when our e-mails are viewed, and track whether our e-mails are forwarded.
                            Tracking Across Third-Party Sites
7.7
                            We do not track users across third-party websites or online services. We use, however, certain third-party analytics providers, such as Google Analytics, and these third parties may combine the information they have collected via our Website and Service with the information they have collected on other third party sites and services. However, we do not permit them to collect name, contact information or Personal Data from you on our Website or in the Service.
                            Consent to our use of cookies and similar technologies
7.8
                            By visiting our Website and accessing the Service, you expressly give your consent to our use of cookies and similar technologies for the purposes described above. Should you not wish that cookies are stored on your computer, smart phone or tablet, you may change your browser settings to disable cookies. Please note, however, that if you choose to disable cookies some parts of the Website and/or the Service may not work properly or at all.
                            
8. USE OF LINKS 

8.1
                            Note that our Website may include links to websites of third parties. Our Privacy Policy does not apply to these websites. We do not take any responsibility for how these websites handle your Personal Data.
                            
9. RETENTION OF PERSONAL DATA 

9.1
                            Readly retains your Personal Data for the time necessary to fulfill any of the purposes of our processing of your Personal Data as outlined in this Privacy Policy or to comply with law, regulations, regulatory requests and relevant orders from competent courts.
                            
10. CONSENT TO TRANSFER PERSONAL DATA TO THIRD COUNTRIES 

10.1
                            In order to provide the Service and to fulfill the other purposes specified in this Privacy Policy, we may transfer and process your Personal Data in other countries, including countries outside the EU and EEA. By creating a User Account and using our Service, you expressly acknowledge and consent to this.
                            
11. ACCESS TO PERSONAL DATA 

11.1
                            You can edit some of the information we store about you under "My Account" in the Service.
                            11.2
                            You have the right to once a year, free of charge, request information of your Personal Data that is being processed by us by sending a written signed application to Readly please see section 14 below for contact details. Moreover, you have the right to request that Personal Data which has not been processed in accordance with the Swedish Personal Data Act shall be restricted, blocked or erased.
                            11.3
                            You may also at any time oppose to our further processing of your Personal Data for direct marketing purposes by notifying us via "My Account" on the Website
                            
12. SPECIAL INFORMATION FOR CALIFORNIA (U.S.) CONSUMERS 

12.1
                                    California residents may request a list of certain third parties to which we have disclosed Personal Data about you for their own direct marketing purposes. You may make one request per calendar year. In your request, please attest to the fact that you are a California resident and provide a current California address for your response. You may request this information in writing by contacting us on the contact details outlined in section 14 below. Please allow up to thirty (30) days for a response.
                                  
13. CHANGES AND AMENDMENTS TO THE PRIVACY POLICY 

13.1
                                Readly may amend or modify this Privacy Policy from time to time. If any substantial amendments are made to this Privacy Policy, we will notify you. Notification is sent to you, either in the Application and/or to the contact information provided by you when registering your User Account, at least thirty (30) days prior to the changes enter into effect. Upon any substantial changes to the Privacy Policy, you will be asked to accept the new terms and conditions the next time you log in to the Service.
                                
14. CONTACT US 

14.1
                                      Readly is the controller of your Personal Data processed under this Privacy Policy, unless otherwise is stated.
                                          14.2
                                          If you have any questions or concerns about our processing of your Personal Data or in relation to this Privacy Policy, please contact us at the following address:
                                          Readly AB
                                          VAT No SE556921112001
                                          Videum Science Park
                                          351 96 Växjö, Sweden
                                          E-mail: support@readly.com
Updated: 2016-06-01



