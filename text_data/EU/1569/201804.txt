







Learn more about our privacy policy and the data that we do collect








































News


Make default search






How Ecosia works


Tree projects



                                        Mobile app
                                    






About us


Blog







                                        Privacy
                                        
                                            New
                                        



FAQ


Settings

















































0





















Add Ecosia to Chrome





                                        This is the number of searches you have performed with Ecosia. On average you need around 45 searches to plant a tree!
                                    


Learn more




















News


Make default search






How Ecosia works


Tree projects



                                                Mobile app
                                            






About us


Blog







                                                Privacy
                                                
                                                    New
                                                



FAQ


Settings

























                                    0
                                




                                        This is the number of searches you have performed with Ecosia. On average you need around 45 searches to plant a tree!
                                    


Learn more































News


Make default search






How Ecosia works


Tree projects



                                            Mobile app
                                        






About us


Blog







                                            Privacy
                                            
                                                New
                                            



FAQ


Settings











×





News


Make default search






How Ecosia works


Tree projects



                                Mobile app
                            






About us


Blog







                                Privacy
                                
                                    New
                                



FAQ


Settings







We respect your privacy
We don’t sell your data to advertisers and have no third party trackers, unlike most other search engines.


Add Ecosia to Chrome














Your searches are always SSL-secured
SSL encryption means that the data you send to Ecosia can’t be eavesdropped on by anyone monitoring your internet traffic. Qualys Labs gives Ecosia an A+ Score for our SSL strength.









No third party trackers
We don’t use services like Google Analytics or social media trackers that expose your activity on Ecosia Search to third parties. However, we do share certain data with our search partner, Bing.









Respect for your settings
We fully honor your browser’s Do Not Track setting. Many sites ignore this since it only indicates your preference and is not mandatory to follow.









No search leakage
We prevent information from our search results page being passed on to the sites you visit. Learn more about what search leakage is.













Learn more about our privacy policy and the data that we do collect



                                Learn more
                            



















SITEMAP

News
How Ecosia works
Tree projects
About us
Mobile app
Privacy
Settings



Resources

FAQ
Financial reports
Blog
Press
Jobs



Imprint

Ecosia GmbH Schinkestraße 9 12047 Berlin, Germany
info@ecosia.org


Chairman Christian Kroll
VAT ID: DE273999090
Commercial Register:
                            
HR 170873, Amtsgericht Charlottenburg



Apps


















facebook_lightgrey





tumblr_lightgrey



















Ecosia GmbH does not assume responsibility for the content of sites to which it links and the way in which search results are displayed. To learn more please read our privacy policy and our terms of service.





/*<![CDATA[*/window.zEmbed||function(e,t){var n,o,d,i,s,a=[],r=document.createElement("iframe");window.zEmbed=function(){a.push(arguments)},window.zE=window.zE||window.zEmbed,r.src="javascript:false",r.title="",r.role="presentation",(r.frameElement||r).style.cssText="display: none",d=document.getElementsByTagName("script"),d=d[d.length-1],d.parentNode.insertBefore(r,d),i=r.contentWindow,s=i.document;try{o=s}catch(c){n=document.domain,r.src='javascript:var d=document.open();d.domain="'+n+'";void(0);',o=s}o.open()._l=function(){var o=this.createElement("script");n&&(this.domain=n),o.id="js-iframe-async",o.src=e,this.t=+new Date,this.zendeskHost=t,this.zEQueue=a,this.body.appendChild(o)},o.write('<body onload="document._l();">'),o.close()}("https://assets.zendesk.com/embeddable_framework/main.js","ecosia.zendesk.com");/*]]>*/


            window.Ecosia = {
                stats: {"activeUsers":"7000000.00","costPerTree":"0.20","dailySearches":"4218912.00","eur=>usd":"1.08","facebookLikes":"216093.00","totalSearches":"321788830916.00","lastTotalTreeCountUpdate":"2018-04-19T06:26:25.054Z","treePlantingDonations":"6688224.00","wwfDonation":"1271557.00","secondsToPlantTree":"1.10","totalTreeCount":25741203},
                newsMoreUrl: 'https://www.ecosia.org/ecosia-news',
                newsEndpoint: 'https://notifications.ecosia.org',
                analyticsUrl: 'https://analytics.ecosia.org/analytics.js',
                userData: {"addOn":0,"autoSuggest":1,"customSettings":0,"deviceType":"pc","adultFilter":"i","language":"en","lastTree":0,"bingMarketCode":"en-us","newTab":0,"treeCount":0,"firstSearch":1,"firstRun":0,"notifications":1,"typeTag":0,"browserDisplay":"Chrome","browser":"chrome","defaultMarket":"en-us"}
            };
        

            zE(function() {
                // Hide the Zendesk widget by default
                zE.hide();
                zE.setLocale('en');
            });
        

(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';fnames[3]='MMERGE3';ftypes[3]='text';fnames[4]='MMERGE4';ftypes[4]='text';}(jQuery));var $mcj = jQuery.noConflict(true);


